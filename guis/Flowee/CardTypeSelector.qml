/* * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick

Item {
    id: root
    property alias title: name.text
    property var features: []
    property bool checkable: false
    property bool checked: false
    signal clicked;

    implicitWidth: name.implicitWidth * 1.8
    implicitHeight: Math.min(contentArea.height + name.height + 15 + 16 + 10 + 10 + 20, 270)
    clip: true


    Rectangle {
        id: background
        property bool hover: false
        property bool selected: root.checkable && root.checked

        color: selected ? palette.light : palette.window
        border.width: selected ? 5 : 2
        border.color: {
            if (selected)
                return mainWindow.floweeGreen
            if (hover)
                return Pay.useDarkSkin ? mainWindow.floweeSalmon : mainWindow.floweeBlue;
            return Pay.useDarkSkin ? "#EEE" : "#7b7f7f"
        }

        anchors.fill: parent
        radius: 10

        Behavior on border.width { NumberAnimation { } }
        Behavior on color { ColorAnimation { } }
        Behavior on border.color { ColorAnimation { } }
    }

    Label {
        id: name
        width: root.width - 20
        x: 10
        fontSizeMode: Text.HorizontalFit
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: mainWindow.font.pixelSize * 1.3
        font.bold: true
        y: 25
    }
    Column {
        id: contentArea
        spacing: 15
        width: parent.width
        anchors.top: name.bottom
        anchors.topMargin: 16

        Repeater {
            model: root.features
            Item {
                width: contentArea.width
                height: listItemLabel.height
                Label {
                    id: listItemLabel
                    text: modelData
                    width: Math.min(contentWidth, parent.width - 50)
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    // horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Rectangle {
                    id: left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: listItemLabel.left
                    anchors.rightMargin: 10
                    width: 6
                    height: 6
                    rotation: 45
                    color: palette.text
                }
                Rectangle {
                    anchors.top: left.top
                    anchors.left: listItemLabel.right
                    anchors.leftMargin: 10
                    width: 6
                    height: 6
                    rotation: 45
                    color: palette.text
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: background.hover = true
        onExited: background.hover = false
        onClicked: {
            var p = root.parent;
            if (p != null) {
                var list = p.children;
                for (let i = 0; i < list.length; ++i) {
                    let item = list[i];
                    if (typeof(item) === typeof(root)) {
                        item.checked = false;
                    }
                }
            }
            if (root.checkable)
                root.checked = true;
            root.clicked();
        }
    }
}
