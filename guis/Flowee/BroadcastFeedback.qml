/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import Flowee.org.pay

// screen to show the result of a (Payment) broadcast.
// This screen won't do anything until you call start().
// hint, on mobile you may want to set rootPage.hideHeader = true;
QQC2.Control {
    id: root
    anchors.fill: parent
    anchors.leftMargin: -10 // go against the margins Page gave us to show more fullscreen.
    anchors.rightMargin: -10

    signal closeButtonPressed;

    function start() {
         background.y = 0;
         background.opacity = 1;
    }
    function reset() {
         background.y = height + 2;
         background.opacity = 0;
    }

    required property double bitcoinAmount
    required property int fiatPrice
    required property string targetAddress// : payment.targetAddress
    required property int status;
    property string personalNote: "";
    property bool showPersonalNote: true
    property bool hideHeader: false

    Rectangle {
        id: background
        width: parent.width
        height: parent.height
        opacity: 0
        visible: opacity > 0
        color: root.status === FloweePay.TxRejected ? "#7f0000" : "#60b671";
        y: height + 2
        MouseArea {
            anchors.fill: parent // eat all mouse events.
        }

        Rectangle {
            id: header
            width: parent.width
            height: 50
            color: mainWindow.floweeBlue
            visible: !root.hideHeader
            QQC2.Control {
                clip: true
                y: 17
                x: 20
                width: 122
                height: 21
                Image {
                    source: "qrc:/FloweePay.svg"
                    // ratio: 449 / 77
                    width: 150
                    height: 26
                    x: -28
                    y: -5
                }
            }
        }

        Label {
            id: statusLabel
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            y: root.hideHeader ? 20 : 70
            color: "#e8e8e8"
            text: {
                var s = root.status;
                if (s === FloweePay.TxSent1 || s === FloweePay.TxWaiting)
                    return qsTr("Sending Payment");
                if (s === FloweePay.TxBroadcastSuccess)
                    return qsTr("Payment Sent");
                if (s === FloweePay.TxRejected)
                    return qsTr("Failed");
                return "";
            }
        }

        ProgressCheckIcon {
            id: progressIcon
            broadcastStatus: root.status
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: statusLabel.bottom
            opacity: root.status === FloweePay.TxRejected ? 0 : 1
            scale: 0.8
        }

        Column {
            width: parent.width
            anchors.top: progressIcon.bottom
            spacing: 6
            Rectangle {
                id: errorTextPane
                visible: errorLabel.text !== ""
                color: mainWindow.errorRedBg
                radius: 10
                width: parent.width
                height: errorLabel.height + 20

                Label {
                    id: errorLabel
                    wrapMode: Text.Wrap
                    x: 10
                    y: 10
                    width: parent.width - 20
                    horizontalAlignment: Qt.AlignHCenter
                    text: root.status === FloweePay.TxRejected ? qsTr("Transaction rejected by network") : ""
                }
            }
            Label {
                id: fiatLabel
                anchors.horizontalCenter: parent.horizontalCenter
                color: statusLabel.color
                font.pixelSize: statusLabel.font.pixelSize * 2.5
                text: {
                    var cents = root.bitcoinAmount * root.fiatPrice / 1000000;
                    cents += 0.5;
                    return Fiat.formattedPrice(cents / 100);
                }
                visible: Fiat.price !== 0
            }

            Item {
                implicitHeight: cryptoAmount.height
                implicitWidth: cryptoAmount.width + 32
                anchors.horizontalCenter: parent.horizontalCenter

                Image {
                    source: "qrc:/bch.svg"
                    width: 22
                    height: 22
                }

                BitcoinAmountLabel {
                    id: cryptoAmount
                    anchors.right: parent.right
                    value: root.bitcoinAmount
                    colorize: false
                    showFiat: false
                    color: statusLabel.color
                }
            }
            Item { width: 10; height: 10 } // spacer

            Label {
                id: addressLabel
                color: statusLabel.color
                visible: root.targetAddress !== ""
                width: parent.width - 20
                x: 10
                horizontalAlignment: Qt.AlignHCenter
                wrapMode: Text.Wrap
                text: qsTr("The payment has been sent to:", "Followed by the address")
            }
            AddressLabel {
                width: Math.min(parent.width - 20, 360)
                anchors.horizontalCenter: parent.horizontalCenter
                highlight: false
                visible: root.targetAddress !== ""
                font.pixelSize: addressLabel.font.pixelSize * 0.8
                txInfo: {
                    "cloakedAddress": root.targetAddress,
                    "address": root.targetAddress
                }
            }

            Item { width: 1; height: 10} // spacer

            TextField {
                id: transactionComment
                anchors.horizontalCenter: parent.horizontalCenter
                width: Math.min(400, parent.width - 40);
                color: "#26282a"
                palette.base: statusLabel.color
                onEditingFinished: root.personalNote = totalText
                placeholderText: qsTr("Add a personal note")
                placeholderTextColor: "#505050"
                text: root.personalNote
                visible: root.showPersonalNote
            }
        }

        BigCloseButton {
            id: closeButtonBg
            x: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            onClicked: root.closeButtonPressed()
        }

        Behavior on opacity { NumberAnimation { } }
        Behavior on y { NumberAnimation { } }
        Behavior on color { ColorAnimation { } }
    }
}
