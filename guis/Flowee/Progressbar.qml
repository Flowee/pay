/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick

Item {
    id: root
    height: 40
    width: 200

    // should be calculated to be between 0 and 1
    required property double progress;
    // Like progress, but overlaying the main progressbar in a separate color
    property double progress2: 0; // should always be smaller than progress

    Label {
        id: percentLabel
        text: (100 * root.progress).toFixed(0) + "%"
        y: (parent.height - contentHeight) / 2
        x: root.width / 2 - width / 2
    }

    Item {
        width: root.width * root.progress
        height: parent.height
        clip: true
        Rectangle {
            color: palette.highlight
            height: root.height
            width: root.width
            radius: 10
        }
        Item {
            clip: true
            width: root.width * root.progress2
            height: parent.height
            Rectangle {
                height: root.height
                width: root.width
                radius: 10
                gradient: Gradient {
                    GradientStop { position: 0; color: palette.shadow }
                    GradientStop { position: 1; color: palette.midlight }
                }
            }
        }
        Label {
            text: percentLabel.text
            y: (parent.height - contentHeight) / 2
            x: root.width / 2 - width / 2
            color: palette.highlightedText
        }
    }

    Rectangle {
        id: outline
        height: parent.height
        width: parent.width
        color: "#00000000"
        border.width: 2
        border.color: palette.midlight
        radius: 10
    }
}
