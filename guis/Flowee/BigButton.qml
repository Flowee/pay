/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2

QQC2.Control {
    id: control
    height: buttonLabel.height + 32
    width: 200
    focus: true
    activeFocusOnTab: true

    property alias text: buttonLabel.text

    // the color of the main button is Flowee - Green.
    property bool isMainButton: false
    signal clicked;

    Rectangle {
        id: background
        radius: 7
        color: isMainButton ? mainWindow.floweeGreen : palette.button
        anchors.fill: parent

        border.width: control.activeFocus ? 1.3 : 0
        border.color: palette.highlight

        Behavior on color { ColorAnimation { } }
    }
    Text {
        id: buttonLabel
        anchors.centerIn: background
        width: Math.min(parent.width - 10, implicitWidth)
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignHCenter
        opacity: enabled ? 1 : 0.4
        font.pixelSize: control.font.pixelSize
        color: control.isMainButton ? "black" : palette.text
    }
    MouseArea {
        anchors.fill: parent
        onClicked: control.clicked();
    }

    Keys.onPressed: (event)=> {
        if (event.modifiers === 0 && event.key === Qt.Key_Space)
            control.clicked();
    }
}
