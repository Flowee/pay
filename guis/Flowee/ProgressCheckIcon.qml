/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Shapes
import Flowee.org.pay;

Item {
    id: root
    width: 160
    height: 160

    property int broadcastStatus: FloweePay.NotStarted;

    Shape {
        id: circleShape
        width: 160
        height: width
        smooth: true
        ShapePath {
            strokeWidth: 20
            strokeColor: "#dedede"
            fillColor: "transparent"
            capStyle: ShapePath.RoundCap
            startX: 100; startY: 10

            PathAngleArc {
                id: progressCircle
                centerX: 80
                centerY: 80
                radiusX: 70; radiusY: 70
                startAngle: {
                    var s = root.broadcastStatus;
                    if (s === FloweePay.TxBroadcastSuccess)
                        return -20;
                    return -80;
                }
                sweepAngle: {
                    var s = root.broadcastStatus;
                    if (s === FloweePay.TxOffered)
                        return 90;
                    if (s === FloweePay.TxSent1)
                        return 150;
                    if (s === FloweePay.TxWaiting || s === FloweePay.TxBroadcastSuccess)
                        return 320;
                    return 10;
                }
                Behavior on sweepAngle {  NumberAnimation { duration: 2500 } }
                Behavior on startAngle {  NumberAnimation { } }
            }
        }
        Behavior on opacity {  NumberAnimation { } }
    }
    Shape {
        id: checkShape
        anchors.fill: circleShape
        smooth: true
        opacity: root.broadcastStatus === FloweePay.TxBroadcastSuccess ? 1 : 0
        ShapePath {
            id: checkPath
            strokeWidth: 16
            strokeColor: "#e8e8e8"
            fillColor: "transparent"
            capStyle: ShapePath.RoundCap
            startX: 52; startY: 80
            PathLine { x: 76; y: 110 }
            PathLine { x: 125; y: 47 }
        }
    }
}
