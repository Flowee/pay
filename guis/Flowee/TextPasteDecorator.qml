/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import Flowee.org.pay

/*
 * This is a small visible item that places itself below a 'buddy' TextField or MultilineTextField
 * in order to have a 'paste' button to paste one of the designated types of information from the
 * clipboard.
 * When the paste fails (no valid info found on clipboard), we give visual feedback.
 */
Item {
    id: root
    y: buddy.height - height / 3
    anchors.horizontalCenter: buddy.horizontalCenter
    implicitWidth: 10
    implicitHeight: labelText.height + 10

    required property Item buddy;
    property alias clipboardTypes: cbh.filter

    ClipboardHelper { id: cbh }

    Rectangle {
        id: errorFeedback
        width: root.parent.width - 40
        height: root.parent.width / 3
        anchors.centerIn: parent
        color: mainWindow.errorRedBg
        radius: 25
        opacity: 0

        Timer {
            interval: 100
            running: errorFeedback.opacity > 0
            repeat: true
            onTriggered: {
                anim.duration = 400
                errorFeedback.opacity = 0
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: anim
                duration: 50
                easing.type: Easing.InOutQuad
            }
        }
    }

    Rectangle {
        x: parent.width / 2 - width / 2
        width: labelText.height + labelText.width + 20 + 5 + 20
        height: labelText.height + 10
        radius: 6
        color: palette.window
        border.color: palette.midlight
        border.width: 1
        visible: root.buddy.totalText === ""

        Image {
            x: 20
            width: labelText.height
            height: width
            source: "qrc:/edit-clipboard" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
            anchors.verticalCenter: parent.verticalCenter
        }

        Label {
            id: labelText
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 20
            text: qsTr("Paste")
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (cbh.text !== "") {
                    root.buddy.text = cbh.text
                } else {
                    anim.duration = 40
                    errorFeedback.opacity = 0.7
                    shaker.start();
                }
            }
        }

        ObjectShaker { id: shaker }
    }
}
