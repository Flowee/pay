/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick

Rectangle {
    id: root
    color: palette.windowText
    radius: 10
    width: Math.min(parent.width - 20, 320)
    height: closeButtonLabel.height + 25
    anchors.horizontalCenter: parent.horizontalCenter
    signal clicked;

    Label {
        id: closeButtonLabel
        text: qsTr("Close")
        anchors.centerIn: parent
        color: "#60b671";
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent
        anchors.margins: -10
        focus: true
        onClicked: root.clicked();
    }
}
