/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Is this transaction a 'move between addresses' tx.
// This is a heuristic and not available in the model, which is why its in the view.
function isMoved(txInfo) {
    if (txInfo === null)
        return false;
    if (txInfo.isCoinbase || txInfo.isFused
            || txInfo.fundsIn === 0 || txInfo.fundsOut === 0)
        return false;
    var diff = txInfo.fundsOut - txInfo.fundsIn
    return diff < 0 && diff > -2500 // then the diff is likely just fees.
}
