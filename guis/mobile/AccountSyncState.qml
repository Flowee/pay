/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import Flowee.org.pay;
import "../Flowee" as Flowee

Item {
    id: root
    visible: !account.uptodate
    height: {
        if (account.uptodate)
            return 0;
        var h = indicator.height + 3;
        if (!root.uptodate)
            h += progressbar.height + 10;
        return h;
    }
    property QtObject account: null
    property bool uptodate: false
    property int startPos: account.initialBlockHeight
    onAccountChanged: checkIfDone();
    function checkIfDone() {
        let startPos = account.initialBlockHeight;
        if (startPos === -2) {
            // special number, the account is just created and waits for transactions.
            uptodate = true;
            return;
        }
        let end = Pay.expectedChainHeight;
        let currentPos = account.lastBlockSynched;
        // only show the progress-circle when its more than 2 days behind
        // and we are not synched
        uptodate = end - startPos < 300 || end - currentPos <= 6;
    }

    Connections {
        target: root.account
        function onLastBlockSynchedChanged() { checkIfDone(); }
    }

    Flowee.Progressbar {
        id: progressbar
        x: 10
        y: 10
        width: parent.width - 20
        visible: !root.uptodate
        progress: {
            let startPos = root.startPos;
            let end = Pay.expectedChainHeight
            if (startPos == 0)
                return 0;
            let currentPos = root.account.lastBlockSynched;
            let totalDistance = end - startPos;
            let ourProgress = currentPos - startPos;
            return ourProgress / totalDistance;
        }
        progress2: {
            let startPos = root.startPos;
            let end = Pay.expectedChainHeight
            if (startPos == 0)
                return 0;
            let currentPos = root.account.lastBlockSynched2;
            let totalDistance = end - startPos;
            let ourProgress = currentPos - startPos;
            return ourProgress / totalDistance;
        }
    }
    Flowee.Label {
        id: indicator
        width: parent.width
        y: root.uptodate ? 0 : progressbar.height + 13
        wrapMode: Text.Wrap
        text: {
            if (mainWindow.isLoading)
                return "";
            var account = portfolio.current;
            if (account === null)
                return "";
            if (Pay.deviceOffline || (account.needsPinToOpen && !account.isDecrypted))
                return qsTr("Status: Offline");
            return account.timeBehind;
        }
        font.italic: true
        Behavior on y {  NumberAnimation { duration: 100 } }
    }
    Behavior on height {  NumberAnimation { duration: 100 } }
}
