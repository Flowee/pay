/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Shapes
import "../Flowee" as Flowee
import "../Utils.js" as Utils
import Flowee.org.pay;

ListView {
    id: root

    property string icon: "qrc:/homeButtonIcon" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
    property string  title: qsTr("Home")

    Rectangle {
        id: backToTopButton
        width: 60
        height: 60
        radius: 35
        anchors.right: parent.right
        anchors.rightMargin: 30
        y: {
            var indexAtTopOfScreen = root.indexAt(10, root.contentY + 10);
            if (indexAtTopOfScreen === -1) { // just in case that's just a section-header
                indexAtTopOfScreen = root.indexAt(10, root.contentY + 80);
            }

            if (indexAtTopOfScreen > 3)
                return root.height - height - 15;
            return height * -1; // out of screen.
        }
        color: "#66000000"
        border.width: 1.3
        border.color: palette.button

        Image {
            id: upIcon
            source: "qrc:/back-arrow.svg"
            rotation: 90
            width: 15
            height: 20
            anchors.centerIn: parent
        }
        MouseArea {
            anchors.fill: parent
            onClicked: root.positionViewAtIndex(0, ListView.Contain);
        }

        Behavior on y {
            NumberAnimation {
                easing.type: Easing.OutElastic
                duration: 500
            }
        }
    }

    QQC2.ScrollBar.vertical: Flowee.ScrollThumb {
        id: thumb
        minimumSize: 0.05
        visible: size < 0.9
        preview: Rectangle {
            width: dateLabel.width + 20
            height: dateLabel.height + 20
            radius: 5
            color: palette.light
            border.width: 1
            border.color: palette.highlight
            Flowee.Label {
                id: dateLabel
                anchors.centerIn: parent
                color: palette.dark
                text: root.model.dateForItem(thumb.position);
            }
        }
    }

    /*
      The structure here is a bit funny.
      But we want to have the entire page scroll in order to show all the transactions we can.
      To avoid a mess of two scroll-areas (or Flickables) we simply make the top part into a
      header of the listview.
     */
    header: Rectangle {
        color: palette.base
        width: root.width
        height: column.height
        Column {
            id: column
            width: root.width - 20
            x: 10
            y: 10

            Flowee.BitcoinAmountLabel {
                id: bchPriceWidget
                opacity: Pay.hideBalance ? 0.2 : 1
                fontPixelSize: 30
                anchors.horizontalCenter: parent.horizontalCenter
                value: {
                    if (Pay.hideBalance)
                        return 88888888;
                    return portfolio.current.balanceConfirmed + portfolio.current.balanceUnconfirmed
                }
                colorize: false
                showFiat: false
            }
            Flowee.Label { // fiat price
                opacity: Pay.hideBalance ? 0.2 : 1
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                text: {
                    if (Pay.hideBalance)
                        return Fiat.currencySymbolPrefix + "——" + Fiat.currencySymbolPost
                    Fiat.formattedPrice(portfolio.current.balanceConfirmed
                          + portfolio.current.balanceUnconfirmed, Fiat.price)
                }
                MouseArea {
                    // make the click area nice and large
                    width: parent.width
                    height: parent.height + bchPriceWidget.height + 10
                    y: 0 - 5- bchPriceWidget.height
                    onClicked: popupOverlay.open(priceDetails, parent)
                    cursorShape: Qt.PointingHandCursor
                }
                Component {
                    id: priceDetails
                    PriceDetails { }
                }
            }

            AccountSyncState {
                account: portfolio.current
                width: parent.width
            }

            Item { width: 10; height: 25 } // spacer

            Row {
                height: startScan.height + 20
                x: (parent.width - width) / 2
                spacing: 25
                Flowee.ImageButton {
                    id: startScan
                    source: "qrc:/qr-code-scan" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    onClicked: thePile.push("ScanQRPage.qml")
                    iconSize: 70
                    text: qsTr("Pay")
                }
                Flowee.ImageButton {
                    iconSize: 70
                    source: "qrc:/receive.svg"
                    onClicked: switchToTab(2) // receive tab
                    text: qsTr("Receive")
                }
            }
        }
    }

    model: portfolio.current.transactions
    clip: true
    width: parent.width
    height: contentHeight
    focus: true
    reuseItems: true
    section.property: "grouping"
    section.labelPositioning: ViewSection.InlineLabels + ViewSection.CurrentLabelAtStart
    section.delegate: Item {
        height: label.height + 15
        width: root.width
        Rectangle {
            color: palette.base
            anchors.fill: parent
        }
        Flowee.Label {
            id: label
            x: 10
            y: 12
            font.bold: true
            font.pixelSize: mainWindow.font.pixelSize * 1.1
            text: portfolio.current.transactions.groupingPeriod(section);
        }
        MouseArea { anchors.fill: parent } // eat all taps
    }
    property bool itIsChristmas: {
        var today = new Date();
        return today.getMonth() == 11 &&
            (today.getDate() == 24 || today.getDate() == 25 || today.getDate() == 26)
    }
    delegate: Rectangle {
        id: transactionDelegate
        property var placementInGroup: model.placementInGroup
        // Is this transaction a 'move between addresses' tx.
        // This is a heuristic and not available in the model, which is why its in the view.
        property bool isMoved: Utils.isMoved(model);
        color: palette.base

        width: root.width
        height: 80
        clip: true

        Rectangle {
            width: parent.width - 16
            x: 8
            // we always have the rounded circles, but if we should not see them, we move them out of the clipped area.
            height: {
                var h = 80
                var placement = transactionDelegate.placementInGroup;

                if (placement !== Wallet.GroupStart && placement !== Wallet.Ungrouped)
                    h += 20;
                if (placement !== Wallet.GroupEnd && placement !== Wallet.Ungrouped)
                    h += 20;
                return h;
            }
            y: {
                var placement = transactionDelegate.placementInGroup;
                if (placement === Wallet.GroupStart || placement === Wallet.Ungrouped)
                    return 0;
                return -20;
            }

            radius: 20
            color: palette.light
            border.width: 1
            border.color: palette.midlight
        }

        Item {
            id: checks // confirmations
            width: Math.max(row.width, 14)
            height: 12
            anchors.right: parent.right
            anchors.rightMargin: 25
            anchors.top: txListItem.baseline
            anchors.topMargin: 4
            property int txHeight: model.height
            property int confirmations: Pay.chainHeight - txHeight + 1
            visible: txHeight === -1 || confirmations < 5

            Rectangle {
                color: "#00000000"
                border.width: 1.3
                border.color: Pay.useDarkSkin ? palette.dark : mainWindow.floweeBlue
                opacity: 0.6
                width: 14
                height: 13
                radius: 4
                visible: checks.confirmations < 2 || checks.txHeight == -1
            }
            Row {
                id: row
                height: parent.height

                Repeater {
                    model: {
                        if (checks.txHeight < 0)
                            return 0;
                        var c = checks.confirmations;
                        return c < 5 ? c : 0;
                    }

                    Shape {
                        width: 12
                        height: 12
                        ShapePath {
                            fillColor: "#00000000"
                            strokeColor: Pay.useDarkSkin ? "#57a56c" : "green";
                            capStyle: ShapePath.FlatCap
                            joinStyle: ShapePath.RoundJoin
                            startX: 3; startY: 6
                            strokeWidth: 2
                            PathLine { x: 5; y: 9 }
                            PathLine { x: 12; y: 2 }
                        }
                    }
                }
            }
        }


        TransactionListItem {
            id: txListItem
            anchors.fill: parent
        }

        // horizontal separator
        Rectangle {
            visible: transactionDelegate.placementInGroup !== Wallet.GroupEnd
                         && transactionDelegate.placementInGroup !== Wallet.Ungrouped;
            anchors.bottom: parent.bottom
            height: 1
            width: parent.width - 16
            x: 8
            color: palette.midlight
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.currentIndex = index;
                var newItem = popupOverlay.open(selectedItem, transactionDelegate, overlayTxListItem);
                newItem.infoObject = portfolio.current.txInfo(model.walletIndex, newItem);
                root.model.freezeModel = true;
            }
            Connections {
                target: popupOverlay
                // unfreeze the model on closing of the popup
                function onIsOpenChanged() {
                    if (!popupOverlay.isOpen)
                        root.model.freezeModel = false;
                }
            }
        }
        Component {
            id: selectedItem
            TransactionInfoSmall { }
        }
        Component {
            id: overlayTxListItem
            Item {
                height: 80
                width: 10
                Rectangle {
                    color: palette.base
                    width: parent.width - 18
                    height: parent.height
                    x: 9
                }
                TransactionListItem {
                    width: parent.width
                    implicitHeight: 60
                    commentEditable: true
                    y: 10
                }
            }

        }
    }
    displaced: Transition { NumberAnimation { properties: "y"; duration: 400 } }

    Keys.forwardTo: Flowee.ListViewKeyHandler {
        target: root
    }
}
