/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2
import "../Flowee" as Flowee
import Flowee.org.pay;
import "WorkflowStarter.js" as WorkflowStarter

FocusScope {
    id: root
    property string icon: "qrc:/sending" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
    property string  title: qsTr("Send")

    Flickable {
        anchors.fill: parent
        contentHeight: col.height
        boundsBehavior: Flickable.StopAtBounds
        ColumnLayout {
            id: col
            width: parent.width - 20
            x: 10
            y: 10
            spacing: 10

            PageTitledBox {
                width: parent.width
                title: qsTr("Start Payment")

                TextButton {
                    text: qsTr("Scan QR")
                    pageButton: true
                    iconSource: "qrc:/qr-code" + (Pay.useDarkSkin ? "-light" : "")  + ".svg";
                    onClicked: thePile.push("ScanQRPage.qml")
                }
                TextButton {
                    text: qsTr("Paste")
                    iconSource: "qrc:/edit-clipboard" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    pageButton: true
                    onClicked: thePile.push(pastePage);
                }

                Repeater {
                    model: ModuleManager.sendMenuItems
                    TextButton {
                        text: modelData.text
                        subtext: modelData.subtext
                        pageButton: true
                        onClicked: thePile.push(modelData.qml)
                    }
                }
            }

            PageTitledBox {
                width: parent.width
                title: qsTr("Options")

                SelectDefaultConfigButton { }

                InstaPayConfigButton { }
            }
        }
    }

    Component {
        id: pastePage
        Page {
            Item { // data
                ClipboardHelper {
                    id: cbh

                    onClipboardScanned: {
                        let t = cbh.type;
                        if (t === ClipboardHelper.Addresses
                                || t === ClipboardHelper.LegacyAddresses
                                || t === ClipboardHelper.AddressUrl) {
                            var item = thePile.replace("PayWithQR.qml");
                            item.start(text);
                            return;
                        }
                        else if (t === ClipboardHelper.PrivateKey) {
                            if (WorkflowStarter.startSweep(text))
                                return;
                        }

                        // if plugin missing, handle privatekey here.
                        if (t === ClipboardHelper.MnemonicSeed || t === ClipboardHelper.PrivateKey) {
                            WorkflowStarter.startImportWithSecret(text);
                        }
                        else {
                            shrug.visible = true;
                            detectedText.text = text;
                        }
                    }
                }
            }
            Image {
                id: shrug
                y: 200
                visible: false
                anchors.horizontalCenter: parent.horizontalCenter
                width: 200
                height: 170
                source: "qrc:/shrug" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
            }

            Flowee.Label {
                id: detectedText
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: shrug.bottom
                anchors.topMargin: 10
                width: root.width - 20
                x: 10
                wrapMode: Text.WrapAnywhere
            }
        }
    }
}
