/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee
import Flowee.org.pay

Page {
    id: root
    hideHeader: true

    Item { // data
        Connections {
            // connect to wallet and when its no longe user owned, close this window.
            target: portfolio.current
            function onIsUserOwnedChanged() {
                // transaction seen arriving, this startup screen has fulfilled its purpose.
                thePile.pop();
            }
        }
        PaymentRequest {
            id: request
            account: portfolio.current
        }
    }
    onActiveFocusChanged: request.start();

    Rectangle {
        id: logo
        width: parent.width + 20
        x: -10
        height: 80
        color: mainWindow.floweeBlue

        Image {
            id: img
            source: "qrc:/FloweePay-light.svg"
            // ratio: 449 / 77
            width: parent.width - 20
            height: width / 449 * 77
            x: 10
            y: 8
        }
    }

    Flickable {
        id: bla
        y: logo.height
        width: parent.width
        height: parent.height - y
        contentWidth: width
        contentHeight: column.height + 20
        clip: true
        Column {
            id: column
            y: 10
            width: parent.width
            spacing: 10

            Flowee.Label {
                text: qsTr("Moving the world towards a Bitcoin\u00a0Cash economy")
                wrapMode: Text.WordWrap
                font.italic: true
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
                width: column.width * 0.8
                font.pixelSize: root.font.pixelSize * 1.2
            }

            TextButton {
                text: qsTr("Continue")
                horizontalAlignment: Text.AlignRight
                pageButton: true
                onClicked: {
                    if (!portfolio.current.isUserOwned) {
                        request.clear(); // to make the QR here the same as there
                        var mainView = thePile.get(0);
                        mainView.currentIndex = 2 // go to the receive-tab
                    }

                    thePile.pop();
                }
            }

            Item { width: 1; height: 10 } // spacer

            GridLayout {
                id: videoSection
                property QtObject infoObject:
                     ModuleManager.moduleSection("socialFeedModule");
                columnSpacing: 20
                rowSpacing: 0
                width: 120 + 120 + 20

                columns: infoObject == null ? 1 : 2
                anchors.horizontalCenter: parent.horizontalCenter

                Rectangle {
                    height: 120
                    width: 120
                    radius: 26
                    color: mainWindow.floweeBlue
                    Item {
                        // clip the logo only, ignore the text part
                        width: 85
                        height: 85
                        clip: true
                        x: 20
                        y: 25
                        Image {
                            source: "qrc:/FloweePay-light.svg"
                            // ratio: 449 / 77
                            width: height / 77 * 449
                            height: 85
                        }
                    }
                    Image {
                        source: "qrc:/play-button.svg"
                        width: 90
                        height: 90
                        opacity: 0.8
                        anchors.centerIn: parent
                    }
                    MouseArea { anchors.fill: parent; onClicked: Qt.openUrlExternally("https://flowee.org/v/gettingstarted.mp4") }
                }

                Image {
                    source: "qrc:/social-feed/social-feed.svg"
                    Layout.minimumWidth: 120
                    Layout.minimumHeight: 120
                    visible: parent.infoObject !== null
                    MouseArea { anchors.fill: parent; onClicked: thePile.push(videoSection.infoObject.qml); }
                }

                Flowee.Label {
                    text: qsTr("Getting Started")
                    wrapMode: Text.WordWrap
                    Layout.maximumWidth: 140
                    MouseArea { anchors.fill: parent; onClicked: Qt.openUrlExternally("https://flowee.org/v/gettingstarted.mp4") }
                }
                TextButton {
                    text: qsTr("All Videos")
                    visible: parent.infoObject !== null
                    horizontalAlignment: Text.AlignHCenter
                    Layout.maximumWidth: 140
                    pageButton: true
                    onClicked: thePile.push(parent.infoObject.qml);
                }
            }

            Item { width: 1; height: 10 } // spacer

            Flowee.BigButton {
                anchors.horizontalCenter: parent.horizontalCenter
                property QtObject infoObject: ModuleManager.moduleSection("sendSweepModule");
                text: qsTr("Claim a Cash Stamp")
                onClicked: thePile.replace(infoObject.qml);
                isMainButton: true
            }

            Item { width: 1; height: 5 } // spacer

            Row {
                spacing: 15
                x: (column.width - width) / 2
                Rectangle {
                    width: 50
                    height: 1
                    color: palette.button
                    anchors.verticalCenter: parent.verticalCenter
                }
                Flowee.Label {
                    text: qsTr("OR")
                }
                Rectangle {
                    width: 50
                    height: 1
                    color: palette.button
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            Flowee.Label {
                id: instructions
                text: qsTr("Scan to send to your wallet") + ":"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                width: column.width * 0.8
                x: column.width / 10
            }
            Flowee.QRWidget {
                width: parent.width
                qrText: request.qr
            }

            Row {
                spacing: 15
                x: (column.width - width) / 2
                Rectangle {
                    width: 50
                    height: 1
                    color: palette.button
                    anchors.verticalCenter: parent.verticalCenter
                }
                Flowee.Label {
                    text: qsTr("OR")
                }
                Rectangle {
                    width: 50
                    height: 1
                    color: palette.button
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            Item { width: 1; height: 5 } // spacer

            Flowee.BigButton {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Add a different wallet")
                onClicked: thePile.replace("./NewAccount.qml");
                isMainButton: true
            }
        }
    }
}
