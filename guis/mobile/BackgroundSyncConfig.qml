/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../Flowee" as Flowee

Page {
    headerText: qsTr("Background Synchronization")
    id: root

    Column {
        width: parent.width
        spacing: 10

        Flowee.Label {
            width: parent.width
            text: qsTr("Without background synchronization your wallets will not see new transactions until you open Flowee Pay")
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Flowee.CheckBox {
            id: checkbox
            text: qsTr("Allow Background Synchronization")
            checked: Pay.backgroundUpdates
            onCheckedChanged:  Pay.backgroundUpdates = checked
        }

        Item { width: 1; height: 10 } // spacer

        QQC2.Slider {
            id: slider
            property int timeSpan: {
                var i = slider.value;
                if (i < 4) {
                    for (var h = 48; i > 0; --i) {
                        h = h / 2;
                    }
                    return h;
                }
                return 6 - (i - 3);
            }
            onTimeSpanChanged: Pay.backgroundUpdateInterval  = timeSpan

            visible: checkbox.checked
            width: parent.width
            stepSize: 1
            from: 0
            value: 3
            to: 8
            snapMode: QQC2.Slider.SnapAlways
            background: Rectangle {
                x: slider.leftPadding
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider.availableWidth
                height: implicitHeight
                radius: 2
                color: palette.button

                Repeater {
                    id: repeater
                    model: slider.to - slider.from - 1
                    Rectangle {
                        width: 1.8
                        height: 10
                        color: palette.button
                        x: {
                            var offset = slider.handle.implicitWidth
                            var totalWidth = slider.width - slider.leftPadding - slider.rightPadding - offset;
                            var unit = totalWidth / (slider.to - slider.from);
                            return offset / 2 + unit + unit * modelData
                        }
                        y: 10
                    }
                }
            }
            handle: Rectangle {
                x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: slider.font.pixelSize
                implicitHeight: implicitWidth
                radius: implicitWidth / 2
                color: Pay.useDarkSkin ? slider.palette.windowText : slider.palette.highlight
            }
        }
        Flowee.Label {
            visible: checkbox.checked
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Every %1 hours").arg(slider.timeSpan)
        }
    }
}
