/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import "../Flowee" as Flowee
import Flowee.org.pay

Item {
    property double amountBch: isMoved ? model.fundsIn
                                       : (model.fundsOut - model.fundsIn)
    property bool isRejected: model.height === -2 // special height as defined by the wallet
    property alias commentEditable: editIcon.visible

    baselineOffset: dateLine.y + dateLine.baselineOffset
    implicitWidth: 360
    implicitHeight: 80

    Item {
        id: ruler
        width: parent.width
        height: 4
        anchors.verticalCenter: parent.verticalCenter
    }

    // icon
    Image {
        id: mainIcon
        property bool receiving: model.fundsIn === 0;
        source: {
            if (model.isFused)
                return "qrc:/cf.svg";
            if (receiving)
                var base = "receiving";
            else if (isMoved)
                base = "move";
            else
                base = "send";
            return "qrc:/tx-"+ base + (Pay.useDarkSkin ? "-light.svg" : ".svg");
        }
        width: 45
        height: 45
        x: 20
        smooth: true
        anchors.verticalCenter: ruler.verticalCenter
        opacity: isRejected ? 0.6 : 1

    }
    Image {
        visible: itIsChristmas
        source: "qrc:/hat.svg"
        width: 60
        height: 60
        rotation: mainIcon.receiving ? 45 : 0
        x: mainIcon.receiving ? 14 : -2
        y: -2
    }

    Flowee.Label {
        id: commentLabel
        anchors.right: price.visible ? price.left : price.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 72
        y: {
            if (price.visible && Pay.activityShowsBch) // baseline align to price
                return price.y + price.baselineOffset - baselineOffset
            return parent.height / 2 - height
        }
        elide: Text.ElideRight

        Component.onCompleted: fetchText();
        function fetchText() {
            var comment = model.comment
            if (comment !== "")
                text = comment;
            else if (model.isCoinbase)
                text = qsTr("Miner Reward");
            else if (model.isFused)
                text = qsTr("Fused");
            else if (model.fundsIn === 0)
                text = qsTr("Received");
            else if (isMoved)
                text = qsTr("Moved");
            else
                text = qsTr("Sent");
        }
    }

    Image {
        id: editIcon
        x: commentLabel.x + commentLabel.contentWidth + 6
        y: commentLabel.y + commentLabel.baselineOffset - height
        width: 16
        height: 16
        smooth: true
        visible: false
        source: "qrc:/edit-pen" + (Pay.useDarkSkin ? "-light" : "") + ".svg"
        enabled: editWidget.visible === false
        MouseArea {
            anchors.fill: parent
            anchors.margins: -15
            onClicked: {
                editWidget.visible = !editWidget.visible;
                if (editWidget.focus)
                    editWidget.forceActiveFocus();
            }
        }
    }
    Flowee.TextField {
        id: editWidget
        property QtObject infoObject: null
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: commentLabel.right
        visible: false
        focus: visible
        text: model.comment
        onEditingFinished: {
            if (editWidget.infoObject === null)
                editWidget.infoObject = portfolio.current.txInfo(model.walletIndex, editWidget);
            editWidget.infoObject.userComment = text;
            commentLabel.fetchText();
            visible = false;
        }
    }

    Flowee.Label {
        id: dateLine
        anchors.top: ruler.bottom
        anchors.left: commentLabel.left
        color: isRejected ? mainWindow.errorRed : palette.text;
        text: {
            var minedHeight = model.height;
            if (minedHeight === -2)
                return qsTr("Rejected")
            var date = model.date;
            return Pay.formatDate(date, FloweePay.NoYear) + " " + Qt.formatTime(date);
        }
    }

    // price in a green rectangle
    Rectangle {
        id: price
        width: amount.width + 10
        height: amount.height + 8
        anchors.right: parent.right
        y: {
            if (Pay.activityShowsBch) // my bottom at parent verticalCenter
                return parent.height / 2 - height
            return parent.height / 2 - height / 2 // i.e my verticalCenter = parent.verticalCenter
        }
        anchors.rightMargin: 20
        radius: 6
        baselineOffset: amount.baselineOffset + 4 // 4 is half the spacing added at height:
        visible: !model.isFused && Pay.isMainChain

        color: (isMoved || amountBch < 0) ? "#00000000"
                 : (Pay.useDarkSkin ? "#1d6828" : "#97e282") // green background
        Flowee.Label {
            id: amount
            text: {
                var dummy = Fiat.currencyName; // trigger a recalc when user changes currency
                var fiatPrice = Fiat.historicalPrice(model.date);
                return Fiat.formattedPrice(Math.abs(amountBch), fiatPrice);
            }
            anchors.centerIn: parent
            opacity: Math.abs(amountBch) < 2000 ? 0.5 : 1
            font.strikeout: isRejected
        }
    }
    Flowee.Label { // plus or minus in front of the price
        visible: price.visible && !isMoved
        text: amountBch >= 0 ? "+" : "-"
        anchors.baseline: price.baseline
        anchors.right: price.left
        opacity: price.opacity
    }
    Flowee.BitcoinAmountLabel {
        visible: !Pay.isMainChain || (price.visible && Pay.activityShowsBch)
        anchors.right: parent.right
        anchors.rightMargin: 25
        anchors.baseline: dateLine.baseline
        value: amountBch
        showFiat: false
        fontPixelSize: amount.font.pixelSize * 0.9
        colorize: isMoved === false
    }
}
