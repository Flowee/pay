/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee

Item {
    id: root
    width: parent.width
    implicitWidth: label.x + Math.max(label.contentWidth, smallLabel.contentWidth)
        + ((rightIcon.implicitWidth === 0) ? 0 : (10 + rightIcon.implicitWidth))
    implicitHeight: Math.max(60, label.height + (smallLabel.text === "" ? 0 : smallLabel.height) + 20)
    height: implicitHeight
    baselineOffset: label.baselineOffset + 10
    Layout.fillWidth: true

    signal clicked
    property alias text: label.text
    property alias horizontalAlignment: label.horizontalAlignment
    property alias subtext: smallLabel.text
    /// When true, show an arrow indicating we open a new page on click
    property bool pageButton: false

    /// when pageButton is false, place an image on the right side instead
    property string imageSource: ""
    property int imageWidth: 20
    property int imageHeight: 20
    property alias buttonId: newIndicator.buttonId

    /// Add an icon before the label
    property string iconSource: ""

    property var currentValue: undefined

    Image {
        id: icon
        visible: root.iconSource !== ""
        source: root.iconSource
        width: 28
        height: 28
        anchors.verticalCenter: parent.verticalCenter
    }

    Flowee.Label {
        id: label
        y: smallLabel.text === "" ? (parent.height - height) / 2 : 10
        x: icon.visible ? 38 : 0
        width: {
            var w = parent.width - x
            if (horizontalAlignment === Text.AlignRight)
                w = w - rightIcon.width - 10
            return w;
        }
        elide: Text.ElideRight
        color: enabled ? palette.windowText : palette.brightText
    }
    Flowee.Label {
        id: currentValueLabel
        y: label.y
        text: {
            if (typeof(root.currentValue) == "undefined")
                return "";
            if (typeof(root.currentValue) == "boolean")
                return root.currentValue ? qsTr("Enabled") : qsTr("Disabled")
            return "" + root.currentValue;
        }
        x: {
            var x = parent.width - contentWidth
            if (rightIcon.item != null)
                x -= rightIcon.width + 10;
            return x;
        }
        color: enabled ? palette.highlight : palette.brightText
    }
    NewIndicator {
        id: newIndicator
        buddy: label
    }
    Flowee.Label {
        id: smallLabel
        anchors.top: label.bottom
        font.pixelSize: label.font.pixelSize * 0.8
        font.bold: false
        color: palette.brightText
        width: label.width
        x: label.x
        wrapMode: Text.WordWrap
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            newIndicator.markSeen();
            root.clicked()
        }
    }

    Loader {
        id: rightIcon
        sourceComponent: {
            if (root.pageButton)
                return pageIcon;
            if (root.imageSource !== "")
                return genericImage;
            return undefined;
        }
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
    }
    Component {
        id: pageIcon
        Image {
            width: 12
            height: 8
            source: Pay.useDarkSkin ? "qrc:/smallArrow-light.svg" : "qrc:/smallArrow.svg";
            rotation: 270
        }
    }
    Component {
        id: genericImage
        Image {
            source: root.imageSource
            width: root.imageWidth
            height: root.imageHeight
        }
    }
}
