/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../ControlColors.js" as ControlColors
import "../Flowee" as Flowee
import Flowee.org.pay;

import QtQuick.Controls.Basic


QQC2.ApplicationWindow {
    id: mainWindow
    title: "Flowee Pay"
    width: 360
    height: 720
    minimumWidth: 300
    minimumHeight: 400
    visible: true
    onVisibleChanged: if (visible) ControlColors.applySkin(mainWindow);

    property bool isLoading: typeof net === "undefined";
    onIsLoadingChanged: {
        // only load our UI when the p2p layer is loaded and all
        // variables are available.
        if (!isLoading) {
            portfolio.limitedArchiveView = true;
            thePile.replace("./MainView.qml");
            if (!portfolio.current.isUserOwned)
                thePile.pushSpecialPage("./StartupScreen.qml");
            else {
                // find if there is any fullscreen plugin enabled
                for (var mod of ModuleManager.registeredModules) {
                    for (var section of mod.sections) {
                        if (section.isStartScreenType && section.enabled) {
                            thePile.pushSpecialPage(section.qml)
                            return;
                        }
                    }
                }
            }
        }
    }
    Component.onCompleted: updateFontSize();
    function updateFontSize() {
        // 75% = > 14.25,  100% => 19,  200% => 28
        mainWindow.font.pixelSize = 17 + (11 * (Pay.fontScaling-100) / 100)
    }
    Connections {
        target: Pay
        function onFontScalingChanged() { updateFontSize(); }
        function onUseDarkSkinChanged() { ControlColors.applySkin(mainWindow); }
    }
    Connections {
        target: Intent
        function onPaymentUrlChanged() {
            if (Intent.paymentUrl != "") {
                let page = thePile.pushSpecialPage("./PayWithQR.qml", true)
                page.secret = Intent.paymentUrl;
            }
        }
        function onSweepKeyChanged() {
            if (Intent.sweepKey != "") {
                var s = ModuleManager.sectionOnPlugin("sendSweepModule", "main");
                if (s) {
                    let page = thePile.pushSpecialPage(s.qml, true);
                    page.secret = Intent.paymentUrl;
                }
            }
        }
        function onStartPaymentScannerChanged() {
            if (Intent.startPaymentScanner) {
                Intent.startPaymentScanner = false;
                thePile.pushSpecialPage("./ScanQRPage.qml", false)
            }
        }
    }

    property color floweeSalmon: "#ff9d94"
    property color floweeBlue: "#0b1088"
    property color floweeGreen: "#90e4b5"
    property color errorRed: Pay.useDarkSkin ? "#ff6568" : "#940000"
    property color errorRedBg: Pay.useDarkSkin ? "#671314" : "#9f1d1f"

    FocusScope {
        id: rootFocusScope
        anchors.fill: parent

        QQC2.StackView {
            id: thePile
            anchors.fill: parent
            initialItem: "./Loading.qml"
            onCurrentItemChanged: if (currentItem != null) currentItem.takeFocus();
            enabled: !menuOverlay.open
            property int tempPageIndex: -1
            property bool exitAfterPopTemp: false
            onDepthChanged: {
                if (tempPageIndex > depth) {
                    tempPageIndex = -1;
                    if (exitAfterPopTemp)
                        mainWindow.close();
                }
            }
            // the idea behind 'special' pages are that there can be no more than one
            // so pushing a second just replaces the previous one.
            // Argument 'existAfter', when it is set to true will cause the application
            // to close after the page is removed. Which is expected behavior on Android
            // activities.
            function pushSpecialPage(item, exitAfter) {
                exitAfterPopTemp = false;
                while (tempPageIndex >= 0 && depth >= tempPageIndex) {
                    pop();
                }

                var newPage = push(item);
                tempPageIndex = depth;
                if (typeof(exitAfter) === "boolean")
                    exitAfterPopTemp = exitAfter;
                return newPage;
            }

            Keys.onPressed: (event)=> {
                if (depth > 1
                        && (event.key === Qt.Key_Escape || event.key === Qt.Key_Back)) {
                    pop();
                    event.accepted = true;
                }
            }
        }
        MenuOverlay {
            id: menuOverlay
            anchors.fill: parent
        }
        Flowee.QRScanner {
            id: scannerOverlay
            anchors.fill: parent
            showCloseButton: true
        }
        Loader {
            source: Pay.appProtection === FloweePay.AppPassword ? "./UnlockApplication.qml" : ""
            anchors.fill: parent
            onLoaded: item.forceActiveFocus();
        }

        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_Escape || event.key === Qt.Key_Back) {
                event.accepted = true;
                // Aborting the camera will simply close its user page.
                if (scannerOverlay.visible) {
                    CameraController.abort();
                }
                // the 'menu' can be closed on back.
                else if (menuOverlay.open) {
                    menuOverlay.open = false;
                }
                else {
                    mainWindow.close();
                }
            }
        }
    }

    QQC2.Popup {
        id: notificationPopup
        y: 110
        x: 25
        width: mainWindow.contentItem.width - 50
        height: label.implicitHeight * 3
        property alias text: label.text
        property QtObject notification: Pay.notification
        property int timeout: 600

        onVisibleChanged: {
            if (!visible) {
                label.text = "";
                Pay.notification = null;
            }
        }
        onNotificationChanged: if (notification) show(notification.message)

        function show(message) {
            if (message === "")
                return;
            text = message;
            visible = true;
            timeout = 600;
        }
        Flowee.Label {
            id: label
            width: parent.width - 12
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
        Rectangle {
            id: timeoutBar
            width: 5
            height: {
                var max = parent.height - 12;
                return max - (notificationPopup.timeout / 600 * max);
            }
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 6
            color: palette.highlight

            Behavior on height { NumberAnimation { duration: 100 } }
        }

        background: Rectangle {
            color: palette.light
            border.color: palette.midlight
            border.width: 1
            radius: 5
        }
        QQC2.Overlay.modeless: Rectangle {
            color: Pay.useDarkSkin ? "#33000000" : "#33ffffff"
        }

        Timer {
            id: notificationPopupTimer
            running: parent.visible && notificationPopup.timeout > 0;
            interval: 100
            onTriggered: {
                var t = notificationPopup.timeout;
                t -= 10;
                notificationPopup.timeout = t;
                if (t <= 0)
                    notificationPopup.visible = false;
            }
        }

    }
    Item { // slide to hide for the notification popup
        width: notificationPopup.width
        height: notificationPopup.height
        x: notificationPopup.x
        y: notificationPopup.y
        visible: notificationPopup.visible
        onXChanged: {
            notificationPopup.x = x;
            if (x < -175 || x > 225)
                notificationPopup.visible = false;
        }

        DragHandler {
            yAxis.enabled: false
            xAxis.enabled: true
            margin: 15
            onActiveChanged: if (!active) parent.x = 25;
        }
    }
}
