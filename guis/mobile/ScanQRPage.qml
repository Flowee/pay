/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2
import "../Flowee" as Flowee
import Flowee.org.pay;
import "WorkflowStarter.js" as WorkflowStarter

Page {
    id: root

    Item { // data
        QRScanner {
            id: scanner
            autostart: true
            isPayment: true // well, thats the main intent anyway
            onFinished: {
                var type = scanType;
                if (type === QRScanner.InvalidType) { // scanning interrupted
                    thePile.pop();
                }
                else if (type === QRScanner.Seed || type === QRScanner.PrivateKeyWIF) {
                    if (WorkflowStarter.startSweep(scanResult))
                        return;
                    // plugin missing, proceed to start the import page flow.
                    WorkflowStarter.startImportWithSecret(scanResult);
                }
                else if (type === QRScanner.PaymentDetails) {
                    var item = thePile.replace("PayWithQR.qml");
                    item.start(scanResult);
                }
                else if (type === QRScanner.PaymentDetailsTestnet) {
                    // TODO if we end up supporting the testnet, this may be useful
                    shrug.visible = true;
                    detectedText.text = scanResult;
                }
            }
        }
    }

    Image {
        id: shrug
        y: 200
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        width: 200
        height: 170
        source: "qrc:/shrug" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
    }

    Flowee.Label {
        id: detectedText
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: shrug.bottom
        anchors.topMargin: 10
        width: root.width - 20
        x: 10
        wrapMode: Text.WrapAnywhere
    }
}
