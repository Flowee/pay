/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2
import "../Flowee" as Flowee
import Flowee.org.pay

Page {
    id: root
    headerText: qsTr("Import Wallet")

    states: [
        State {
            name: "entryPage"
            PropertyChanges { target: entryPage; x: 0 }
            PropertyChanges { target: privKeydetailsPage; x: width + 10 }
            PropertyChanges { target: seedDetailsPage; x: width + 10 }
        },
        State {
            name: "privKeyDetailsPage"
            PropertyChanges { target: entryPage; x: -10 - width }
            PropertyChanges { target: privKeydetailsPage; x: 0 }
        },
        State {
            name: "seedDetailsPage"
            PropertyChanges { target: entryPage; x: -10 - width }
            PropertyChanges { target: seedDetailsPage; x: 0 }
        }
    ]
    state: "entryPage"

    property alias secret: secretText.text

    backHandler: function handler() {
        // we practically have two or 3 pages inside this on Page object, plus a popup!
        // we should make the 'back' button be aware of this.
        if (checkBlockchainPopup.visible) {
            checkBlockchainPopup.whenDone = undefined; // cancel import
            checkBlockchainPopup.visible = false;
        }
        else if (state !== "entryPage")
            state = "entryPage";
        else
            thePile.pop();
    }
    Keys.onPressed: (event)=> {
        if (event.key === Qt.Key_Escape || event.key === Qt.Key_Back) {
            event.accepted = true;
            backHandler();
        }
    }

    function toNextPage() {
        var type = entryPage.typedData;
        if (type === Wallet.PrivateKey) {
            state = "privKeyDetailsPage";
            privKeydetailsPage.takeFocus();
        } else if (type === Wallet.CorrectMnemonic || type === Wallet.ElectrumMnemonic) {
            state = "seedDetailsPage";
            seedDetailsPage.takeFocus();
        }
    }

    function historicalHeights() {
        return [
            100406, 105564, 111133, 115516, 120731, 127374, 133753,
            138539, 142965, 147309, 150955, 155150, 159601, 164289,
            168947, 173332, 177653, 182030, 186680, 191257, 196146,
            200982, 205472, 210052, 214090, 218495, 223659, 228486,
            233645, 238422, 243833, 248955, 254735, 260648, 266613,
            272024, 277464, 282948, 288364, 292981, 298197, 303092,
            308376, 312919, 317984, 322972, 327423, 332040, 336384,
            340917, 345605, 349713, 354152, 358436, 362974, 367405,
            371964, 376579, 380994, 385802, 390708, 395508, 400464,
            404739, 409332, 413834, 418421, 422658, 427282, 431993,
            436359, 441045, 445568, 450483, 455193, 459389, 463935,
            468621, 473312, 478024, 484018, 489925, 499847, 506195,
            510496, 514976, 519421, 523442, 527942, 532251, 536702,
            541001, 545452, 549912, 554201, 558596, 562898, 567326,
            571781, 575836, 580290, 584604, 589044, 593355, 597824,
            602298, 606618, 611066, 615384, 619871, 624312, 628475,
            632848, 637166, 641633, 645955, 650396, 654855, 659133,
            663626, 667895, 672364, 676770, 680773, 685462, 689903,
            694154, 698312, 702882, 707322, 711585, 716117, 720443,
            724876, 729367, 733391, 737876, 742106, 746494, 750878,
            755311, 759815
        ];
    }

    function heightOfBlockAtTime(dateTime) {
        let height = Pay.heightOfBlockAtTime(dateTime);
        // This is the import page, an import can start before the
        // checkpoint we have, and without headers the above method will
        // not actually resolve.
        if (height <= 0) { // then we need to resolve it ourselves.
            // block-heights is one entry per month, first entry is Jan 2011
            let baseline = new Date('2011-01-01');
            let index = (dateTime.getYear() - baseline.getYear()) * 12 + dateTime.getMonth() + 1;

            height = historicalHeights()[index - 1];
        }
        return height;
    }


    function dateOnHeight(height) {
        // This class, in contrary to the similar method on the Pay class, returns -1 if
        // the client doesn't have the headers available to do the lookup.
        var m = seedImportHelper.monthOnHeight(height);
        if (m === -1) {
            // use our lookup table to select the historical month / year.
            let heights = historicalHeights();
            for (var index = 0; index < heights.length; ++index) {
                if (heights[index] > height)
                    break;
            }
            index = index - 1;
            m = index % 12 + 1;
            var y = 2011 + Math.floor(index / 12);
        }
        else {
            y = seedImportHelper.yearOnHeight(height);
        }

        return {
            month: m,
            year: y
        };
    }

    Column {
        id: entryPage
        width: parent.width
        y: 10
        spacing: 20

        property var typedData: Pay.identifyString(secretText.totalText)
        property bool finished: typedData === Wallet.PrivateKey || typedData === Wallet.CorrectMnemonic || typedData === Wallet.ElectrumMnemonic

        onFinishedChanged: root.toNextPage();
        PageTitledBox {
            id: buttonsBox
            title: qsTr("Select import method")
            width: parent.width
            Item {
                width: parent.width
                height: scanButton.height + 20
                Flowee.ImageButton {
                    id: scanButton
                    source: "qrc:/qr-code-scan" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    onClicked: scanner.start();
                    iconSize: Math.min(entryPage.width / 4, 100)
                    x: (parent.width - width) / 2 // while NFC is not enabled..
                    // x: (parent.width - width * 2 - 20) / 2
                    y: 10
                    text: qsTr("Scan QR")
                }
                QRScanner {
                    id: scanner
                    onFinished: {
                        if ((scanType === QRScanner.Seed || QRScanner.PrivateKeyWIF) && scanResult !== "")
                            secretText.text = scanResult;
                        // make sure to give focus back to this page after the camera took it.
                        scanButton.forceActiveFocus();
                    }
                }
                Rectangle {
                    id: nfcButton
                    width: scanButton.width
                    visible: false
                    height: width
                    radius: 90
                    color: "#00000000"
                    border.color: "yellow"
                    border.width: 2
                    x: (parent.width - width * 2 - 20) / 2 + width + 20
                    y: 10

                    Flowee.Label {
                        anchors.centerIn: parent
                        text: "NFC"
                    }
                }
            }
        }
        Row {
            spacing: 15
            x: (entryPage.width - width) / 2
            Rectangle {
                width: 50
                height: 1
                color: palette.button
                anchors.verticalCenter: parent.verticalCenter
            }
            Flowee.Label {
                text: qsTr("OR")
            }
            Rectangle {
                width: 50
                height: 1
                color: palette.button
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        PageTitledBox {
            id: textSecretBox
            title: qsTr("Secret as text", "The seed-phrase or private key")
            width: parent.width
            Item {
                width: parent.width
                height: pasteButton.visibe ? pasteButton.height / 3 * 2 : 0 + secretText.height

                Flowee.MultilineTextField {
                    id: secretText
                    width: parent.width
                    clip: true
                    height: Math.max((pasteButton.height - 10) * 2.3, implicitHeight)
                    font.family: "monospace"
                }

                Flowee.TextPasteDecorator {
                    id: pasteButton
                    buddy: secretText
                    clipboardTypes: ClipboardHelper.PrivateKey + ClipboardHelper.MnemonicSeed
                }
            }
            Flowee.Label {
                text: {
                     if (entryPage.typedData === Wallet.PartialMnemonicWithTypo) {
                        var bareText = secretText.text;
                        // if inputMethodComposing true, that makes it simple to avoid the word that
                        //  is being edited the 'text' property of secretText omits that one.
                        if (!secretText.inputMethodComposing) {
                            // in case we're editing without there being an inputmethod we find the word
                            // based on the char-pos.
                            let cp = secretText.cursorPosition;
                            let startEditWord = bareText.lastIndexOf(' ', cp);
                            let endEditWord = bareText.indexOf(' ', cp);

                            let before = bareText.substr(0, startEditWord);
                            let after = endEditWord > 0 ? bareText.substr(endEditWord) : "";
                            bareText = before + after;
                        }

                        if (Pay.identifyString(bareText) === Wallet.PartialMnemonicWithTypo)
                            return qsTr("Unknown word(s) found");
                    }
                    return ""
                }
                color: mainWindow.errorRed
                visible: text !== ""
            }
        }

        Flowee.Button {
            anchors.right: parent.right
            text: qsTr("Next")
            visible: parent.finished

            onClicked: root.toNextPage();
        }

        Behavior on x { NumberAnimation { } }
    }

    Item {
        id: privKeydetailsPage
        x: width + 10
        width: parent.width
        height: parent.height - 20
        y: 10

        function takeFocus() {
            singleAddress.forceActiveFocus();
        }

        ColumnLayout {
            spacing: 10
            width: parent.width
            PageTitledBox {
                title: qsTr("Address to import")
                Layout.fillWidth: true
                Flowee.LabelWithClipboard {
                    // this shows the bitcoincash address matching the private key
                    font.pixelSize: singleAddress.font.pixelSize * 0.9
                    text: {
                        if (root.state !== "privKeyDetailsPage")
                            return "";
                        return Pay.addressForPrivKey(secretText.text);
                    }
                }
            }

            PageTitledBox {
                title: qsTr("New Wallet Name")
                visible: {
                    // don't ask for a name when the user imports a
                    // wallet the first thing in a new instance.
                    var all = portfolio.rawAccounts;
                    if (all.length === 1 && !all[0].isUserOwned)
                        return false;
                    return true;
                }
                Flowee.TextField {
                    id: accountName
                    width: parent.width
                }
            }

            Flowee.CheckBox {
                id: singleAddress
                Layout.fillWidth: true
                text: qsTr("Force Single Address");
                toolTipText: qsTr("When enabled, no extra addresses will be auto-generated in this wallet.\nChange will come back to the imported key.")
                checked: true
            }

            PageTitledBox {
                title: qsTr("Oldest Transaction")
                Item {
                    implicitWidth: parent.width
                    implicitHeight: ageButton.height
                    Flowee.BigButton {
                        id: ageButton
                        text: qsTr("Check Age", "online check for wallet age")
                        enabled: !privKeyImportHelper.checking
                        isMainButton: true;

                        onClicked: {
                            // setting new values here will start the check.
                            privKeyImportHelper.secretType = Wallet.PrivateKey
                            privKeyImportHelper.secret = secretText.text
                        }
                        ImportHelper {
                            id: privKeyImportHelper
                            onCheckingChanged: {
                                if (checking)
                                    return;
                                emptyPrivKeyWarningLabel.visible = false;
                                ageButton.isMainButton = false;
                                if (resultCount === 0) {
                                    emptyPrivKeyWarningLabel.visible = true;
                                }
                                else if (resultCount === 1) {
                                    let date = root.dateOnHeight(startHeight(0));
                                    oldestTransactionChooser.item.month.currentIndex = date.month - 1;
                                    oldestTransactionChooser.item.year.currentIndex = date.year - 2011;
                                    oldestTransactionChooser.item.enabled = false;

                                    privImportStartButton.isMainButton = true;
                                }
                            }
                        }

                        // TODO add warning
                    }
                }
                Loader {
                    id: oldestTransactionChooser
                    width: parent.width
                    sourceComponent: oldestTransactionChooser_component
                }
            }
            Flowee.Label {
                id: emptyPrivKeyWarningLabel
                color: mainWindow.errorRed
                text: qsTr("Nothing found for wallet")
                visible: false
            }

            Flowee.BigButton {
                id: privImportStartButton
                text: qsTr("Start")
                Layout.alignment: Qt.AlignRight

                onClicked: {
                    var bh = 0;
                    if (privKeyImportHelper.resultCount > 0)
                        bh = privKeyImportHelper.startHeight(0);
                    else
                        bh = root.heightOfBlockAtTime(oldestTransactionChooser.item.selectedDate);

                    checkBlockchainPopup.oldestBlock = bh;
                    checkBlockchainPopup.whenDone = startImport;
                    checkBlockchainPopup.open();
                }

                function startImport() {
                    if (privKeyImportHelper.resultCount > 0) {
                        var options = Pay.createImportedWallet(secretText.text, accountName.text, privKeyImportHelper.startHeight(0))
                    } else {
                        var height = root.heightOfBlockAtTime(oldestTransactionChooser.item.selectedDate);
                        options = Pay.createImportedWallet(secretText.text, accountName.text, height)
                    }
                    options.forceSingleAddress = singleAddress.checked;

                    for (let a of portfolio.accounts) {
                        if (a.id === options.accountId) {
                            portfolio.current = a;
                            break;
                        }
                    }
                    thePile.pop();
                    thePile.pop();
                }
            }
        }

        Behavior on x { NumberAnimation { } }
    }

    Column {
        id: seedDetailsPage
        x: width + 10
        y: 10
        width: parent.width
        height: parent.height - 20
        spacing: 10

        function takeFocus() {
            if (nameField.visible)
                nameField.forceActiveFocus();
            else
                seedCheckButton.forceActiveFocus();
        }

        PageTitledBox {
            id: nameField
            title: qsTr("New Wallet Name")
            width: parent.width
            visible: {
                // don't ask for a name when the user imports a
                // wallet the first thing in a new instance.
                var all = portfolio.rawAccounts;
                if (all.length === 1 && !all[0].isUserOwned)
                    return false;
                return true;
            }
            Flowee.TextField {
                id: accountName2
                width: parent.width
            }
        }

        Flowee.BigButton {
            id: seedCheckButton
            text: qsTr("Discover Details", "online check for wallet details")
            enabled: !seedImportHelper.checking
            isMainButton: true;

            onClicked: {
                // setting new values here will start the check.
                seedImportHelper.secretType = entryPage.typedData
                seedImportHelper.secret = secretText.text
                seedImportHelper.password = passwordField.text
            }
            ImportHelper {
                id: seedImportHelper
                onCheckingChanged: {
                    if (checking)
                        return;
                    emptySeedWarningLabel.visible = false;
                    if (resultCount === 0) { // empty
                        seedCheckButton.isMainButton = false;
                        emptySeedWarningLabel.visible = true;
                        passwordBox.visible = true;
                    }
                    else if (resultCount >= 1) {
                        // TODO what to do if there are more then 1?

                        let date = root.dateOnHeight(startHeight(0));
                        oldestTransactionChooser2.item.month.currentIndex = date.month - 1;
                        oldestTransactionChooser2.item.year.currentIndex = date.year - 2011;
                        oldestTransactionChooser2.item.enabled = false;
                        derivationPath.text = derivation(0);
                        derivationPath.enabled = false;

                        seedCheckButton.isMainButton = false;
                        seedStartButton.isMainButton = true;
                    }
                }
            }

            // TODO add warning
        }

        PageTitledBox {
            title: qsTr("Oldest Transaction")
            width: parent.width
            Loader {
                id: oldestTransactionChooser2
                width: parent.width
                sourceComponent: oldestTransactionChooser_component
            }
        }
        PageTitledBox {
            id: derivationLabel
            title: qsTr("Derivation Path")
            width: parent.width
            Flowee.TextField {
                id: derivationPath
                property bool derivationOk: Pay.checkDerivation(text);
                width: parent.width
                text: "m/44'/0'/0'" // What most BCH wallets are created with
                color: derivationOk ? palette.text : "red"
            }
        }
        Flowee.Label {
            id: emptySeedWarningLabel
            color: mainWindow.errorRed
            text: qsTr("Nothing found for seed. Does it have a password?")
            visible: false
            width: parent.width
            wrapMode: Text.Wrap
        }

        PageTitledBox {
            id: passwordBox
            title: qsTr("Password")
            width: parent.width
            visible: false
            Flowee.TextField {
                id: passwordField
                width: parent.width
                placeholderText: qsTr("imported wallet password")
                onTotalTextChanged: seedCheckButton.isMainButton = true;
            }
        }

        Flowee.BigButton {
            id: seedStartButton
            text: qsTr("Start")
            anchors.right: parent.right
            onClicked: {
                var bh = 0;
                if (seedImportHelper.resultCount > 0)
                    bh = seedImportHelper.startHeight(0);
                else
                    bh = root.heightOfBlockAtTime(oldestTransactionChooser2.item.selectedDate);
                checkBlockchainPopup.oldestBlock = bh;
                checkBlockchainPopup.whenDone = startImport;
                checkBlockchainPopup.open();
            }

            function startImport() {
                if (seedImportHelper.resultCount > 0) {
                    var options = Pay.createImportedHDWallet(secretText.text, passwordField.text,
                            derivationPath.text, accountName2.text, seedImportHelper.startHeight(0),
                            seedImportHelper.isElectrumSeed(0));
                } else {
                    var height = root.heightOfBlockAtTime(oldestTransactionChooser2.item.selectedDate);
                    options = Pay.createImportedHDWallet(secretText.text, passwordField.text,
                            derivationPath.text, accountName2.text, height);
                }

                for (let a of portfolio.accounts) {
                    if (a.id === options.accountId) {
                        portfolio.current = a;
                        break;
                    }
                }
                thePile.pop();
                thePile.pop();
            }
        }

        Behavior on x { NumberAnimation { } }
    }

    Item { // non-(default) visible items below.


        /*
         * The users wallet may not actually have the old headers on the device because the
         * user never needed them. So to make the import work, we first sill need to actually
         * download the missing headers. Use the block-headers checker (from the module 'blocks')
         * to verify and initiate this.
         */
        QQC2.Popup {
            id: checkBlockchainPopup

            property int oldestBlock: 0
            property var whenDone: null

            width: root.width - 20
            height: checkerLoader.height + 20

            background: Rectangle {
                color: palette.light
                border.color: palette.midlight
                border.width: 1
                radius: 5
            }
            modal: true
            closePolicy: QQC2.Popup.NoAutoClose
            Loader {
                id: checkerLoader
                width: parent.width - 20
                height: {
                    if (item == null)
                        return 100;
                    return item.implicitHeight
                }

                onLoaded: {
                    closeMonitor.target = item;
                    // console.log("Loading completed, start checking on height: " + checkBlockchainPopup.oldestBlock);
                    item.wantedHeight = checkBlockchainPopup.oldestBlock
                }
                Connections {
                    id: closeMonitor
                    function onVisibleChanged() {
                        let i = checkerLoader.item;
                        if (i == null) // shouldn't really happen..
                            return;
                        let visible = i.visible;
                        if (!visible) {
                            closeMonitor.target = null;
                            checkerLoader.source = "";
                            checkBlockchainPopup.close();
                            // call the callback to initiate the actual import.
                            checkBlockchainPopup.whenDone();
                        }
                    }
                }
            }

            onVisibleChanged: {
                if (!visible)
                    return;

                // Shipped in module 'blocks' we invoke the 'checker', should it exist.
                var section = ModuleManager.sectionOnPlugin("blocks", "checker");
                if (section !== null)
                    checkerLoader.source = section.qml; // This loads the plugin.
                else
                    close();
            }
        }


        Component {
            id: oldestTransactionChooser_component
            Flow {
                property alias month: inner_month
                property alias year: inner_year
                property date selectedDate: {
                    return new Date(inner_year.currentIndex + 2011, inner_month.currentIndex, 1);
                }

                width: parent.width
                spacing: 10
                Flowee.ComboBox {
                    id: inner_month
                    model: {
                        let locale = Qt.locale();
                        var list = [];
                        for (let i = 0; i <= 11; ++i) {
                            list.push(locale.monthName(i));
                        }
                        return list;
                    }
                    width: implicitWidth * 1.3 // this makes it fit for bigger fonts.
                }
                Flowee.ComboBox {
                    id: inner_year
                    model: {
                        var list = [];
                        let last = new Date().getFullYear();
                        for (let i = 2011; i <= last; ++i) {
                            list.push(i);
                        }
                        return list;
                    }
                    currentIndex: 12;
                }
            }
        }
    }
}
