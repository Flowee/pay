/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import "../Flowee" as Flowee

MainViewBase {
    showFilterIcon: currentIndex === 0

    AccountHistory {
        anchors.fill: parent
        PopupOverlay { id: popupOverlay }
        Component {
            id: filterPopup
            FilterPopup { }
        }
    }
    SendTransactionsTab {
        anchors.fill: parent
    }
    ReceiveTab {
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10
    }
    FocusScope {
        id: apps
        property string icon: "qrc:/exploretab" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
        property string title: qsTr("Explore")
        property int buttonId: 73573
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        Column {
            width: parent.width

            Repeater {
                model: ModuleManager.exploreTabItems
                TextButton {
                    text: modelData.text
                    subtext: modelData.subtext
                    iconSource: modelData.icon
                    pageButton: true
                    onClicked: thePile.push(modelData.qml)
                }
            }

            TextButton {
                width: parent.width
                text: qsTr("Find More")
                iconSource: "qrc:/more" + (Pay.useDarkSkin ? "-light" : "")  + ".svg";
                pageButton: true
                buttonId: 95231
                onClicked: thePile.push("ExploreModules.qml")
            }
        }
    }

    // only visible on AccountHistory for now.
    onFilterIconClicked: popupOverlay.open(filterPopup, null)
}
