/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import "../Flowee" as Flowee
import Flowee.org.pay;

FocusScope {
    id: root
    Rectangle {
        anchors.fill: parent
        color: palette.window
    }
    MouseArea { // eat all mouse events.
        anchors.fill: parent
    }

    Image {
        source: "qrc:/background.jpg"
        width: parent.width
        height: {
            if (Pay.unlockingKeyboard === FloweePay.FullKeyboard)
                return parent.height;
            if (Pay.unlockingKeyboard === FloweePay.SmallNumbersKeyboard)
                return parent.height - 225;
            return 0;
        }
    }

    UnlockWidget {
        anchors.fill: parent
        anchors.margins: 10
        onPasswordEntered: if (!Pay.checkAppPassword(password)) passwordIncorrect();
        // the above background means the widget should prefer white text
        assumeDarkBackground: Pay.unlockingKeyboard !== FloweePay.BigNumbersKeyboard
    }
    Keys.onPressed: (event)=> {
        event.accepted = true;
        if (event.key === Qt.Key_Escape || event.key === Qt.Key_Back)
            mainWindow.close(); // exit app on 'back'.
    }

    Column {
        id: quickReceive
        x: 60
        y: {
            var uk = Pay.unlockingKeyboard;
            if (uk === FloweePay.BigNumbersKeyboard)
                return parent.height + 10;
            if (uk === FloweePay.FullKeyboard)
                return 300;
            if (uk === FloweePay.SmallNumbersKeyboard)
                return parent.height - 270 - height
        }
        spacing: 6
        opacity: 0
        property bool havePortfolio: typeof portfolio !== "undefined";
        onHavePortfolioChanged: opacity = portfolio.current.isSingleAddressAccount ? 0 : 1
        Behavior on opacity { NumberAnimation { } }

        Image {
            source: "qrc:/back-arrow"
            rotation: 270
            width: 30
            height: 30
        }
        Flowee.Label {
            color: "white"
            text: qsTr("Quick Receive")
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Qt.AlignHCenter
            x: 15 - width / 2
            width: 90
        }
    }
    MouseArea {
        id: quickReceiveButton
        focus: true
        enabled: quickReceive.visible
        anchors.fill: quickReceive
        onClicked: myLittleStack.push(receive)
    }

    FocusScope {
        anchors.fill: parent
        Rectangle {
            id: background
            anchors.fill: parent
            color: palette.window
            visible: myLittleStack.depth > 0
            MouseArea { anchors.fill: parent; acceptedButtons: Qt.AllButtons } // catch all

            QQC2.StackView {
                id: myLittleStack
                anchors.fill: parent
                onCurrentItemChanged: if (currentItem != null) currentItem.takeFocus();
            }
        }
        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_Escape || event.key === Qt.Key_Back) {
                if (myLittleStack.depth <= 1) {
                    myLittleStack.clear();
                    quickReceiveButton.forceActiveFocus();
                }
                else
                    myLittleStack.pop();
                event.accepted = true;
            }
        }
    }

    Component {
        id: receive
        Page {
            function takeFocus() {
                receiveTab.verticalTab = 1; // the money input
                receiveTab.forceActiveFocus();
            }
            backHandler: function handler() { myLittleStack.clear() }
            headerText: receiveTab.title
            function switchToTab(tab) {
                myLittleStack.clear();
            }
            ReceiveTab {
                id: receiveTab
                anchors.fill: parent
                showInstructions: false
            }
        }
    }
}
