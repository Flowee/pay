/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick

Rectangle {
    id: root
    color: Pay.useDarkSkin ? "#0b45a2" : "#0d61b4"
    width: 14
    height: 14
    radius: 7
    x: visible ? buddy.x + buddy.contentWidth - 1 : 0;
    y: {
        if (!visible)
            return 0;
        return buddy.y + buddy.baselineOffset - height / 2 - buddy.font.pixelSize * 0.8
    }
    property int buttonId: 0

    // the label this is drawn over.
    required property var buddy

    function markSeen() {
        var bID = root.buttonId;
        if (bID !== 0) {
            NewIndicatorProvider.seen(bID);
            root.visible = false;
        }
    }

    visible: {
        var bID = root.buttonId;
        if (bID === 0)
            return false;
        return NewIndicatorProvider.isNew(bID);
    }
}
