/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee
import Flowee.org.pay;

// the widget that shows up on the AccountHistory
// allowing one to change the timeline filters
Item {
    implicitHeight: coreColumn.height + 10
    width: parent.width

    function toggleFlag(flag, on) {
        var cf = portfolio.current.transactions.includeFlags
        if (((cf & flag) > 0) === on) // nothing to do.
            return;
        if (on)
            cf += flag;
        else
            cf -= flag;
        portfolio.current.transactions.includeFlags = cf
    }

    ColumnLayout {
        id: coreColumn
        width: parent.width
        spacing: 10
        Flowee.Label {
            text: qsTr("Transactions Filter")
        }
        Item {
            width: 80 * 3 + 20
            Layout.alignment: Qt.AlignHCenter
            implicitHeight: 80
            Rectangle {
                width: 80
                height: 80
                radius: 40
                x: 10

                border.width: 3
                color: checked ? palette.alternateBase : palette.base
                border.color: checked ? palette.midlight : palette.mid
                Image {
                    source: "qrc:/tx-send" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    width: 35
                    height: 35
                    smooth: true
                    x: 35
                    y: 9
                    opacity: parent.checked ? 1 : 0.5
                }
                Image {
                    source: "qrc:/tx-receiving" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    width: 35
                    height: 35
                    smooth: true
                    y: 36
                    x: 10
                    opacity: parent.checked ? 1 : 0.5
                }


                property bool checked: {
                    var flags = portfolio.current.transactions.includeFlags;
                    var wanted = Wallet.IncludeSentTransactions
                                | Wallet.IncludeReceivedTransactions;
                    return (flags & wanted) === wanted;
                }
                MouseArea {
                    anchors.fill: parent
                    enabled: !parent.checked
                    onClicked: {
                        toggleFlag(Wallet.IncludeSentTransactions, true);
                        toggleFlag(Wallet.IncludeReceivedTransactions, true);
                    }
                }
            }
            Rectangle {
                width: 80
                height: 80
                radius: 40
                x: 100

                border.width: 3
                color: checked ? palette.alternateBase : palette.base
                border.color: checked ? palette.midlight : palette.mid

                Image {
                    source: "qrc:/tx-receiving" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    width: 50
                    height: 50
                    smooth: true
                    y: 11
                    x: 12
                    opacity: parent.checked ? 1 : 0.5
                }
                property bool checked: {
                    var flags = portfolio.current.transactions.includeFlags;
                    var filter = Wallet.IncludeSentTransactions
                                | Wallet.IncludeReceivedTransactions;
                    return (flags & filter) === Wallet.IncludeReceivedTransactions;
                }
                MouseArea {
                    anchors.fill: parent
                    enabled: !parent.checked
                    onClicked: {
                        toggleFlag(Wallet.IncludeSentTransactions, false);
                        toggleFlag(Wallet.IncludeReceivedTransactions, true);
                    }
                }
            }
            Rectangle {
                width: 80
                height: 80
                radius: 40
                x: 190

                border.width: 3
                color: checked ? palette.alternateBase : palette.base
                border.color: checked ? palette.midlight : palette.mid

                Image {
                    source: "qrc:/tx-send" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
                    width: 50
                    height: 50
                    smooth: true
                    x: 15
                    y: 11
                    opacity: parent.checked ? 1 : 0.5
                }
                property bool checked: {
                    var flags = portfolio.current.transactions.includeFlags;
                    var filter = Wallet.IncludeSentTransactions
                                | Wallet.IncludeReceivedTransactions;
                    return (flags & filter) === Wallet.IncludeSentTransactions;
                }
                MouseArea {
                    anchors.fill: parent
                    enabled: !parent.checked
                    onClicked: {
                        toggleFlag(Wallet.IncludeSentTransactions, true);
                        toggleFlag(Wallet.IncludeReceivedTransactions, false);
                    }
                }
            }
        }
        Rectangle {
            id: cfCheckbox
            width: 80
            height: 80
            radius: 6
            color: checked ? palette.alternateBase : palette.base
            border.width: 3
            border.color: checked ? palette.midlight : palette.mid
            visible: portfolio.current.hasAnonimityTransactions

            property bool checked: (portfolio.current.transactions.includeFlags
                                    & Wallet.IncludeCFs) > 0;

            Image {
                source: "qrc:/cf.svg";
                width: 60
                height: 60
                smooth: true
                anchors.centerIn: parent
                opacity: cfCheckbox.checked ? 1 : 0.5
            }
            MouseArea {
                anchors.fill: parent
                onClicked: toggleFlag(Wallet.IncludeCFs, !cfCheckbox.checked);
            }
        }
        Flowee.CheckBox {
            width: parent.width
            text: qsTr("Only with a comment", "This is a statement about a transaction");
            checked: !(portfolio.current.transactions.includeFlags & Wallet.IncludeTxWithoutComment) > 0
            onToggled: toggleFlag(Wallet.IncludeTxWithoutComment, !checked);
        }
    }
}
