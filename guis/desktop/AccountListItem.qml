/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../Flowee" as Flowee

Item {
    id: root
    property QtObject account: null
    height: column.height + 18
    signal clicked;

    property int startPos: account.initialBlockHeight

    //background
    Rectangle {
        property bool selected: portfolio.current === account

        anchors.fill: parent
        anchors.bottomMargin: 6
        id: background
        property bool hover: false
        radius: 7
        color: selected ? palette.light : palette.window
        border.width: 1.5
        border.color: {
            if (portfolio.current === account)
                return mainWindow.floweeGreen
            if (hover)
                return Pay.useDarkSkin ? mainWindow.floweeSalmon : mainWindow.floweeBlue;
            return Pay.useDarkSkin ? "#EEE" : "#7b7f7f"
        }

        Timer {
            id: startTimer
            property int wavy: 0
            running: {
                if (root.account.isArchived || Pay.offline)
                    return false;
                if (root.account.lastBlockSynched === root.startPos) {
                    if (Pay.chainHeight === root.startPos) // fresh new wallet
                        return false;
                    return true;
                }
                return false;
            }
            interval: 300
            repeat: true
            onTriggered: {
                var w = wavy;
                if (w >= 2)
                    wavy = 0;
                else
                    wavy = w + 1;
            }
        }

        Rectangle {
            height: parent.height
            width: {
                let startPos = root.startPos;
                let end = Pay.expectedChainHeight
                if (startPos == 0)
                    return 0;
                let currentPos = root.account.lastBlockSynched;
                if (currentPos === startPos) { // waiting for start
                    var wave = startTimer.wavy;
                    if (wave > 10)
                        wave = 20 - wave;
                    return wave * 8;
                }
                let totalDistance = end - startPos;
                let ourProgress = currentPos - startPos;
                return parent.width * (ourProgress / totalDistance);
            }
            radius: parent.radius
            border.width: parent.border.width
            border.color: "#00000000"
            color: mainWindow.floweeGreen
            opacity: root.account.uptodate ? 0 : 0.2
            Behavior on opacity { NumberAnimation {} }
            Behavior on width { NumberAnimation { easing.type: Easing.InExpo; duration: startTimer.running ? 300 : 1} }
        }
        Rectangle {
            height: parent.height
            anchors.bottom: parent.bottom
            width: {
                let startPos = root.startPos;
                let end = Pay.expectedChainHeight
                if (startPos == 0)
                    return 0;
                let currentPos = root.account.lastBlockSynched2;
                let totalDistance = end - startPos;
                let ourProgress = currentPos - startPos;
                return parent.width * (ourProgress / totalDistance);
            }
            radius: parent.radius
            border.width: parent.border.width
            border.color: "#00000000"
            color: mainWindow.floweeGreen
            opacity: root.account.uptodate ? 0 : 0.2
            Behavior on opacity { NumberAnimation {} }
        }
    }

    Column {
        id: column
        spacing: 3
        x: 20
        y: 6
        width: parent.width // - 13
        Flowee.Label {
            text: root.account.name
            font.bold: true
            width: parent.width - 20
            clip: true
        }
        WalletEncryptionStatus {
            id: walletEncryptionStatus
            width: parent.width
            account: root.account
        }
        Flow {
            width: parent.width
            spacing: 10
            visible: lastReceive.text !== "" && !root.account.isArchived
            Flowee.Label {
                text: qsTr("Last Transaction") + ":"
            }
            Flowee.Label {
                id: lastReceive
                text: Pay.formatDate(account.lastMinedTransaction)
            }
        }
        Flowee.Label {
            visible: root.account.isArchived
            text: visible ? account.timeBehind : ""
        }
    }

    Rectangle {
        color: Pay.useDarkSkin ? mainWindow.floweeSalmon : mainWindow.floweeBlue
        width: 7
        height: 7
        radius: 7
        x: 12
        anchors.verticalCenter: parent.verticalCenter

        visible: {
            if (portfolio.current === root.account)
                return false;

            return root.account.hasFreshTransactions
        }
    }

    Flowee.ArrowPoint {
        id: point
        x: 1.3
        visible: portfolio.current === account;
        anchors.verticalCenter: parent.verticalCenter
        color: background.border.color
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            portfolio.current = account
            root.clicked()
        }
        onEntered: background.hover = true
        onExited: background.hover = false
    }

    AccountConfigMenu {
        id: accountConfigMenu
        anchors.right: parent.right
        anchors.rightMargin: 6
        y: 6
        account: root.account
    }
}
