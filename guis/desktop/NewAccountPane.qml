/* * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../Flowee" as Flowee

FocusScope {
    id: newAccountsPane
    anchors.fill: parent
    focus: true

    Rectangle {
        color: "black"
        anchors.fill: parent
        opacity: 0.4
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true // to eat the hover events from the guys in the background.
            onClicked: newAccountsPane.visible = false
        }
    }
    Rectangle {
        color: palette.window
        anchors.fill: contentArea
        anchors.margins: -10 // have an inset
    }

    Item {
        id: contentArea
        x: 50
        y: 50
        height: Math.min(parent.height - 100, 800);
        width: parent.width - 100
        clip: true

        MouseArea { anchors.fill: parent }
        Item {
            id: door
            width: parent
            height: parent
            ColumnLayout {
                id: verticalTabs
                width: accountTypePreferred.implicitWidth
                spacing: 10
                opacity: mainArea.item === null ? 1 : 0.65

                Flowee.CardTypeSelector {
                    id: accountTypePreferred
                    title: qsTr("New HD wallet")
                    checkable: true
                    Layout.fillWidth: true
                    onClicked: mainArea.source = "./NewAccountCreateHDAccount.qml"

                    features: [
                        qsTr("Seed-phrase based", "Context: wallet type"),
                        qsTr("Easy to backup", "Context: wallet type"),
                        qsTr("Most compatible", "The most compatible wallet type")
                    ]
                }
                Flowee.CardTypeSelector {
                    id: accountTypeImport
                    title: qsTr("Import Existing Wallet")
                    Layout.fillWidth: true
                    checkable: true
                    onClicked: mainArea.source = "./NewAccountImportAccount.qml"

                    features: [
                        qsTr("Imports seed-phrase"),
                        qsTr("Imports private key")
                    ]
                }
                Flowee.CardTypeSelector {
                    id: accountTypeBasic
                    Layout.fillWidth: true
                    title: qsTr("New Basic Wallet")
                    checkable: true
                    onClicked: mainArea.source = "./NewAccountCreateBasicAccount.qml"

                    features: [
                        qsTr("Private keys based", "Property of a wallet"),
                        qsTr("Difficult to backup", "Context: wallet type"),
                        qsTr("Great for brief usage", "Context: wallet type")
                    ]
                }
            }
            Loader {
                id: mainArea
                x: verticalTabs.width + 10
                property double columnWidth: verticalTabs.width * 1.4

                width: {
                    var i = item;
                    if (i === null)
                        return door.width - x;
                    return i.implicitWidth
                }
                onWidthChanged: {
                    if (mainArea.item == null) {
                        door.x = 0;
                        return;
                    }
                    var avail = contentArea.width
                    var taken = verticalTabs.width + mainArea.item.implicitWidth
                    if (taken < avail)
                        door.x = 0;
                    else
                        door.x = avail - taken;
                }
            }

            Behavior on x { NumberAnimation { }}
        }
        Flowee.CloseIcon {
            id: closeIcon
            anchors.right: parent.right
            onClicked: newAccountsPane.visible = false
        }
    }

    Component.onCompleted: forceActiveFocus() // we assume this component is used in a Loader
    Keys.onPressed: (event)=> {
        if (event.key === Qt.Key_Escape) {
            newAccountsPane.visible = false;
            event.accepted = true;
        }
    }
}
