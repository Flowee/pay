/*
 * This file is part of the Flowee project
 * Copyright (C) 2021, 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee


Item {
    implicitWidth: columnWidth // columnWidth is defined  by loader in NewAccountPane
    height: col.height

    // ColumnLayoud is trying to outsmart us and when I supply implicitWidth it ends up giving
    // one that is based on its content instead of the one I specifically told it to use.
    // Which is why we wrap the columnLayout in an Item.

    ColumnLayout {
        id: col
        spacing: 10
        width: parent.width

        Flowee.Label {
            id: title
            text: qsTr("Create a new empty wallet with simple multi-address capability ")
            Layout.fillWidth: true
            font.bold: true
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
        RowLayout {
            id: nameRow
            spacing: 6
            Flowee.Label {
                text: qsTr("Name") + ":";
                Layout.alignment: Qt.AlignBaseline
            }
            Flowee.TextField {
                id: accountName
                Layout.fillWidth: true
            }
        }

        Flowee.CheckBox {
            id: singleAddress
            text: qsTr("Force Single Address");
            toolTipText: qsTr("When enabled, this wallet will be limited to one address.\nThis ensures only one private key will need to be backed up")
            checked: true
        }


        Flowee.Button {
            id: button
            text: qsTr("Go")
            Layout.alignment: Qt.AlignRight
            onClicked: {
                var options = Pay.createNewBasicWallet(accountName.text);
                options.forceSingleAddress = singleAddress.checked;
                for (let a of portfolio.accounts) {
                    if (a.id === options.accountId) {
                        portfolio.current = a;
                        break;
                    }
                }
                newAccountsPane.visible = false;
            }
        }
    }
}
