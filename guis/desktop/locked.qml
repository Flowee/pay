/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../ControlColors.js" as ControlColors

import QtQuick.Controls.Basic

ApplicationWindow {
    id: mainWindow
    visible: true
    width: Pay.windowWidth
    height: Pay.windowHeight
    minimumWidth: 240
    minimumHeight: 300
    title: "Flowee Pay"

    onVisibleChanged: if (visible) ControlColors.applySkin(mainWindow)

    Component.onCompleted: updateFontSize(mainWindow);
    function updateFontSize(window) {
        // 75% = > 14.25,  100% => 19,  200% => 28
        window.font.pixelSize = 17 + (11 * (Pay.fontScaling-100) / 100)
    }

    Item {
        id: mainScreen
        anchors.fill: parent
        focus: true

        Label {
            x: 10
            y: 20
            width: Math.min(parent.width - 20, 200);
            text: qsTr("Already running?")
        }

    }
}

