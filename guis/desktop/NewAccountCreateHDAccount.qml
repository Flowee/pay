/*
 * This file is part of the Flowee project
 * Copyright (C) 2021, 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee

Item {
    id: newAccountCreateHDAccount
    height: col.height
    implicitWidth: columnWidth // columnWidth is defined  by loader in NewAccountPane

    ColumnLayout {
        id: col
        spacing: 10
        width: parent.width
        Flowee.Label {
            id: title
            text: qsTr("Create a new wallet with smart creation of addresses from a single seed-phrase")
            Layout.fillWidth: true
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.bold: true
        }
        RowLayout {
            id: nameRow
            Flowee.Label {
                text: qsTr("Name") + ":";
                Layout.alignment: Qt.AlignBaseline
            }
            Flowee.TextField {
                id: accountName
                Layout.fillWidth: true
            }
        }
        Item {
            height: button.height
            Layout.fillWidth: true

            Flowee.Button {
                id: button
                text: qsTr("Go")
                anchors.right: parent.right
                onClicked: {
                    var options = Pay.createNewWallet(derivationPath.text, /* password */"", accountName.text);
                    for (let a of portfolio.accounts) {
                        if (a.id === options.accountId) {
                            portfolio.current = a;
                            break;
                        }
                    }
                    newAccountsPane.visible = false;
                }
            }
        }

        Flowee.GroupBox {
            title: qsTr("Advanced Options")
            Layout.fillWidth: true
            columns: 3

            Flowee.Label {
                text: qsTr("Derivation") + ":"
                Layout.fillWidth: false
            }
            Flowee.TextField {
                id: derivationPath
                text: "m/44'/0'/0'" // What most wallets use to import by default
                color: Pay.checkDerivation(text) ? palette.text : "red"
                Layout.fillWidth: true
            }
            Item { width: 1; height: 1; Layout.fillWidth: true } // spacer
        }
    }
}
