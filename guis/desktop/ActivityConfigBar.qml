/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee
import Flowee.org.pay;

Item {
    id: root
    height: 50
    Item {
        x: -11
        height: parent.height
        width: 20
        clip: true
        Rectangle {
            color: mainWindow.floweeGreen
            height: 80
            width: 40
            anchors.bottom: parent.bottom
            radius: 10
            
            Rectangle {
                width: parent.width
                height: parent.height
                y: -6
                x: 6
                radius: 6
                color: palette.light
            }
        }
    }
    
    property QtObject wallet: portfolio.current;
    onWalletChanged: wallet.transactions.filterString = filterText.text.trim();
    function toggleFlag(flag, on) {
        var cf = portfolio.current.transactions.includeFlags
        if (((cf & flag) > 0) === on) // nothing to do.
            return;
        if (on)
            cf += flag;
        else
            cf -= flag;
        portfolio.current.transactions.includeFlags = cf
    }

    Flowee.TextField {
        id: filterText
        y: 6
        width: Math.min(500, parent.width - 20)
        placeholderText: qsTr("Filter")
        onTextChanged: portfolio.current.transactions.filterString = text.trim();
    }

    Rectangle {
        id: cfCheckbox
        anchors.left: filterText.right
        anchors.leftMargin: 10
        y: 4
        width: 40
        height: 40
        radius: 2
        color: checked ? palette.alternateBase : palette.base
        border.width: 2
        border.color: {
            if (cfHoverArea.containsMouse)
                return Pay.useDarkSkin ? mainWindow.floweeSalmon : mainWindow.floweeBlue;
            if (checked)
                return palette.midlight;
            return palette.mid
        }
        visible: portfolio.current.hasAnonimityTransactions
        
        property bool checked: (portfolio.current.transactions.includeFlags
                                & Wallet.IncludeCFs) > 0;
        
        Image {
            source: "qrc:/cf.svg";
            width: 30
            height: 30
            smooth: true
            anchors.centerIn: parent
            opacity: cfCheckbox.checked ? 1 : 0.5
        }
        MouseArea {
            id: cfHoverArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: toggleFlag(Wallet.IncludeCFs, !cfCheckbox.checked);
        }
    }

    Rectangle {
        id: sendReceiveFilter
        anchors.left: cfCheckbox.right
        anchors.leftMargin: 10
        border.width: 2
        color: palette.base
        border.color: sendReceiveMouse.containsMouse
                      ? (Pay.useDarkSkin ? mainWindow.floweeSalmon : mainWindow.floweeBlue)
                      : palette.mid
        height: 44
        width: grid.width + 12
        y: 4
        radius: 2

        property bool includeSent: {
            var flags = portfolio.current.transactions.includeFlags;
            var filter = Wallet.IncludeSentTransactions;
            return (flags & filter) === filter;
        }
        property bool includeReceived: {
            var flags = portfolio.current.transactions.includeFlags;
            var filter = Wallet.IncludeReceivedTransactions;
            return (flags & filter) === filter;
        }

        Grid {
            id: grid
            y: 4
            x: 6
            columns: 2
            columnSpacing: 6
            Rectangle {
                color: "#00000000"
                border.width: 1.3
                border.color: palette.button
                width: 14
                height: 14

                Flowee.Label {
                    text: "✓"
                    y: -6
                    x: 2
                    font.pixelSize: 16
                    color: Pay.useDarkSkin ? "#86ffa8" : "green";
                    visible: sendReceiveFilter.includeReceived
                }
            }
            Flowee.Label {
                text: qsTr("Received")
                font.pixelSize: 14
            }
            Rectangle {
                color: "#00000000"
                border.width: 1.3
                border.color: palette.button
                width: 14
                height: 14
                Flowee.Label {
                    text: "✓"
                    y: -6
                    x: 2
                    font.pixelSize: 16
                    color: Pay.useDarkSkin ? "#86ffa8" : "green";
                    visible: sendReceiveFilter.includeSent
                }
            }
            Flowee.Label {
                text: qsTr("Sent")
                font.pixelSize: 14
            }
        }

        MouseArea {
            id: sendReceiveMouse
            anchors.fill: parent
            hoverEnabled: true
            onClicked:  popupTabsOverlay.open(sendReceivePopup, mapToGlobal(width, height));
        }
    }
    Component {
        id: sendReceivePopup
        GridLayout {
            rowSpacing: 10
            columns: 2
            Flowee.CheckBox {
                checked: sendReceiveFilter.includeReceived
                onClicked: toggleFlag(Wallet.IncludeReceivedTransactions, checked);
            }
            Flowee.Label {
                text: qsTr("Received")
            }
            Flowee.CheckBox {
                checked: sendReceiveFilter.includeSent
                onClicked: toggleFlag(Wallet.IncludeSentTransactions, checked);
            }
            Flowee.Label {
                text: qsTr("Sent")
            }
        }
    }
}
