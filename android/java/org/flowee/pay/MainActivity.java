/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flowee.pay;

import org.qtproject.qt.android.bindings.QtActivity;
import android.Manifest;
import android.os.*;
import android.content.Intent;
import android.net.Uri;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.provider.Settings;
import android.content.pm.PackageManager;


public class MainActivity extends QtActivity
{
    public native void setPaymentIntentOnCPP(String data);
    public native void startScan();
    public native void notificationPermissionDenied();

    // the C++ should call this one when it finished startup.
    public void onQtAppStarted()
    {
        filterAndForward(getIntent());
    }

    public void enableRegularUpdates(int hours)
    {
        if (updatesInterval == hours)
            return;
        updatesInterval = hours;
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (alarmManager == null)
            return;

        // They can be scheduled, but they won't do anything unless the
        // user approves the previous ask.
        Intent serviceIntent = new Intent(this, PeriodicService.class);
        serviceIntent.setPackage("org.flowee.pay");
        PendingIntent pendingIntent = PendingIntent.getService(this, 9875,
                serviceIntent, PendingIntent.FLAG_IMMUTABLE);

        if (hours < 1) {
            alarmManager.cancel(pendingIntent);
            return;
        }

        long HourInMillis = 60 * 60 * 1000;
        // Schedule to run approximately every \a hours hour.
        alarmManager.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP, // Use elapsed time, wake device if asleep
                SystemClock.elapsedRealtime() + hours * HourInMillis, // first run
                hours * HourInMillis,  // Repeat interval
                pendingIntent
        );

        // ask to run in the background
        Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }

    public void requestNotificationPermission()
    {
        PayNotifications me = PayNotifications.instance(null);
        if (me.areNotificationsEnabled())
            return;

        String perms[] = new String[1];
        perms[0] = Manifest.permission.POST_NOTIFICATIONS;
        requestPermissions(perms, 1); // 1 is a requestCode
    }

    @Override
    // call-back from requestPermission
    public void onRequestPermissionsResult (int requestCode, String[] permissions,
                int[] grantResults)
    {
        if (requestCode == 1) { // POST_NOTIFICATIONS
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                notificationPermissionDenied();
        }
    }

    public boolean isPowerSaveMode()
    {
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        if (pm != null)
            return pm.isPowerSaveMode();
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        filterAndForward(intent);
    }

    private void filterAndForward(Intent intent)
    {
        if (intent.getAction() == Intent.ACTION_VIEW) {
            // lets find out what the user wants to 'view'.
            String data = intent.getDataString();
            if (data != null
                    && (data.startsWith("bitcoincash:") || data.startsWith("bch-wif:"))) {
                // a payment method. We like that.
                setPaymentIntentOnCPP(data);
            }
        }
        else if (intent.getAction() == Intent.ACTION_QUICK_VIEW) {
            startScan();
        }
    }

    private int updatesInterval = -1; // in hours
}
