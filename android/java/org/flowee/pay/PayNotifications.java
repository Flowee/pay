/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flowee.pay;

import java.util.Vector;
import java.util.ListIterator;
import android.app.Notification;
import android.app.NotificationManager;
import android.service.notification.StatusBarNotification;
import android.content.Context;
import android.app.NotificationChannel;

import org.flowee.pay.test.R;

public class PayNotifications
{
    private static final int BlockNotificationId = 9839874;
    private static PayNotifications g_singleton = null;
    public static PayNotifications instance(Context context)
    {
        if (g_singleton == null)
            g_singleton = new PayNotifications(context);
        return g_singleton;
    }

    public boolean areNotificationsEnabled()
    {
        return instance(null).m_notificationManager.areNotificationsEnabled();
    }


    private Notification.Builder m_blockMessageBuilder = null;
    private NotificationManager m_notificationManager = null;
    private String m_blockChannelId = null;
    private String m_bgSyncChannelId = null;
    private Vector<String> m_blockFoundMessages = new Vector<String>();

    private int m_walletMessageId = 1;
    private String m_walletChannelId = null;

    public static void setup(Context context)
    {
        // create the signleton as it creates our channels
        PayNotifications.instance(context);
    }

    public static void notifyBlock(String message)
    {
        PayNotifications.instance(null).notifyBlock_priv(message);
    }

    public PayNotifications(Context context)
    {
        // see example here on how to make these translatable:
        // https://developer.android.com/develop/ui/views/notifications/build-notification#java
        NotificationChannel newBlocksChannel = new NotificationChannel("blocks", "New Blocks",
                NotificationManager.IMPORTANCE_DEFAULT);
        m_notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        m_notificationManager.createNotificationChannel(newBlocksChannel);
        m_blockChannelId = newBlocksChannel.getId();
        m_blockMessageBuilder = new Notification.Builder(context, m_blockChannelId);

        NotificationChannel bgChannel = new NotificationChannel("bgsync", "Sync",
                NotificationManager.IMPORTANCE_LOW);
        m_notificationManager.createNotificationChannel(bgChannel);
        m_bgSyncChannelId = bgChannel.getId();
    }

    private void notifyBlock_priv(String message)
    {
        // We split this into the title and the message since that is how the anatomy of android
        // notifications work.
        int dot = message.indexOf('.');
        String title = "";
        if (dot > 0) {
            title = message.substring(0, dot + 1);
            message = message.substring(dot + 1);
            message = message.trim(); // likely a space behind the dot.
        }

        // We append the new text to the active notification if exists.
        StatusBarNotification[] notifications = m_notificationManager.getActiveNotifications();
        boolean stillActive = false;
        for (StatusBarNotification n : notifications) {
            if (n.getId() == BlockNotificationId) {
                stillActive = true;
                break;
            }
        }
        if (stillActive) {
            m_blockFoundMessages.add(0, message);
            int lines = 0;
            message = null;
            ListIterator<String> iter = m_blockFoundMessages.listIterator();
            while (iter.hasNext()) {
                String m = iter.next();
                if (lines++ < 6) {
                    if (message == null)
                        message = m;
                    else
                        message += "\n" + m;
                }
                else { // lets keep only some
                    iter.remove();
                }
            }
        } else {
            m_blockFoundMessages.clear();
            m_blockFoundMessages.add(message);
        }

        // https://developer.android.com/reference/android/app/Notification.Builder?hl=en
        m_blockMessageBuilder.setSmallIcon(R.drawable.icon)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(0xA0F87) // flowee blue
                .setAutoCancel(true);

        // we always use the one BlockNotificationId for this as we never want to have more than one
        // notification in the air of this type.
        m_notificationManager.notify(BlockNotificationId, m_blockMessageBuilder.build());
    }

    public Notification buildBackgroundNotification(Context context)
    {
        Notification.Builder builder = new Notification.Builder(context, m_bgSyncChannelId);
        builder.setContentTitle("Background Updator")
                .setSmallIcon(R.drawable.icon)
                .setContentText("Wallet sync")
                .setColor(0xA0F87) // flowee blue
                .setOngoing(true);
        return builder.build();
    }
}
