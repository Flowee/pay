/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flowee.pay;

import org.qtproject.qt.android.bindings.QtService;
import android.os.*;

public class PeriodicService extends QtService {
    // called from C++
    public void done() {
        stopSelf();
    }

    public boolean isPowerSaveMode()
    {
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        if (pm != null)
            return pm.isPowerSaveMode();
        return false;
    }
}
