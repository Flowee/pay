#!/bin/bash

TAG=$1
if test -z "$TAG"; then
    echo "Missing required argument 'TAG'"
    exit
fi
echo "Based on Qt version $TAG" >> /etc/versions
source /etc/profile
export NINJA_STATUS='[%u/%r/%f] '

function checkout (
    repo=$1
    (cd /usr/local/cache
    if ! test -d $repo.git; then
        git clone --bare https://code.qt.io/qt/$repo.git
    fi)
    (cd ~builduser
    if git clone -l /usr/local/cache/$repo.git -b $TAG
    then
        echo ".. OK"
    else
        echo "Calling exit"
        exit 1
    fi)
)

# The QtBase builds are different.
QTBASE_FLAGS="-no-widgets \
    -no-dbus \
    -no-feature-testlib \
    -no-feature-sql \
    -no-feature-xml \
    -no-feature-networkproxy \
    -no-feature-socks5 \
    -no-feature-brotli \
    -no-feature-dnslookup \
    -no-feature-topleveldomain \
    -no-feature-textmarkdownreader \
    -no-feature-textmarkdownwriter \
    -no-feature-textodfwriter"
# on qtbase flags:
# I checked and noticed that colornames are required (qml fails to load otherwise)
# the cssparser is also required for properly loading svgs.

checkout qtbase
cd ~builduser
patch -d qtbase -p1 < /usr/local/bin/qtbase-revert-qtranslator.patch
mkdir -p ~builduser/build/qtbase
cd ~builduser/build/qtbase
~builduser/qtbase/configure \
    -prefix /usr/local  \
    -no-openssl \
    -nomake examples \
    $QTBASE_FLAGS
cmake --build . --parallel
cmake --install .
rm -rf ~builduser/build/*

###  Android build
mkdir -p ~builduser/build/qtbase
cd ~builduser/build/qtbase
~builduser/qtbase/configure \
    -platform android-clang \
    -prefix /opt/android-qt6/ \
    -android-ndk /opt/android-ndk \
    -android-sdk /opt/android-sdk \
    -qt-host-path /usr/local \
    -android-abis arm64-v8a \
    -android-style-assets \
    -openssl-linked \
    $QTBASE_FLAGS \
    -- \
    -DOPENSSL_USE_STATIC_LIBS=ON \
    -DOPENSSL_ROOT_DIR=/opt/android-ssl
cmake --build . --parallel
cmake --install .
rm -rf ~builduser/build/*
rm -rf ~builduser/qtbase

# All the others.
for i in qtshadertools qtdeclarative qtsvg qtmultimedia
do
    checkout $i
    mkdir -p ~builduser/build/$i
    cd ~builduser/build/$i
    CONF=""
    if test "$i" = "qtdeclarative"; then
        CONF=" \
            -no-feature-quick-tableview \
            -no-feature-quick-treeview \
            -no-feature-qml-xmllistmodel \
            -no-feature-quick-designer \
            -no-feature-qml-network \
            -no-feature-qml-preview \
            -no-feature-qml-worker-script \
            -no-feature-quick-animatedimage \
            -no-feature-quicktemplates2-calendar \
            -no-feature-quickcontrols2-ios \
            -no-feature-quickcontrols2-macos \
            -no-feature-quickcontrols2-windows \
            -no-feature-quickcontrols2-fluentwinui3 \
            -no-feature-quickcontrols2-universal \
            -no-feature-quickcontrols2-imagine \
            -no-feature-quickcontrols2-fusion \
            -no-feature-qml-debug \
            -no-feature-qml-profiler \
            -no-feature-qml-ssl \
            -no-feature-qml-xml-http-request \
            -no-feature-quick-pathview \
            -no-feature-quick-pixmap-cache-threaded-download \
            -no-feature-qml-table-model \
        "
    fi
    /usr/local/bin/qt-configure-module ~builduser/$i $CONF
    cmake --build . --parallel
    cmake --install .
    cd ~builduser
    rm -rf build/*

    # Android
    mkdir -p ~builduser/build/$i
    cd ~builduser/build/$i
    /opt/android-qt6/bin/qt-configure-module ~builduser/$i $CONF
    cmake --build . --parallel && cmake --install .
    cd ~builduser
    rm -rf build/*
    rm -rf $i
done

