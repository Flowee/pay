#!/bin/bash
# this runs as root.

function makeAur(
    cd ~builduser
    pkg="$1"
    git clone https://aur.archlinux.org/$pkg.git
    cd "$pkg"
    commit=$2
    if test -n "$commit"
    then
        git checkout $commit
    fi
    chown builduser:builduser -R . .git
    for i in /usr/local/cache/$pkg*zst; do
        ln -s "$i" .
    done
    for i in /usr/local/cache/*zip; do
        if test -f $i; then
            ln -s "$i" .
        fi
    done
    su builduser -c makepkg
    find . -type f -name '*zst' -exec ln "{}" /usr/local/cache ';'
)


# r27
makeAur android-ndk 658ef36823525853a1338d51020320a9fb1b9cbd
# 35.0.2
makeAur android-sdk-platform-tools ac481561ad3ab25cd17a4a62571159176ab6f584
# r34.0.0-2
makeAur android-sdk-build-tools ce7b51fed9ef7e0db9ee681883204adde1ef3808
# 16.0
makeAur android-sdk-cmdline-tools-latest 91763061af0ee3d077246c18bb4b6fe55fc7c566
# 35_r01
makeAur android-platform 5506c16537f770278bcb694451800ceb07e92de8

pacman -U --noconfirm /usr/local/cache/*zst

