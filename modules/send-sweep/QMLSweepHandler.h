/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QMLSWEEPHANDLER_H
#define QMLSWEEPHANDLER_H


#include <FloweePay.h>
#include <utils/TransactionBuilder.h>
#include <TxInfoObject.h>
#include <utils/primitives/PrivateKey.h>
#include <utils/streaming/ConstBuffer.h>
#include "TransactionsFetcher.h"
#include <AccountInfo.h>

#include <memory>

class TransactionsFetcher;
class AccountInfo;

class QMLSweepHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString privKey READ privKey WRITE setPrivKey NOTIFY privKeyChanged FINAL)
    Q_PROPERTY(Error error READ error NOTIFY errorChanged FINAL)
    Q_PROPERTY(AccountInfo *account READ currentAccount WRITE setCurrentAccount NOTIFY currentAccountChanged)
    Q_PROPERTY(FloweePay::BroadcastStatus broadcastStatus  READ broadcastStatus NOTIFY broadcastStatusChanged)
    Q_PROPERTY(QString sweepAddress READ sweepAddress WRITE setSweepAddress NOTIFY sweepAddressChanged FINAL)
    Q_PROPERTY(QString targetAddress READ targetAddress WRITE setTargetAddress NOTIFY targetAddressChanged FINAL)
    Q_PROPERTY(bool prepared READ prepared WRITE setPrepared NOTIFY preparedChanged FINAL)
    Q_PROPERTY(double sweepTotal READ sweepTotal NOTIFY sweepTotalChanged FINAL)
    Q_PROPERTY(int numTokensFound READ numTokensFound NOTIFY numTokensFoundChanged FINAL)
    Q_PROPERTY(int numOutputsFound READ numOutputsFound NOTIFY numOutputsFoundChanged FINAL)
    /// progress in 0 ... 1000
    Q_PROPERTY(int downloadProgress READ downloadProgress NOTIFY downloadProgressChanged FINAL)
public:
    QMLSweepHandler(QObject *parent = nullptr);

    enum Error {
        NoError,
        InvalidInput,
        NoBackendFound, // no indexer services
        FileError,
        DataInconsistency // specifically the data we got from the indexer.
    };
    Q_ENUM(Error)

    QString privKey() const;
    void setPrivKey(const QString &newPrivKey);

    Error error() const;
    void setError(Error newError);

    AccountInfo *currentAccount() const;
    void setCurrentAccount(AccountInfo *account);

    Q_INVOKABLE void markUserApproved();

    FloweePay::BroadcastStatus broadcastStatus() const;

    QString targetAddress() const;
    void setTargetAddress(const QString &newTargetAddress);

    bool prepared() const;
    void setPrepared(bool newPrepared);

    double sweepTotal() const;
    void setSweepTotal(double newSweepTotal);

    int numTokensFound() const;
    void setNumTokensFound(int newNumTokensFound);

    int numOutputsFound() const;
    void setNumOutputsFound(int newNumOutputsFound);

    QString sweepAddress() const;
    void setSweepAddress(const QString &newSweepAddress);

    int downloadProgress() const;
    void setDownloadProgress(int newDownloadProgress);

signals:
    void privKeyChanged();
    void errorChanged();
    void currentAccountChanged();
    void broadcastStatusChanged();
    void targetAddressChanged();
    void preparedChanged();
    void sweepTotalChanged();
    void numTokensFoundChanged();
    void numOutputsFoundChanged();
    void sweepAddressChanged();

    void downloadProgressChanged();

private slots:
    void start();
    void startTxBuilder(const QList<TransactionsFetcher::Output> &result);

private:
    AccountInfo *m_account = nullptr;
    QString m_privKey;
    QString m_sweepAddress;
    QString m_targetAddress;
    Streaming::ConstBuffer m_addressHash;
    Error m_error = NoError;
    PrivateKey m_key;
    TransactionBuilder m_builder;
    TransactionsFetcher *m_fetcher;
    std::shared_ptr<TxInfoObject> m_infoObject;
    bool m_prepared = false; // tx is prepared
    bool m_txBroadcastStarted = false;
    double m_sweepTotal = 0;

    int m_numTokensFound = 0;
    int m_numOutputsFound = 0;
    int m_downloadProgress = 0; // from 0 to 1000 percent.
};

#endif
