/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRANSACTIONSFETCHER_H
#define TRANSACTIONSFETCHER_H

#include <ElectronXClient.h>
#include <uint256.h>
#include <streaming/ConstBuffer.h>

class TransactionsFetcher : public ElectronXClient
{
    Q_OBJECT
public:
    explicit TransactionsFetcher(QObject *parent = nullptr);

    struct Output {
        QString txid;
        int outIndex = -1;
        QString filename;
    };

    void start(const Streaming::ConstBuffer &scripthash);

    int numTokensFound() const {
        return m_tokensFound;
    }
    int numOutputsFound() const {
        return m_coinsFound;
    }

signals:
    void searchComplete(); // the numCoins/numTokens have been determined
    void finished(const QList<TransactionsFetcher::Output> &result);
    void fetched(int utxoCount);

protected:
    void handshakeCompleted() override;
    void handleResponse(int id, const QJsonObject &data) override;

private:
    void checkAllAvailable();
    Streaming::ConstBuffer m_scriptHash;

    int m_coinsFound = 0;
    int m_tokensFound = 0;
    int m_utxosDownloaded = 0;
    int64_t m_balance = 0;

    QList<Output> m_outputs;
};

#endif
