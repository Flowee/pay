/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import "../Flowee" as Flowee
import "../mobile" as Mobile;
import Flowee.org.pay;
import Flowee.org.pay.SendSweep;

Mobile.Page {
    id: root
    headerText: qsTr("Sweep coins")

    property alias secret: sweeper.privKey

    Item { // data
        SweepHandler {
            id: sweeper
            account: pay.portfolio.current
        }
    }

    Column {
        width: parent.width
        spacing: 6

        Flowee.Label {
            text: qsTr("Sweeping from address:")
            font.bold: true
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            wrapMode: Text.Wrap
        }
        Flowee.Label {
            text: sweeper.sweepAddress
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            fontSizeMode: Text.HorizontalFit
        }
        Flowee.Label {
            id: coinsLabel
            visible: sweeper.numOutputsFound > 0 || sweeper.prepared
            text: qsTr("Found %1 coins on address.", "this is a simple number", sweeper.numOutputsFound).arg(sweeper.numOutputsFound)
        }
        Flowee.Label {
            visible: sweeper.numTokensFound > 0
            text: qsTr("Ignoring %1 tokens.", "Number of CashTokens", sweeper.numTokensFound).arg(sweeper.numTokensFound)
        }

        Flowee.Progressbar {
            width: parent.width
            visible: !sweeper.prepared && sweeper.error === SweepHandler.NoError
            progress: sweeper.downloadProgress / 1000
        }

        Item {
            id: busyIndicator
            visible: !sweeper.prepared && sweeper.error === SweepHandler.NoError
            width: 105
            height: visible ? 105  : 1
            anchors.horizontalCenter: parent.horizontalCenter
            RotationAnimation on rotation {
                loops: Animation.Infinite
                from: 0
                to: 360
                duration: 6000
                running: busyIndicator.visible
            }

            Repeater {
                model: 6
                Item {
                    width: 105
                    height: 35
                    y: 35
                    Rectangle {
                        color: mainWindow.floweeGreen
                        width: 30
                        height: 30
                        x: 70
                        radius: 15
                    }
                    rotation: 360 / 6 * index
                }
            }
        }

        Rectangle {
            color: mainWindow.errorRedBg
            width: parent.width
            height: sweepErrorLabel.text === "" ? 0 : sweepErrorLabel.height + 20
            radius: 6
            Flowee.Label {
                id: sweepErrorLabel
                width: parent.width - 20
                wrapMode: Text.Wrap
                horizontalAlignment: Qt.AlignHCenter
                anchors.centerIn: parent
                text: {
                    var err = sweeper.error;
                    if (err === SweepHandler.InvalidInput)
                        return qsTr("Failed to understand QR");
                    if (err === SweepHandler.NoBackendFound)
                        return "No indexing servers found";
                    if (err === SweepHandler.FileError)
                        return "File not found error"; // not translated as this should never happen.
                    if (err === SweepHandler.DataInconsistency)
                        return qsTr("Indexer results invalid. Please try again.");

                    if (err === SweepHandler.NoError)
                        return "";
                }
            }
        }

        Flowee.BitcoinAmountLabel {
            font.pixelSize: coinsLabel.font.pixelSize * 1.2
            visible: sweeper.prepared
            value: sweeper.sweepTotal
            anchors.right: parent.right
        }
    }

    Mobile.AccountSelectorWidget {
        id: walletSelector
        visible: !portfolio.singleAccountSetup
        y: 320
        onSelectedAccountChanged: sweeper.account = selectedAccount
    }
    Flowee.Label {
        visible: walletSelector.visible
        anchors.bottom: walletSelector.top
        text: qsTr("Transfer to:")
    }

    Mobile.SlideToApprove {
        id: slideToApprove
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        width: parent.width
        enabled: sweeper.prepared && sweeper.numOutputsFound > 0
        visible: !sweeper.account.needsPinToOpen
        onActivated: {
             root.hideHeader = true;
             broadcastFeedback.start();
             sweeper.markUserApproved();
        }
    }

    Flowee.BroadcastFeedback {
        id: broadcastFeedback

        status: sweeper.broadcastStatus
        bitcoinAmount: sweeper.sweepTotal
        fiatPrice: Fiat.price
        targetAddress: sweeper.targetAddress
        showPersonalNote: false

        onCloseButtonPressed:  {
            var mainView = thePile.get(0);
            mainView.currentIndex = 0; // go to the 'main' tab.
            thePile.pop();
        }
    }
}
