/*
 * This file is part of the Flowee project
 * Copyright (C) 2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SendSweepModuleInfo.h"
#include "QMLSweepHandler.h"

#include <QQmlEngine> // for the qmlRegisterType

ModuleInfo * SendSweepModuleInfo::build()
{
    qmlRegisterType<QMLSweepHandler>("Flowee.org.pay.SendSweep", 1, 0, "SweepHandler");
    return new SendSweepModuleInfo();
}

SendSweepModuleInfo::SendSweepModuleInfo()
    : m_actionTabItem(new ModuleSection(ModuleSection::OtherSectionTypeDefault, this))
{
    setId("sendSweepModule");
    setTitle(tr("Claim Cash Stamp"));
    setDescription(tr("A QR code with a CashStamp can be taken to transfer the money to your wallet."));

    m_actionTabItem->setText(tr("Claim a Cash Stamp"));
    m_actionTabItem->setStartQMLFile("qrc:/send-sweep/StartScan.qml");
    addSection(m_actionTabItem);

    // this one is used to have a full screen page at application start,
    // but we only enable this when the 'bch-wif' scheme was used to start Flowee Pay
    auto introScreenSection = new ModuleSection(ModuleSection::CustomSectionType, this);
    introScreenSection->setStartQMLFile("qrc:/send-sweep/SendPage.qml");
    introScreenSection->setSectionId("main");
    addSection(introScreenSection);
}

void SendSweepModuleInfo::setEnabled(bool on)
{
    m_enabled = on;
    m_actionTabItem->setEnabled(on);
    emit enabledChanged();
}
