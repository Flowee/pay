/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Controls as QQC2
import "../mobile" as Mobile;
import Flowee.org.pay;

Mobile.Page {
    Item { // data
        QRScanner {
            id: scanner
            autostart: true
            helpText: qsTr("Scan QR (WIF) to find funds", "Please note that WIF and QR are names")
            onFinished: {
                var rc = scanResult
                if (rc === "" // scanning interrupted
                        || scanType !== QRScanner.PrivateKeyWIF) {
                    thePile.pop();
                    return;
                }
                thePile.replace("SendPage.qml", { "secret": rc },
                        QQC2.StackView.Immediate);
            }
        }
    }
}
