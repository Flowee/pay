/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BlocksModuleInfo.h"
#include "BlockHeadersChecker.h"
#include "BlocksDataProvider.h"

#include <QQmlEngine> // for the qmlRegisterType

ModuleInfo * BlocksModuleInfo::build()
{
    ModuleInfo *info = new ModuleInfo();
    info->setId("blocks");
    info->setTitle(tr("Blockchain History"));
    info->setDescription(tr("Investigate the historical blockchain. See blocks as they get mined, or download more history."));
    info->setIconSource("qrc:/blocks/blocks.svg");

/*  Not usable yet.
    auto menuSection = new ModuleSection(ModuleSection::MainMenuItem, info);
    menuSection->setText(tr("Blocks"));
    menuSection->setSubtext(tr("History of our chain"));
    menuSection->setStartQMLFile("qrc:/blocks/Configuration.qml");
    info->addSection(menuSection);
*/

    // The custom section that is used by the main app to check if
    // the blockchain currently available is should be expanded.
    auto checker = new ModuleSection(ModuleSection::CustomSectionType, info);
    checker->setSectionId("checker");
    checker->setStartQMLFile("qrc:/blocks/DownloadChecker.qml");
    info->addSection(checker);

    qmlRegisterType<BlockHeadersChecker>("Flowee.org.pay.blocks", 1, 0, "Checker");
    qmlRegisterType<BlocksDataProvider>("Flowee.org.pay.blocks", 1, 0, "BlocksData");

    return info;
}
