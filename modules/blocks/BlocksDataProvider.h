/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BLOCKSDATAPROVIDER_H
#define BLOCKSDATAPROVIDER_H

#include <QObject>
#include <p2p/Blockchain.h>

class DataSource : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool fromStaticFile READ fromStaticFile CONSTANT FINAL)
    Q_PROPERTY(int startBlock READ startBlock CONSTANT FINAL)
    Q_PROPERTY(int endBlock READ endBlock CONSTANT FINAL)
public:
    DataSource(const Blockchain::DataSource &ds, QObject *parent = nullptr);

    bool fromStaticFile() const;
    int startBlock() const;
    int endBlock() const;

private:
    bool m_fromStaticFile;
    int m_startBlock;
    int m_endBlock;
};

class BlocksDataProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<DataSource *> dataSources READ dataSources CONSTANT FINAL)
public:
    explicit BlocksDataProvider(QObject *parent = nullptr);

    QList<DataSource *> dataSources() const;

private:
    QList<DataSource*> m_dataSources;
};

#endif
