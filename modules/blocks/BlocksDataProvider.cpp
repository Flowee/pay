/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BlocksDataProvider.h"

#include <FloweePay.h>


DataSource::DataSource(const Blockchain::DataSource &ds, QObject *parent)
    : QObject(parent),
    m_fromStaticFile(ds.source == Blockchain::StaticHeadersSource),
    m_startBlock(ds.startBlock),
    m_endBlock(ds.endBlock)
{
}

bool DataSource::fromStaticFile() const
{
    return m_fromStaticFile;
}

int DataSource::startBlock() const
{
    return m_startBlock;
}

int DataSource::endBlock() const
{
    return m_endBlock;
}


// -----------------------------------------------------------

BlocksDataProvider::BlocksDataProvider(QObject *parent)
    : QObject{parent}
{
    auto sources = FloweePay::instance()->p2pNet()->blockchain().dataSources();
    for (const auto &s : sources) {
        m_dataSources.append(new DataSource(s, this));
    }
}


QList<DataSource *> BlocksDataProvider::dataSources() const
{
    return m_dataSources;
}
