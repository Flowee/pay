/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BLOCKHEADERSCHECKER_H
#define BLOCKHEADERSCHECKER_H

#include <QFile>
#include <QObject>

class QNetworkReply;

class BlockHeadersChecker : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int wantedHeight READ wantedHeight WRITE setWantedHeight NOTIFY wantedHeightChanged FINAL)
    Q_PROPERTY(int totalDownload READ totalDownload NOTIFY totalDownloadChanged FINAL)
    Q_PROPERTY(int bytesDownloaded READ bytesDownloaded NOTIFY bytesDownloadedChanged FINAL)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged FINAL)
public:
    BlockHeadersChecker(QObject *parent = nullptr);

    enum Status {
        Unset,
        NoDownloadNeeded,
        Checking,
        Downloading,
        Verifying,
        Success,

        DiskFailure,
        NetworkFailure
    };
    Q_ENUM(Status)

    int wantedHeight() const;
    void setWantedHeight(int h);

    /// the download progress in bytes
    void setBytesDownloaded(int count);
    int bytesDownloaded();

    /// the download target in bytes
    int totalDownload() const;
    void setTotalDownload(int newTotalDownload);

    void setStatus(Status newStatus);
    Status status() const;

    Q_INVOKABLE void restart();

signals:
    void wantedHeightChanged();
    void bytesDownloadedChanged();
    void totalDownloadChanged();
    void statusChanged();
    // helper signal for threading.
    void finishUp(Status status);

private slots:
    void startChecking();
    void headerReturned();
    void downloadFinished();
    void onBytesDownloaded();

private:
    void processNewHeaders();

    int m_wantedHeight = 0;
    int m_checkpoint = 0; // start download at checkpoint-height
    int m_downloadTo = 0; // download to this point (we have the rest)
    int m_totalDownload = 0;
    int m_bytesDownloaded = 0; // download progress

    QNetworkReply *m_headerReply = nullptr;
    QNetworkReply *m_downloadReply = nullptr;
    QFile *m_newHeaders = nullptr;
    Status m_status = Unset;
};

#endif
