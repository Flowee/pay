/*
 * This file is part of the Flowee project
 * Copyright (C) 2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee
import "../mobile";
import Flowee.org.pay.blocks as Blocks;

Page {
    headerText: qsTr("Blocks")

    Item {
        Blocks.BlocksData {
            id: blocksData
        }
    }

    Column {
        spacing: 6

        Flowee.Label {
            text: "Headers on this device"
        }

        Repeater {
            model: blocksData.dataSources
            Rectangle {
                width: 50
                height: 50
                color: modelData.fromStaticFile ? "#66b0ff" : "#1231a3"

                Flowee.Label {
                    anchors.left: parent.right
                    anchors.leftMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    text: "from: " + modelData.startBlock + " - " + modelData.endBlock
                }
            }
        }
    }
}
