/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick;
import QtQuick.Controls as QQC2;
import "../Flowee" as Flowee;
import "../mobile";
import Flowee.org.pay;


Page {
    id: destinationEditPage
    headerText: qsTr("Edit Destination")
    
    property QtObject sendAllAction: QQC2.Action {
        checkable: true
        checked: paymentDetail.maxSelected
        text: qsTr("Send All", "all money in wallet")
        onTriggered: paymentDetail.maxSelected = checked
    }
    
    Flowee.Label {
        id: destinationLabel
        text: qsTr("Bitcoin Cash Address") + ":"
    }
    
    Flowee.MultilineTextField {
        id: destination
        anchors.top: destinationLabel.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.right: parent.right
        height: Math.max(destinationLabel.height * 2.3, implicitHeight)
        focus: enabled
        property var addressType: Pay.identifyString(totalText);
        text: paymentDetail.address
        nextFocusTarget: priceInput
        enabled: paymentDetail.editable
        onTotalTextChanged: {
            paymentDetail.address = totalText
            addressInfo.createInfo();
        }
        color: {
            if (!activeFocus && totalText !== "" && !addressInfo.addressOk)
                return mainWindow.errorRed
            return palette.windowText
        }
    }
    
    Flowee.LabelWithClipboard {
        id: nativeLabel
        width: parent.width
        anchors.top: destination.bottom
        anchors.topMargin: 10
        
        // only show if its substantially different
        visible: text!== "" && text !== destination.text && destination.text !== paymentDetail.formattedTarget
        text: paymentDetail.niceAddress
        clipboardText: paymentDetail.formattedTarget // the one WITH bitcoincash:
        font.italic: true
        menuText: qsTr("Copy Address")
    }
    Flowee.AddressInfoWidget {
        id: addressInfo
        anchors.top: nativeLabel.visible ? nativeLabel.bottom : destination.bottom
        width: parent.width
        addressType: destination.addressType
    }
    PriceInputWidget {
        id: priceInput
        width: parent.width
        anchors.top: addressInfo.bottom
        paymentBackend: paymentDetail
        fiatFollowsSats: paymentDetail.fiatFollows
        onFiatFollowsSatsChanged: paymentDetail.fiatFollows = fiatFollowsSats
    }
    
    AccountSelectorWidget {
        id: walletNameBackground
        anchors.bottom: numericKeyboard.top
        anchors.bottomMargin: 10
        stickyAccount: true
        onSelectedAccountChanged: payment.account = selectedAccount
        
        balanceActions: [ sendAllAction ]
    }
    
    NumericKeyboardWidget {
        id: numericKeyboard
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        width: parent.width
        enabled: !paymentDetail.maxSelected
        dataInput: priceInput
    }
    
    Rectangle {
        color: mainWindow.errorRedBg
        radius: 15
        width: parent.width
        height: warningColumn.height + 20
        anchors.top: nativeLabel.bottom
        // BTC address entered warning.
        visible: (destination.addressType === Wallet.LegacySH
                  || destination.addressType === Wallet.LegacyPKH)
                 && paymentDetail.forceLegacyOk === false;
        
        Column {
            id: warningColumn
            x: 10
            y: 10
            width: parent.width - 20
            spacing: 10
            Flowee.Label {
                font.bold: true
                font.pixelSize: warning.font.pixelSize * 1.2
                text: qsTr("Warning")
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Flowee.Label {
                id: warning
                width: parent.width
                color: "white"
                text: qsTr("This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
            Item {
                width: parent.width
                height: okButton.height
                QQC2.Button {
                    id: okButton
                    anchors.right: parent.right
                    text: qsTr("I am certain")
                    onClicked: paymentDetail.forceLegacyOk = true
                }
            }
        }
    }
    Flowee.TextPasteDecorator {
        buddy: destination
        clipboardTypes: ClipboardHelper.Addresses + ClipboardHelper.LegacyAddresses
        y: destination.y + destination.height - height / 3
    }

    Flowee.ImageButton {
        source: "qrc:/qr-code" + (Pay.useDarkSkin ? "-light.svg" : ".svg");
        iconSize: 28
        anchors.right: destination.right
        anchors.rightMargin: 10
        anchors.top: destination.top
        anchors.topMargin: 10
        visible: destination.totalText.length < 5
        onClicked: scanner.start();
    }
    Item {
        QRScanner {
            id: scanner
            isPayment: true // well, thats the main intent anyway
            onFinished: {
                if (scanType === QRScanner.PaymentDetails) {
                    destination.text = scanResult;
                }
            }
        }
    }
}
