/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SocialFeedModuleInfo.h"
#include "FeedDataProvider.h"

#include <QQmlEngine> // for the qmlRegisterType

ModuleInfo * SocialFeedModuleInfo::build()
{
    ModuleInfo *info = new ModuleInfo();
    info->setId("socialFeedModule");
    info->setTitle(tr("Help and Learning"));
    info->setDescription(tr("Want to see the experts show how to use Bitcoin Cash "
                "with Flowee Pay? Find all you want via this library of videos."));
    info->setIconSource("qrc:/social-feed/social-feed.svg");
    info->setPriority(20);
    info->setButtonId(13427);

    auto last = new ModuleSection(ModuleSection::OtherSectionType, info);
    last->setText(info->title());
    last->setStartQMLFile("qrc:/social-feed/Listing.qml");
    info->addSection(last);

    // notice that a dash is not allowed in the name
    qmlRegisterType<FeedDataProvider>("Flowee.org.pay.socialfeed", 1, 0, "Feeds");

    return info;
}
