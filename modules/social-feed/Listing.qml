/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick
import QtQuick.Layouts
import "../Flowee" as Flowee
import "../mobile";
import Flowee.org.pay.socialfeed;

Page {
    headerText: qsTr("Videos")
    id: root

    Item { // data
        Feeds {
            id: feeds
        }
    }

    ListView {
        anchors.fill: parent
        model: feeds.items

        delegate: Item {
            width: ListView.view.width
            height: content.height + 10

            Rectangle {
                color: (index % 2 === 0) ? palette.base : palette.alternateBase
                width: parent.width + 20
                x: -10
                height: parent.height
            }

            Column {
                id: content
                width: parent.width
                spacing: 6

                Flowee.Label {
                    text: modelData.title
                    font.pixelSize: root.font.pixelSize * 1.2
                    width: parent.width
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Flowee.Label {
                    text: Pay.formatDate(modelData.date)
                    font.pixelSize: root.font.pixelSize * 0.8
                }
                Flowee.Label {
                    text: modelData.text
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
                Item {
                    width: parent.width
                    height: row.height
                    Row {
                        id: row
                        y: -6
                        anchors.right: parent.right
                        height: buttonText.height
                        spacing: 10
                        Flowee.Label {
                            id: buttonText
                            text: qsTr("Run time:") + " " + modelData.videoLength;
                        }
                        Image {
                            id: icon
                            width: 12
                            height: 8
                            source: Pay.useDarkSkin ? "qrc:/smallArrow-light.svg" : "qrc:/smallArrow.svg";
                            rotation: 270
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally(modelData.url)
            }
        }
    }
}
