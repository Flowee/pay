/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FEEDDATAPROVIDER_H
#define FEEDDATAPROVIDER_H

#include <QDateTime>
#include <QNetworkReply>
#include <QObject>
#include <QList>

class ListItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date CONSTANT FINAL)
    Q_PROPERTY(QString videoLength READ videoLength CONSTANT FINAL)
    Q_PROPERTY(QString text READ text CONSTANT FINAL)
    Q_PROPERTY(QString title READ title CONSTANT FINAL)
    Q_PROPERTY(QString url READ url CONSTANT FINAL)
public:
    ListItem(QObject *parent = nullptr);

    QDate date() const;
    QString videoLength() const;
    QString text() const;
    QString title() const;
    QString url() const;

    QDate m_date;
    QString m_videoLength;
    QString m_text;
    QString m_title;
    QString m_url;
};

class FeedDataProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<ListItem*> items READ listItems NOTIFY listItemsChanged FINAL)
public:
    explicit FeedDataProvider(QObject *parent = nullptr);

    QList<ListItem*> listItems() const;

signals:
    void listItemsChanged();

private slots:
    void start();
    void downloadFinished();
    void errored(QNetworkReply::NetworkError err);
    void sslErrors(const QList<QSslError> &errors);

private:
    void readFeed();

    QNetworkReply *m_reply = nullptr;
    QDateTime m_lastCheck;
    QList<ListItem*> m_listItems;
};

#endif
