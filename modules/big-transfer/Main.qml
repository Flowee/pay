/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick;
import QtQuick.Controls as QQC2;
import QtQuick.Layouts;
import "../mobile" as Mobile;
import "../Flowee" as Flowee;
import Flowee.org.pay;
import Flowee.org.pay.bigtransfer;

Mobile.Page {
    id: root
    headerText: qsTr("Wallet to Wallet")
    property alias initialWallet: fromWalletSelector.startingAccount

    ColumnLayout {
        width: parent.width
        spacing: 10

        Flowee.Label {
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("Select two wallets to transfer funds simply, using anonimity preserving transactions.")
        }

        Mobile.PageTitledBox {
            title: qsTr("Spending Wallet")

            // TODO Popup with just the name
            Mobile.AccountSelectorWidget {
                id: fromWalletSelector
                onSelectedAccountChanged: transferManager.fromAccount = selectedAccount;
            }
            GridLayout {
                columns: 2
                columnSpacing: 10
                rowSpacing: 10
                Flowee.Label {
                    text: qsTr("Addresses") + ":"
                }
                Flowee.Label {
                    text: transferManager.addressCount
                }
                Flowee.Label {
                    text: qsTr("Coins") + ":"
                }
                Flowee.Label {
                    text: transferManager.coinCount
                }
            }
        }
        Mobile.PageTitledBox {
            title: qsTr("Destination Wallet")
            // TODO Popup with just the name
            // TODO add an entry "create new HD wallet"
            Mobile.AccountSelectorWidget {
                startingAccount: null
                onSelectedAccountChanged: transferManager.toAccount = selectedAccount;
            }
        }

        // button to start.
        Flowee.Button {
            Layout.alignment: Qt.AlignRight
            text: qsTr("Prepare...")

            enabled: transferManager.inputsOk
            onClicked: {
                transferManager.prepare();
                thePile.push("ShowPrepared.qml", { "transferManager" : transferManager } );
            }
        }
    }

    Item {
        // data
        TransferManager {
            id: transferManager
        }
    }
}
