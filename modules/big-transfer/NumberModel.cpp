/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NumberModel.h"

/*
 * This number model has some ranges.
 * 1 - 39 as number  (count = 39)
 * 40 - 100 every 10. (count = 7)
 * 130 - 400  every 30 (count = 9)
 */

NumberModel::NumberModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


int NumberModel::rowCount(const QModelIndex &) const
{
    return 56;
}

QVariant NumberModel::data(const QModelIndex &index_, int role) const
{
    assert(role == Number);
    if (role != Number)
        return QVariant();
    const int index = index_.row();
    int rc = index;
    if (index < 40)
        rc = index + 1;
    else if (index < 46)
        rc = (index - 39) * 10 + 40;
    else
        rc = (index - 46) * 30 + 130;

    return QVariant::fromValue<int>(rc);
}

QHash<int, QByteArray> NumberModel::roleNames() const
{
    QHash<int, QByteArray> answer;
    answer[Number] = "number";
    return answer;
}
