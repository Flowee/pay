/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BigTransferModuleInfo.h"
#include "QMLTransferManager.h"

#include <QQmlEngine> // for the qmlRegisterType

ModuleInfo * BigTransferModuleInfo::build()
{
    std::srand(time(nullptr));
    qmlRegisterType<QMLTransferManager>("Flowee.org.pay.bigtransfer", 1, 0, "TransferManager");
    return new BigTransferModuleInfo();
}

BigTransferModuleInfo::BigTransferModuleInfo()
{
    setId("bigTransfer");
    setTitle(tr("Wallet to Wallet"));
    setDescription(tr("Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses."));
    setPriority(5);

    ModuleSection *sendTab = new ModuleSection(ModuleSection::SendMethod, this);
    sendTab->setText(tr("Wallet to Wallet"));
    sendTab->setSubtext(tr("Move funds to another wallet"));
    sendTab->setStartQMLFile("qrc:/bigtransfer/Main.qml");
    addSection(sendTab);

    ModuleSection *app = new ModuleSection(ModuleSection::OtherSectionType, this);
    app->setText(tr("Wallet to Wallet"));
    app->setSubtext(sendTab->subtext());
    app->setStartQMLFile(sendTab->startQMLFile());
    addSection(app);
}
