/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRANSFERMANAGER_H
#define TRANSFERMANAGER_H

#include "NumberModel.h"

#include <FloweePay.h>
#include <AccountInfo.h>

#include <utils/primitives/PublicKey.h>

class PreviewTx;

class QMLTransferManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(AccountInfo *fromAccount READ fromAccount WRITE setFromAccount NOTIFY fromAccountChanged FINAL)
    Q_PROPERTY(AccountInfo *toAccount READ toAccount WRITE setToAccount NOTIFY toAccountChanged FINAL)
    Q_PROPERTY(QList<QObject *> transactions READ transactions NOTIFY transactionsChanged FINAL)
    Q_PROPERTY(int addressCount READ addressCount NOTIFY addressCountChanged FINAL)
    Q_PROPERTY(int coinCount READ coinCount NOTIFY coinCountChanged FINAL)
    Q_PROPERTY(int unsentTxCount READ unsentTxCount NOTIFY  unsentTxCountChanged FINAL)
    Q_PROPERTY(bool inputsOk READ inputsOk NOTIFY inputsOkChanged FINAL)
    Q_PROPERTY(NumberModel* outputModel READ outputModel FINAL)
public:
    QMLTransferManager(QObject *parent = nullptr);

    AccountInfo *fromAccount() const;
    void setFromAccount(AccountInfo *newFromAccount);

    AccountInfo *toAccount() const;
    void setToAccount(AccountInfo *newToAccount);

    // num of addresss on the 'from' account
    int addressCount() const;
    // total num of coins on the 'from' account
    int coinCount() const;

    /// how many items in the transactions, which have not yet been approved.
    int unsentTxCount() const;

    bool inputsOk() const;

    Q_INVOKABLE void prepare();
    Q_INVOKABLE void send(QObject *previewTx);
    Q_INVOKABLE void sendAll();

    QList<QObject *> transactions() const;

    NumberModel *outputModel();

signals:
    void fromAccountChanged();
    void toAccountChanged();
    void addressCountChanged();
    void coinCountChanged();
    void inputsOkChanged();
    void transactionsChanged();
    void unsentTxCountChanged();

private:
    void setAddressCount(int newAddressCount);
    void setCoinCount(int newCoinCount);
    void freePreparedTransactions();
    Tx createTx(PreviewTx *tx);

    AccountInfo *m_fromAccount = nullptr;
    AccountInfo *m_toAccount = nullptr;
    int m_addressCount = 0;
    int m_coinCount = 0;
    bool m_prepareRunning = false;
    QList<QObject*> m_transactions; // PreviewTx instances
    NumberModel *m_model = nullptr;
};


class Address : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString address READ address CONSTANT FINAL)
    Q_PROPERTY(QString cloakedAddress READ cloakedAddress CONSTANT FINAL)
public:
    explicit Address(const QString &address, const QString &cloaked, QObject *parent);

    QString address() const;
    QString cloakedAddress() const;

private:
    QString m_address;
    QString m_cloaked;
};


class PreviewTx : public QObject {
    Q_OBJECT
    Q_PROPERTY(int64_t value READ value CONSTANT FINAL)
    Q_PROPERTY(int inputCount READ inputCount CONSTANT FINAL)
    Q_PROPERTY(QObject* from READ from CONSTANT FINAL)
    Q_PROPERTY(bool sent READ sent NOTIFY sentChanged FINAL)
    Q_PROPERTY(FloweePay::BroadcastStatus broadcastStatus READ broadcastStatus NOTIFY broadcastStatusChanged FINAL)
    Q_PROPERTY(int outputCount READ outputCount WRITE setOutputCount NOTIFY outputCountChanged FINAL)
public:
    PreviewTx(QObject *parent = nullptr);

    bool sent() const;

    int64_t value() const;
    QObject *from() const;
    int inputCount() const;

    int64_t m_value = 0;
    Address *m_from = nullptr;
    bool m_sent = false;

    std::vector<Wallet::OutputRef> m_utxos;

    void setFinalTx(const std::shared_ptr<TxInfoObject> &finalTx);

    FloweePay::BroadcastStatus broadcastStatus() const;

    int outputCount() const;
    void setOutputCount(int c);

signals:
    void sentChanged();
    void broadcastStatusChanged();
    void outputCountChanged();

private:
    // the one we created
    std::shared_ptr<TxInfoObject> m_finalTx;
    int m_outputCount = 1;
};

#endif
