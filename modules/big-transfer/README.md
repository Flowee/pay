This module provides a screen that allows a wallet to have most or all
of its content transferred to another wallet, without giving up anonimity.

If you have a large number of coins in a wallet then the normal "send all"
method will simply merge all coins into one transaction. The downside of
creating one transaction is that it indicates to anyone checking the
blockchain that all those addresses are owned by the same person. Thereby
giving up any anonimity those addresses would have provided.

This module instead creates a transaction per address that is in use in
the old wallet and picks a fresh address from the target wallet in order
to avoid the obvious joining that would give away our anonimity.


