/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TestFiatUtils.h"
#include <PriceDataProvider.h>

//#include <QStandardPaths>
//#include <QDir>
//#include <QCoreApplication>
#include <qtest.h>
#include <qtestcase.h>

class MockFiat : public PriceDataProvider
{
public:
    MockFiat() : PriceDataProvider() {
        m_thousandSeparator = ',';
        m_decimalPoint = '.';
    }
    void setGroupSeparator(QChar k) {
        m_thousandSeparator = k;
    }
    void setDecimalPoint(QChar k) {
        m_decimalPoint = k;
    }
};

void TestFiatUtils::testFormattedPrice()
{
    MockFiat data;
    data.setGroupSeparator('{');
    data.setDecimalPoint('}');
    QCOMPARE(data.formattedPrice(1), "0}01");
    QCOMPARE(data.formattedPrice(8172412), "81{724}12");
    QCOMPARE(data.formattedPrice(100), "1}00");
    QCOMPARE(data.formattedPrice(109), "1}09");
    data.setGroupSeparator(QChar(8217)); // what de_ch uses.
    QCOMPARE(data.formattedPrice(999109), "9\u2019991}09");
    data.setDecimalPoint(QChar(8364)); // what de_ch uses.
    QCOMPARE(data.formattedPrice(999109), "9\u2019991\u20ac09");
}

QTEST_MAIN(TestFiatUtils)
