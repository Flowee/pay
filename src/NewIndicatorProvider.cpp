/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NewIndicatorProvider.h"

#include <QTimer>
#include <QSettings>

constexpr const char *STARTDATE_KEY = "first-start";
constexpr const char *SEEN = "seen";

namespace {
// these enums are reused by the actual QML pages. We don't export
// them or anything, so they are just random numbers to the reader...
enum Pages {
    BackupPage = 92387,
    WalletsPage = 32948,
    ModulesPage = 73573,
    ExploreModulesPage = 95231,
    HelpAndLearningModule = 13427
};
}

NewIndicatorProvider::NewIndicatorProvider(QObject *parent)
    : QObject(parent)
{
    m_pages.insert({WalletsPage, NewForEveryone});
    m_pages.insert({BackupPage, NewForEveryone});
    m_pages.insert({ModulesPage, NewSince202502});
    m_pages.insert({ExploreModulesPage, NewSince202502});
    m_pages.insert({HelpAndLearningModule, NewSince202502});

    load();
    if (m_appInstallDate == 0) {
        // update this one when a new enum is introduced
        m_appInstallDate = NewSince202502;
        m_dirty = true;
    }
}

NewIndicatorProvider::~NewIndicatorProvider()
{
    save();
}

void NewIndicatorProvider::load()
{
    assert(m_seenPages.empty());
    QSettings appConfig;
    appConfig.beginGroup("NewIndicator");
    m_appInstallDate = appConfig.value(STARTDATE_KEY, 0).toInt();
    auto seen = appConfig.value(SEEN, QString()).toString();
    for (const auto &sid : seen.split(' ')) {
        bool ok;
        int id = sid.toInt(&ok);
        if (ok)
            m_seenPages.insert(id);
    }
}

void NewIndicatorProvider::save()
{
    if (!m_dirty)
        return;

    QSettings appConfig;
    appConfig.beginGroup("NewIndicator");
    appConfig.setValue(STARTDATE_KEY, m_appInstallDate);
    QStringList seenList;
    for (auto i = m_seenPages.begin(); i != m_seenPages.end(); ++i) {
        seenList.append(QString::number(*i));
    }
    appConfig.setValue(SEEN, seenList.join(' '));
    m_dirty = false;
}

bool NewIndicatorProvider::isNew(int id) const
{
    auto iter = m_seenPages.find(id);
    if (iter != m_seenPages.end())
        return false;
    auto i = m_pages.find(id);
    if (i == m_pages.end())
        return false;

    return  i->second == NewForEveryone || i->second >= m_appInstallDate;
}

void NewIndicatorProvider::seen(int id)
{
    auto i = m_pages.find(id);
    if (m_pages.end() == i) // we don't care
        return;
    auto iter = m_seenPages.find(id);
    if (iter != m_seenPages.end()) // already seen
        return;
    m_seenPages.insert(id);
    m_dirty = true;
    // save changes after 45 secs.
    QTimer::singleShot(45 * 1000, this, SLOT(save()));
}
