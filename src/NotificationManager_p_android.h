/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NOTIFICATIONMANAGER_P_ANDROID_H
#define NOTIFICATIONMANAGER_P_ANDROID_H

#include <QObject>

class NotificationManager;

class NotificationManagerPrivate : public QObject
{
    Q_OBJECT
public:
    explicit NotificationManagerPrivate(NotificationManager *qq);

    void newBlockSeen_fromNetwork(int height);
    void segmentUpdated_fromNetwork();

    /* transaction notifications are either routed to the Android
     * notification system, or when the application is visible they
     * will create an in-app (aka QML) popup.
     * Never both in one instance.
     */
    void createAndroidTxNotification();
    void createQmlNotification();

signals:
    // emitted by notifyNewBlock_fromNetwork, in order to move the call to the Qt thread.
    void newBlockSeenSignal(int blockHeight);
    void segmentUpdatedSignal();

private slots:
    void newBlockSeen(int blockHeight);
    // the wallet has updated.
    void segmentUpdated();

private:
    NotificationManager * const q;
};


class Notification : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString message READ message NOTIFY messageChanged FINAL)
public:
    Notification(QObject *parent = nullptr);

    QString message() const;
    void setMessage(const QString &newMessage);

signals:
    void messageChanged();

private:
    QString m_message;
};

#endif
