/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IMPORTHANDLER_H
#define IMPORTHANDLER_H

#include "ElectronXClient.h"

class ImportHandler : public ElectronXClient
{
    Q_OBJECT
public:
    explicit ImportHandler(QObject *parent = nullptr);

    enum WalletType {
        FromSeed,
        FromPrivateKey,
        FromXPub,
        FromXPriv
    };

    /**
     * Find the import details for a verified to be correct type/text.
     *
     * This method returns instantly and will emit finished() when checking has completed.
     */
    void startCheck(WalletType type, const QString &text, const QString &password = QString());

    struct ImportData {
        std::vector<uint32_t> derivation;
        bool electrum = false;
        int firstBlockHeight = 0;
    };

    QList<ImportData> found() const;

signals:
    void finished();

protected:
    void handshakeCompleted() override;
    void handleResponse(int id, const QJsonObject &data) override;

private:
    void handleFirstUse(const QJsonObject &rootObject);
    void checkNext();

private:
    QString m_input;
    QString m_password; // seeds may have a password

    // what is the Input supposed to re-present?
    WalletType m_type;

    // for seeds, we need to check multiple options, we iterate through these.
    enum NextToCheck {
        MainDerivation,
        MainDerivationChange,
        Derivation145,
        Derivation145Change,
        MainDerivationElectrumMnemonic,
        Derivation145ElectrumMnemonic,
        Done
    };
    NextToCheck m_nextToCheck = MainDerivation;

    ImportData m_currentlyChecking;
    QList<ImportData> m_found;
};

#endif
