/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ElectronXClient.h"

#include <QSettings>
#include <QSslSocket>
#include <QJsonDocument>
#include <QTimer>

constexpr const char *USERAGENT = "net/useragent";

ElectronXClient::ElectronXClient(QObject *parent)
    : QObject(parent),
      m_electronServer(new QSslSocket(this))
{
    connect (m_electronServer, SIGNAL(connected()), this, SLOT(connectionEstablished()));
    connect (m_electronServer, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect (m_electronServer, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    connect (m_electronServer, SIGNAL(readyRead()), this, SLOT(socketDataAvailable()));

    connect (m_electronServer, &QSslSocket::sslErrors, [=](const QList<QSslError> &errors) {
        logInfo(10005) << "sslErrors" << errors.size();
        for (const auto &e : errors) {
            logInfo(10005) << " " << e.errorString();
        }
    });
}

void ElectronXClient::setService(const EndPoint &ep)
{
    m_serviceAddress = ep;
    assert(!m_serviceAddress.hostname.empty());
}

EndPoint ElectronXClient::service() const
{
    return m_serviceAddress;
}

void ElectronXClient::connectionEstablished()
{
    // first version we need is 1.5.2 because that is the one that introduced the
    // get_first_use method.
    // The second version is the highest we've tested to work.
    QString call("{\"jsonrpc\":\"2.0\","
                        "\"method\":\"server.version\","
                        "\"params\": [\"%1\", [\"1.5.2\", \"1.5.3\"]], \"id\": 1}");

    QSettings defaultConfig(":/defaults.ini", QSettings::IniFormat);
    QString useragent = defaultConfig.value(USERAGENT, "Flowee Pay Wallet").toString();
    call = call.arg(useragent);

    m_electronServer->write(call.toUtf8());
    m_electronServer->write("\n");
}

void ElectronXClient::disconnected()
{
    logDebug(10005);
}

void ElectronXClient::socketError(QAbstractSocket::SocketError error)
{
    int failureLevel = 40;
    if (error == QAbstractSocket::SslHandshakeFailedError) {
        logCritical(10005) << "Remote peer failed SSL handshake" << m_serviceAddress;
        failureLevel = 400;
    } else if (error == QAbstractSocket::NetworkError) {
        // probably our side...
        failureLevel = 1;
    } else {
        logCritical(10005) << error;
    }
    m_error = true;
    emit failed(failureLevel);
}

void ElectronXClient::socketDataAvailable()
{
    const auto data = m_electronServer->readAll();
    int from = 0;
    int to = -1;
    do {
        QByteArrayView line(data);
        to = line.indexOf('\n', from);
        if (to < 0)
            to = data.size();

        line = line.sliced(from, to - from);
        from = to + 1;

        QJsonDocument doc;
        doc = QJsonDocument::fromJson(line.toByteArray());
        if (!doc.isObject()) {
            logCritical(10005) << "Not valid JSON";
            m_error = true;
            emit failed(140);
            m_electronServer->close();
            return;
        }
        const auto o = doc.object();
        const int id = o.value("id").toInt();
        if (id == 1)
            handleVersion(o);
        else
            handleResponse(id, o);
    } while (from < data.size());
}

void ElectronXClient::watchdogTimeout()
{
    if (m_electronServer->state() == QAbstractSocket::HostLookupState) {
        // DNS slowness.
        // nothing specific to our remote. No point in reporting an error
    }
    else if (m_electronServer->state() == QAbstractSocket::ConnectingState) {
        logCritical(10005) << "ElectronXClient watchdog; connect too slow, remote unavailable";
        // ok, it should really have finished connecting already.
        m_electronServer->close();
        m_error = true;
        emit failed(2);
    }
}

void ElectronXClient::connectToServer()
{
    logCritical(10005) << "ElectronXClient connecting to" << m_serviceAddress;
    m_electronServer->connectToHostEncrypted(
        QString::fromStdString(m_serviceAddress.hostname), m_serviceAddress.announcePort);

    QTimer::singleShot(4000, this, SLOT(watchdogTimeout()));
}


void ElectronXClient::handleVersion(const QJsonObject &rootObject)
{
    auto result = rootObject.value("result");
    if (!result.isArray()) {
        logFatal(10005) << "No viable protocol-version found for this server";
        m_error = true;
        m_electronServer->close();
        emit failed(500);
        return;
    }

    handshakeCompleted();
}

bool ElectronXClient::errored() const
{
    return m_error;
}
