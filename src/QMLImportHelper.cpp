/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "QMLImportHelper.h"
#include "FloweePay.h"
#include "IndexerServices.h"

#include <HDMasterKey.h>
#include <QDateTime>
#include <QTimer>

QMLImportHelper::QMLImportHelper(QObject *parent)
    : QObject{parent},
    m_importHandler(new ImportHandler(this))
{
    // make sure that we'll have a list of indexer services when we
    // need them later.
    FloweePay::instance()->indexerServices()->populate();

    connect (m_importHandler, SIGNAL(finished()),
        this, SLOT(checkFinished()), Qt::QueuedConnection);

    connect (m_importHandler, &ImportHandler::failed, this, [=](int score) {
        auto services = FloweePay::instance()->indexerServices();
        services->punish(m_importHandler->service(), score);
        m_checking = false;
        // try again
        this->startCheck();
    }, Qt::QueuedConnection);
}

bool QMLImportHelper::checking() const
{
    return m_checking;
}

void QMLImportHelper::checkFinished()
{
    assert(m_checking);
    if (m_importHandler->errored()) {
        logWarning(10006) << "Import lookup failed, lets punish host and retry";
        logInfo(10006) << "  host to punish:" << m_importHandler->service();
        FloweePay::instance()->indexerServices()->punish(m_importHandler->service(), 60);
        m_checking = false; // to lure startCheck() into a sense of normalcy
        startCheck();
        return;
    }
    m_results = m_importHandler->found();
    emit resultCountChanged();
    setChecking(false);
}

void QMLImportHelper::setChecking(bool newChecking)
{
    if (m_checking == newChecking)
        return;
    m_checking = newChecking;
    emit checkingChanged();
}

void QMLImportHelper::dataChanged()
{
    if (m_dataChanged || m_checking)
        return;
    m_dataChanged = true;
    if (!m_results.isEmpty()) {
        m_results.clear();
        emit resultCountChanged();
    }
    QTimer::singleShot(0, this, SLOT(startCheck()));
}

QString QMLImportHelper::error() const
{
    return m_error;
}

void QMLImportHelper::setError(const QString &newError)
{
    if (m_error == newError)
        return;
    m_error = newError;
    logWarning(10006) << m_error;
    emit errorChanged();
}

WalletEnums::StringType QMLImportHelper::secretType() const
{
    return m_secretType;
}

void QMLImportHelper::setSecretType(const WalletEnums::StringType &newSecretType)
{
    if (m_secretType == newSecretType)
        return;
    m_secretType = newSecretType;
    emit secretTypeChanged();
    dataChanged();
}

void QMLImportHelper::startCheck()
{
    assert(m_checking == false);
    m_dataChanged = false;

    auto service = FloweePay::instance()->indexerServices()->service();
    if (service.hostname.empty()) {
        // lets not translate this, since this is likely an
        // internal error (aka bug) or simply a lack of Internet.
        setError("Failed to find available service");
        return;
    }
    m_importHandler->setService(service);
    ImportHandler::WalletType type;
    switch (m_secretType) {
    case WalletEnums::PrivateKey:
        type = ImportHandler::FromPrivateKey;
        break;
    case WalletEnums::CorrectMnemonic:
    case WalletEnums::ElectrumMnemonic:
        type = ImportHandler::FromSeed;
        break;
    case WalletEnums::XPub:
        type = ImportHandler::FromXPub;
        break;
    case WalletEnums::XPriv:
        type = ImportHandler::FromXPriv;
        break;
    default:
        return;
    }

    setChecking(true);
    m_importHandler->startCheck(type, m_secret, m_password);
}

QString QMLImportHelper::password() const
{
    return m_password;
}

void QMLImportHelper::setPassword(const QString &newPassword)
{
    if (m_password == newPassword)
        return;
    m_password = newPassword;
    emit passwordChanged();
    dataChanged();
}

QString QMLImportHelper::secret() const
{
    return m_secret;
}

void QMLImportHelper::setSecret(const QString &newSecret)
{
    if (m_secret == newSecret)
        return;
    m_secret = newSecret;
    emit secretChanged();
    dataChanged();
}

int QMLImportHelper::startHeight(int resultIndex) const
{
    if (m_results.size() <= resultIndex)
        return -1;
    return m_results.at(resultIndex).firstBlockHeight;
}

QString QMLImportHelper::derivation(int resultIndex) const
{
    if (m_results.size() <= resultIndex)
        return QString();
    QString answer("m/");
    auto path = m_results.at(resultIndex).derivation;
    if (path.size() == 5 && path.at(3) == 0 && path.at(4) == 0) {
        // the importer finds the first address, but we need the
        // base, so we cut off the last two zero's.
        path.resize(3);
    }

    for (size_t i = 0; i < path.size(); ++i) {
        uint32_t x = path.at(i);
        const bool hard = x >= HDMasterKey::Hardened;
        if (hard)
            x -= HDMasterKey::Hardened;
        answer += QString::number(x);
        if (hard)
            answer += "'";
        if (i < path.size() - 1)
            answer += "/";
    }
    return answer;
}

bool QMLImportHelper::isElectrumSeed(int resultIndex) const
{
    if (m_results.size() <= resultIndex)
        return false;
    return m_results.at(resultIndex).electrum;
}

int QMLImportHelper::monthOnHeight(int height) const
{
    assert(height > 10000);
    try {
        auto block = FloweePay::instance()->p2pNet()->blockchain().block(height);
        QDateTime date = QDateTime::fromSecsSinceEpoch(block.nTime);
        return date.date().month();
    } catch (...) {
        return -1;
    }
}

int QMLImportHelper::yearOnHeight(int height) const
{
    assert(height > 10000);
    try {
        auto block = FloweePay::instance()->p2pNet()->blockchain().block(height);
        QDateTime date = QDateTime::fromSecsSinceEpoch(block.nTime);
        return date.date().year();
    } catch (...) {
        return -1;
    }
}
