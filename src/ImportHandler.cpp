/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ImportHandler.h"

#include <utils/Logger.h>

#include <utils/HDMasterKey.h>
#include <utils/HDMasterPubkey.h>
#include <utils/cashaddr.h>
#include <FloweePay.h>

#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>
#include <QSslSocket>
#include <base58.h>

ImportHandler::ImportHandler(QObject *parent)
    : ElectronXClient(parent)
{
}

void ImportHandler::startCheck(WalletType type, const QString &text, const QString &password)
{
    assert(!m_serviceAddress.hostname.empty());
    if (m_serviceAddress.hostname.empty())
        throw std::runtime_error("Invalid use, startCheck called without a service");
    m_error = false;
    m_nextToCheck = MainDerivation;
    m_type = type;
    assert(m_type >= FromSeed && m_type <= FromXPub);
    m_input = text;
    assert(!m_input.isEmpty());
    m_password = password;
    m_found.clear();
    m_currentlyChecking = ImportData();

    connectToServer();
}

void ImportHandler::handleFirstUse(const QJsonObject &rootObject)
{
    auto result_ = rootObject.value("result");
    if (!result_.isNull()) {
        auto result = result_.toObject();
        auto foundOne = m_currentlyChecking;
        foundOne.firstBlockHeight = result.value("block_height").toInt(-1);
        if (foundOne.firstBlockHeight > 0)
            m_found.append(foundOne);
    }

    if (m_nextToCheck == Done) {
        emit finished();
        m_electronServer->close();
        return;
    }
    checkNext();
}

void ImportHandler::checkNext()
{
    QString call("{\"jsonrpc\":\"2.0\","
                        "\"method\":\"blockchain.scripthash.get_first_use\","
                        "\"params\": [\"%1\"], \"id\": 2}");

    PublicKey pubKey;
    if (m_type == FromPrivateKey) {
        auto privKey = PrivateKey::fromBase58(m_input.toStdString());
        if (privKey.isValid())
            pubKey = privKey.getPubKey();
        m_nextToCheck = Done;
    }
    else {
        HDMasterKey::MnemonicType type;
        auto derivation = HDMasterKey::deriveFromString("m/44'/0'/0'/0/0");
        assert(derivation.size() == 5);
        switch (m_nextToCheck) {
        case MainDerivation:
            type = HDMasterKey::BIP39Mnemonic;
            m_nextToCheck = MainDerivationChange;
            break;
        case MainDerivationChange:
            type = HDMasterKey::BIP39Mnemonic;
            derivation[3] = 1;
            m_nextToCheck = Derivation145;
            break;
        case Derivation145:
            type = HDMasterKey::BIP39Mnemonic;
            derivation[1] = 145 + HDMasterKey::Hardened;
            m_nextToCheck = Derivation145Change;
            break;
        case Derivation145Change:
            type = HDMasterKey::BIP39Mnemonic;
            derivation[1] = 145 + HDMasterKey::Hardened;
            derivation[3] = 1;
            m_nextToCheck = m_type == FromSeed ? MainDerivationElectrumMnemonic : Done;
            break;
        case MainDerivationElectrumMnemonic:
            type = HDMasterKey::ElectrumMnemonic;
            m_nextToCheck = Derivation145ElectrumMnemonic;
            break;
        case Derivation145ElectrumMnemonic:
            type = HDMasterKey::ElectrumMnemonic;
            derivation[1] = 145 + HDMasterKey::Hardened;
            m_nextToCheck = Done;
            break;
        default:
            assert(false);
            return;
        }
        m_currentlyChecking.derivation = derivation;
        m_currentlyChecking.electrum = type == HDMasterKey::ElectrumMnemonic;

        if (m_type == FromXPub) {
            HDMasterPubkey xpub = HDMasterPubkey::fromXPub(m_input.toStdString());
            pubKey = xpub.derive(derivation);
            if (xpub.level() > 2)
                m_nextToCheck = Done;
        }
        else if (m_type == FromSeed) {
            HDMasterKey master = HDMasterKey::fromMnemonic(m_input.toStdString(), type);
            auto privKey = master.derive(derivation);
            pubKey = privKey.getPubKey();
        }
    }

    if (!pubKey.isValid()) {
        logFatal(10005) << "Invalid input, can't derive pubkey";
        emit finished();
        m_electronServer->close();
        return;
    }
    auto pkh = pubKey.getKeyId();
    CashAddress::Content cashAddress;
    cashAddress.type = CashAddress::PUBKEY_TYPE;
    cashAddress.hash = std::vector<uint8_t>(pkh.begin(), pkh.end());
    auto scriptHash = CashAddress::createHashedOutputScript(cashAddress);
    call = call.arg(QString::fromStdString(scriptHash.toHex_reversed()));
    m_electronServer->write(call.toUtf8());
    m_electronServer->write("\n");
}

void ImportHandler::handshakeCompleted()
{
    assert(m_nextToCheck != Done);
    checkNext();
}

void ImportHandler::handleResponse(int id, const QJsonObject &data)
{
    if (id == 2)
        handleFirstUse(data);
    else
        logCritical() << "Received unrecognized response, ignoring. ID:" << id;
}

QList<ImportHandler::ImportData> ImportHandler::found() const
{
    return m_found;
}
