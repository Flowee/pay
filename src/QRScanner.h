/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QRSCANNER_H
#define QRSCANNER_H

#include <QObject>
#include <QString>

class QRScanner : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ScanType scanType READ scanType NOTIFY scanTypeChanged)
    Q_PROPERTY(bool isPayment READ isPayment WRITE setIsPayment NOTIFY isPaymentChanged FINAL)
    Q_PROPERTY(QString scanResult READ scanResult NOTIFY scanResultChanged)
    Q_PROPERTY(bool autostart READ autostart WRITE setAutostart NOTIFY autostartChanged)
    Q_PROPERTY(bool isScanning READ isScanning NOTIFY isScanningChanged)
    // Q_PROPERTY(ResultSource resultSource READ resultSource NOTIFY scanResultChanged)
    /// Set a help text to be displayed at the top of the scanning-page.
    Q_PROPERTY(QString helpText READ helpText WRITE setHelpText NOTIFY helpTextChanged FINAL)
public:
    explicit QRScanner(QObject *parent = nullptr);

    Q_INVOKABLE void start();
    Q_INVOKABLE void abort();

    enum ScanType {
        Seed,
        PaymentDetails,
        PaymentDetailsTestnet,
        PrivateKeyWIF,

        InvalidType = 100
    };
    Q_ENUM(ScanType)

    ScanType scanType() const;
    void setScanType(ScanType type);

    /// Notice that resultString may be empty if we didn't scan any valid QR
    void finishedScan(const QString &resultString, ScanType foundDataType);

    QString scanResult() const;
    void resetScanResult();

    bool autostart() const;
    void setAutostart(bool newAutostart);

    bool isScanning() const;
    void setIsScanning(bool now);

    QString helpText() const;
    void setHelpText(const QString &newHelpText);

    // we open the camera with a high expectation of the QR being a payment link.
    void setIsPayment(bool isPayment);
    bool isPayment() const;

private slots:
    void completed();

signals:
    void finished();
    void scanTypeChanged();
    void scanResultChanged();
    void autostartChanged();
    void isScanningChanged();
    void helpTextChanged();
    void isPaymentChanged();

    /**
     * This signal is emitted when autostart has been skipped.
     * Autostart only happens once for the lifetime of this QML item,
     * the autostart property will never be evaluated again.
     */
    void autostartSkipped();

private:
    ScanType m_scanType;
    // ResultSource m_resultSource = Camera;
    QString m_scanResult;
    QString m_helpText;
    bool m_autostart = false;
    bool m_isScanning = false;
    bool m_isPayment = false;
};

#endif
