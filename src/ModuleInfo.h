/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MODULE_INFO_H
#define MODULE_INFO_H

#include <QObject>
#include <streaming/BufferPool.h>
#include <streaming/ConstBuffer.h>

#include "ModuleSection.h"

/**
 * This is part of the Flowee Pay Modules section.
 *
 * A module is an optional product that is shipped in the Flowee Pay application
 * in a way that it can be enabled or disabled by the user.
 * The ModuleInfo is the metadata about the module, data that is shown to the user
 * to allow them to select if they like to enable this module or not.
 */
class ModuleInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged FINAL)
    /// returns true if there is at least one section that can be enabled
    Q_PROPERTY(bool hasUISections READ hasUISections CONSTANT FINAL)
    /// returns true is enabling this makes it show up somewhere in the UI
    Q_PROPERTY(bool canBeEnabled READ canBeEnabled CONSTANT FINAL)
    Q_PROPERTY(QString title READ title CONSTANT FINAL)
    Q_PROPERTY(QString description READ description CONSTANT FINAL)
    Q_PROPERTY(QList<ModuleSection*> sections READ sections CONSTANT FINAL)
    Q_PROPERTY(QString iconSource READ iconSource CONSTANT FINAL)
    Q_PROPERTY(QString id READ id CONSTANT FINAL)
    /// the buttonId used by the NewIndicatorProvider / NewIndicator.qml
    Q_PROPERTY(int buttonId READ buttonId WRITE setButtonId NOTIFY buttonIdChanged FINAL)
public:
    explicit ModuleInfo(QObject *parent = nullptr);

    virtual void loadSettings(const Streaming::ConstBuffer &data);
    virtual Streaming::ConstBuffer saveSettings(std::shared_ptr<Streaming::BufferPool> &pool) const;

    /**
     * The module's unique ID. This is used in save-files
     * and similar, and never shown to a user.
     */
    void setId(const QString &newId);
    QString id() const;

    /**
     * >The title as shown in the module selector UI.
     */
    void setTitle(const QString &newTitle);
    QString title() const;

    /**
     * >The description of this module, as shown in the module selector UI.
     */
    void setDescription(const QString &newDescription);
    QString description() const;

    /**
     * Sections define where a module is used, add them to "plug" your module into the UI
     */
    void addSection(ModuleSection *section);
    const QList<ModuleSection *> &sections() const;

    /**
     * Called from loading of settings.
     * The default will make all sections change their enabled to \a on
     *
     * Please make sure to emit enabledChanged if needed.
     */
    virtual void setEnabled(bool on);
    bool enabled() const;

    bool canBeEnabled() const;

    /**
     * Sets the path to the icon for this module.
     * This should be part of the QRC system and inside the module dir.
     */
    void setIconSource(const QString &newIconSource);
    QString iconSource() const;

    /// returns true if there is at least one section that is user-visible
    bool hasUISections() const;

    /// the priority is used to sort modules in a user visible list. Highest gets shown first.
    int priority() const;
    void setPriority(int newPriority);

    int buttonId() const;
    void setButtonId(int newButtonId);

signals:
    void enabledChanged();

    void buttonIdChanged();

protected:
    bool m_enabled = false;

private:
    QString m_id;
    QString m_title;
    QString m_description;
    QString m_translationUnit;
    QString m_iconSource;
    int m_priority = -1;
    int m_buttonId = 0;

    QList<ModuleSection*> m_sections;
};

#endif
