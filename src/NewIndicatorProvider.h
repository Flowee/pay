/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NEWINDICATORPROVIDER_H
#define NEWINDICATORPROVIDER_H

#include <QObject>
#include <set>
#include <map>

class NewIndicatorProvider : public QObject
{
    Q_OBJECT
public:
    explicit NewIndicatorProvider(QObject *parent = nullptr);
    ~NewIndicatorProvider();

    Q_INVOKABLE bool isNew(int id) const;
    Q_INVOKABLE void seen(int id);

public slots:
    void save();

private:
    void load();

    enum PageNewNess {
        NewForEveryone, ///< everyone should have a new indicator once.
        // Users that started using the software after YYYYMM date won't see a new indicator.
        NewSince202501,
        NewSince202502,
    };

    int m_appInstallDate = 0;
    std::map<int, PageNewNess> m_pages;
    std::set<int> m_seenPages;
    bool m_dirty = false;
};

#endif
