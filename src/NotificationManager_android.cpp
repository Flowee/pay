/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NotificationManager.h"
#include "NotificationManager_p_android.h"
#include "FloweePay.h"
#include <utils/Logger.h>

#include <QSettings>
#include <QCoreApplication>
#if TARGET_OS_Android
# include <QJniObject>
constexpr const char *ClassName = "org/flowee/pay/PayNotifications";
#endif

NotificationManager::NotificationManager(QObject *parent)
    : QObject(parent),
    d(new NotificationManagerPrivate(this))
{
    setCollation(true);
    QSettings appConfig;
    m_newBlockMuted = appConfig.value(KEY_MUTE, true).toBool();
}

void NotificationManager::notifyNewBlock(const P2PNet::Notification &notification)
{
    if (m_newBlockMuted)
        return;
    if (!m_headerSyncComplete)
        return;

    d->newBlockSeen_fromNetwork(notification.blockHeight);
}

void NotificationManager::segmentUpdated(const P2PNet::Notification&)
{
    d->segmentUpdated_fromNetwork();
}

void NotificationManager::requestNotificationPermissions()
{
    auto japp = QJniObject(QNativeInterface::QAndroidApplication::context());
    japp.callObjectMethod("requestNotificationPermission", "()V");
}


// ---------------------------------------------------------
NotificationManagerPrivate::NotificationManagerPrivate(NotificationManager *qq)
    : q(qq)
{
    connect (this, &NotificationManagerPrivate::newBlockSeenSignal,
             this, &NotificationManagerPrivate::newBlockSeen, Qt::QueuedConnection);
    connect (this, &NotificationManagerPrivate::segmentUpdatedSignal,
             this, &NotificationManagerPrivate::segmentUpdated, Qt::QueuedConnection);
}

void NotificationManagerPrivate::newBlockSeen_fromNetwork(int height)
{
    emit newBlockSeenSignal(height);
}

void NotificationManagerPrivate::segmentUpdated_fromNetwork()
{
    emit segmentUpdatedSignal();
}

// on the local thread.
void NotificationManagerPrivate::newBlockSeen(int blockHeight)
{
    QString message;
    if (FloweePay::instance()->chain() == P2PNet::MainChain)
        message = tr("Bitcoin Cash block mined. Height: %1").arg(blockHeight);
    else
        message = tr("tBCH (testnet4) block mined: %1").arg(blockHeight);

    auto task = QNativeInterface::QAndroidApplication::runOnAndroidMainThread([message]() {
        QJniEnvironment env;
        auto jmessage = QJniObject::fromString(message);
        jclass notifications = env.findClass(ClassName);
        QJniObject::callStaticMethod<void>(notifications, "notifyBlock",
                                           "(Ljava/lang/String;)V", jmessage);
    });
    // The java side will handle everything from here, so we just ignore the task.
    Q_UNUSED(task);
}

// on the local thread.
void NotificationManagerPrivate::segmentUpdated()
{
    if (FloweePay::instance()->headless())
        createAndroidTxNotification();
    else
        createQmlNotification();
}

void NotificationManagerPrivate::createAndroidTxNotification()
{
    // TODO
}

void NotificationManagerPrivate::createQmlNotification()
{
    int txCount;
    QString message = q->describeCollated(txCount);
    if (message.isEmpty())
        return;

    Notification *n = new Notification(q);
    connect (n, &QObject::destroyed, this, [=]{
        q->flushCollate();
    });
    // TODO does this not need a title?
    n->setMessage(message);
    FloweePay::instance()->setNotification(n);
}


Notification::Notification(QObject *parent)
    : QObject(parent)
{
}

QString Notification::message() const
{
    return m_message;
}

void Notification::setMessage(const QString &newMessage)
{
    if (m_message == newMessage)
        return;
    m_message = newMessage;
    emit messageChanged();
}
