/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QMLIMPORTHELPER_H
#define QMLIMPORTHELPER_H

#include <QObject>

#include "ImportHandler.h"
#include "WalletEnums.h"

class QMLImportHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool checking READ checking WRITE setChecking NOTIFY checkingChanged FINAL)
    Q_PROPERTY(QString secret READ secret WRITE setSecret NOTIFY secretChanged FINAL)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged FINAL)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged FINAL)
    Q_PROPERTY(WalletEnums::StringType secretType READ secretType WRITE setSecretType NOTIFY secretTypeChanged FINAL)
    Q_PROPERTY(int resultCount READ resultCount NOTIFY resultCountChanged FINAL)
public:
    explicit QMLImportHelper(QObject *parent = nullptr);

    // returns true while the async checking is in progress
    bool checking() const;

    QString secret() const;
    void setSecret(const QString &newSecret);

    QString password() const;
    void setPassword(const QString &newPassword);

    WalletEnums::StringType secretType() const;
    void setSecretType(const WalletEnums::StringType &newSecretType);

    int resultCount() const {
        return m_results.count();
    }

    QString error() const;
    void setError(const QString &newError);

    // present to the QML side a single field from the result-set.
    Q_INVOKABLE int startHeight(int resultIndex) const;
    Q_INVOKABLE QString derivation(int resultIndex) const;
    Q_INVOKABLE bool isElectrumSeed(int resultIndex) const;

    Q_INVOKABLE int monthOnHeight(int height) const;
    Q_INVOKABLE int yearOnHeight(int height) const;

signals:
    void checkingChanged();
    void secretChanged();
    void passwordChanged();
    void secretTypeChanged();
    void resultCountChanged();
    void errorChanged();

private slots:
    void startCheck();
    void checkFinished();

private:
    void setChecking(bool newChecking);
    // will make sure that we call startCheck in the next event-loop iteration.
    void dataChanged();


    QList<ImportHandler::ImportData> m_results;

    bool m_checking = false;
    bool m_dataChanged = false;
    QString m_secret;
    QString m_password;
    QString m_error;
    WalletEnums::StringType m_secretType = WalletEnums::Unknown;

    ImportHandler *m_importHandler;
};

#endif
