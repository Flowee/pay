/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PaymentDetailComment_p.h"
#include <primitives/script.h>

PaymentDetailComment::PaymentDetailComment(Payment *parent)
    : PaymentDetail(parent, Payment::CommentOutput)
{
}

QString PaymentDetailComment::commentStr() const
{
    return m_commentStr;
}

void PaymentDetailComment::setCommentStr(const QString &cs)
{
    assert(m_editable);
    if (!m_editable)
        return;
    if (m_commentStr == cs)
        return;
    m_commentStr = cs;
    emit commentStrChanged();
    if (!m_commentBytes.isEmpty()) // the string one is now leading
        setCommentBytes(Streaming::ConstBuffer());
    checkValid();
    emit sizeChanged();
}

QString PaymentDetailComment::preview() const
{
    return QString::fromUtf8(m_commentBytes.toString());
}

bool PaymentDetailComment::editable() const
{
    return m_editable;
}

void PaymentDetailComment::setEditable(bool on)
{
    if (m_editable == on)
        return;
    m_editable = on;
    emit editableChanged();
}

void PaymentDetailComment::setCommentBytes(const Streaming::ConstBuffer &comment)
{
    m_commentBytes = comment;
    if (!m_commentStr.isEmpty()) { // the bytes one is now leading.
        m_commentStr.clear();
        emit commentStrChanged();
    }
    emit sizeChanged();
    checkValid();
}

Streaming::ConstBuffer PaymentDetailComment::commentBytes() const
{
    return m_commentBytes;
}

int PaymentDetailComment::size() const
{
    int payloadSize = m_commentBytes.size();
    if (payloadSize == 0 && !m_commentStr.isEmpty())
         payloadSize = m_commentStr.toUtf8().size();

    // The actual comment is preceeded by the op-return and push opcodes which we have
    // to count too.
    if (payloadSize == 0)
        return 1;
    else if (payloadSize < OP_PUSHDATA1)
        return 2 + payloadSize;
    return 3 + payloadSize;
}

void PaymentDetailComment::checkValid()
{
    // the only validity check is that the total transaction doesn't exceed the number of
    // op return max bytes.

    auto payment = qobject_cast<Payment*>(parent());
    assert(payment);
    int size = 0;
    const int MAX_COMMENT_SIZE = 223; // is-standard rule from 2021
    const auto details = payment->paymentDetails();
    for (const auto detail_ : details) {
        const auto detail = qobject_cast<PaymentDetailComment*>(detail_);
        if (detail) {
            assert(detail->type() == Payment::CommentOutput);
            size += detail->size();
        }
    }
    setValid(size <= MAX_COMMENT_SIZE);
}
