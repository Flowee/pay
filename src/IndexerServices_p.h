/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INDEXERSERVICES_P_H
#define INDEXERSERVICES_P_H


#include "IndexerServices.h"
#include <QObject>
#include <QTcpSocket>
#include <streaming/BufferPool.h>

class FetchIndexerServicePeers : public QObject
{
    Q_OBJECT
public:
    FetchIndexerServicePeers(const QString &server, QObject *parent = nullptr);

    std::deque<IndexerServices::Indexer> servicesFound() const;

    int failScore() const;

signals:
    void finished();

private slots:
    void connectionEstablished();
    void disconnected();
    void socketError(QAbstractSocket::SocketError error);
    void socketDataAvailable();

    // if the remote doesn't respond or is just slow.
    void timeout();

private:
    QTcpSocket *m_remote;
    std::deque<IndexerServices::Indexer> m_result;
    int m_failScore = 0;
    Streaming::BufferPool m_pool;
};

#endif
