/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NOTIFICATIONMANAGER_H
#define NOTIFICATIONMANAGER_H

#include <QObject>
#include <NotificationListener.h>

class NotificationManagerPrivate;

class NotificationManager : public QObject, public NotificationListener
{
    Q_OBJECT
public:
    explicit NotificationManager(QObject *parent = nullptr);

    void notifyNewBlock(const P2PNet::Notification &notification) override;
    void segmentUpdated(const P2PNet::Notification &notification) override;

    bool newBlockMuted() const;
    void setNewBlockMuted(bool mute);

    // When Flowee Pay first starts it synchronizes with the network
    // and we get a notifyNewBlock call almost every time.
    // This is annoying and useless, so lets ignore it until we got a sync-complete.
    void headerSyncComplete();

signals:
    void newBlockMutedChanged();

private:
    bool m_headerSyncComplete = false;
    bool m_newBlockMuted = false;

    QString describeCollated(int &txCount) const;
    void requestNotificationPermissions();

    static constexpr const char *KEY_MUTE = "notificationNewblockMute";
    friend class NotificationManagerPrivate;
    NotificationManagerPrivate *d;
};

#endif
