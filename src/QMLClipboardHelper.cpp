/*
 * This file is part of the Flowee project
 * Copyright (C) 2023-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FloweePay.h"
#include "QMLClipboardHelper.h"
#include "WalletEnums.h"

#include <QGuiApplication>
#include <QClipboard>
#include <QTimer>
#include <QMimeData>

QMLClipboardHelper::QMLClipboardHelper(QObject *parent)
    : QObject{parent}
{
    // when the app regains main-app status, check for changes in the clipboard.
    auto guiApp = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    assert(guiApp);
    connect(guiApp, &QGuiApplication::applicationStateChanged, this, [=](Qt::ApplicationState state) {
        if (state == Qt::ApplicationActive)
            delayedParse();
    });
    // parse it in the next event, when properties are fully set
    delayedParse();
}

void QMLClipboardHelper::parseClipboard()
{
    m_delayedParseStarted = false;
    if (!m_enabled)
        return;
    auto *mimeData = QGuiApplication::clipboard()->mimeData();
    if (mimeData == nullptr) // on Wayland this is possible
        return;
    QString text;
    if (mimeData->hasText())
        text = mimeData->text();
    else if (mimeData->hasHtml())
        text = mimeData->html();

    if (text.isEmpty() || m_filter == NoFilter) {
        setClipboardText(text, Empty);

        emit clipboardScanned();
        return;
    }
    // often found whitespace, make it all spaces.
    text = text.replace(QLatin1String("<br>"), QLatin1String(" "));
    text = text.replace(QLatin1String("\n"), QLatin1String(" "));
    text = text.replace(QLatin1String("\r"), QLatin1String(" "));
    text = text.trimmed();

    auto type = FloweePay::instance()->identifyString(text);
    if (type == WalletEnums::Unknown || type == WalletEnums::PartialMnemonicWithTypo || type == WalletEnums::PartialMnemonic) { // try harder
        QString partial(text);
        QString prefix = QString::fromStdString(chainPrefix()) + ":";
        while (true) { // find all 'words' starting with our chain prefix.
            auto index = partial.indexOf(prefix);
            if (index < 0)
                break;
            auto end = partial.indexOf(' ', index + prefix.size());
            QString tillSpace = partial.mid(index, end);
            const int endOfAddress = tillSpace.indexOf('?');
            if (endOfAddress > 0) {
                type = FloweePay::instance()->identifyString(tillSpace.left(endOfAddress));
                if (type == WalletEnums::CashPKH || type == WalletEnums::CashSH) {
                    text = tillSpace;
                    break;
                }
            }
            // look further.
            partial = partial.mid(index + 1);
        }

        prefix = "bch-wif:";
        while (true) { // find all 'words' starting with "bch-wif:"
            auto index = partial.indexOf(prefix);
            if (index < 0)
                break;
            index += prefix.size();
            auto end = partial.indexOf(' ', index);
            if (end < 0)
                end = partial.size();
            if (end - index >= 50 && end - index < 55) {
                QString wif = partial.mid(index, end);
                type = FloweePay::instance()->identifyString(wif);
                if (type == WalletEnums::PrivateKey) {
                    text = wif;
                    break;
                }
            }

            // look further.
            partial = partial.mid(index + 1);
        }
        if (type != WalletEnums::CashPKH && type != WalletEnums::CashSH && type != WalletEnums::PrivateKey) {
            // still no success.
            // try to find the address even if it doesn't have the prefix.
            for (auto &word : text.split(' ', Qt::SkipEmptyParts)) {
                if (word.length() > 40 && word.length() < 50) {
                    QString address = prefix + word;
                    type = FloweePay::instance()->identifyString(address);
                    if (type == WalletEnums::CashPKH || type == WalletEnums::CashSH) {
                        text = address;
                        break;
                    }
                }
            }
        }
    }

    Type typeFound = NoFilter;
    if (m_filter.testAnyFlag(Addresses)
               && (type == WalletEnums::CashPKH || type == WalletEnums::CashSH)) {
        typeFound = Addresses;
        const int endOfAddress = text.indexOf('?');
        if (endOfAddress > 0) {
            const QString prefix = QString::fromStdString(chainPrefix()) + ":";
            if (text.startsWith(prefix))
                typeFound = AddressUrl;
        }
    }
    else if (m_filter.testAnyFlag(LegacyAddresses)
              && (type == WalletEnums::LegacyPKH || type == WalletEnums::LegacySH))
        typeFound = LegacyAddresses;
    else if (m_filter.testAnyFlag(PrivateKey) && type == WalletEnums::PrivateKey)
        typeFound = PrivateKey;
    else if (m_filter.testAnyFlag(MnemonicSeed)
            && (type == WalletEnums::CorrectMnemonic || type == WalletEnums::ElectrumMnemonic))
        typeFound = MnemonicSeed;
    else if (m_filter.testAnyFlag(XPub) && type == WalletEnums::XPub)
        typeFound = XPub;
    else if (m_filter.testAnyFlag(XPriv) && type == WalletEnums::XPriv)
        typeFound = XPriv;

    if (typeFound != NoFilter)
        setClipboardText(text, typeFound);
    emit clipboardScanned();
}

void QMLClipboardHelper::setClipboardText(const QString &text, Type type)
{
    if (m_text == text && m_type == type)
        return;
    m_text = text;
    m_type = type;
    emit textChanged();
}

void QMLClipboardHelper::delayedParse()
{
    if (m_delayedParseStarted)
        return;
    QTimer::singleShot(0, this, &QMLClipboardHelper::parseClipboard);
    m_delayedParseStarted = true;
}

bool QMLClipboardHelper::enabled() const
{
    return m_enabled;
}

void QMLClipboardHelper::setEnabled(bool newEnabled)
{
    if (m_enabled == newEnabled)
        return;
    m_enabled = newEnabled;
    emit enabledChanged();
    parseClipboard();
}

QString QMLClipboardHelper::clipboardText() const
{
    return m_text;
}

QMLClipboardHelper::Type QMLClipboardHelper::type() const
{
    return m_type;
}

void QMLClipboardHelper::setFilter(QMLClipboardHelper::Types filter)
{
    if (m_filter == filter)
        return;
    m_filter = filter;
    emit filterChanged();
    delayedParse();
}

QMLClipboardHelper::Types QMLClipboardHelper::filter() const
{
    return m_filter;
}
