/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEEPAY_H
#define FLOWEEPAY_H

#include "NotificationManager.h"
#include "WalletEnums.h"

#include <QObject>

#include <p2p/DownloadManager.h>
#include <p2p/P2PNetInterface.h>
#include <p2p/HeaderSyncInterface.h>
#include <apputils/Mnemonic.h>
#include <utils/WorkerThreads.h>
#include <utils/uint256.h>

#include <QList>
#include <QString>
#include <QDateTime>
#include <QLockFile>
#include <QNetworkAccessManager>


class Wallet;
class NewWalletConfig;
class KeyId;
class PriceDataProvider;
class CameraController;
class IndexerServices;

const std::string &chainPrefix();
QString renderAddress(const KeyId &pubkeyhash);

class FloweePay : public QObject, public WorkerThreads, public HeaderSyncInterface, public P2PNetInterface
{
    Q_OBJECT
    Q_PROPERTY(QString version READ version CONSTANT)
    Q_PROPERTY(QString libsVersion READ libsVersion CONSTANT)
    Q_PROPERTY(ApplicationProtection appProtection READ appProtection WRITE setAppProtection NOTIFY appProtectionChanged)
    // p2p net
    Q_PROPERTY(int headerChainHeight READ headerChainHeight NOTIFY headerChainHeightChanged)
    Q_PROPERTY(int expectedChainHeight READ expectedChainHeight NOTIFY expectedChainHeightChanged)
    Q_PROPERTY(FloweePay::NetworkCertainty blockHeightCertainty READ blockHeightCertainty NOTIFY blockHeightCertaintyChanged FINAL)
    Q_PROPERTY(int chainHeight READ chainHeight NOTIFY headerChainHeightChanged)
    Q_PROPERTY(bool isMainChain READ isMainChain CONSTANT)
    // GUI user settings
    Q_PROPERTY(int windowWidth READ windowWidth WRITE setWindowWidth NOTIFY windowWidthChanged)
    Q_PROPERTY(int windowHeight READ windowHeight WRITE setWindowHeight NOTIFY windowHeightChanged)
    Q_PROPERTY(bool useDarkSkin READ darkSkin WRITE setDarkSkin NOTIFY darkSkinChanged)
    Q_PROPERTY(bool skinFollowsPlatform READ skinFollowsPlatform WRITE setSkinFollowsPlatform NOTIFY skinFollowsPlatformChanged FINAL)
    Q_PROPERTY(bool hideBalance READ hideBalance WRITE setHideBalance NOTIFY hideBalanceChanged)
    Q_PROPERTY(bool activityShowsBch READ activityShowsBch WRITE setActivityShowsBch NOTIFY activityShowsBchChanged)
    Q_PROPERTY(int fontScaling READ fontScaling WRITE setFontScaling NOTIFY fontScalingChanged)
    Q_PROPERTY(int dspTimeout READ dspTimeout WRITE setDspTimeout NOTIFY dspTimeoutChanged)
    // User setting based on which unit (milli/sats/etc) the user choose
    Q_PROPERTY(int unitAllowedDecimals READ unitAllowedDecimals NOTIFY unitChanged)
    Q_PROPERTY(UnitOfBitcoin unit READ unit WRITE setUnit NOTIFY unitChanged)
    Q_PROPERTY(QString unitName READ unitName NOTIFY unitChanged)
    Q_PROPERTY(UnlockingKeyboard unlockingKeyboard READ unlockingKeyboard WRITE setUnlockingKeyboard NOTIFY unlockingKeyboardChanged FINAL)
    // notifications
    Q_PROPERTY(QObject *notification READ notification WRITE setNotification NOTIFY notificationChanged FINAL)
    Q_PROPERTY(bool newBlockMuted READ newBlockMuted WRITE setNewBlockMuted NOTIFY newBlockMutedChanged)
    Q_PROPERTY(bool privateMode READ privateMode WRITE setPrivateMode NOTIFY privateModeChanged)
    Q_PROPERTY(bool offline READ isOffline CONSTANT)
    Q_PROPERTY(bool deviceOffline READ deviceOffline WRITE setDeviceOffline NOTIFY deviceOfflineChanged FINAL)
    /// if true, supported platforms will try to update also when the app isn't running.
    Q_PROPERTY(bool backgroundUpdates READ backgroundUpdates WRITE setBackgroundUpdates NOTIFY backgroundUpdatesChanged FINAL)
    /// In hours. Ignored if backgroundUpdates is false.
    Q_PROPERTY(int backgroundUpdateInterval READ backgroundUpdateInterval WRITE setBackgroundUpdateInterval NOTIFY backgroundUpdateIntervalChanged FINAL)

    Q_PROPERTY(QString chainPrefix READ qchainPrefix CONSTANT FINAL)
public:
    enum UnitOfBitcoin {
        BCH,
        MilliBCH,
        MicroBCH,
        Bits,
        Satoshis
    };
    Q_ENUM(UnitOfBitcoin)

    enum UnlockingKeyboard {
        FullKeyboard,
        SmallNumbersKeyboard,
        BigNumbersKeyboard
    };
    Q_ENUM(UnlockingKeyboard)

    /**
     * The protection the user has selected for his Flowee Pay.
     */
    enum ApplicationProtection {
        NoProtection,       //< No application-wide protection. Notice individual wallets can be protected.
        AppPassword,        //< A single password for the app. No encryption.
        AppUnlocked         //< App-password has been provided.
    };
    Q_ENUM(ApplicationProtection)

    /**
     * The broadcast status is a statemachine to indicate if the transaction
     * has been offered to the network to the final state of
     * either TxRejected or TxBroadcastSuccess
     *
     * The statemachine goes like this;
     *
     * 0. `NotStarted`
     * 1. After the API call 'broadcast()' we offer the transaction to all peers.
     *    `TxOffered`
     * 2. A peer responds by downloading the actual transaction from us.
     *    `TxWaiting`
     * 3. Optionally, a peer responds with 'rejected' if the transaction is somehow wrong.
     *    `TxRejected`
     *    Stop here.
     * 4. We waited a little time and no rejected came in, implying 2 or more peers like our tx.
     *    `TxBroadcastSuccess`
     */
    enum BroadcastStatus {
        NotStarted,     //< We have not yet seen a call to broadcast()
        TxOffered,      //< Tx has not been offered to any peers.
        TxSent1,        //< Tx has been sent to at least one peer.
        TxWaiting,      //< Tx has been downloaded by more than one peer.
        TxBroadcastSuccess, //< Tx broadcast and accepted by multiple peers.
        TxRejected      //< Tx has been offered, downloaded and rejected by at least one peer.
    };
    Q_ENUM(BroadcastStatus)

    enum NetworkCertainty {
        NotCertain = P2PNet::NotCertain,
        ReasonablyCertain = P2PNet::ReasonablyCertain,
        Certain = P2PNet::Certain,
    };
    Q_ENUM(NetworkCertainty)

    FloweePay();

    /**
     * Select the chain that the FloweePay singleton will be created for.
     * Only has effect if you call this before the first call to instance();
     */
    static void selectChain(P2PNet::Chain chain);
    static FloweePay *instance();
    static void sendTransactionNotification(const P2PNet::Notification &notification);

    QList<Wallet *> wallets() const;

    /**
     * Return the p2pNet owned by this application.
     * Notice that we lazy-initialize this at first call, which is quite expensive.
     * The startP2PInit() method should be called once, to ensure the p2pNet initializes
     * in a worker-thread.
     */
    DownloadManager* p2pNet();

    /**
     * Return the fiat-prices data provider owned by this application.
     */
    PriceDataProvider *prices() const;

    /// return the amount of milli-seconds we wait for a double-spent-proof
    int dspTimeout() const;
    void setDspTimeout(int milliseconds);

    /// return the app data location
    QString basedir() const;

    /**
     * Load p2p layer in a worker-thread.
     * The signal loadComplete() will be triggered when that is done, after which it is safe to call
     * the p2pNet() method.
     * \sa loadComplete();
     */
    void startP2PInit();

    /// for a price, in satoshis, return a formatted string in unitName().
    Q_INVOKABLE inline QString amountToString(double price) const {
        return FloweePay::amountToString(static_cast<qint64>(price), m_unit);
    }
    /// for a price, in satoshis, return a formatted string in unitName(), removing trailing zeros.
    Q_INVOKABLE QString amountToStringPretty(double price) const;
    /// for a price, in satoshis, return a formatted string in unitName().
    QString amountToString(qint64 price) const {
        return FloweePay::amountToString(price, m_unit);
    }
    /// for a price, in satoshis, return a formatted string
    static QString amountToString(qint64 price, UnitOfBitcoin unit);

    enum DateFormatOption {
        NoYear,     ///< always avoid adding the year.
        SmartDate   ///< Use 'yesterday' and similar short hands, add year if it is not this year.
    };
    Q_ENUM(DateFormatOption)

    /**
     * Returns a locale adjusted rendering of a date.
     * @code
     * import Flowee.org.pay
     * Label { text = Pay.formatDate(myDate, FloweePay.SmartDate) }
     * @endcode
     * @see formatDateTime()
     */
    Q_INVOKABLE QString formatDate(QDateTime date, DateFormatOption format = SmartDate) const;
    /**
     * Returns a user-friendly formatting of the given date-time.
     * This incudes us returning words (translated) like "today" or "an hour ago".
     */
    Q_INVOKABLE QString formatDateTime(QDateTime datetime) const;
    /**
     * Returns the formatDateTime() formatted date of the blocks timestamp.
     */
    Q_INVOKABLE QString formatBlockTime(int blockHeight) const;
    Q_INVOKABLE QDateTime timeOfBlockHeight(int blockHeight) const;
    Q_INVOKABLE int heightOfBlockAtTime(QDateTime time) const;

    /// create a new HD wallet with an optional name.
    Q_INVOKABLE NewWalletConfig* createNewWallet(const QString &derivationPath, const QString &password = QString(), const QString &walletName = QString());
    /// create a new seed-less wallet with an optional name.
    Q_INVOKABLE NewWalletConfig* createNewBasicWallet(const QString &walletName = QString());
    /// swipe a paper wallet with an optional name
    Q_INVOKABLE NewWalletConfig* createImportedWallet(const QString &privateKey, const QString &walletName, int startHeight);
    /// Alternative arguments version. With date instead of block-height
    Q_INVOKABLE NewWalletConfig* createImportedWallet(const QString &privateKey, const QString &walletName, const QDateTime &date);

    /// Find out about the address and return an instance of AddressInfo if known.
    Q_INVOKABLE QObject* researchAddress(const QString &address, QObject *parent);

    /**
     * If property appProtection is set to AppPassword, this method will return true
     * only if the provided password is the one set by the user.
     */
    Q_INVOKABLE bool checkAppPassword(const QString &password);
    Q_INVOKABLE void setAppPassword(const QString &password);

    /**
     * Helper method to convert a private address to a BitcoinCash address.
     */
    Q_INVOKABLE QString addressForPrivKey(const QString &privateKey) const;

    /**
     * Import a mnemonics based (BIP39)  wallet.
     * Warning; will throw if the mnemonic is invalid
     *
     * @param mnemonic, the list of words (validated!) in the BIP39 format.
     * @param password, the optional password that goes with a BIP39 wallet.
     * @param derivationPath the path that turns this mnemonic into a hierarchically deterministic wallet.
     * @param walletName the name the user knows this wallet as.
     * @param startHeight the first block we should check for transactions, or zero for "future blocks"
     *        If you set this to 1 then we set it to a more sane value of when Bitcoin became more well known.
     * @param electrumFormat if true, interpret the mnemonic phrase using the Electrum format rather than BIP39.
     */
    Q_INVOKABLE NewWalletConfig *createImportedHDWallet(const QString &mnemonic, const QString &password,
                                                        const QString &derivationPath, const QString &walletName, int startHeight = 0,
                                                        bool electrumFormat = false);
    /// Alternative arguments version. With date instead of block-height
    Q_INVOKABLE NewWalletConfig *createImportedHDWallet(const QString &mnemonic, const QString &password,
                                                        const QString &derivationPath, const QString &walletName, const QDateTime &date,
                                                        bool electrumFormat = false);

    Q_INVOKABLE bool checkDerivation(const QString &path) const;

    /// take a bitcoin-related string and identify the type.
    Q_INVOKABLE WalletEnums::StringType identifyString(const QString &string) const;

    /// return a string version of the \a unit name. tBCH for instance.
    Q_INVOKABLE QString nameOfUnit(FloweePay::UnitOfBitcoin unit) const;

    /**
     * Change the currency based on the country code (nl_NL / en_US)
     */
    Q_INVOKABLE void setCountry(const QString &countrycode);
    Q_INVOKABLE QStringList recentCountries() const;

    /// returns the unit of our prices. BCH, for instance.
    QString unitName() const;
    /**
     * Returns the amount of digits allowed behind the decimal-separator.
     * The 'unit' the user pics (BCH/milli-bch/micro-bch) just moves the decimal separator
     * and we measure that by the amount of digits we still allow behind the separator.
     * For the "BCH" unit this is 8, for sats this is zero.
     */
    int unitAllowedDecimals() const;

    int windowWidth() const;
    void setWindowWidth(int windowWidth);

    int windowHeight() const;
    void setWindowHeight(int windowHeight);

    bool darkSkin() const;
    void setDarkSkin(bool darkSkin);

    bool skinFollowsPlatform() const;
    void setSkinFollowsPlatform(bool newSkinFollowsPlatform);

    UnitOfBitcoin unit() const;
    void setUnit(const UnitOfBitcoin &unit);

    /**
     * Return the chain-height of validated headers.
     * When we connect to some peers we get updated headers and thus the first
     * indication of the world status is the header chain height.
     */
    int headerChainHeight() const;

    /**
     * Certainty grows as more validated peers agree.
     */
    NetworkCertainty blockHeightCertainty() const;

    /**
     * Return the chain-height that based on the date/time we expect
     * to be at.
     */
    int expectedChainHeight() const;

    /**
     * The best known chainHeight.
     * This combines the best of headerChainHeight and expectedChainHeight in one, giving the
     * user the best know height based on all the info we have.
     * On startup the headerChainHeight is likely going to be outdated. Which would result in bad info.
     * So before those headers have arrived we therefore will
     * use the mathematically determined expectedChainHeight. We stop using
     * that as soon as we get some movement in the headers, as the expected
     * one may be off by several blocks.
     */
    int chainHeight();

    // HeaderSyncInterface interface
    void setHeaderSyncHeight(int height) override;
    void headerSyncComplete() override;

    // P2PNetInterface interface
    void newBlockHeightCertainty(P2PNet::NetworkCertainty certainty) override;

    /**
     *  Returns true if this is the mainchain.
     *  \see selectChain()
     */
    bool isMainChain() const {
        return m_chain == P2PNet::MainChain;
    }
    /**
     *  Returns the chain this Pay was created with.
     *  \see selectChain()
     */
    P2PNet::Chain chain() const;
    const std::string &chainPrefix() const { return m_chainPrefix; }
    QString qchainPrefix() const; // for the QML property

    /**
     * return if the user engaged the hide-balance feature to avoid people seeing the balance on-screen.
     */
    bool hideBalance() const;
    /**
     * Set the hide-balance on or off.
     */
    void setHideBalance(bool hideBalance);

    Q_INVOKABLE void copyToClipboard(const QString &text);
    Q_INVOKABLE void openInExplorer(const QString &txid);

    QString version() const;
    QString libsVersion() const;

    /// Simple bool to register user preference
    bool isOffline() const;
    /// register the user preference for being offline.
    void setOffline(bool offline);

    /**
     * run the p2p networking agents, unless isOffline() is true.
     * After the startP2PInit has successfully completed, call this
     * to start the actions running on the network layer which will
     * sync the databases from the p2p net.
     */
    void startNet();

    /// If true, no notifications about new blocks will be shown
    bool newBlockMuted() const;
    /// If true, no notifications about new blocks will be shown
    void setNewBlockMuted(bool mute);

    // return the CameraController previously set on this app singleton
    CameraController *cameraController();
    // set the cameraController for the singleton to take ownership of.
    void setCameraController(CameraController *cc);

    int fontScaling() const;
    void setFontScaling(int newFontScaling);

    bool activityShowsBch() const;
    void setActivityShowsBch(bool newActivityShowsBch);

    bool privateMode() const;
    void setPrivateMode(bool newPrivateMode);

    ApplicationProtection appProtection() const;
    void setAppProtection(ApplicationProtection newAppProtection);

    IndexerServices *indexerServices() const;

    /// removes wallet from the list of wallets this app knows about.
    void removeWallet(Wallet *wallet);

    // return the app-wide network access manager.
    QNetworkAccessManager* network();

    UnlockingKeyboard unlockingKeyboard() const;
    void setUnlockingKeyboard(UnlockingKeyboard newUnlockingKeyboard);

    bool headless() const;

    QObject *notification() const;
    void setNotification(QObject *newNotification);

    /**
     * FloweePay will attempt to lock the datadirectory at startup in order
     * to avoid multiple instances running at the same time. This is not a hard
     * check, we simply set this bool in the constuctor of this class to true
     * if some other process is still holding a lock.
     */
    bool lockFailed() const;

    bool deviceOffline() const;
    void setDeviceOffline(bool newDeviceOffline);

    bool backgroundUpdates() const;
    void setBackgroundUpdates(bool on);

    int backgroundUpdateInterval() const;
    void setBackgroundUpdateInterval(int hours);

signals:
    void loadComplete();
    /// \internal
    void loadComplete_priv();
    /// \internal
    void startSaveData_priv();
    void unitChanged();
    void walletsChanged();
    void darkSkinChanged();
    void windowWidthChanged();
    void windowHeightChanged();
    void headerChainHeightChanged();
    void expectedChainHeightChanged();
    void dspTimeoutChanged();
    void hideBalanceChanged();
    void newBlockMutedChanged();
    void fontScalingChanged();
    void activityShowsBchChanged();
    void totalBalanceConfigChanged();
    void privateModeChanged();
    void appProtectionChanged();
    void skinFollowsPlatformChanged();
    void blockHeightCertaintyChanged();
    void internal_heightCertaintyChanged(); // not thread-safe
    void unlockingKeyboardChanged();
    void notificationChanged();
    void deviceOfflineChanged();
    void backgroundUpdatesChanged();
    void backgroundUpdateIntervalChanged();

private slots:
    void loadingCompleted();
    void saveData();

private:
    struct AccountConfigData {
        // a per-wallet bool indicating its balance should be counted in the whole
        bool countBalance = true;
        bool privateWallet = false; // is hidden in private mode
        bool allowInstaPay = false;
        // per currency-code upper limit where payments are auto-approved.
        QMap<QString, int> fiatInstaPayLimits;
    };

    void init();
    void setupPlatform();
    void shutdown();
    void saveAll();
    // create wallet and add to list. Please consider calling walletsChanged() after
    Wallet *createWallet(const QString &name);
    uint32_t walletStartHeightHint() const;
    void connectToWallet(Wallet *wallet);

    mutable Mnemonic m_hdSeedValidator;

    UnitOfBitcoin m_unit = BCH;
    ApplicationProtection m_appProtection = NoProtection;
    P2PNet::Chain m_chain = P2PNet::MainChain;
    uint256 m_appProtectionHash;
    QString m_basedir;
    std::string m_chainPrefix;
    std::unique_ptr<DownloadManager> m_downloadManager;
    std::unique_ptr<PriceDataProvider> m_prices;
    std::unique_ptr<QNetworkAccessManager> m_network;
    NotificationManager m_notifications;
    QObject *m_notification = nullptr;
    CameraController* m_cameraController;
    IndexerServices *m_indexerServices; // the Electrum indexer index.
    QList<Wallet*> m_wallets;
    QHash<uint16_t, AccountConfigData> m_accountConfigs; // key is wallet-segment-id
    QLockFile m_lockFile;
    int m_dspTimeout = 5000;
    int m_windowWidth = 500;
    int m_windowHeight = 500;
    int m_fontScaling = 100;
    UnlockingKeyboard m_unlockingKeyboard = SmallNumbersKeyboard;
    bool m_loadingCompleted = false; // 'init()' completed
    bool m_loadCompleteEmitted = false; // avoid emitting this on every unlock
    bool m_appUnlocked = false;
    bool m_headless = false;
    bool m_darkSkin = true;
    bool m_skinFollowsPlatform = false;
    bool m_createStartWallet = false;
    bool m_hideBalance = false;
    /// Show the BCH amount involved in a transaction on the activity screen.
    bool m_activityShowsBch = false;
    bool m_offline = false; // user wants to run the app offline
    bool m_deviceOffline = false; // the platform reports we have no network
    bool m_gotHeadersSyncedOnce = false;
    bool m_privateMode = false; // wallets marked private are hidden when true
    bool m_lockFailed = false;
    bool m_backgroundUpdates = false;
    int m_backgroundUpdateInterval = 6; // in hours

    friend class AccountConfig;
};

#endif
