/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NOTIFICATIONMANAGER_P_DBUS_H
#define NOTIFICATIONMANAGER_P_DBUS_H

#include <QDateTime>
#include <QObject>
#include <QVariantMap>

class QDBusMessage;
class QDBusInterface;

class NotificationManager;

class NotificationManagerPrivate : public QObject
{
    Q_OBJECT
public:
    explicit NotificationManagerPrivate(NotificationManager *qq);

    void newBlockSeen_fromNetwork(int height);
    void segmentUpdated_fromNetwork();

signals:
    // emitted by notifyNewBlock_fromNetwork, in order to move the call to the Qt thread.
    void newBlockSeenSignal(int blockHeight);
    void segmentUpdatedSignal();

private slots:
    // newBlockSeen on the local thread.
    void newBlockSeen(int blockHeight);
    // the wallet has updated.
    void segmentUpdated();

    // callbacks from dbus
    void walletUpdateNotificationShown(uint id);
    void actionInvoked(uint id, const QString &actionKey);
    void notificationClosed(uint id, uint subId);
    void newBlockNotificationShown(uint id);

private:
    QDBusInterface *remote();

    QDBusInterface *m_remote = nullptr;
    bool m_openingNewFundsNotification = false;
    uint32_t m_blockNotificationId = 0;
    uint32_t m_newFundsNotificationId = 0;

    QVariantMap m_newBlockHints;
    QVariantMap m_walletUpdateHints;

    NotificationManager * const q;
};

#endif
