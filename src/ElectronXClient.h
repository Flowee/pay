/*
 * This file is part of the Flowee project
 * Copyright (C) 2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ELECTRONXCLIENT_H
#define ELECTRONXCLIENT_H

#include <QObject>

#include <QAbstractSocket>
#include <NetworkEndPoint.h>
#include <QJsonObject>

class QSslSocket;


class ElectronXClient : public QObject
{
    Q_OBJECT
public:
    explicit ElectronXClient(QObject *parent = nullptr);

    void setService(const EndPoint &ep);
    EndPoint service() const;

    /// returns true if an error occurred since startCheck()
    bool errored() const;

signals:
    void failed(int faulureLevel);

private slots:
    void connectionEstablished();
    void disconnected();
    void socketError(QAbstractSocket::SocketError error);
    void socketDataAvailable();

    void watchdogTimeout();

protected:
    virtual void handshakeCompleted() = 0;
    virtual void handleResponse(int id, const QJsonObject &data) = 0;
    void connectToServer();

private:
    void handleVersion(const QJsonObject &rootObject);

protected:
    EndPoint m_serviceAddress;
    QSslSocket *m_electronServer;
    bool m_error = false;
};

#endif
