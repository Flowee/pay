/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FloweePay.h"
#include "NotificationManager.h"
#include "PriceDataProvider.h"
#include <QSettings>

#include <utils/Logger.h>

bool NotificationManager::newBlockMuted() const
{
    return m_newBlockMuted;
}

void NotificationManager::setNewBlockMuted(bool mute)
{
    if (m_newBlockMuted == mute)
        return;
    m_newBlockMuted = mute;
    QSettings appConfig;
    appConfig.setValue(KEY_MUTE, m_newBlockMuted);
    emit newBlockMutedChanged();

    if (!mute)
        requestNotificationPermissions();
}

void NotificationManager::headerSyncComplete()
{
    m_headerSyncComplete = true;
}

QString NotificationManager::describeCollated(int &txCount) const
{
    const auto data = collatedData();
    txCount = 0;
    if (data.empty())
        return QString();

    int64_t deposited = 0;
    int64_t spent = 0;
    for (const auto &item : data) {
        deposited += item.deposited;
        spent += item.spent;
        txCount += item.txCount;
    }

    const auto gained = deposited - spent;
    auto pricesOracle = FloweePay::instance()->prices();
    QString gainedStr;
    if (pricesOracle->price())
        gainedStr = pricesOracle->formattedPrice(gained, pricesOracle->price());
    if (gainedStr.isEmpty())  {
        // no price data available (yet). Display crypto units
        gainedStr = QString("%1 %2")
                        .arg(FloweePay::instance()->amountToStringPretty((double) gained),
                             FloweePay::instance()->unitName());
    }
    if (gained > 0) // since we indicate adding, we always want the plus there
        gainedStr = QString("+%1").arg(gainedStr);

    // body-text
    if (data.size() > 1)
        return tr("%1 new transactions across %2 wallets found (%3)").arg(txCount).arg(data.size()).arg(gainedStr);
    if (gained < 0 && txCount == 1)
        return tr("A payment of %1 has been sent").arg(gainedStr.mid(1));
    return tr("%1 new transactions found (%2)", "", txCount).arg(txCount).arg(gainedStr);
}
