#include "UserIntent.h"

#include <QThread>

UserIntent::UserIntent(QObject *parent)
    : QObject{parent}
{
    // ensure that the emitSignals is always called on the Qt thread.
    connect (this, SIGNAL(needsEmit()), this, SLOT(emitSignals()), Qt::QueuedConnection);
    // Same for processNewIntents
    connect (this, SIGNAL(newIntentString()), this, SLOT(processNewIntents()), Qt::QueuedConnection);
}

void UserIntent::setPaymentIntent(const QString &pi)
{
    m_intentString = pi;
    if (QThread::currentThread() != thread()) {
        emit newIntentString();
        return;
    }
    processNewIntents();
}

void UserIntent::processNewIntents()
{
    auto pi = m_intentString;
    m_intentString.clear();
    setPaymentUrl(QString());
    setSweepKey(QString());
    if (pi.startsWith("bitcoincash:")) {
        setPaymentUrl(pi);
    } else if (pi.startsWith("bch-wif:")) {
        setSweepKey(pi.mid(8)); // mid to cut off the prefix
    }
}

bool UserIntent::startPaymentScanner() const
{
    return m_startPaymentScanner;
}

void UserIntent::setStartPaymentScanner(bool on)
{
    if (m_startPaymentScanner == on)
        return;
    m_startPaymentScanner = on;
    emit startPaymentScannerChanged();
}

QString UserIntent::paymentUrl() const
{
    return m_paymentUrl;
}

void UserIntent::setPaymentUrl(const QString &newPaymentUrl)
{
    if (m_paymentUrl == newPaymentUrl)
        return;
    m_paymentUrl = newPaymentUrl;
    emit paymentUrlChanged();
}

QString UserIntent::sweepKey() const
{
    return m_sweepKey;
}

void UserIntent::setSweepKey(const QString &wif)
{
    if (m_sweepKey == wif)
        return;
    m_sweepKey = wif;
    emit sweepKeyChanged();
}

void UserIntent::emitSignals()
{
    if (QThread::currentThread() != thread()) {
        emit needsEmit(); // calls this method again, but from the Gui thread.
        return;
    }
    if (!m_sweepKey.isEmpty())
        emit sweepKeyChanged();
    if (!m_paymentUrl.isEmpty())
        emit paymentUrlChanged();
}
