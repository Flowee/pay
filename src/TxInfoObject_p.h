/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TXINFOOBJECT_P_H
#define TXINFOOBJECT_P_H

#include <QObject>

#include <QMutex>
#include <p2p/BroadcastTxData.h>
#include <QTimer>

class Wallet;
class TxInfoObject;

class TxInfoPrivate : public QObject
{
    Q_OBJECT
public:
    TxInfoPrivate(TxInfoObject *parent);

    struct Status {
        int sent = 0; // number of peers we sent this transaction to.
        int inProgress = 0; // number of peers in progress.
        int failed = 0; // number of peers reported failure.
    };
    /// Fill a status struct where we take the time things in flight into account.
    Status calcStatus() const;

    void sentVia(const std::shared_ptr<Peer> &peer);
    void txRejected(int connectionId, BroadcastTxData::RejectReason reason, const std::string &message);

    mutable QMutex lock;
    const Wallet *wallet;
    int txIndex;

    struct Broadcast {
        int connectionId = -1;
        EndPoint ep;
        bool failed = false;
        BroadcastTxData::RejectReason rejected = BroadcastTxData::NoReason;
        std::string rejectMsg;
        uint64_t sentTime = 0; // ms since epoch.
    };
    QList<Broadcast> broadcasts;
    TxInfoObject *q;

    // to avoid emitting finished() more then once.
    bool finished = false;
    QTimer checkStatusTimer;

    // this class is multi-threading by need. The callbacks forwarded from the
    // main class can happen in any thread, so the sentVia/txRejected are not
    // on our associated thread.
    // As these methods are also our main events and thus triggers, we notify the
    // via signals about changes which are connected to our own slots below that
    // then will be executed in our own thread.
    // multi-threading for dummies ;-)
signals:
    void broadcastsChanged();

private slots:
    void checkState();
};

#endif
