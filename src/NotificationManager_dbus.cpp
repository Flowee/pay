/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NotificationManager.h"
#include "NotificationManager_p_dbus.h"
#include "FloweePay.h"

#include <utils/Logger.h>

#include <QDBusInterface>
#include <QThread>
#include <QTimer>
#include <QSettings>

NotificationManager::NotificationManager(QObject *parent)
    : QObject(parent),
    d(new NotificationManagerPrivate(this))
{
    setCollation(true);
    QSettings appConfig;
    m_newBlockMuted = appConfig.value(KEY_MUTE, false).toBool();
}

void NotificationManager::notifyNewBlock(const P2PNet::Notification &notification)
{
    d->newBlockSeen_fromNetwork(notification.blockHeight);
}

void NotificationManager::segmentUpdated(const P2PNet::Notification&)
{
    d->segmentUpdated_fromNetwork();
}

void NotificationManager::requestNotificationPermissions()
{
}

// /////////////////////////////////

NotificationManagerPrivate::NotificationManagerPrivate(NotificationManager *qq)
    : QObject(qq),
    q(qq)
{
    // We use the notification spec (v 1.2)
    // https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html
    m_newBlockHints["desktop-entry"] = "org.flowee.pay";
    m_newBlockHints["urgency"] = 0; // low
    // "sound-file" // filename TODO
    m_walletUpdateHints["desktop-entry"] = "org.flowee.pay";
    m_walletUpdateHints["urgency"] = 1; // normal

    // setup slots for DBUS-signals. Essentially remote callbacks from the deskop notifications app
    QDBusConnection::sessionBus().connect(QString(), QString(), "org.freedesktop.Notifications",
                                          "NotificationClosed", this, SLOT(notificationClosed(uint,uint)));
    QDBusConnection::sessionBus().connect(QString(), QString(), "org.freedesktop.Notifications",
                                          "ActionInvoked", this, SLOT(actionInvoked(uint,QString)));

    connect (this, &NotificationManagerPrivate::newBlockSeenSignal,
             this, &NotificationManagerPrivate::newBlockSeen, Qt::QueuedConnection);
    connect (this, &NotificationManagerPrivate::segmentUpdatedSignal,
             this, &NotificationManagerPrivate::segmentUpdated, Qt::QueuedConnection);
}

void NotificationManagerPrivate::newBlockSeen_fromNetwork(int height)
{
    emit newBlockSeenSignal(height);
}

void NotificationManagerPrivate::segmentUpdated_fromNetwork()
{
    emit segmentUpdatedSignal();
}

void NotificationManagerPrivate::newBlockSeen(int blockHeight)
{
    assert(QThread::currentThread() == thread());
    if (q->m_newBlockMuted)
        return;
    if (!q->m_headerSyncComplete)
        return;
    auto iface = remote();
    if (!iface->isValid()) return;
    QVariantList args;
    args << QVariant("Flowee Pay"); // app-name
    args << QVariant(m_blockNotificationId); // replaces-id
    args << QString(); // app_icon (not needed since we say which desktop file we are)
    args << QString(); // body-text
    if (FloweePay::instance()->chain() == P2PNet::MainChain)
        args << tr("Bitcoin Cash block mined. Height: %1").arg(blockHeight); // summary text
    else
        args << tr("tBCH (testnet4) block mined: %1").arg(blockHeight); // summary text
    QStringList actions; // actions
    actions << "mute" << tr("Mute");
    args << actions;
    args << m_newBlockHints;
    args << -1; // timeout (ms) -1 means to let the server decide
    if (!iface->callWithCallback("Notify", args, this,
                                 SLOT(newBlockNotificationShown(uint)))) {
        logWarning() << "dbus down, can't show notifications";
    }
}

void NotificationManagerPrivate::newBlockNotificationShown(uint id)
{
    m_blockNotificationId = id;
}

void NotificationManagerPrivate::segmentUpdated()
{
    if (m_openingNewFundsNotification) {
        // we are currently (async) opening a notification, wait until its actually open.
        QTimer::singleShot(50, this, SLOT(segmentUpdated()));
        return;
    }

    auto iface = remote();
    if (!iface->isValid()) return;

    int txCount;
    QString message = q->describeCollated(txCount);
    if (message.isEmpty())
        return;

    QVariantList args;
    args << QVariant("Flowee Pay"); // app-name
    args << QVariant(m_newFundsNotificationId); // replaces-id
    args << QString(); // app_icon (not needed since we say which desktop file we are)
    args << tr("New Transactions", "dialog-title", txCount);
    args << message;
    args << QStringList();
    args << m_walletUpdateHints;
    args << -1; // timeout (ms) -1 means to let the server decide
    if (!iface->callWithCallback("Notify", args, this,
                                 SLOT(walletUpdateNotificationShown(uint)))) {
        logWarning() << "dbus down, can't show notifications";
    }
    if (m_newFundsNotificationId == 0) {
        // The assignment of m_newFundsNotificationId is async, as such
        // we want to remember we are opening one and not have multiple calls in
        // flight at the same time
        m_openingNewFundsNotification = true;
    }
}

QDBusInterface *NotificationManagerPrivate::remote()
{
    assert(QThread::currentThread() == thread());
    if (m_remote == nullptr) {
        m_remote = new QDBusInterface("org.freedesktop.Notifications", // service
                                      "/org/freedesktop/Notifications", // path
                                      "org.freedesktop.Notifications", // interface name (duplicate of service)
                                      QDBusConnection::sessionBus(), this);
        if (!m_remote->isValid())
            logWarning() << qPrintable(QDBusConnection::sessionBus().lastError().message());
    }

    return m_remote;
}

void NotificationManagerPrivate::walletUpdateNotificationShown(uint id)
{
    m_newFundsNotificationId = id;
    m_openingNewFundsNotification = false;
}

void NotificationManagerPrivate::actionInvoked(uint, const QString &actionKey)
{
    if (actionKey == "mute")
        q->setNewBlockMuted(true);
}

void NotificationManagerPrivate::notificationClosed(uint32_t id, uint32_t reason)
{
    if (m_blockNotificationId == id) {
        m_blockNotificationId = 0;
    }
    else if (m_newFundsNotificationId == id) {
        m_newFundsNotificationId = 0;
        q->flushCollate();
    }
}
