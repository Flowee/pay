/*
 * This file is part of the Flowee project
 * Copyright (C) 2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Periodic.h"
#include "FloweePay.h"
#include "PriceDataProvider.h"
#include "Wallet.h"

#include <QCoreApplication>
#include <QFile>

struct CommandLineParserData;
std::unique_ptr<QFile> handleStaticChain(CommandLineParserData *cld);

Periodic::Periodic()
    : m_verifyHandle(new ECCVerifyHandle())
{
    ECC_Start(); // Init crypto lib.
}

Periodic::~Periodic()
{
    ECC_Stop();
}

int Periodic::run()
{
    int argc = 0;
    char *argv[] = { nullptr };
    QCoreApplication qapp(argc, argv);
    qapp.setOrganizationName("flowee");
    qapp.setApplicationName("pay");

    auto app = FloweePay::instance();
    if (app->lockFailed())
        return 1;

    auto blockheaders = handleStaticChain(nullptr);
    QObject::connect(app, &FloweePay::loadComplete, app, [=]() {
        auto app = FloweePay::instance();
        if (app->deviceOffline()) {
            QCoreApplication::quit();
            return;
        }
        // for each wallet check if it is up to date yet.
        for (Wallet *wallet : app->wallets()) {
            QObject::connect(wallet, &Wallet::lastBlockSynchedChanged,
                    this, &Periodic::checkFinished, Qt::QueuedConnection);
        }
        QObject::connect(app, &FloweePay::blockHeightCertaintyChanged,
                this, &Periodic::checkFinished);
        app->startNet(); // lets go!
        app->prices()->start();
    });

    // watchdog timer.
    QTimer::singleShot(120 * 1000, &qapp, []() { // no more than 2 minuts
        // Watchdog kicked in, shutting down
        QCoreApplication::quit();
    });

    FloweePay::instance()->startP2PInit();
    return qapp.exec();
}

void Periodic::checkFinished()
{
    auto *app = FloweePay::instance();
    if (app->blockHeightCertainty() != FloweePay::Certain)
        return;
    bool done = true;
    for (auto *wallet : app->wallets()) {
        if (wallet->segment()->priority() == PrivacySegment::OnlyManual)
            continue;
        assert(wallet->segment());
        auto lbs = wallet->segment()->lastBlockSynched();
        if (lbs != -2 // special value for just created wallet
            && app->chainHeight() != lbs)
            done = false;
    }
    if (done)
        QCoreApplication::quit();
}
