/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "IndexerServices.h"
#include "IndexerServices_p.h"

#include <utils/random.h>
#include <utils/Logger.h>
#include <utils/streaming/BufferPools.h>
#include <utils/streaming/MessageBuilder.h>
#include <utils/streaming/MessageParser.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QThread>
#include <QTimer>
#include <ripemd160.h>
#include <QFile>

#include <fstream>

namespace {
// for the save-file
enum ServicesTags {
    Separator,
    Hostname,
    IPAddress,
    PortNum,
    SecurePortNum,
    ProtocolVersion,
    Punishment
};


constexpr const char * FILENAME = "/electrum.dat";
}


IndexerServices::IndexerServices(const QString &basedir, boost::asio::io_context &ioContext, QObject *parent)
    : QObject{parent},
    m_resolver(ioContext),
    m_basedir(basedir)
{
    connect (this, SIGNAL(startMaybeFindServices()), this, SLOT(maybeFindServices()), Qt::QueuedConnection);

    load();
}

void IndexerServices::populate()
{
    m_resolver.async_resolve("ec-seed.flowee.cash", "http", std::bind(&IndexerServices::onSeedLookupComplete,
                        this, std::placeholders::_1, std::placeholders::_2));
}

void IndexerServices::load()
{
    m_knownServices.clear();
    const QString filebase = m_basedir + FILENAME;
    QFile in(filebase);
    if (!in.open(QIODevice::ReadOnly))
        return;

    const auto dataSize = in.size();
    auto pool = Streaming::pool(dataSize);
    in.read(pool->begin(), dataSize);
    Streaming::MessageParser parser(pool->commit(dataSize));
    Indexer indexItem;
    while (parser.next() == Streaming::FoundTag) {
        switch (parser.tag()) {
        case Separator:
            m_knownServices.push_back(indexItem);
            indexItem = Indexer();
            break;
        case Hostname:
            indexItem.hostname = QString::fromStdString(parser.stringData());
            break;
        case IPAddress:
            indexItem.ip = QString::fromStdString(parser.stringData());
            break;
        case PortNum:
            indexItem.port = parser.intData();
            break;
        case SecurePortNum:
            indexItem.securePort = parser.intData();
            break;
        case ProtocolVersion:
            indexItem.protocolVersion = parser.longData();
            break;
        case Punishment:
            indexItem.punishment = parser.intData();
            break;
        }
    }
}

void IndexerServices::save()
{
    auto buffer = std::make_shared<Streaming::BufferPool>(m_knownServices.size() * 1000);
    Streaming::MessageBuilder builder(buffer);
    for (const auto &service : m_knownServices) {
        builder.add(Hostname, service.hostname.toStdString());
        builder.add(IPAddress, service.ip.toStdString());
        builder.add(PortNum, service.port);
        builder.add(SecurePortNum, service.securePort);
        builder.add(ProtocolVersion, (uint64_t) service.protocolVersion);
        builder.add(Punishment, service.punishment);
        builder.add(Separator, false);
    }
    auto data = builder.buffer();

    const QString filebase = m_basedir + FILENAME;
    QFile origFile(filebase);
    if (origFile.open(QIODevice::ReadOnly)) {
        CRIPEMD160 fileHasher;
        auto origContent = origFile.readAll();
        fileHasher.write(origContent.data(), origContent.size());
        char fileHash[CRIPEMD160::OUTPUT_SIZE];
        fileHasher.finalize(fileHash);

        CRIPEMD160 memHasher;
        memHasher.write(data.begin(), data.size());
        char memHash[CRIPEMD160::OUTPUT_SIZE];
        memHasher.finalize(memHash);
        if (memcmp(fileHash, memHash, CRIPEMD160::OUTPUT_SIZE) == 0) {
            // no changes, so don't write.
            return;
        }
    }

    try {
        std::string filebaseStr(filebase.toStdString());
        std::ofstream out(filebaseStr + "~");
        out.write(data.begin(), data.size());
        out.flush();
        out.close();
        std::filesystem::rename(filebaseStr + "~", filebaseStr);
    } catch (const std::exception &e) {
        logCritical(10006) << "Failed to create electrum.dat file. Permissions issue?" << e;
    }
}

EndPoint IndexerServices::service() const
{
    EndPoint ep;
    QMutexLocker locker(&m_mutex);
    std::vector<Indexer> eligible;
    for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
        if (iter->punishment < 250 && iter->securePort > 0
                && iter->hostname != iter->ip
                && (iter->protocolVersion  == 0 || iter->protocolVersion >= 0x010502)) {
            eligible.push_back(*iter);
        }
    }
    if (!eligible.empty()) {
        int best = -1;
        // try 5 random ones in order to avoid picking one with a worse punishment.
        for (int attempt = 0; attempt < 5; ++attempt) {
            int index = GetRand(eligible.size());
            if (best == -1) {
                best = index;
            } else if (index != best) {
                const auto &a = eligible.at(best);
                const auto &b = eligible.at(index);
                if (b.punishment < a.punishment)
                    best = index;
            }
        }
        const auto &result = eligible.at(best);
        ep.hostname = result.hostname.toStdString();
        ep.announcePort = result.securePort;
        ep.peerPort = result.securePort;
    }
    return ep;
}

void IndexerServices::punish(const EndPoint &ep, int score)
{
    QMutexLocker locker(&m_mutex);
    std::vector<Indexer> eligible;
    QString hostname = QString::fromStdString(ep.hostname);
    for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
        if (iter->hostname == hostname && ep.announcePort == iter->securePort) {
            iter->punishment += score;
            save();
        }
    }
}

void IndexerServices::onSeedLookupComplete(const boost::system::error_code &error, boost::asio::ip::tcp::resolver::results_type results)
{
    if (error)
        return;

    QMutexLocker locker(&m_mutex);
    for (auto dnsIter = results.begin(); dnsIter != results.end(); ++dnsIter) {
        const QString ip = QString::fromStdString(dnsIter->endpoint().address().to_string());
        bool found = false;

        for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
            if (iter->ip == ip) {
                found = true;
                iter->punishment = std::max(0, iter->punishment - 100);
                break;
            }
        }

        if (!found) {
            Indexer indexer;
            indexer.ip = ip;
            indexer.hostname = ip;
            indexer.port = 50001;
            m_knownServices.push_back(indexer);
        }
    }

    emit startMaybeFindServices(); // make sure that one is called in the main thread.
}

void IndexerServices::maybeFindServices()
{
    assert(thread() == QThread::currentThread());
    if (m_fetcher)
        return;
    int goodFound = 0;
    Indexer known;
    QMutexLocker locker(&m_mutex);
    for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
        if (iter->punishment < 250) {
            if (iter->securePort > 0)
                ++goodFound;
            if (iter->port != -1 && (iter->protocolVersion  == 0 || iter->protocolVersion >= 0x010502)) {
                // To randomly select an item from the list, replace
                // with later one in 25% of the cases.
                if (known.port <= 0 || (std::rand() % 4) == 0)
                    known = *iter;
            }
        }
    }
    if (goodFound > 10 || known.port <= 0)
        return;

    m_fetcher = new FetchIndexerServicePeers(known.ip, this);
    connect (m_fetcher, &FetchIndexerServicePeers::finished, this, [=]() {
        QMutexLocker locker(&m_mutex);
        if (!m_fetcher)
            return;
        if (m_fetcher->failScore()) { // the remote didn't follow the protocol properly, down prioritize.
            for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
                if (iter->ip == known.ip) {
                    iter->punishment += m_fetcher->failScore();
                    break;
                }
            }
        }

        for (const auto &server : m_fetcher->servicesFound()) {
            bool found = false;
            for (auto iter = m_knownServices.begin(); iter != m_knownServices.end(); ++iter) {
                if (iter->hostname == server.hostname || iter->ip == server.ip) {
                    found = true;
                    iter->hostname = server.hostname;
                    iter->ip = server.ip;
                    iter->port = server.port;
                    iter->securePort = server.securePort;
                    iter->protocolVersion = server.protocolVersion;
                    break;
                }
            }
            if (!found)
                m_knownServices.push_back(server);
        }
        m_fetcher->deleteLater();
        m_fetcher = nullptr;

        save();

        // run it again, to check we have enough service providers.
        QTimer::singleShot(1000, this, SLOT(maybeFindServices()));
    });
}


// ---------------------------------------------------------

FetchIndexerServicePeers::FetchIndexerServicePeers(const QString &server, QObject *parent)
    : QObject(parent),
    m_remote(new QTcpSocket(this)),
    m_pool(100000) // 100KB for the peers.subscribe should be enough, no?
{
    connect (m_remote, SIGNAL(connected()), this, SLOT(connectionEstablished()));
    connect (m_remote, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect (m_remote, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    connect (m_remote, SIGNAL(readyRead()), this, SLOT(socketDataAvailable()));

    logDebug(10006) << "Connect to host:" << server;
    m_remote->connectToHost(server, 50001);

    // watchdog timer.
    QTimer::singleShot(4000, this, SLOT(timeout()));
}

std::deque<IndexerServices::Indexer> FetchIndexerServicePeers::servicesFound() const
{
    return m_result;
}

void FetchIndexerServicePeers::connectionEstablished()
{
    logDebug(10006) << "ConnectionEstablished";
    QString call("{\"jsonrpc\":\"2.0\","
                        "\"method\":\"server.peers.subscribe\","
                        "\"params\":[], \"id\": 314}");

    m_remote->write(call.toUtf8());
    m_remote->write("\n");
}

void FetchIndexerServicePeers::disconnected()
{
    logFatal(10006) << "disconnected";
    emit finished();
}

void FetchIndexerServicePeers::socketError(QAbstractSocket::SocketError error)
{
    logFatal(10006) << "Peer gave error:" << error;
    m_remote->close();
    m_failScore = 100;
    emit finished();
}

void FetchIndexerServicePeers::socketDataAvailable()
{
    auto length = m_remote->read(m_pool.data(), m_pool.capacity());
    const bool isFullMessage = m_pool.data()[length - 1] == '\n';
    m_pool.markUsed(length);
    if (!isFullMessage) {
        if (m_pool.capacity() < 100) { // message too big.
            m_remote->close();
            m_failScore = 500;
            emit finished();
        }
        return;
    }

    auto data = m_pool.commit();
    QJsonDocument doc;
    doc = QJsonDocument::fromJson(QByteArray(data.begin(), data.size()));
    if (!doc.isObject()) {
        m_remote->close();
        m_failScore = 500; // invalid json
        emit finished();
        return;
    }
    auto o = doc.object();
    if (o.value("id").toInt() == 314) {
        std::deque<IndexerServices::Indexer> newServices;
        auto result_ = o.value("result");
        if (!result_.isArray()) {
            m_failScore = 500; // invalid json
            emit finished();
            return;
        }
        auto result = result_.toArray();
        for (int serverIndex = 0; serverIndex < result.count(); ++serverIndex) {
            auto server_ = result.at(serverIndex);
            if (server_.isArray()) {
                auto server = server_.toArray();
                if (server.count() != 3)
                    continue;
                if (!server.at(2).isArray())
                    continue;

                IndexerServices::Indexer indexer;
                indexer.ip = server.at(0).toString();
                indexer.hostname = server.at(1).toString();
                if (indexer.hostname.size() > 750) // too long
                    continue;
                auto params = server.at(2).toArray();
                for (int i = 0; i < params.count(); ++i) {
                    QString dat = params.at(i).toString();
                    if (dat.startsWith('v')) {
                        auto versionNumbers = dat.mid(1).split('.');
                        while (versionNumbers.size() < 3) versionNumbers.append("0");
                        if (versionNumbers.size() < 5) {
                            for (const auto &ver : std::as_const(versionNumbers)) {
                                uint32_t intValue = ver.toInt();
                                indexer.protocolVersion = indexer.protocolVersion << 8;
                                if (intValue < 256)
                                    indexer.protocolVersion += intValue;
                            }
                        }
                    }
                    else if (dat.startsWith('t')) {
                        if (dat.length() > 1)
                            indexer.port = dat.mid(1).toInt();
                        else
                            indexer.port = 50001;
                    }
                    else if (dat.startsWith('s')) {
                        if (dat.length() > 1)
                            indexer.securePort = dat.mid(1).toInt();
                        else
                            indexer.securePort = 50002;
                    }
                }
                try {
                    auto ip = boost::asio::ip::make_address(indexer.ip.toStdString());
                    Q_UNUSED(ip);
                    // the above throws if not a real IP (but an onion for instance) so we filter our
                    // input based on that simple metric.
                    logDebug(10006) << "Received an address";
                    newServices.push_back(indexer);
                } catch (...) {}
            }
        }
        m_result = newServices;

        emit finished();
    }
}

void FetchIndexerServicePeers::timeout()
{
    if (m_remote->state() == QAbstractSocket::HostLookupState) {
        // DNS slowness.
        // nothing specific to our remote.
    }
    else if (m_remote->state() == QAbstractSocket::ConnectingState) {
        logCritical(10006) << "Watchdog; simple connect too slow, remote unavailable";
        // ok, it should really have finished connecting already.
        m_failScore = 2; // remote may simply not be listening
        m_remote->close();
        emit finished();
    }
}

int FetchIndexerServicePeers::failScore() const
{
    return m_failScore;
}
