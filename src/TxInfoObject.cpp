/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TxInfoObject.h"
#include "TxInfoObject_p.h"

#include "Wallet.h"
#include "Payment.h"

#include <QThread>
#include <QTimer>

#include <chrono>

constexpr int SECTION = 10004;
constexpr int WaitingTimeoutMs = 950;

TxInfoObject::TxInfoObject(Wallet *wallet, const Tx &tx, int walletTxIndex)
    : BroadcastTxData(tx),
    d(new TxInfoPrivate(this)),
    m_status(FloweePay::NotStarted)
{
    assert(wallet);
    d->wallet = wallet;
    if (walletTxIndex < 1) { // lets look it up now!
        auto iter = d->wallet->m_txidCache.find(hash());
        if (iter == d->wallet->m_txidCache.end())
            throw std::runtime_error("Transaction not in wallet");
        walletTxIndex = iter->second;
    }
    d->txIndex = walletTxIndex;

    // this is 'queued' because the wallet may be owning us and may delete us in that slot.
    connect(this, SIGNAL(finished(int,bool)), d->wallet, SLOT(broadcastTxFinished(int,bool)), Qt::QueuedConnection);
}

void TxInfoObject::sentVia(const std::shared_ptr<Peer> &peer)
{
    d->sentVia(peer);
}

void TxInfoObject::txRejected(int connectionId, RejectReason reason, const std::string &message)
{
    d->txRejected(connectionId, reason, message);
}

uint16_t TxInfoObject::privSegment() const
{
    assert(d->wallet);
    assert(d->wallet->segment());
    return d->wallet->segment()->segmentId();
}

int TxInfoObject::txIndex() const
{
    return d->txIndex;
}

void TxInfoObject::setBroadcastStatus(const FloweePay::BroadcastStatus &status)
{
    assert(thread() == QThread::currentThread());
    if (m_status == status)
        return;
    if (m_status < FloweePay::TxSent1 && status >= FloweePay::TxSent1)
        emit sentOne();
    if (m_status < FloweePay::TxRejected && status >= FloweePay::TxRejected)
        emit rejectionSeen();
    m_status = status;
    emit broadcastStatusChanged();
}

FloweePay::BroadcastStatus TxInfoObject::broadcastStatus() const
{
    return m_status;
}


/// ----------------------------------------------

TxInfoPrivate::TxInfoPrivate(TxInfoObject *parent)
    : QObject(parent),
    q(parent)
{
    checkStatusTimer.setInterval(220);
    connect (&checkStatusTimer, SIGNAL(timeout()), this, SLOT(checkState()));
    // the next one is queued because of the jumping of threads.
    connect (this, SIGNAL(broadcastsChanged()),
        this, SLOT(checkState()), Qt::QueuedConnection);
}

TxInfoPrivate::Status TxInfoPrivate::calcStatus() const
{
    QMutexLocker l(&lock);
    Status rc;
    rc.sent = broadcasts.size();
    rc.inProgress = 0;

    const auto time = std::chrono::system_clock::now();
    const auto milliTime = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch());
    const uint64_t cutoff = milliTime.count() - WaitingTimeoutMs;

    for (const auto &broadcast : broadcasts) {
        if (broadcast.failed)
            rc.failed++;
        else if (broadcast.sentTime > cutoff)
            rc.inProgress++;
    }
    return rc;
}

void TxInfoPrivate::sentVia(const std::shared_ptr<Peer> &peer)
{
    assert(peer.get());
     /*
     * This is called directly from a peer's network processing code. Each peer may
     * be on a different 'strand' (effectively a thread) as a result of using NetworkManager,
     * so we lock.
     */
     QMutexLocker l(&lock);
     const auto id = peer->connectionId();
     for (auto iter = broadcasts.begin(); iter != broadcasts.end(); ++iter) {
        if (iter->connectionId == id) // avoid duplicates
            return;
     }

    Broadcast broadcast;
    broadcast.connectionId = id;
    broadcast.ep = peer->peerAddress().peerAddress();
    const auto time = std::chrono::system_clock::now();
    const auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch());
    broadcast.sentTime = millis.count();
    broadcasts.append(broadcast);
    emit broadcastsChanged();
}

void TxInfoPrivate::txRejected(int connectionId, BroadcastTxData::RejectReason reason, const std::string &message)
{
    // reason is hinted at using BroadcastTxData::RejectReason
    logCritical(SECTION) << "Transaction rejected" << reason << message;

    /*
     * This is called directly from a peer's network processing code. Each peer may
     * be on a different 'strand' (effectively a thread) as a result of using NetworkManager,
     * so we lock.
     */
    QMutexLocker l(&lock);
    for (auto iter = broadcasts.begin(); iter != broadcasts.end(); ++iter) {
        if (iter->connectionId == connectionId) {
            if (iter->failed == false) {
                iter->failed = true;
                iter->rejected = reason;
                iter->rejectMsg = message;
                emit broadcastsChanged();
            }
            break;
        }
    }
}

/*
 * The broadcast happens in INV style, and we wait for the peer to then get the data,
 * which we notice with the 'sentOne' call. Called once per peer.
 * But likely only one or maybe two actually call this as the more likely scenario
 * (especially on slower networks) is that they get the transaction from each other.
 *
 * Except when the transaction is bad, and we get a rejected message (but only once)
 * and naturally that once is from each peer.
 * And that is where this method is most useful, to detect that the network rejects
 * our transaction.
 */
void TxInfoPrivate::checkState()
{
    assert(thread() == QThread::currentThread());
    const Status status = calcStatus(); // this uses the mutex. Just so you know..
    logDebug(SECTION) << "sent:" << status.sent << "in progress:" << status.inProgress << "failed:" << status.failed;

    FloweePay::BroadcastStatus bs = FloweePay::TxWaiting;
    if (status.sent == 0)
        bs = FloweePay::TxOffered;
    else if (status.sent > 0)
        bs = FloweePay::TxSent1;
    if (status.failed)
        bs = FloweePay::TxRejected;
    else if (status.sent - status.inProgress >= 2)
        bs = FloweePay::TxBroadcastSuccess;
    q->setBroadcastStatus(bs);

    if (!finished && status.sent >= 1) {
        // a typical wallet has 3 peers.
        // we react when at least 2 downloaded our transaction and didn't report a failure
        if (status.sent - status.inProgress >= 2) {
            emit q->finished(txIndex,
                /* bool success = */ status.sent - status.inProgress - status.failed >= 1);

            finished = true;
            checkStatusTimer.stop();
        }

        if (!finished && !checkStatusTimer.isActive())
            checkStatusTimer.start();
    }
}
