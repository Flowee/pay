#ifndef USERINTENT_H
#define USERINTENT_H

#include <QObject>

/**
 * This class enables OS integration.
 * The front end consumes the data from this class while platform specific handlers
 * set it, using the UserIntents instance as a clearinghouse abstracting away the
 * platform differences.
 */
class UserIntent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString paymentUrl READ paymentUrl WRITE setPaymentUrl NOTIFY paymentUrlChanged FINAL)
    Q_PROPERTY(QString sweepKey READ sweepKey WRITE setSweepKey NOTIFY sweepKeyChanged FINAL)
    Q_PROPERTY(bool startPaymentScanner READ startPaymentScanner WRITE setStartPaymentScanner NOTIFY startPaymentScannerChanged FINAL)
public:
    explicit UserIntent(QObject *parent = nullptr);

    void setPaymentIntent(const QString &string);

    QString paymentUrl() const;
    void setPaymentUrl(const QString &newPaymentUrl);

    QString sweepKey() const;
    void setSweepKey(const QString &wif);

    bool startPaymentScanner() const;
    void setStartPaymentScanner(bool on);

public slots:
    /* To make QML register the properties, emit relevant change signals.
     * This is a bit of a hack to avoid race conditions during startup of
     * the app.
     * The QML will only start listening after a init time, any
     * properties set will just be here but a on_Changed callback
     * won't happen. To avoid lots of QML code, we simple allow emitting them again.
     */
    void emitSignals();

private slots:
    void processNewIntents();

signals:
    void paymentUrlChanged();
    void sweepKeyChanged();
    void startPaymentScannerChanged();

    // internal signal
    void needsEmit();
    // internal signal
    void newIntentString();

private:
    QString m_paymentUrl;
    QString m_sweepKey;
    bool m_startPaymentScanner = false;

    QString m_intentString;
};

#endif
