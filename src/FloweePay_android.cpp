/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FloweePay.h"
#include "IndexerServices.h"
#include "PriceDataProvider.h"
#include "Wallet.h"

#include <QGuiApplication>
#include <QJniObject>
#include <QTimer>

#include <boost/asio/system_timer.hpp>

namespace {
static QTimer *g_checkOfflineTimer = nullptr;
static QTimer *g_updateBackgroundTimer = nullptr;
static boost::asio::system_timer *g_freezeTimer = nullptr;

void checkOnline() {
    QJniEnvironment env;
    jclass floweeNetworks = env.findClass("org/flowee/pay/Networks");
    const jboolean rc = QJniObject::callStaticMethod<jboolean>(floweeNetworks,
            "isInternetAvailable", "(Landroid/content/Context;)Z",
            QJniObject(QNativeInterface::QAndroidApplication::context()));
    auto *fp = FloweePay::instance();
    const bool wasOffline = fp->deviceOffline();
    const bool isOffline = JNI_FALSE == rc;
    fp->setDeviceOffline(isOffline);
    if (wasOffline && !isOffline) { // came online
        fp->startNet();
        fp->prices()->start();
    }
    if (!isOffline && g_checkOfflineTimer) // we're online. Stop the timer
        g_checkOfflineTimer->stop();
}

void backgroundUpdaterToggled()
{
    auto *pay = FloweePay::instance();
    auto main = QJniObject(QNativeInterface::QAndroidApplication::context());
    main.callObjectMethod("enableRegularUpdates", "(I)V",
            pay->backgroundUpdates() ? pay->backgroundUpdateInterval() : 0);
}

void followBGSettings()
{
    if (g_updateBackgroundTimer == nullptr) {
        g_updateBackgroundTimer = new QTimer(FloweePay::instance());
        g_updateBackgroundTimer->setTimerType(Qt::CoarseTimer);
        g_updateBackgroundTimer->setInterval(2000);
        QObject::connect (g_updateBackgroundTimer, &QTimer::timeout, FloweePay::instance(),  [=]() {
            auto *pay = FloweePay::instance();
            int h = 0; // zero is effectively a cancel
            if (pay->backgroundUpdates())
                h = pay->backgroundUpdateInterval();
            auto main = QJniObject(QNativeInterface::QAndroidApplication::context());
            main.callObjectMethod("enableRegularUpdates", "(I)V", h);
            g_updateBackgroundTimer->deleteLater();
            g_updateBackgroundTimer = nullptr;
        });
    }
    g_updateBackgroundTimer->stop();
    g_updateBackgroundTimer->start();
}

void handleFreeze()
{
    auto *pay = FloweePay::instance();
    auto wallets = pay->wallets();
    const jboolean powerSaveOn = QJniObject(QNativeInterface::QAndroidApplication::context())
        .callMethod<jboolean>("isPowerSaveMode", "()Z");
    if (powerSaveOn == JNI_FALSE) {
        for (const auto &wallet : wallets) {
            assert(wallet->segment());
            if (wallet->walletIsImporting()) { // allow running in background
                // check again if it is still running in a while.
                g_freezeTimer->expires_after(std::chrono::seconds(100));
                g_freezeTimer->async_wait([](const boost::system::error_code &error) {
                    if (!error) handleFreeze();
                });
                return;
            }
        }
    }
    for (auto &wallet : wallets) {
        wallet->saveWallet();
    }
    pay->p2pNet()->saveData();
    pay->p2pNet()->setPowerMode(P2PNet::LowPower);
}

}

void FloweePay::setupPlatform()
{
    assert(m_downloadManager.get());
    // ask the Android system which interfaces there are;
    QJniEnvironment env;
    jclass floweeNetworks = env.findClass("org/flowee/pay/Networks");
    quint32 flags = QJniObject::callStaticMethod<jint>(floweeNetworks, "networkSupport", "()I");
    if (flags != 0) {
        logInfo() << "org.flowee.pay.Networks.networkSupport() returns flags:" << flags;
        auto &addressDb = m_downloadManager->connectionManager().peerAddressDb();
        addressDb.setSupportIPv4Net((flags & 1) == 1);
        addressDb.setSupportIPv6Net((flags & 2) == 2);
    }
    checkOnline();

    auto guiApp = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    if (guiApp == nullptr) {
        // this is headless.
        return;
    }

    assert(QThread::currentThread() == thread()); // can't make timers otherwise
    assert(!m_offline); // not available on Android
    assert(!g_checkOfflineTimer);
    g_checkOfflineTimer = new QTimer(this);
    g_checkOfflineTimer->setTimerType(Qt::VeryCoarseTimer);
    g_checkOfflineTimer->setInterval(7000);
    connect (g_checkOfflineTimer, &QTimer::timeout, &checkOnline);
    g_checkOfflineTimer->start(); // but that won't happen for another 7s.

    // on Android, an app is either full screen (active) or inactive and not visible.
    // It is expected we save state as we move to inactive state in order to make the app
    // trivial to kill without loss of data.
    // The above 'aboutToQuit' will not be given enough CPU time to actually save much.
    assert(guiApp);
    static QDateTime g_sleepStart;
    connect(guiApp, &QGuiApplication::applicationStateChanged, this, [=](Qt::ApplicationState state) {
        if (state == Qt::ApplicationInactive || state == Qt::ApplicationSuspended) {
            if (g_sleepStart.isNull()) {
                logInfo() << "App no longer active. Start saving data";
                g_sleepStart = QDateTime::currentDateTimeUtc();
                saveAll();
                p2pNet()->saveData();
                saveData();
                m_indexerServices->save();
                g_checkOfflineTimer->stop();
            }
            /*
             * This is to do powersaving management.
             * When the app is not in the foreground for 90 seconds, we may need to
             * turn off all processing in order to avoid wasting CPU.
             * Notice that we use a boost timer on the IoContext because Qt actually
             * freezes its threads automatically.
             */
            if (g_freezeTimer == nullptr)
                g_freezeTimer = new boost::asio::system_timer(ioContext());
            g_freezeTimer->expires_after(std::chrono::seconds(90));
            g_freezeTimer->async_wait([](const boost::system::error_code &error) {
                if (!error) handleFreeze();
            });
        }
        else if (state == Qt::ApplicationActive) {
            if (g_freezeTimer)
                g_freezeTimer->cancel();
            g_checkOfflineTimer->start();
            checkOnline();
            /*
             * We are brought back to the foreground.
             *
             * On different iterations of Android the behavior can differ quite substantially
             * when it comes to being allowed to continue using resources while not being active.
             * As such there is no real way to know what being inactive has done to our network
             * connections. They may have been kept alive for whatever time we were
             * not active, they may all have been removed or timed out already.
             *
             * What we'll do is to start the actions again which will check up on our connections
             * and create new ones if we need them.
             */
            m_downloadManager->setPowerMode(P2PNet::NormalPower);
            if (!m_deviceOffline && m_loadingCompleted)
                p2pNet()->start();

            if (m_appProtection == AppUnlocked
                    && g_sleepStart.addSecs(60 * 10) < QDateTime::currentDateTimeUtc()) {
                // re-lock the app after 10 minutes of not being in the front.
                setAppProtection(AppPassword);
            }
            g_sleepStart = QDateTime();
        }
    });

    connect (this, &FloweePay::backgroundUpdateIntervalChanged, &followBGSettings);
    connect (this, &FloweePay::backgroundUpdatesChanged, &backgroundUpdaterToggled);
    if (!FloweePay::instance()->deviceOffline())
        backgroundUpdaterToggled(); // make sure that we tell the android side.
}

bool fp_platformSkinDark()
{
    auto context = QJniObject(QNativeInterface::QAndroidApplication::context());
    // In Java:
    //   context.getResources().getConfiguration().uiMode
    auto resources = context.callObjectMethod("getResources",
            "()Landroid/content/res/Resources;");
    auto config = resources.callObjectMethod("getConfiguration",
            "()Landroid/content/res/Configuration;");
    auto uiMode = config.getField<jint>("uiMode");
    constexpr int UI_MODE_NIGHT_MASK = 0x30;
    constexpr int UI_MODE_NIGHT_YES = 0x20;
    const bool dark = (uiMode & UI_MODE_NIGHT_MASK) == UI_MODE_NIGHT_YES;
    return dark;
}
