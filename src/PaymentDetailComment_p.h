/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PAYMENT_DETAIL_COMMENT_H
#define PAYMENT_DETAIL_COMMENT_H

#include "Payment.h"

/*
 * The comment-detail object represents a single op-return output.
 *
 * When the payload is set using setCommentBytes, the detail should be not editable
 * since it makes no sense to provide the user the option to edit a byte-array.
 * The preview() will try to find text content from it, in a lossy manner.
 *
 * User editable objects should use commentStr which is a normal property.
 */
class PaymentDetailComment : public PaymentDetail
{
    Q_OBJECT
    Q_PROPERTY(QString commentString READ commentStr WRITE setCommentStr NOTIFY commentStrChanged FINAL)
    Q_PROPERTY(QString preview READ preview CONSTANT FINAL)
    /**
     * An output created by a payment protocol is not editable by users.
     */
    Q_PROPERTY(bool editable READ editable WRITE setEditable NOTIFY editableChanged FINAL)
    Q_PROPERTY(int size READ size NOTIFY sizeChanged FINAL)
public:
    explicit PaymentDetailComment(Payment *parent);

    QString commentStr() const;
    void setCommentStr(const QString &newCommentStr);

    QString preview() const;

    bool editable() const;
    void setEditable(bool edit);

    void setCommentBytes(const Streaming::ConstBuffer &comment);
    Streaming::ConstBuffer commentBytes() const;

    int size() const;

signals:
    void commentStrChanged();
    void editableChanged();
    void sizeChanged();

private:
    void checkValid();

    bool m_editable = true;
    QString m_commentStr;
    Streaming::ConstBuffer m_commentBytes;
};

inline PaymentDetailComment* PaymentDetail::toComment() {
    assert(m_type == Payment::CommentOutput);
    return static_cast<PaymentDetailComment*>(this);
}

#endif
