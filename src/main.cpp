/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BitcoinValue.h"
#include "FloweePay.h"
#include "NewWalletConfig.h"
#include "NewIndicatorProvider.h"
#include "Payment.h"
#include "PriceDataProvider.h"
#include "TransactionInfo.h"
#include "PortfolioDataProvider.h"
#include "PaymentRequest.h"
#include "PaymentBackend.h"
#include "QRCreator.h"
#include "QMLClipboardHelper.h"
#include "QMLImportHelper.h"
#include "MenuModel.h"
#include "ModuleManager.h"
#include "QRScanner.h"
#ifndef NO_MULTIMEDIA
# include "CameraController.h"
#endif

#include <primitives/PrivateKey.h> // for ECC_Start()

#include <QGuiApplication>
#include <QFontDatabase>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTranslator>

#include <signal.h>


namespace {
void HandleSigTerm(int) {
    QCoreApplication::quit();
}

struct ECC_State
{
    ECC_State() {
        // Init crypto lib.
        ECC_Start();
        globalVerifyHandle.reset(new ECCVerifyHandle());
    }
    ~ECC_State() {
        globalVerifyHandle.reset();
        ECC_Stop();
    }

    std::unique_ptr<ECCVerifyHandle> globalVerifyHandle;
};
}

#ifdef TARGET_OS_Android
# include "Periodic.h"
# include "main_utils_android.cpp"
#else
#include "UserIntent.h"
# include "main_utils.cpp"
#endif


// defined in qml_path_helper.cpp.in
void handleLocalQml(QQmlApplicationEngine &engine);

int main(int argc, char *argv[])
{
#ifdef TARGET_OS_Android
    if (argc > 1 && strcmp(argv[1], "--headless") == 0) {
        const jboolean powerSaveOn = QJniObject(QNativeInterface::QAndroidApplication::context())
            .callMethod<jboolean>("isPowerSaveMode", "()Z");
        if (powerSaveOn == JNI_TRUE)
            return 0;
        Periodic periodic;
        int rc = periodic.run();
        QJniObject(QNativeInterface::QAndroidApplication::context())
            .callObjectMethod("done", "()V");
        return rc;
    }
#endif
    QGuiApplication qapp(argc, argv);
    qapp.setOrganizationName("flowee");
    qapp.setApplicationName("pay");
    qapp.setApplicationVersion("2025.03.0");
    qapp.setWindowIcon(QIcon(":/FloweePay.png"));
    qapp.setDesktopFileName("org.flowee.pay");

    // Clean shutdown on SIGTERM
    struct sigaction sa;
    sa.sa_handler = HandleSigTerm;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(SIGTERM, &sa, nullptr);
    sigaction(SIGINT, &sa, nullptr);
    // Ignore SIGPIPE
    signal(SIGPIPE, SIG_IGN);

    UserIntent paymentIntent;
    setupCallbacks(&paymentIntent);

    qmlRegisterType<WalletEnums>("Flowee.org.pay", 1, 0, "Wallet");
    qmlRegisterType<BitcoinValue>("Flowee.org.pay", 1, 0, "BitcoinValue");
    qmlRegisterType<NewWalletConfig>("Flowee.org.pay", 1, 0, "NewWalletConfig");
    qmlRegisterType<Payment>("Flowee.org.pay", 1, 0, "Payment");
    qmlRegisterType<PaymentRequest>("Flowee.org.pay", 1, 0, "PaymentRequest");
    qmlRegisterType<PaymentBackend>("Flowee.org.pay", 1, 0, "PaymentBackend");
    qmlRegisterType<QMLClipboardHelper>("Flowee.org.pay", 1, 0, "ClipboardHelper");
    qmlRegisterType<QMLImportHelper>("Flowee.org.pay", 1, 0, "ImportHelper");

    auto cld = createCLD(qapp);
    initLogger(cld);

    // load the modules and its translations units first, which gives them the lowest priority
    ModuleManager modules;
    // then the core translations.
    static const char* languagePacks[] = {
#ifdef DESKTOP
        "floweepay-desktop",
#elif MOBILE
        "floweepay-mobile",
#endif
        "floweepay-common",
        nullptr
    };

    for (int i = 0; languagePacks[i]; ++i) {
        auto *translator = new QTranslator(&qapp);
        if (translator->load(QLocale(), languagePacks[i], QLatin1String("_"), QLatin1String(":/i18n")))
            QCoreApplication::installTranslator(translator);
        else
            delete translator;
    }

    ECC_State crypo_state; // allows the secp256k1 to function.
    qmlRegisterType<TransactionInfo>("Flowee.org.pay", 1, 0, "TransactionInfo");
    qmlRegisterType<PaymentRequest>("Flowee.org.pay", 1, 0, "PaymentRequest");
    qmlRegisterUncreatableType<FloweePay>("Flowee.org.pay", 1, 0, "FloweePay", "");

    NewIndicatorProvider newIndicatorProvider;
    MenuModel menuModel(&modules);
    QQmlApplicationEngine engine;
#if QT_VERSION >= QT_VERSION_CHECK(6, 4, 0)
    // quit on error in the QMLs
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed, &qapp, [=]() {
        logFatal() << "QML has errors, aborting on purpose now";
        abort();
    }, Qt::QueuedConnection);
#endif

    auto app = FloweePay::instance();
    engine.rootContext()->setContextProperty("Pay", app);
    if (app->lockFailed()) {
        engine.load("qrc:///desktop/locked.qml");
        return qapp.exec();
    }
    engine.addImageProvider(QLatin1String("qr"), new QRCreator(QRCreator::URLEncoded));
    engine.addImageProvider(QLatin1String("qr-raw"), new QRCreator(QRCreator::RawString));
    engine.rootContext()->setContextProperty("Intent", &paymentIntent);
    engine.rootContext()->setContextProperty("Fiat", app->prices());
    engine.rootContext()->setContextProperty("MenuModel", &menuModel);
    engine.rootContext()->setContextProperty("ModuleManager", &modules);
    engine.rootContext()->setContextProperty("NewIndicatorProvider", &newIndicatorProvider);

    qmlRegisterType<QRScanner>("Flowee.org.pay", 1, 0, "QRScanner");
#ifndef NO_MULTIMEDIA
    CameraController *cc = new CameraController(app);
    app->setCameraController(cc);
    engine.rootContext()->setContextProperty("CameraController", cc);
#endif

    handleLocalQml(engine);
    engine.load(engine.baseUrl().url() +
#ifdef DESKTOP
        "/desktop"
#elif MOBILE
        "/mobile"
#endif
                "/main.qml");

    PortfolioDataProvider *portfolio = nullptr;

    auto blockheaders = handleStaticChain(cld);
    QObject::connect(FloweePay::instance(), &FloweePay::loadComplete, &engine, [&engine, &cld, &portfolio]() {
        if (portfolio == nullptr) {
            // the portfolio can only be properly constructed when the init completed
            portfolio = new PortfolioDataProvider(&engine);
            engine.rootContext()->setContextProperty("portfolio", portfolio);
        }
        if (FloweePay::instance()->appProtection() != FloweePay::AppPassword)
            loadCompleteHandler(engine, cld);
    });
    FloweePay::instance()->startP2PInit();


    // We ship our own font to not have to depend on the host system's installed fonts
    // for 'special' characters like arrows and stars.
    int fontId = QFontDatabase::addApplicationFont(":/Flowee-Symbols.otf");
    if (fontId == -1) {
        logCritical() << "Loading of our symbol font failed!";
    }
    return qapp.exec();
}
