/*
 * This file is part of the Flowee project
 * Copyright (C) 2022-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FloweePay.h"
#include "NetDataProvider.h"
#include "ModuleManager.h"
#include "UserIntent.h"

#include <P2PNet.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFileInfo>
#include <QQmlContext>
#include <QJniObject>

#ifdef NETWORK_LOGGER
# include "NetworkLogClient.h"
#endif

struct CommandLineParserData
{
};

static UserIntent *g_userIntent = nullptr;

void JNI_setPaymentIntent(JNIEnv *env, jobject thiz, jstring data)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    QJniObject intentObject(data);
    g_userIntent->setPaymentIntent(intentObject.toString());
}

void JNI_startQRScanner(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    g_userIntent->setStartPaymentScanner(true);
}

void JNI_notificationPermissionDenied(JNIEnv *env, jobject thiz)
{
    // turn this off if the user doesn't allow android notifications.
    // The UI will reflect this and enabling it will request the
    // permission again.
    FloweePay::instance()->setNewBlockMuted(true);
}


void setupCallbacks(UserIntent *pi)
{
    g_userIntent = pi;
    const JNINativeMethod methods[] = {
        {"setPaymentIntentOnCPP", "(Ljava/lang/String;)V", reinterpret_cast<void*>(JNI_setPaymentIntent)},
        {"startScan", "()V", reinterpret_cast<void*>(JNI_startQRScanner)},
        {"notificationPermissionDenied", "()V", reinterpret_cast<void*>(JNI_notificationPermissionDenied)}
    };
    QJniEnvironment env;
    env.registerNativeMethods("org/flowee/pay/MainActivity", methods, 3);

    // setup the notification channels and data
    jclass notifications = env.findClass("org/flowee/pay/PayNotifications");
    QJniObject::callStaticMethod<void>(notifications, "setup",
            "(Landroid/content/Context;)V", QNativeInterface::QAndroidApplication::context());
}

CommandLineParserData* createCLD(QGuiApplication &app)
{
    return new CommandLineParserData();
}

void initLogger(CommandLineParserData *cld)
{
    auto *logger = Log::Manager::instance();
    Log::Verbosity defaultVerbosity = Log::FatalLevel;
#ifdef NETWORK_LOGGER
    defaultVerbosity =  Log::DebugLevel;
#endif
    logger->clearChannels();
    logger->clearLogLevels(defaultVerbosity);
#ifdef NETWORK_LOGGER
    logger->addChannel(new NetworkLogClient(FloweePay::instance()->ioService()));
#endif
}

std::unique_ptr<QFile> handleStaticChain(CommandLineParserData*)
{
    std::unique_ptr<QFile> blockheaders; // pointer to own the memmapped blockheaders file.

    /*
     * On Android the way to get bigger files into the app is to use the
     * 'assets' subsystem.
     * Android is smart and doesn't actually install those files, just
     * provides an interface to the still compressed archive.
     *
     * Unfortunately, that means we can't memory map them and as such we
     * need to do a one-time copy of those files to our private homedir.
     */
    QFileInfo target("staticHeaders");
    QFileInfo orig("assets:/blockheaders");
    const QString infoFilePath = target.absoluteFilePath() + ".info";

    bool install = orig.exists() && !target.exists();
    if (install) {
        // make sure we have a local copy
        QFile::remove(infoFilePath);
        QFile::remove(target.absoluteFilePath());
        bool ok = QFile::copy(orig.filePath(), target.absoluteFilePath());
        if (!ok) {
            logFatal() << "Failed copying blockheaders";
            abort();
        }
    }
    if (!target.exists()) // We didn't find a source for the headers
        abort();

    blockheaders.reset(new QFile(target.absoluteFilePath()));
    if (!blockheaders->open(QIODevice::ReadOnly)) { // if can't be opened for reading.
        blockheaders.reset();
        return blockheaders;
    }

    // if not previously created, create metadata file now.
    if (!QFile::exists(infoFilePath))
        Blockchain::createStaticHeaders(blockheaders->fileName().toStdString(),
                                        infoFilePath.toStdString());
    Blockchain::setStaticChain(blockheaders->map(0, blockheaders->size()),
                blockheaders->size(), infoFilePath.toStdString());
    blockheaders->close();
    return blockheaders;
}

void loadCompleteHandler(QQmlApplicationEngine &engine, CommandLineParserData *cld)
{
    NetDataProvider *netData = new NetDataProvider(&engine);
    FloweePay *app = FloweePay::instance();
    app->p2pNet()->addP2PNetListener(netData);
    netData->startTimer();

    engine.rootContext()->setContextProperty("net", netData);

    if (g_userIntent) {
        auto myActivity = QJniObject(QNativeInterface::QAndroidApplication::context());
        myActivity.callObjectMethod("onQtAppStarted", "()V");
    }

    app->startNet(); // lets go!
}
