/*
 * This file is part of the Flowee project
 * Copyright (C) 2024-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INDEXERSERVICES_H
#define INDEXERSERVICES_H

#include <QMutex>
#include <QObject>
#include <QString>

#include <NetworkEndPoint.h>
#include <boost/asio.hpp>
#include <deque>

using boost::asio::ip::tcp;

class FetchIndexerServicePeers;

class IndexerServices : public QObject
{
    Q_OBJECT
public:
    explicit IndexerServices(const QString &basedir, boost::asio::io_context &ioContext, QObject *parent = nullptr);

    void populate();

    struct Indexer {
        QString hostname;
        QString ip;
        int port = -1;
        int securePort = -1;
        uint32_t protocolVersion = 0;

        int punishment = 0;
    };

    void save();

    /// return a single, (semi) random service provider.
    EndPoint service() const;

    void punish(const EndPoint &ep, int score);

signals:
    void startMaybeFindServices();

private slots:
    void maybeFindServices();

private:
    void load();

    void onSeedLookupComplete(const boost::system::error_code &error, boost::asio::ip::tcp::resolver::results_type results);

    std::deque<Indexer> m_knownServices;
    tcp::resolver m_resolver;
    mutable QMutex m_mutex;
    QString m_basedir;

    FetchIndexerServicePeers *m_fetcher = nullptr;
};

#endif
