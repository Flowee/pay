<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation>Sin conexión</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation>Monedero: Actualizado</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation>
        <numerusform>Retraso: %1 semana´s, %2 día´s</numerusform>
        <numerusform>Retraso: %1 semanas, %2 días</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation>
        <numerusform>Retraso: %1 día</numerusform>
        <numerusform>Retraso: %1 días</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation>Actualizado</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation>Actualizando</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation>
        <numerusform>Todavía %1 hora de retraso</numerusform>
        <numerusform>Todavía %1 horas de retraso</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation>Este es un monedero de dirección única.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation>Este monedero está basado en una frase-semilla HD</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation>Este es un monedero de múltiples-direcciones simple.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation>propio</translation>
    </message>
  </context>
  <context>
    <name>BigCloseButton</name>
    <message>
      <location filename="../guis/Flowee/BigCloseButton.qml" line="31"/>
      <source>Close</source>
      <translation>Cerrar</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="96"/>
      <source>Sending Payment</source>
      <translation>Enviando Pago</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="98"/>
      <source>Payment Sent</source>
      <translation>Pago Enviado</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="100"/>
      <source>Failed</source>
      <translation>Fallido</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="133"/>
      <source>Transaction rejected by network</source>
      <translation>Transacción rechazada por la red</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="179"/>
      <source>The payment has been sent to:</source>
      <comment>Followed by the address</comment>
      <translation>El pago ha sido enviado a:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="202"/>
      <source>Add a personal note</source>
      <translation>Añadir una nota personal</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation>La moneda se ha fusionado para aumentar el anonimato</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation>Valor ahora</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation>Valor entonces</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="433"/>
      <source>Initial Wallet</source>
      <translation>Monedero inicial</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="615"/>
      <location filename="../src/FloweePay.cpp" line="673"/>
      <source>Today</source>
      <translation>Hoy</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="617"/>
      <location filename="../src/FloweePay.cpp" line="675"/>
      <source>Yesterday</source>
      <translation>Ayer</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="662"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation>Ahora</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="663"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation>
        <numerusform>Hace %1 minuto</numerusform>
        <numerusform>Hace %1 minutos</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="667"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation>Hace ½ hora</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="669"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation>
        <numerusform>Hace %1 hora</numerusform>
        <numerusform>Hace %1 horas</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation>Copiar</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Explore</source>
      <translation>Explorar</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Settings</source>
      <translation>Configuraciones</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Security</source>
      <translation>Seguridad</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>About</source>
      <translation>Acerca de</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>Wallets</source>
      <translation>Monederos</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager.cpp" line="79"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation>%1 nuevas transacciones entre %2 monederos encontradas (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager.cpp" line="81"/>
      <source>A payment of %1 has been sent</source>
      <translation>Un pago de %1 ha sido enviado</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager.cpp" line="82"/>
      <source>%1 new transactions found (%2)</source>
      <translation type="unfinished">
        <numerusform>%1 nuevas transacciones encontradas (%2)</numerusform>
        <numerusform>%1 new transactions found (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="86"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Bloque de Bitcoin Cash minado. Altura: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="88"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>Bloque de tBCH (testnet4) minado: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="107"/>
      <source>Mute</source>
      <translation>Silenciar</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="142"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation>
        <numerusform>Nueva transacción</numerusform>
        <numerusform>Nuevas transacciones</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation>PIN inválido</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation>Monedero bloqueado</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation>No se han seleccionado suficientes fondos para cubrir la comisión</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation>¡El monedero no posee suficientes fondos para procesar el pago!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation>Transacción demasiado larga. La cantidad seleccionada necesita demasiadas monedas.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation>Solicitud recibida por un canal inseguro. ¡Cualquiera podría haberla alterado!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation>Descarga de solicitud de pago fallida.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation>Solicitud de pago ilegible</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation>Solicitud de pago expirada</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation>El límite de pago instantáneo es %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation>Monedero seleccionado: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="109"/>
      <source>Copied to clipboard</source>
      <translation>Copiado al portapapeles</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Copy</source>
      <translation>Copiar</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="85"/>
      <source>Paste</source>
      <translation>Pegar</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation>Pegar</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation>Cambio #%1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation>Principal #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation>Sin confirmar</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation>
        <numerusform>%1 hora</numerusform>
        <numerusform>%1 horas</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation>
        <numerusform>%1 día</numerusform>
        <numerusform>%1 días</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation>
        <numerusform>%1 semana</numerusform>
        <numerusform>%1 semanas</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation>
        <numerusform>%1 mes</numerusform>
        <numerusform>%1 meses</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation>Cambio #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="250"/>
      <source>Today</source>
      <translation>Hoy</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="252"/>
      <source>Yesterday</source>
      <translation>Ayer</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="254"/>
      <source>Earlier this week</source>
      <translation>Anteriormente en esta semana</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="256"/>
      <source>This week</source>
      <translation>Esta semana</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="258"/>
      <source>Earlier this month</source>
      <translation>Anteriormente este mes</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsModel</name>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="83"/>
      <source>Change #%1</source>
      <translation>Cambio #%1</translation>
    </message>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="85"/>
      <source>Main #%1</source>
      <translation>Principal #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation>Explicación</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation>Monedas a / b a) cantidad de monedas en posesión. b) historial de cantidad de monedas.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="162"/>
      <source>Copy Address</source>
      <translation>Copiar la dirección</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="166"/>
      <source>QR of Address</source>
      <translation>QR de la dirección</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="173"/>
      <source>Copy Private Key</source>
      <translation>Copiar clave privada</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="177"/>
      <source>QR of Private Key</source>
      <translation>QR de la clave privada</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="206"/>
      <source>Coins: %1 / %2</source>
      <translation>Monedas: %1 / %2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="228"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation>Firmado con firmas de Schnorr en el pasado</translation>
    </message>
  </context>
</TS>
