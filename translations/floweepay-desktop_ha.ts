<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ha" sourcelanguage="en">
  <context>
    <name>AccountConfigMenu</name>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="26"/>
      <source>Details</source>
      <translation>Bayanai</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Unarchive</source>
      <translation>Cire Takardu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Archive Wallet</source>
      <translation>Ma'ajiyin taskoki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>Make Primary</source>
      <translation>Haɗin farko</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>★ Primary</source>
      <translation>Mafari</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="51"/>
      <source>Protect With Pin...</source>
      <translation>Tsarewa da ɗan makulli...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="58"/>
      <source>Open</source>
      <comment>Open encrypted wallet</comment>
      <translation>Budewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="63"/>
      <source>Close</source>
      <comment>Close encrypted wallet</comment>
      <translation>Kullewa</translation>
    </message>
  </context>
  <context>
    <name>AccountDetails</name>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="76"/>
      <source>Wallet Details</source>
      <translation>Bayanan asusun ajiya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="100"/>
      <source>Name</source>
      <translation>Suna</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="140"/>
      <source>Sync Status</source>
      <translation>Daidaita matsayi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="189"/>
      <source>Encryption</source>
      <translation>Rufewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="200"/>
      <location filename="../guis/desktop/AccountDetails.qml" line="367"/>
      <source>Password</source>
      <translation>Kwadon shiga</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="229"/>
      <source>Open</source>
      <translation>Bude</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="239"/>
      <source>Invalid PIN</source>
      <translation>Makulli Mara inganci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="255"/>
      <source>Include balance in total</source>
      <translation>Haɗa ma'aunin chanji baki ɗaya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="267"/>
      <source>Hide in private mode</source>
      <translation>Ɓoye a yanayin sirri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="277"/>
      <source>Address List</source>
      <translation>Jerin adireshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="283"/>
      <source>Change Addresses</source>
      <translation>Chanjin adireshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="286"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Canjawa tsakanin jerin adireshin da wasu za su iya biyan ku dashi, da adiresoshin da asusun ɗin ke amfani da su don aika canji ga kanku.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="290"/>
      <source>Used Addresses</source>
      <translation>Adireshin da aka yi amfani da shi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="293"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Canzawa tsakanin adiresoshin Bitcoin da akayi amfani da wanda ba'a yi amfani da su ba</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="313"/>
      <source>Backup details</source>
      <translation>Ajiyayyen bayanai</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="330"/>
      <source>Seed-phrase</source>
      <translation>Jimlar iri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="348"/>
      <source>Copy</source>
      <translation>Kwafi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="376"/>
      <source>Seed format</source>
      <translation>Tsarin iri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="387"/>
      <source>Derivation</source>
      <translation>Samo asali</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="400"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case of computer failure.</source>
      <translation>Da fatan za a ajiye jimlar iri akan takarda, cikin tsari mai kyau, tare da hanyar da aka samo asali. Wannan nau'in zai ba ku damar dawo da walat ɗin ku idan akwai gazawar kwamfuta.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="410"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Muhimmanci&lt;/b&gt;: Kada ku taɓa raba jumlar irin ku ga wasu!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="418"/>
      <source>This wallet is protected by password (pin-to-pay). To see the backup details you need to provide the password.</source>
      <translation>Wannan asusun na tsare da almar sirri (pin-to-pay). Don ganin bayanan ciki akwai buƙatar samar da kalmar wucewa.</translation>
    </message>
  </context>
  <context>
    <name>AccountListItem</name>
    <message>
      <location filename="../guis/desktop/AccountListItem.qml" line="144"/>
      <source>Last Transaction</source>
      <translation>Ciniki na ƙarshe</translation>
    </message>
  </context>
  <context>
    <name>ActivityConfigBar</name>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="66"/>
      <source>Filter</source>
      <translation type="unfinished">Filter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="156"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="197"/>
      <source>Received</source>
      <translation>An samu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="175"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="204"/>
      <source>Sent</source>
      <translation>An aika</translation>
    </message>
  </context>
  <context>
    <name>AddressDbStats</name>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="30"/>
      <source>IP Addresses</source>
      <translation type="unfinished">IP Addresses</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="49"/>
      <source>Total found</source>
      <translation type="unfinished">Total found</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="57"/>
      <source>Tried</source>
      <translation type="unfinished">Tried</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="64"/>
      <source>Punished count</source>
      <translation type="unfinished">Punished count</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="71"/>
      <source>Banned count</source>
      <translation type="unfinished">Banned count</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="79"/>
      <source>IP-v4 count</source>
      <translation type="unfinished">IP-v4 count</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="87"/>
      <source>IP-v6 count</source>
      <translation type="unfinished">IP-v6 count</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="97"/>
      <source>Pardon the Banned</source>
      <translation type="unfinished">Pardon the Banned</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="123"/>
      <source>Close</source>
      <translation>Kulle</translation>
    </message>
  </context>
  <context>
    <name>NetView</name>
    <message numerus="yes">
      <location filename="../guis/desktop/NetView.qml" line="31"/>
      <source>Peers (%1)</source>
      <translation>
        <numerusform>Takwarori (%1)</numerusform>
        <numerusform>Takwarori (%1)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="83"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adireshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="90"/>
      <source>Start-height: %1</source>
      <translation>Fara-tsawo: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="93"/>
      <source>ban-score: %1</source>
      <translation>Tsame-ci: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="116"/>
      <source>Peer for wallet: %1</source>
      <translation>Haɗin asusu %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="139"/>
      <source>Disconnect Peer</source>
      <translation type="unfinished">Disconnect Peer</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="143"/>
      <source>Ban Peer</source>
      <translation type="unfinished">Ban Peer</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="162"/>
      <source>Close</source>
      <translation>Kulle</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateBasicAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="39"/>
      <source>Create a new empty wallet with simple multi-address capability </source>
      <translation>Wannan yana haifar da sabon asusu mara komai tare da sauƙin adireshi da yawa </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="48"/>
      <source>Name</source>
      <translation>Suna</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="59"/>
      <source>Force Single Address</source>
      <translation>Tilasta Adireshi Guda Daya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="60"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Lokacin da aka kunna, wannan asusun za'a iyakance shi ga adireshi ɗaya. Wannan yana tabbatar da maɓallin keɓaɓɓen maɓalli ɗaya ne kawai zai buƙaci a yi masa tallafi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="67"/>
      <source>Go</source>
      <translation>Tafi</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateHDAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="33"/>
      <source>Create a new wallet with smart creation of addresses from a single seed-phrase</source>
      <translation>Wannan yana haifar da sabon asusu mara komai tare da ƙirƙirar adireshi masu wayo daga jimla iri ɗaya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="41"/>
      <source>Name</source>
      <translation>Suna</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="55"/>
      <source>Go</source>
      <translation>Tafi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="71"/>
      <source>Advanced Options</source>
      <translation>Zaɓuɓɓuka na ci gaba</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="76"/>
      <source>Derivation</source>
      <translation>Samo asali</translation>
    </message>
  </context>
  <context>
    <name>NewAccountImportAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="90"/>
      <source>Select import method</source>
      <translation type="unfinished">Select import method</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="103"/>
      <source>Camera</source>
      <translation type="unfinished">Camera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="142"/>
      <source>OR</source>
      <translation>Ko</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="154"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation type="unfinished">Secret as text</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="194"/>
      <source>Unknown word(s) found</source>
      <translation type="unfinished">Unknown word(s) found</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="217"/>
      <source>Address to import</source>
      <translation type="unfinished">Address to import</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="232"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="353"/>
      <source>New Wallet Name</source>
      <translation type="unfinished">New Wallet Name</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="252"/>
      <source>Force Single Address</source>
      <translation>Tilasta Adireshi Guda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="253"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Lokacin da aka kunna, ba za'a samar da ƙarin adireshi ta atomatik a cikin wannan wallet ɗin ba. Canji zai dawo kan maɓallin da aka shigo da shi.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="258"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="414"/>
      <source>Oldest Transaction</source>
      <translation>Tsohon ciniki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="266"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation type="unfinished">Check Age</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="310"/>
      <source>Nothing found for wallet</source>
      <translation type="unfinished">Nothing found for wallet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="317"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="461"/>
      <source>Start</source>
      <translation type="unfinished">Start</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="373"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation type="unfinished">Discover Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="425"/>
      <source>Derivation</source>
      <translation>Samo asali</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="439"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation type="unfinished">Nothing found for seed. Does it have a password?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="447"/>
      <source>Password</source>
      <translation>Kwadon shiga</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="454"/>
      <source>imported wallet password</source>
      <translation type="unfinished">imported wallet password</translation>
    </message>
  </context>
  <context>
    <name>NewAccountPane</name>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="64"/>
      <source>New HD wallet</source>
      <translation type="unfinished">New HD wallet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="70"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Tushen jimlar iri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="71"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Sauƙi wurin dawo da ajiya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="72"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Mafi dacewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="77"/>
      <source>Import Existing Wallet</source>
      <translation>Shigo da asusun da ke akwai</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="83"/>
      <source>Imports seed-phrase</source>
      <translation>Shigo da jumla iri-iri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="84"/>
      <source>Imports private key</source>
      <translation>Shigo da maɓalli na sirri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="90"/>
      <source>New Basic Wallet</source>
      <translation type="unfinished">New Basic Wallet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="95"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Maɓallai masu zaman kansu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="96"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Wahalar dawo da ajiya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="97"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Mai girma don taƙaitaccen amfani</translation>
    </message>
  </context>
  <context>
    <name>PaymentTweakingPanel</name>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="80"/>
      <source>Add Detail</source>
      <translation>Ƙara Dalla-dalla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="94"/>
      <source>Coin Selector</source>
      <translation>Zaɓin Tsabar kudi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="104"/>
      <source>To override the default selection of coins that are used to pay a transaction, you can add the &apos;Coin Selector&apos; where the wallets coins will be made visible.</source>
      <translation>Domin soke tsoffin zaɓin tsabar kudi waɗanda ake amfani da su don yin mu'amala, zaku iya ƙara &apos;tsabar kudi r&apos; inda tsabar kudi inda za'a iya gani.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="107"/>
      <source>Comment</source>
      <translation type="unfinished">Comment</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="117"/>
      <source>This allows adding a public comment, that will be included in the transaction and seen by everyone.</source>
      <translation type="unfinished">This allows adding a public comment, that will be included in the transaction and seen by everyone.</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTransactionPane</name>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="51"/>
      <source>Share your QR code or copy address to receive</source>
      <translation>Bada lambar QR ɗinku ko a kwafi adireshin don karɓa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="68"/>
      <source>Encrypted Wallet</source>
      <translation>Rufaffen asusu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="70"/>
      <source>Import Running...</source>
      <translation>Tsarin Shiga na gudu...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="146"/>
      <source>Checking</source>
      <translation>Dubawa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="167"/>
      <source>High risk transaction</source>
      <translation type="unfinished">High risk transaction</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="169"/>
      <source>Payment Seen</source>
      <translation>Anga shaidar biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="171"/>
      <source>Payment Accepted</source>
      <translation>An Karɓa Biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="173"/>
      <source>Payment Settled</source>
      <translation>Biya An daidaita</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="191"/>
      <source>Instant payment failed. Wait for confirmation. (double spent proof received)</source>
      <translation>Biyan nan take ya kasa. Jira tabbaci. (Anga shaida guda biyu da aka kashe)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="209"/>
      <source>Description</source>
      <translation>Bayani</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="221"/>
      <source>Amount</source>
      <translation>Adadi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Clear</source>
      <translation>Share</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Done</source>
      <translation>An gama</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionPane</name>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="125"/>
      <source>Confirm delete</source>
      <translation>Tabbatar da gogewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="126"/>
      <source>Do you really want to delete this detail?</source>
      <translation>Shin kuna son share wannan dalla-dalla?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="135"/>
      <source>Add Destination</source>
      <translation>Ƙara madakata</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="141"/>
      <source>Prepare</source>
      <translation>Shirya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="157"/>
      <source>Enter your PIN</source>
      <translation>Shigar da lambar tsaro</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="166"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="454"/>
      <source>Warning</source>
      <translation>Gargaɗi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="173"/>
      <source>Payment request warnings:</source>
      <translation>Gargadin neman biyan kuɗi:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="191"/>
      <source>Transaction Details</source>
      <translation>Cikakken Bayanin Kasuwanci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="205"/>
      <source>Not prepared yet</source>
      <translation>Ba'a shirya ba tukuna</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="211"/>
      <source>Copy transaction-ID</source>
      <translation>Kwafi shaidar ma'amala-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="214"/>
      <source>Fee</source>
      <translation>Kudin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="224"/>
      <source>Transaction size</source>
      <translation>Girman ciniki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="232"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="237"/>
      <source>Fee per byte</source>
      <translation>Kudin kowane byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="248"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="262"/>
      <source>Send</source>
      <translation>Aika</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="320"/>
      <source>Destination</source>
      <translation>Madakata</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="327"/>
      <source>Max available</source>
      <comment>The maximum balance available</comment>
      <translation>Mafi girman samuwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="336"/>
      <source>%1 to %2</source>
      <comment>summary text to pay X-euro to address M</comment>
      <translation>%1 to %2</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="354"/>
      <source>Enter Bitcoin Cash Address</source>
      <translation>Shigar da Adireshin Kuɗi na Bitcoin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="379"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="666"/>
      <source>Copy Address</source>
      <translation>Kwafi Adireshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="389"/>
      <source>Amount</source>
      <translation>Adadi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="413"/>
      <source>Max</source>
      <translation>Matsakaici</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="459"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Wannan adireshin BTC ne, wanda tsabar kudin da bai dace ba. Kuɗin ku na iya yin asara kuma Flowee ba za ta sami hanyar dawo da su ba. Shin kun tabbata wannan shine daidai adireshin?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="471"/>
      <source>Continue</source>
      <translation>Ci gaba</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="475"/>
      <source>Cancel</source>
      <translation>Soke</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="496"/>
      <source>Coin Selector</source>
      <translation>Zaɓin Tsabar kudi</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/SendTransactionPane.qml" line="497"/>
      <source>Selected %1 %2 in %3 coins</source>
      <comment>selected 2 BCH in 5 coins</comment>
      <translation>
        <numerusform>An zaɓi %1 %2 a cikin %3 tsabar kudi</numerusform>
        <numerusform>An zaɓi %1 %2 a cikin %3 tsabar kudi</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="508"/>
      <source>Total</source>
      <comment>Number of coins</comment>
      <translation>Jimilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="515"/>
      <source>Needed</source>
      <translation>Ake Bukata</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="525"/>
      <source>Selected</source>
      <translation>An zaɓa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="532"/>
      <source>Value</source>
      <translation>Daraja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="570"/>
      <source>Locked coins will never be used for payments. Right-click for menu.</source>
      <translation>Kullallun tsabar kudi ba za a taɓa amfani da su don biyan kuɗi ba. Danna-dama don menu.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="609"/>
      <source>Age</source>
      <translation>Shekaru</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Unselect All</source>
      <translation>Cire Zaɓi Duk</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Select All</source>
      <translation>Zaɓi Duk</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Unlock coin</source>
      <translation>Buɗe tsabar kudi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Lock coin</source>
      <translation>Kulle tsabar kudi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="694"/>
      <source>Public-comment</source>
      <translation type="unfinished">Public-comment</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="702"/>
      <source>Add a comment you want to include in the transaction, visible for everyone.</source>
      <translation type="unfinished">Add a comment you want to include in the transaction, visible for everyone.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="703"/>
      <source>Custom message, to be included in the transaction.</source>
      <translation type="unfinished">Custom message, to be included in the transaction.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="708"/>
      <source>Text</source>
      <translation type="unfinished">Text</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="718"/>
      <source>Size</source>
      <translation>Girman</translation>
    </message>
  </context>
  <context>
    <name>SettingsPane</name>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="26"/>
      <source>Settings</source>
      <translation>Saituna</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="37"/>
      <source>Unit</source>
      <translation>Naúrar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="107"/>
      <source>Show Bitcoin Cash value on Activity page</source>
      <translation>Nuna ƙimar Bitcoin Cash akan shafin Aiki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="120"/>
      <source>Show Block Notifications</source>
      <translation>Nuna Sanarwa da ta toshe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="121"/>
      <source>When a new block is mined, Flowee Pay shows a desktop notification</source>
      <translation>Lokacin da aka haƙa sabon toshe, Flowee Pay yana nuna sanarwar tebur</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="135"/>
      <source>Night Mode</source>
      <translation>Yanayin dare</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="146"/>
      <source>Private Mode</source>
      <translation>Yanayin sirri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="147"/>
      <source>Hides private wallets while enabled</source>
      <translation>Ɓoye sirrin asusu yayin kunnawa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="153"/>
      <source>Font sizing</source>
      <translation>Girman haruffa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="194"/>
      <source>Version</source>
      <translation>Siga</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="202"/>
      <source>Library Version</source>
      <translation>Sigar Laburare</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="211"/>
      <source>Synchronization</source>
      <translation>Haɗa Aiki tare</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="216"/>
      <source>Network Status</source>
      <translation>Matsayin hanyar sadarwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="226"/>
      <source>Address Stats</source>
      <translation type="unfinished">Address Stats</translation>
    </message>
  </context>
  <context>
    <name>Transaction</name>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="65"/>
      <source>Miner Reward</source>
      <translation>Ladan Ma'adinai</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="67"/>
      <source>Fused</source>
      <translation>Fuskanci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="69"/>
      <source>Received</source>
      <translation>An samu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="71"/>
      <source>Moved</source>
      <translation>Motsa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="72"/>
      <source>Sent</source>
      <translation>An aika</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="80"/>
      <source>rejected</source>
      <translation>an ƙii</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="31"/>
      <source>Transaction Details</source>
      <translation>Cikakken Bayanin Kasuwanci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="101"/>
      <source>First Seen</source>
      <translation type="unfinished">First Seen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="115"/>
      <source>Rejected</source>
      <translation>An ƙii</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="117"/>
      <source>Mined at</source>
      <translation>An haƙo daga</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionDetails.qml" line="134"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 tubalin da suka gabata</numerusform>
        <numerusform>%1 tubalan da suka wuce</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="141"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="146"/>
      <source>Comment</source>
      <translation type="unfinished">Comment</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="163"/>
      <source>Fees paid</source>
      <translation>An biya kudade</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="169"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="184"/>
      <source>Size</source>
      <translation>Girman</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="188"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="195"/>
      <source>Is Coinbase</source>
      <translation type="unfinished">Is Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="205"/>
      <source>Copy transaction-ID</source>
      <translation>Kwafi shaidar ma'amala-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="219"/>
      <source>Fused from my addresses</source>
      <translation>An haɗe daga adiresoshin na</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="221"/>
      <source>Sent from my addresses</source>
      <translation>An aiko daga adiresoshin na</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="223"/>
      <source>Sent from addresses</source>
      <translation>An aiko daga adiresoshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="281"/>
      <source>Fused into my addresses</source>
      <translation>An haɗe cikin adiresoshin na</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="283"/>
      <source>Received at addresses</source>
      <translation>An karɓa a adiresoshi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="284"/>
      <source>Received at my addresses</source>
      <translation>An karɓa a adireshi na</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="44"/>
      <source>Transaction is rejected</source>
      <translation>An ƙi ma'amalar ciniki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="46"/>
      <source>Processing</source>
      <translation>Sarrafawa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="59"/>
      <source>Mined</source>
      <translation>Haƙar ma'adinai</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="70"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 tubalin da suka gabata</numerusform>
        <numerusform>%1 tubalan da suka wuce</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="106"/>
      <source>Fees</source>
      <translation>Kudin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="121"/>
      <source>Copy transaction-ID</source>
      <translation>Kwafi shaidar ma'amala-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="146"/>
      <source>Holds a token</source>
      <translation>Rike alama</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="168"/>
      <source>Opening Website</source>
      <translation>Buɗe Yanar Gizon</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryption</name>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="63"/>
      <source>Protect your wallet with a password</source>
      <translation>Kare asusun ku da kalmar sirri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="88"/>
      <source>Pin to Pay</source>
      <translation>Matsa lamba don biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="93"/>
      <source>Protect your funds</source>
      <comment>pin to pay</comment>
      <translation>Kare kuɗin ku</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="94"/>
      <source>Fully open, except for sending funds</source>
      <comment>pin to pay</comment>
      <translation>Cikakken buɗewa, Sai dai don aika kudi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="95"/>
      <source>Keeps in sync</source>
      <comment>pin to pay</comment>
      <translation>Yana ci gaba da daidaitawa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="100"/>
      <source>Pin to Open</source>
      <translation>Pin don Buɗe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="105"/>
      <source>Protect your entire wallet</source>
      <comment>pin to open</comment>
      <translation>Kare dukkan asusun ku</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="106"/>
      <source>Balance and history protected</source>
      <comment>pin to open</comment>
      <translation>Ma'auni da tarihi a tsare suke</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="107"/>
      <source>Requires Pin to view, sync or pay</source>
      <comment>pin to open</comment>
      <translation>Da buƙatar Pin don dubawa, daidaitawa ko biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="117"/>
      <source>Make &quot;%1&quot; wallet require a pin to pay</source>
      <translation>Yi &quot;%1&quot; asusu yana buƙatar Pin don biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="118"/>
      <source>Make &quot;%1&quot; wallet require a pin to open</source>
      <translation>Yi &quot;%1&quot; asusu yana buƙatar Pin don buɗewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="130"/>
      <location filename="../guis/desktop/WalletEncryption.qml" line="142"/>
      <source>Wallet already has pin to open applied</source>
      <translation>Asusun tuni yana da Pin don buɗewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="132"/>
      <source>Wallet already has pin to pay applied</source>
      <translation>Asusun tuni yana da Pin don biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="133"/>
      <source>Your wallet will get partially encrypted and payments will become impossible without a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Asusun ku zai sami rufaffen ɓangarori kuma biyan kuɗi zai yi wuya ba tare da kalmar sirri ba. Idan babu ajiyar wannan asusun ɗin, fara yin ɗaya.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="143"/>
      <source>Your full wallet gets encrypted, opening it will need a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Cikakken asusun ku yana ɓoye, domin buɗe shi za'a buƙaci kalmar sirri. Idan ba ku da ajiyar wannan asusun, fara yin ɗaya.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="169"/>
      <source>Password</source>
      <translation>Kwadon shiga</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="192"/>
      <source>Wallet</source>
      <translation>Asusu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="214"/>
      <source>Encrypt</source>
      <translation>Rufewa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="221"/>
      <source>Invalid password to open this wallet</source>
      <translation>Kalmar sirri mara inganci don buɗe wannan asusu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="233"/>
      <source>Close</source>
      <translation>Kulle</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="243"/>
      <source>Repeat password</source>
      <translation>Maimaita kalmar sirri</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="244"/>
      <source>Please confirm the password by entering it again</source>
      <translation>Da fatan za a tabbatar da kalmar wucewa ta hanyar sake shigar da shi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="249"/>
      <source>Typed passwords do not match</source>
      <translation>Rubutun kalmomin shiga ba su dace ba</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryptionStatus</name>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="46"/>
      <source>Pin to Open</source>
      <translation>Pin don Buɗe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="48"/>
      <source>Pin to Pay</source>
      <translation>Matsa lamba don biya</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="50"/>
      <source>(Opened)</source>
      <comment>Wallet is decrypted</comment>
      <translation>(An buɗe)</translation>
    </message>
  </context>
  <context>
    <name>locked</name>
    <message>
      <location filename="../guis/desktop/locked.qml" line="51"/>
      <source>Already running?</source>
      <translation type="unfinished">Already running?</translation>
    </message>
  </context>
  <context>
    <name>main</name>
    <message>
      <location filename="../guis/desktop/main.qml" line="210"/>
      <source>Activity</source>
      <translation>Ayyuka</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="250"/>
      <source>Archived wallets do not check for activities. Balance may be out of date.</source>
      <translation>Asusun ɗin da aka adana ba sa bincika ayyuka. Ma'auni na iya zama ya tsufa.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="254"/>
      <source>Unarchive</source>
      <translation>Cire Takardu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="287"/>
      <source>This wallet needs a password to open.</source>
      <translation>Wannan asusun tana buƙatar kalmar sirri don buɗewa.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="295"/>
      <source>Password:</source>
      <translation>Kalmar wucewa:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="315"/>
      <source>Invalid password</source>
      <translation>Kalmar shiga mara inganci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="320"/>
      <source>Open</source>
      <translation>Bude</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="398"/>
      <source>Send</source>
      <translation>Aika</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="406"/>
      <source>Receive</source>
      <translation>Karɓa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="525"/>
      <source>Balance</source>
      <translation>Sauran kudi</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="594"/>
      <source>Main</source>
      <comment>balance (money), non specified</comment>
      <translation>Babban</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="604"/>
      <source>Unconfirmed</source>
      <comment>balance (money)</comment>
      <translation>Ba'a tabbatar ba</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="613"/>
      <source>Immature</source>
      <comment>balance (money)</comment>
      <translation>Rashin cika</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="662"/>
      <source>1 BCH is: %1</source>
      <translation>1 BCH shi ne: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="709"/>
      <source>Network status</source>
      <translation>Matsayin hanyar sadarwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="723"/>
      <source>Offline</source>
      <translation>Baya kan na'ura</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="747"/>
      <source>Add Bitcoin Cash wallet</source>
      <translation>Sanya asusun Bitcoin Cash</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/main.qml" line="778"/>
      <source>Archived wallets [%1]</source>
      <comment>Arg is wallet count</comment>
      <translation>
        <numerusform>Asusun da aka adana [%1]</numerusform>
        <numerusform>Asusun da aka adana [%1]</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="808"/>
      <source>Preparing...</source>
      <translation>Shiryawa...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="891"/>
      <source>QR-Scan</source>
      <translation type="unfinished">QR-Scan</translation>
    </message>
  </context>
</TS>
