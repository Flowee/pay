<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation type="unfinished">Offline</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation type="unfinished">Wallet: Up to date</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation type="unfinished">
        <numerusform>Behind: %1 week, %2 days</numerusform>
        <numerusform>Behind: %1 weeks, %2 days</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation type="unfinished">
        <numerusform>Behind: %1 day</numerusform>
        <numerusform>Behind: %1 days</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation type="unfinished">Up to date</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation type="unfinished">Updating</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation type="unfinished">
        <numerusform>Still an hour behind</numerusform>
        <numerusform>Still %1 hours behind</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation type="unfinished">This wallet is a single-address wallet.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation type="unfinished">This wallet is based on a HD seed-phrase</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation type="unfinished">This wallet is a simple multiple-address wallet.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation type="unfinished">self</translation>
    </message>
  </context>
  <context>
    <name>BigCloseButton</name>
    <message>
      <location filename="../guis/Flowee/BigCloseButton.qml" line="31"/>
      <source>Close</source>
      <translation type="unfinished">Close</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="96"/>
      <source>Sending Payment</source>
      <translation type="unfinished">Sending Payment</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="98"/>
      <source>Payment Sent</source>
      <translation type="unfinished">Payment Sent</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="100"/>
      <source>Failed</source>
      <translation type="unfinished">Failed</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="133"/>
      <source>Transaction rejected by network</source>
      <translation type="unfinished">Transaction rejected by network</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="179"/>
      <source>The payment has been sent to:</source>
      <comment>Followed by the address</comment>
      <translation type="unfinished">The payment has been sent to:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="202"/>
      <source>Add a personal note</source>
      <translation type="unfinished">Add a personal note</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation type="unfinished">Coin has been fused for increased anonymity</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation type="unfinished">Value now</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation type="unfinished">Value then</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="433"/>
      <source>Initial Wallet</source>
      <translation type="unfinished">Initial Wallet</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="615"/>
      <location filename="../src/FloweePay.cpp" line="673"/>
      <source>Today</source>
      <translation type="unfinished">Today</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="617"/>
      <location filename="../src/FloweePay.cpp" line="675"/>
      <source>Yesterday</source>
      <translation type="unfinished">Yesterday</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="662"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation type="unfinished">Now</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="663"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation type="unfinished">
        <numerusform>A minute ago</numerusform>
        <numerusform>%1 minutes ago</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="667"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation type="unfinished">½ hour ago</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="669"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation type="unfinished">
        <numerusform>An hour ago</numerusform>
        <numerusform>%1 hours ago</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Explore</source>
      <translation type="unfinished">Explore</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Settings</source>
      <translation type="unfinished">Settings</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Security</source>
      <translation type="unfinished">Security</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>About</source>
      <translation type="unfinished">About</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>Wallets</source>
      <translation type="unfinished">Wallets</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager.cpp" line="79"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation type="unfinished">%1 new transactions across %2 wallets found (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager.cpp" line="81"/>
      <source>A payment of %1 has been sent</source>
      <translation type="unfinished">A payment of %1 has been sent</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager.cpp" line="82"/>
      <source>%1 new transactions found (%2)</source>
      <translation type="unfinished">
        <numerusform>%1 new transactions found (%2)</numerusform>
        <numerusform>%1 new transactions found (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="86"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation type="unfinished">Bitcoin Cash block mined. Height: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="88"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation type="unfinished">tBCH (testnet4) block mined: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="107"/>
      <source>Mute</source>
      <translation type="unfinished">Mute</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="142"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation type="unfinished">
        <numerusform>New Transactions</numerusform>
        <numerusform>New Transactions</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation type="unfinished">Invalid PIN</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation type="unfinished">Wallet is locked</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation type="unfinished">Not enough funds selected for fees</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation type="unfinished">Not enough funds in wallet to make payment!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation type="unfinished">Transaction too large. Amount selected needs too many coins.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation type="unfinished">Request received over insecure channel. Anyone could have altered it!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation type="unfinished">Download of payment request failed.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation type="unfinished">Payment request unreadable</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation type="unfinished">Payment request expired</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation type="unfinished">Instant Pay limit is %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation type="unfinished">Selected wallet: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="109"/>
      <source>Copied to clipboard</source>
      <translation type="unfinished">Copied to clipboard</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="85"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation type="unfinished">Change #%1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation type="unfinished">Main #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation type="unfinished">Unconfirmed</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation type="unfinished">
        <numerusform>an hour</numerusform>
        <numerusform>%1 hours</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation type="unfinished">
        <numerusform>%1 day</numerusform>
        <numerusform>%1 days</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation type="unfinished">
        <numerusform>%1 week</numerusform>
        <numerusform>%1 weeks</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation type="unfinished">
        <numerusform>%1 month</numerusform>
        <numerusform>%1 months</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation type="unfinished">Change #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="250"/>
      <source>Today</source>
      <translation type="unfinished">Today</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="252"/>
      <source>Yesterday</source>
      <translation type="unfinished">Yesterday</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="254"/>
      <source>Earlier this week</source>
      <translation type="unfinished">Earlier this week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="256"/>
      <source>This week</source>
      <translation type="unfinished">This week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="258"/>
      <source>Earlier this month</source>
      <translation type="unfinished">Earlier this month</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsModel</name>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="83"/>
      <source>Change #%1</source>
      <translation type="unfinished">Change #%1</translation>
    </message>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="85"/>
      <source>Main #%1</source>
      <translation type="unfinished">Main #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation type="unfinished">Explanation</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation type="unfinished">Coins a / b
 a) active coin-count.
 b) historical coin-count.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="162"/>
      <source>Copy Address</source>
      <translation type="unfinished">Copy Address</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="166"/>
      <source>QR of Address</source>
      <translation type="unfinished">QR of Address</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="173"/>
      <source>Copy Private Key</source>
      <translation type="unfinished">Copy Private Key</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="177"/>
      <source>QR of Private Key</source>
      <translation type="unfinished">QR of Private Key</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="206"/>
      <source>Coins: %1 / %2</source>
      <translation type="unfinished">Coins: %1 / %2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="228"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation type="unfinished">Signed with Schnorr signatures in the past</translation>
    </message>
  </context>
</TS>
