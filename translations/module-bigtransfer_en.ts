<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="en">
  <context>
    <name>BigTransferModuleInfo</name>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="33"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="38"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="44"/>
      <source>Wallet to Wallet</source>
      <translation type="unfinished">Wallet to Wallet</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="34"/>
      <source>Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses.</source>
      <translation type="unfinished">Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="39"/>
      <source>Move funds to another wallet</source>
      <translation type="unfinished">Move funds to another wallet</translation>
    </message>
  </context>
  <context>
    <name>Main</name>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="28"/>
      <source>Wallet to Wallet</source>
      <translation type="unfinished">Wallet to Wallet</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="38"/>
      <source>Select two wallets to transfer funds simply, using anonimity preserving transactions.</source>
      <translation type="unfinished">Select two wallets to transfer funds simply, using anonimity preserving transactions.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="42"/>
      <source>Spending Wallet</source>
      <translation type="unfinished">Spending Wallet</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="54"/>
      <source>Addresses</source>
      <translation type="unfinished">Addresses</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="60"/>
      <source>Coins</source>
      <translation type="unfinished">Coins</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="68"/>
      <source>Destination Wallet</source>
      <translation type="unfinished">Destination Wallet</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="80"/>
      <source>Prepare...</source>
      <translation type="unfinished">Prepare...</translation>
    </message>
  </context>
  <context>
    <name>ShowPrepared</name>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="29"/>
      <source>Verify %1 Transactions</source>
      <translation type="unfinished">
        <numerusform>Verify Transaction</numerusform>
        <numerusform>Verify %1 Transactions</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="103"/>
      <source>Coins</source>
      <translation type="unfinished">Coins</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="190"/>
      <source>Send Now</source>
      <translation type="unfinished">Send Now</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="205"/>
      <source>Create and send all %1 transactions</source>
      <translation type="unfinished">
        <numerusform>Create and send the transaction</numerusform>
        <numerusform>Create and send all %1 transactions</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="282"/>
      <source>Target Coins</source>
      <comment>indicates a number</comment>
      <translation type="unfinished">Target Coins</translation>
    </message>
  </context>
</TS>
