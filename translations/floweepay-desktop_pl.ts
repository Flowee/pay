<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>AccountConfigMenu</name>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="26"/>
      <source>Details</source>
      <translation>Szczegóły</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Unarchive</source>
      <translation>Przywróć</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Archive Wallet</source>
      <translation>Zarchiwizuj portfel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>Make Primary</source>
      <translation>Ustaw jako główny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>★ Primary</source>
      <translation>★ Główny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="51"/>
      <source>Protect With Pin...</source>
      <translation>Chroń za pomocą pinu...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="58"/>
      <source>Open</source>
      <comment>Open encrypted wallet</comment>
      <translation>Otwórz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="63"/>
      <source>Close</source>
      <comment>Close encrypted wallet</comment>
      <translation>Zamknij</translation>
    </message>
  </context>
  <context>
    <name>AccountDetails</name>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="76"/>
      <source>Wallet Details</source>
      <translation>Szczegóły portfela</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="100"/>
      <source>Name</source>
      <translation>Nazwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="140"/>
      <source>Sync Status</source>
      <translation>Status synchronizacji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="189"/>
      <source>Encryption</source>
      <translation>Szyfrowanie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="200"/>
      <location filename="../guis/desktop/AccountDetails.qml" line="367"/>
      <source>Password</source>
      <translation>Hasło</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="229"/>
      <source>Open</source>
      <translation>Otwórz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="239"/>
      <source>Invalid PIN</source>
      <translation>Nieprawidłowy PIN</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="255"/>
      <source>Include balance in total</source>
      <translation>Uwzględnij saldo w podsumowaniu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="267"/>
      <source>Hide in private mode</source>
      <translation>Ukryj w trybie prywatnym</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="277"/>
      <source>Address List</source>
      <translation>Lista adresów</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="283"/>
      <source>Change Addresses</source>
      <translation>Adresy zawierające resztę</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="286"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Przełącza pomiędzy adresami, na które możesz otrzymać środki od innych a adresami, na które portfel wysyła własną resztę.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="290"/>
      <source>Used Addresses</source>
      <translation>Wykorzystane adresy</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="293"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Przełącza pomiędzy wykorzystanymi i niewykorzystanymi adresami</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="313"/>
      <source>Backup details</source>
      <translation>Szczegóły kopii zapasowej</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="330"/>
      <source>Seed-phrase</source>
      <translation>Seed</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="348"/>
      <source>Copy</source>
      <translation>Skopiuj</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="376"/>
      <source>Seed format</source>
      <translation>Format seeda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="387"/>
      <source>Derivation</source>
      <translation>Derywacja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="400"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case of computer failure.</source>
      <translation>Prosimy o zapisanie seeda oraz ścieżki derywacji na papierze, z zachowaniem kolejności słów. Pozwoli to na odzyskanie portfela w przypadku awarii komputera.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="410"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Ważne&lt;/b&gt;: Nigdy nie udostępniaj seeda innym!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="418"/>
      <source>This wallet is protected by password (pin-to-pay). To see the backup details you need to provide the password.</source>
      <translation>Ten portfel jest chroniony hasłem (PIN by płacić). Aby zobaczyć szczegóły kopii zapasowej musisz podać hasło.</translation>
    </message>
  </context>
  <context>
    <name>AccountListItem</name>
    <message>
      <location filename="../guis/desktop/AccountListItem.qml" line="144"/>
      <source>Last Transaction</source>
      <translation>Ostatnia transakcja</translation>
    </message>
  </context>
  <context>
    <name>ActivityConfigBar</name>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="66"/>
      <source>Filter</source>
      <translation>Znajdź</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="156"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="197"/>
      <source>Received</source>
      <translation>Otrzymano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="175"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="204"/>
      <source>Sent</source>
      <translation>Wysłano</translation>
    </message>
  </context>
  <context>
    <name>AddressDbStats</name>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="30"/>
      <source>IP Addresses</source>
      <translation>Adresy IP</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="49"/>
      <source>Total found</source>
      <translation>Łącznie znaleziono</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="57"/>
      <source>Tried</source>
      <translation>Wypróbowano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="64"/>
      <source>Punished count</source>
      <translation>Ukarano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="71"/>
      <source>Banned count</source>
      <translation>Zbanowano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="79"/>
      <source>IP-v4 count</source>
      <translation>IP-v4</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="87"/>
      <source>IP-v6 count</source>
      <translation>IP-v6</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="97"/>
      <source>Pardon the Banned</source>
      <translation>Ułaskaw Zbanowanych</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="123"/>
      <source>Close</source>
      <translation>Zamknij</translation>
    </message>
  </context>
  <context>
    <name>NetView</name>
    <message numerus="yes">
      <location filename="../guis/desktop/NetView.qml" line="31"/>
      <source>Peers (%1)</source>
      <translation>
        <numerusform>Peer (%1)</numerusform>
        <numerusform>Peers (%1)</numerusform>
        <numerusform>Peers (%1)</numerusform>
        <numerusform>Peers (%1)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="83"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="90"/>
      <source>Start-height: %1</source>
      <translation>Wysokość początkowa: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="93"/>
      <source>ban-score: %1</source>
      <translation>punktacja banu: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="116"/>
      <source>Peer for wallet: %1</source>
      <translation>Peer dla portfela: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="139"/>
      <source>Disconnect Peer</source>
      <translation>Rozłącz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="143"/>
      <source>Ban Peer</source>
      <translation>Zbanuj peera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="162"/>
      <source>Close</source>
      <translation>Zamknij</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateBasicAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="39"/>
      <source>Create a new empty wallet with simple multi-address capability </source>
      <translation>Utwórz nowy pusty portfel z prostą funkcją wieloadresową </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="48"/>
      <source>Name</source>
      <translation>Nazwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="59"/>
      <source>Force Single Address</source>
      <translation>Wymuś jeden adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="60"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Gdy włączone, ten portfel będzie ograniczony do jednego adresu.
Pozwala to na tworzenie kopii zapasowej tylko jednego klucza prywatnego</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="67"/>
      <source>Go</source>
      <translation>Dalej</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateHDAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="33"/>
      <source>Create a new wallet with smart creation of addresses from a single seed-phrase</source>
      <translation>Utwórz nowy, pusty portfel z możliwością kreowania adresów z losowego ciągu wyrazów (seeda)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="41"/>
      <source>Name</source>
      <translation>Nazwa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="55"/>
      <source>Go</source>
      <translation>Dalej</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="71"/>
      <source>Advanced Options</source>
      <translation>Opcje zaawansowane</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="76"/>
      <source>Derivation</source>
      <translation>Derywacja</translation>
    </message>
  </context>
  <context>
    <name>NewAccountImportAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="90"/>
      <source>Select import method</source>
      <translation>Wybierz metodę importu</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="103"/>
      <source>Camera</source>
      <translation>Aparat</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="142"/>
      <source>OR</source>
      <translation>LUB</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="154"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Sekret jako tekst</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="194"/>
      <source>Unknown word(s) found</source>
      <translation>Znaleziono nieznane słowa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="217"/>
      <source>Address to import</source>
      <translation>Adres do zaimportowania</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="232"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="353"/>
      <source>New Wallet Name</source>
      <translation>Nazwa nowego portfela</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="252"/>
      <source>Force Single Address</source>
      <translation>Wymuś jeden adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="253"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Gdy włączone, dodatkowe adresy nie zostaną automatycznie stworzone dla tego portfela. Reszta wróci do zaimportowanego klucza.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="258"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="414"/>
      <source>Oldest Transaction</source>
      <translation>Najstarsza transakcja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="266"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Sprawdź wiek portfela</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="310"/>
      <source>Nothing found for wallet</source>
      <translation>Nie znaleziono nic dla portfela</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="317"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="461"/>
      <source>Start</source>
      <translation>Rozpocznij</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="373"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Poznaj szczegóły</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="425"/>
      <source>Derivation</source>
      <translation>Derywacja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="439"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Nic nie znaleziono dla seeda. Czy posiada hasło?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="447"/>
      <source>Password</source>
      <translation>Hasło</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="454"/>
      <source>imported wallet password</source>
      <translation>hasło do importowanego portfela</translation>
    </message>
  </context>
  <context>
    <name>NewAccountPane</name>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="64"/>
      <source>New HD wallet</source>
      <translation>Nowy portfel HD</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="70"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Bazuje na seedzie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="71"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Łatwa kopia zapasowa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="72"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Najbardziej kompatybilny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="77"/>
      <source>Import Existing Wallet</source>
      <translation>Importuj istniejący portfel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="83"/>
      <source>Imports seed-phrase</source>
      <translation>Importuje seeda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="84"/>
      <source>Imports private key</source>
      <translation>Importuje klucz prywatny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="90"/>
      <source>New Basic Wallet</source>
      <translation>Nowy portfel podstawowy</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="95"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Bazuje na kluczach prywatnych</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="96"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Trudna kopia zapasowa</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="97"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Świetny do krótkotrwałego użytku</translation>
    </message>
  </context>
  <context>
    <name>PaymentTweakingPanel</name>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="80"/>
      <source>Add Detail</source>
      <translation>Dodaj szczegóły</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="94"/>
      <source>Coin Selector</source>
      <translation>Wybór monet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="104"/>
      <source>To override the default selection of coins that are used to pay a transaction, you can add the &apos;Coin Selector&apos; where the wallets coins will be made visible.</source>
      <translation>Aby zastąpić domyślny zestaw monet, które zostaną użyte do zapłaty transakcji, użyj &apos;Wyboru monet&apos;, za pomocą którego zobaczysz i wybierzesz monety z tych zawartych w portfelu.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="107"/>
      <source>Comment</source>
      <translation>Komentarz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="117"/>
      <source>This allows adding a public comment, that will be included in the transaction and seen by everyone.</source>
      <translation>Pozwala na dodanie publicznego komentarza, który będzie uwzględniony w transakcji i widoczny dla wszystkich.</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTransactionPane</name>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="51"/>
      <source>Share your QR code or copy address to receive</source>
      <translation>Udostępnij kod QR lub skopiuj adres, aby otrzymać wpłatę</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="68"/>
      <source>Encrypted Wallet</source>
      <translation>Zaszyfrowany Portfel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="70"/>
      <source>Import Running...</source>
      <translation>Trwa Importowanie...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="146"/>
      <source>Checking</source>
      <translation>Sprawdzanie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="167"/>
      <source>High risk transaction</source>
      <translation>Transakcja o wysokim ryzyku</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="169"/>
      <source>Payment Seen</source>
      <translation>Wykryto płatność</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="171"/>
      <source>Payment Accepted</source>
      <translation>Płatność zaakceptowana</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="173"/>
      <source>Payment Settled</source>
      <translation>Płatność rozliczona</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="191"/>
      <source>Instant payment failed. Wait for confirmation. (double spent proof received)</source>
      <translation>Płatność błyskawiczna nie powiodła się. Poczekaj na potwierdzenie. (otrzymano dowód podwójnej płatności)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="209"/>
      <source>Description</source>
      <translation>Opis</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="221"/>
      <source>Amount</source>
      <translation>Kwota</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Clear</source>
      <translation>Wyczyść</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Done</source>
      <translation>Gotowe</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionPane</name>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="125"/>
      <source>Confirm delete</source>
      <translation>Potwierdź usunięcie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="126"/>
      <source>Do you really want to delete this detail?</source>
      <translation>Czy na pewno chcesz usunąć te szczegóły?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="135"/>
      <source>Add Destination</source>
      <translation>Dodaj odbiorcę</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="141"/>
      <source>Prepare</source>
      <translation>Przygotuj</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="157"/>
      <source>Enter your PIN</source>
      <translation>Wprowadź PIN</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="166"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="454"/>
      <source>Warning</source>
      <translation>Uwaga!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="173"/>
      <source>Payment request warnings:</source>
      <translation>Ostrzeżenia dotyczące żądania płatności:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="191"/>
      <source>Transaction Details</source>
      <translation>Szczegóły transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="205"/>
      <source>Not prepared yet</source>
      <translation>Jeszcze nie przygotowano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="211"/>
      <source>Copy transaction-ID</source>
      <translation>Kopiuj ID transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="214"/>
      <source>Fee</source>
      <translation>Opłata</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="224"/>
      <source>Transaction size</source>
      <translation>Rozmiar transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="232"/>
      <source>%1 bytes</source>
      <translation>%1 B</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="237"/>
      <source>Fee per byte</source>
      <translation>Opłata za bajt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="248"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation type="unfinished">%1 sat/byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="262"/>
      <source>Send</source>
      <translation>Wyślij</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="320"/>
      <source>Destination</source>
      <translation>Odbiorca</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="327"/>
      <source>Max available</source>
      <comment>The maximum balance available</comment>
      <translation>Maksymalnie dostępne</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="336"/>
      <source>%1 to %2</source>
      <comment>summary text to pay X-euro to address M</comment>
      <translation>%1 do %2</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="354"/>
      <source>Enter Bitcoin Cash Address</source>
      <translation>Wprowadź adres Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="379"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="666"/>
      <source>Copy Address</source>
      <translation>Skopiuj Adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="389"/>
      <source>Amount</source>
      <translation>Kwota</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="413"/>
      <source>Max</source>
      <translation>Maks.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="459"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Jest to adres BTC, który jest niekompatybilny z adresem BCH. Twoje środki mogą zostać utracone i Flowee nie będzie miało możliwości ich odzyskania. Czy na pewno chcesz użyć tego adresu BTC?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="471"/>
      <source>Continue</source>
      <translation>Kontynuuj</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="475"/>
      <source>Cancel</source>
      <translation>Anuluj</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="496"/>
      <source>Coin Selector</source>
      <translation>Wybór monet</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/SendTransactionPane.qml" line="497"/>
      <source>Selected %1 %2 in %3 coins</source>
      <comment>selected 2 BCH in 5 coins</comment>
      <translation>
        <numerusform>Wybrano %1 %2 w %3 monecie</numerusform>
        <numerusform>Wybrano %1 %2 w %3 monetach</numerusform>
        <numerusform>Wybrano %1 %2 w %3 monetach</numerusform>
        <numerusform>Wybrano %1 %2 w %3 monety</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="508"/>
      <source>Total</source>
      <comment>Number of coins</comment>
      <translation>Dostępne</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="515"/>
      <source>Needed</source>
      <translation>Potrzebne</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="525"/>
      <source>Selected</source>
      <translation>Wybrane</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="532"/>
      <source>Value</source>
      <translation>Wartość</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="570"/>
      <source>Locked coins will never be used for payments. Right-click for menu.</source>
      <translation>Zablokowane monety nigdy nie zostaną użyte do płatności. Kliknij, aby rozwinąć menu.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="609"/>
      <source>Age</source>
      <translation>Wiek</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Unselect All</source>
      <translation>Odznacz wszystkie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Select All</source>
      <translation>Zaznacz wszystkie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Unlock coin</source>
      <translation>Odblokuj monetę</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Lock coin</source>
      <translation>Zablokuj monetę</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="694"/>
      <source>Public-comment</source>
      <translation>Publiczny komentarz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="702"/>
      <source>Add a comment you want to include in the transaction, visible for everyone.</source>
      <translation>Dodaj komentarz, który chcesz zawrzeć w transakcji, widoczny dla wszystkich.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="703"/>
      <source>Custom message, to be included in the transaction.</source>
      <translation>Komunikat użytkownika, który ma być zawarty w transakcji.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="708"/>
      <source>Text</source>
      <translation>Tekst</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="718"/>
      <source>Size</source>
      <translation>Rozmiar</translation>
    </message>
  </context>
  <context>
    <name>SettingsPane</name>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="26"/>
      <source>Settings</source>
      <translation>Ustawienia</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="37"/>
      <source>Unit</source>
      <translation>Jednostka</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="107"/>
      <source>Show Bitcoin Cash value on Activity page</source>
      <translation>Pokaż wartość Bitcoin Cash na stronie Aktywności</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="120"/>
      <source>Show Block Notifications</source>
      <translation>Pokaż powiadomienia o blokach</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="121"/>
      <source>When a new block is mined, Flowee Pay shows a desktop notification</source>
      <translation>Gdy nowy blok zostanie wykopany, Flowee Pay pokazuje powiadomienia na pulpicie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="135"/>
      <source>Night Mode</source>
      <translation>Tryb nocny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="146"/>
      <source>Private Mode</source>
      <translation>Tryb prywatny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="147"/>
      <source>Hides private wallets while enabled</source>
      <translation>Ukrywa prywatne portfele</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="153"/>
      <source>Font sizing</source>
      <translation>Rozmiar czcionki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="194"/>
      <source>Version</source>
      <translation>Wersja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="202"/>
      <source>Library Version</source>
      <translation>Wersja biblioteki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="211"/>
      <source>Synchronization</source>
      <translation>Synchronizacja</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="216"/>
      <source>Network Status</source>
      <translation>Status sieci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="226"/>
      <source>Address Stats</source>
      <translation>Statystyki adresu</translation>
    </message>
  </context>
  <context>
    <name>Transaction</name>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="65"/>
      <source>Miner Reward</source>
      <translation>Nagroda dla górnika</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="67"/>
      <source>Fused</source>
      <translation>Fused</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="69"/>
      <source>Received</source>
      <translation>Otrzymano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="71"/>
      <source>Moved</source>
      <translation>Przeniesiono</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="72"/>
      <source>Sent</source>
      <translation>Wysłano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="80"/>
      <source>rejected</source>
      <translation>odrzucona</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="31"/>
      <source>Transaction Details</source>
      <translation>Szczegóły transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="101"/>
      <source>First Seen</source>
      <translation>Zaobserwowano</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="115"/>
      <source>Rejected</source>
      <translation>Odrzucono</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="117"/>
      <source>Mined at</source>
      <translation>Wykopano</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionDetails.qml" line="134"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 blok temu</numerusform>
        <numerusform>%1 bloki temu</numerusform>
        <numerusform>%1 bloków temu</numerusform>
        <numerusform>%1 bloku temu</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="141"/>
      <source>Waiting for block</source>
      <translation>Oczekiwanie na blok</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="146"/>
      <source>Comment</source>
      <translation>Komentarz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="163"/>
      <source>Fees paid</source>
      <translation>Koszt transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="169"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bajtów</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="184"/>
      <source>Size</source>
      <translation>Rozmiar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="188"/>
      <source>%1 bytes</source>
      <translation>%1 B</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="195"/>
      <source>Is Coinbase</source>
      <translation>Czy Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="205"/>
      <source>Copy transaction-ID</source>
      <translation>Skopiuj ID transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="219"/>
      <source>Fused from my addresses</source>
      <translation>Fuzja z moich adresów</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="221"/>
      <source>Sent from my addresses</source>
      <translation>Wysłano z moich adresów</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="223"/>
      <source>Sent from addresses</source>
      <translation>Wysłano z adresów</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="281"/>
      <source>Fused into my addresses</source>
      <translation>Fuzja na mój adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="283"/>
      <source>Received at addresses</source>
      <translation>Wpłynęło na adresy</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="284"/>
      <source>Received at my addresses</source>
      <translation>Wpłynęło na moje adresy</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="44"/>
      <source>Transaction is rejected</source>
      <translation>Transakcja została odrzucona</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="46"/>
      <source>Processing</source>
      <translation>Przetwarzanie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="59"/>
      <source>Mined</source>
      <translation>Wykopano</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="70"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 blok temu</numerusform>
        <numerusform>%1 bloki temu</numerusform>
        <numerusform>%1 bloków temu</numerusform>
        <numerusform>%1 bloku temu</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="106"/>
      <source>Fees</source>
      <translation>Opłaty</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="121"/>
      <source>Copy transaction-ID</source>
      <translation>Kopiuj ID transakcji</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="146"/>
      <source>Holds a token</source>
      <translation>Przechowuje token</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="168"/>
      <source>Opening Website</source>
      <translation>Otwieranie Strony</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryption</name>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="63"/>
      <source>Protect your wallet with a password</source>
      <translation>Zabezpiecz swój portfel hasłem</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="88"/>
      <source>Pin to Pay</source>
      <translation>PIN by płacić</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="93"/>
      <source>Protect your funds</source>
      <comment>pin to pay</comment>
      <translation>Zabezpiecz swoje środki</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="94"/>
      <source>Fully open, except for sending funds</source>
      <comment>pin to pay</comment>
      <translation>W pełni otwarty, z wyjątkiem funkcji wysyłania środków</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="95"/>
      <source>Keeps in sync</source>
      <comment>pin to pay</comment>
      <translation>Utrzymuje synchronizację</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="100"/>
      <source>Pin to Open</source>
      <translation>PIN by otworzyć</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="105"/>
      <source>Protect your entire wallet</source>
      <comment>pin to open</comment>
      <translation>Zabezpiecz cały portfel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="106"/>
      <source>Balance and history protected</source>
      <comment>pin to open</comment>
      <translation>Zabezpieczone saldo i historia</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="107"/>
      <source>Requires Pin to view, sync or pay</source>
      <comment>pin to open</comment>
      <translation>Wymaga pinu do wyświetlenia, synchronizacji lub zapłaty</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="117"/>
      <source>Make &quot;%1&quot; wallet require a pin to pay</source>
      <translation>Portfel &quot;%1&quot; będzie wymagał podania PINu by płacić</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="118"/>
      <source>Make &quot;%1&quot; wallet require a pin to open</source>
      <translation>Portfel &quot;%1&quot; będzie wymagał podania PINu by go otworzyć</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="130"/>
      <location filename="../guis/desktop/WalletEncryption.qml" line="142"/>
      <source>Wallet already has pin to open applied</source>
      <translation>Ten portfel ma już opcję "PIN by otworzyć"</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="132"/>
      <source>Wallet already has pin to pay applied</source>
      <translation>Ten portfel ma już opcję "PIN by płacić"</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="133"/>
      <source>Your wallet will get partially encrypted and payments will become impossible without a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Twój portfel zostanie częściowo zaszyfrowany, a płatności staną się niemożliwe bez hasła. Jeśli nie masz kopii zapasowej tego portfela, zrób ją najpierw.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="143"/>
      <source>Your full wallet gets encrypted, opening it will need a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Cały Twój portfel zostanie zaszyfrowany, otwarcie go będzie wymagało hasła. Jeśli nie masz kopii zapasowej tego portfelu, najpierw ją wykonaj.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="169"/>
      <source>Password</source>
      <translation>Hasło</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="192"/>
      <source>Wallet</source>
      <translation>Portfel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="214"/>
      <source>Encrypt</source>
      <translation>Zaszyfruj</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="221"/>
      <source>Invalid password to open this wallet</source>
      <translation>Nieprawidłowe hasło do otwarcia tego portfela</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="233"/>
      <source>Close</source>
      <translation>Zamknij</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="243"/>
      <source>Repeat password</source>
      <translation>Powtórz hasło</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="244"/>
      <source>Please confirm the password by entering it again</source>
      <translation>Proszę potwierdzić hasło wpisując je ponownie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="249"/>
      <source>Typed passwords do not match</source>
      <translation>Podane hasła różnią się</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryptionStatus</name>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="46"/>
      <source>Pin to Open</source>
      <translation>PIN by otworzyć</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="48"/>
      <source>Pin to Pay</source>
      <translation>PIN by płacić</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="50"/>
      <source>(Opened)</source>
      <comment>Wallet is decrypted</comment>
      <translation>(otwarty)</translation>
    </message>
  </context>
  <context>
    <name>locked</name>
    <message>
      <location filename="../guis/desktop/locked.qml" line="51"/>
      <source>Already running?</source>
      <translation>Już uruchomione?</translation>
    </message>
  </context>
  <context>
    <name>main</name>
    <message>
      <location filename="../guis/desktop/main.qml" line="210"/>
      <source>Activity</source>
      <translation>Aktywność</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="250"/>
      <source>Archived wallets do not check for activities. Balance may be out of date.</source>
      <translation>Zarchiwizowane portfele nie sprawdzają aktywności. Saldo może być nieaktualne.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="254"/>
      <source>Unarchive</source>
      <translation>Przywróć</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="287"/>
      <source>This wallet needs a password to open.</source>
      <translation>Ten portfel wymaga hasła do otwarcia.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="295"/>
      <source>Password:</source>
      <translation>Hasło:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="315"/>
      <source>Invalid password</source>
      <translation>Nieprawidłowe hasło</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="320"/>
      <source>Open</source>
      <translation>Otwórz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="398"/>
      <source>Send</source>
      <translation>Wyślij</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="406"/>
      <source>Receive</source>
      <translation>Odbierz</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="525"/>
      <source>Balance</source>
      <translation>Saldo</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="594"/>
      <source>Main</source>
      <comment>balance (money), non specified</comment>
      <translation>Główny</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="604"/>
      <source>Unconfirmed</source>
      <comment>balance (money)</comment>
      <translation>Niepotwierdzona</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="613"/>
      <source>Immature</source>
      <comment>balance (money)</comment>
      <translation>Immature</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="662"/>
      <source>1 BCH is: %1</source>
      <translation>1 BCH to: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="709"/>
      <source>Network status</source>
      <translation>Status sieci</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="723"/>
      <source>Offline</source>
      <translation>Offline</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="747"/>
      <source>Add Bitcoin Cash wallet</source>
      <translation>Dodaj portfel Bitcoin Cash</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/main.qml" line="778"/>
      <source>Archived wallets [%1]</source>
      <comment>Arg is wallet count</comment>
      <translation>
        <numerusform>Zarchiwizowany portfel [%1]</numerusform>
        <numerusform>Zarchiwizowane portfele [%1]</numerusform>
        <numerusform>Zarchiwizowane portfele [%1]</numerusform>
        <numerusform>Zarchiwizowane portfele [%1]</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="808"/>
      <source>Preparing...</source>
      <translation>Przygotowuję…</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="891"/>
      <source>QR-Scan</source>
      <translation>Skanowanie QR</translation>
    </message>
  </context>
</TS>
