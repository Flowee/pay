<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>Listing</name>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="25"/>
      <source>Videos</source>
      <translation>Videos</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="80"/>
      <source>Run time:</source>
      <translation>Laufzeit:</translation>
    </message>
  </context>
  <context>
    <name>SocialFeedModuleInfo</name>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="27"/>
      <source>Help and Learning</source>
      <translation>Hilfe und Lernen</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="28"/>
      <source>Want to see the experts show how to use Bitcoin Cash with Flowee Pay? Find all you want via this library of videos.</source>
      <translation>In dieser Video-Bibliothek zeigen Ihnen Experten wie Sie Bitcoin Cash mit Flowee Pay verwenden.</translation>
    </message>
  </context>
</TS>
