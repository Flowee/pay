<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ha" sourcelanguage="en">
  <context>
    <name>BuildTransactionModuleInfo</name>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="23"/>
      <source>Create Transactions</source>
      <translation>Ƙirƙiri Ma'amaloli</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="24"/>
      <source>This module allows building more powerful transactions in one simple user interface.</source>
      <translation>Wannan tsarin yana ba da damar gina ƙarin ma'amaloli masu ƙarfi a cikin sauƙin mai amfani guda ɗaya.</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="28"/>
      <source>Build Transaction</source>
      <translation>Gina Ma'amala</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="29"/>
      <source>Manually select templates</source>
      <translation type="unfinished">Manually select templates</translation>
    </message>
  </context>
  <context>
    <name>DestinationEditPage</name>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="27"/>
      <source>Edit Destination</source>
      <translation type="unfinished">Edit Destination</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="32"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Tura duka</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="38"/>
      <source>Bitcoin Cash Address</source>
      <translation type="unfinished">Bitcoin Cash Address</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="75"/>
      <source>Copy Address</source>
      <translation>Kwafi Adireshi</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="131"/>
      <source>Warning</source>
      <translation>Gargaɗi</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="139"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Wannan adireshin BTC ne, wanda tsabar kudin da bai dace ba. Kuɗin ku na iya yin asara kuma Flowee ba za ta sami hanyar dawo da su ba. Shin kun tabbata wannan shine daidai adireshin?</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="148"/>
      <source>I am certain</source>
      <translation type="unfinished">I am certain</translation>
    </message>
  </context>
  <context>
    <name>PayToOthers</name>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="28"/>
      <source>Build Transaction</source>
      <translation>Gina Ma'amala</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="64"/>
      <source>Building Error</source>
      <comment>error during build</comment>
      <translation>Kuskuren gini</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="72"/>
      <source>Add Payment Detail</source>
      <comment>page title</comment>
      <translation>Ƙara Bayanin Biyan Kuɗi</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="80"/>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="485"/>
      <source>Add Destination</source>
      <translation>Ƙara madakata</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="81"/>
      <source>an address to send money to</source>
      <translation>Adireshin da za'a aika kudi zuwa</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="99"/>
      <source>Confirm Sending</source>
      <comment>confirm we want to send the transaction</comment>
      <translation>Tabbatar da Aika</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="119"/>
      <source>TXID</source>
      <translation>TXID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="127"/>
      <source>Copy transaction-ID</source>
      <translation>Kwafi shaidar ma'amala-ID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="130"/>
      <source>Fee</source>
      <translation>Kudin</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="137"/>
      <source>Transaction size</source>
      <translation>Girman ciniki</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="140"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="143"/>
      <source>Fee per byte</source>
      <translation>Kudin kowane byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="150"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/bytes</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="182"/>
      <source>Destination</source>
      <translation>Madakata</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="191"/>
      <source>unset</source>
      <comment>indication of desination not being set</comment>
      <translation type="unfinished">unset</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="192"/>
      <source>invalid</source>
      <comment>address is not correct</comment>
      <translation>Ba daidai ba</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="199"/>
      <source>Copy Address</source>
      <translation>Kwafi Adireshi</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Edit</source>
      <translation>Jawo don Gyarawa</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Delete</source>
      <translation>Jawo don gogewa</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Unlock Wallet</source>
      <translation>Buɗe Asusu</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Prepare Payment...</source>
      <translation>Shirya Biya...</translation>
    </message>
  </context>
</TS>
