<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>Listing</name>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="25"/>
      <source>Videos</source>
      <translation>Videos</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="80"/>
      <source>Run time:</source>
      <translation>Looptijd:</translation>
    </message>
  </context>
  <context>
    <name>SocialFeedModuleInfo</name>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="27"/>
      <source>Help and Learning</source>
      <translation>Hulp en leren</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="28"/>
      <source>Want to see the experts show how to use Bitcoin Cash with Flowee Pay? Find all you want via this library of videos.</source>
      <translation>Wil je de experts je laten zien hoe je Bitcoin Cash met Flowee Pay kunt gebruiken? Vind alles wat je wilt via deze bibliotheek van video's.</translation>
    </message>
  </context>
</TS>
