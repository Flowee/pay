<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>BuildTransactionModuleInfo</name>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="23"/>
      <source>Create Transactions</source>
      <translation>Utwórz Transakcje</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="24"/>
      <source>This module allows building more powerful transactions in one simple user interface.</source>
      <translation>Ten moduł umożliwia tworzenie potężniejszych transakcji w jednym prostym interfejsie użytkownika.</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="28"/>
      <source>Build Transaction</source>
      <translation>Utwórz transakcję</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="29"/>
      <source>Manually select templates</source>
      <translation>Wybierz szablony ręcznie</translation>
    </message>
  </context>
  <context>
    <name>DestinationEditPage</name>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="27"/>
      <source>Edit Destination</source>
      <translation>Edytuj Odbiorcę</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="32"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Wyślij wszystko</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="38"/>
      <source>Bitcoin Cash Address</source>
      <translation>Adres Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="75"/>
      <source>Copy Address</source>
      <translation>Skopiuj adres</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="131"/>
      <source>Warning</source>
      <translation>Uwaga!</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="139"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Jest to adres BTC, który jest niekompatybilny z adresem BCH. Twoje środki mogą zostać utracone i Flowee nie będzie miało możliwości ich odzyskania. Czy na pewno chcesz użyć tego adresu BTC?</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="148"/>
      <source>I am certain</source>
      <translation>Na pewno</translation>
    </message>
  </context>
  <context>
    <name>PayToOthers</name>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="28"/>
      <source>Build Transaction</source>
      <translation>Utwórz transakcję</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="64"/>
      <source>Building Error</source>
      <comment>error during build</comment>
      <translation>Błąd przy tworzeniu</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="72"/>
      <source>Add Payment Detail</source>
      <comment>page title</comment>
      <translation>Dodaj szczegół płatności</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="80"/>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="485"/>
      <source>Add Destination</source>
      <translation>Dodaj Odbiorcę</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="81"/>
      <source>an address to send money to</source>
      <translation>adres do wysłania pieniędzy</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="99"/>
      <source>Confirm Sending</source>
      <comment>confirm we want to send the transaction</comment>
      <translation>Potwierdź Wysyłanie</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="119"/>
      <source>TXID</source>
      <translation>TXID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="127"/>
      <source>Copy transaction-ID</source>
      <translation>Kopiuj ID transakcji</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="130"/>
      <source>Fee</source>
      <translation>Opłata</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="137"/>
      <source>Transaction size</source>
      <translation>Rozmiar transakcji</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="140"/>
      <source>%1 bytes</source>
      <translation>%1 bajtów</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="143"/>
      <source>Fee per byte</source>
      <translation>Opłata za bajt</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="150"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/bajt</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="182"/>
      <source>Destination</source>
      <translation>Odbiorca</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="191"/>
      <source>unset</source>
      <comment>indication of desination not being set</comment>
      <translation>nie ustawiono</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="192"/>
      <source>invalid</source>
      <comment>address is not correct</comment>
      <translation>nieprawidłowy</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="199"/>
      <source>Copy Address</source>
      <translation>Skopiuj adres</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Edit</source>
      <translation>Przeciągnij, aby edytować</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Delete</source>
      <translation>Przeciągnij, aby usunąć</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Unlock Wallet</source>
      <translation>Odblokuj portfel</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Prepare Payment...</source>
      <translation>Przygotuj płatność...</translation>
    </message>
  </context>
</TS>
