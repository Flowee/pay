<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>BigTransferModuleInfo</name>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="33"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="38"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="44"/>
      <source>Wallet to Wallet</source>
      <translation>Portemonnee naar Portemonnee</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="34"/>
      <source>Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses.</source>
      <translation>Verplaats veel munten tussen portemonnees, optimaliseer voor anonimiteit met één transactie per adres. Het is mogelijk om ze over verschillende adressen op te splitsen.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="39"/>
      <source>Move funds to another wallet</source>
      <translation>Verplaats saldo naar een andere portemonnee</translation>
    </message>
  </context>
  <context>
    <name>Main</name>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="28"/>
      <source>Wallet to Wallet</source>
      <translation>Portemonnee naar Portemonnee</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="38"/>
      <source>Select two wallets to transfer funds simply, using anonimity preserving transactions.</source>
      <translation>Selecteer twee portemonnees om geld over te maken, met behulp van anonimiteit behoudende transacties.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="42"/>
      <source>Spending Wallet</source>
      <translation>Uitgave portemonnee</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="54"/>
      <source>Addresses</source>
      <translation>Adressen</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="60"/>
      <source>Coins</source>
      <translation>Munten</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="68"/>
      <source>Destination Wallet</source>
      <translation>Bestemming portemonnee</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="80"/>
      <source>Prepare...</source>
      <translation>Bereid voor...</translation>
    </message>
  </context>
  <context>
    <name>ShowPrepared</name>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="29"/>
      <source>Verify %1 Transactions</source>
      <translation>
        <numerusform>Verifieer %1 transactie</numerusform>
        <numerusform>Verifieer %1 transacties</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="103"/>
      <source>Coins</source>
      <translation>Munten</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="190"/>
      <source>Send Now</source>
      <translation>Verstuur nu</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="205"/>
      <source>Create and send all %1 transactions</source>
      <translation>
        <numerusform>Verstuur de transactie</numerusform>
        <numerusform>Verstuur alle %1 transacties</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="282"/>
      <source>Target Coins</source>
      <comment>indicates a number</comment>
      <translation>Bestemmingsmunten</translation>
    </message>
  </context>
</TS>
