<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>BuildTransactionModuleInfo</name>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="23"/>
      <source>Create Transactions</source>
      <translation>Crear Transacciones</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="24"/>
      <source>This module allows building more powerful transactions in one simple user interface.</source>
      <translation>Este módulo permite construir transacciones más potentes en una interfaz de usuario simple.</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="28"/>
      <source>Build Transaction</source>
      <translation>Construir transacción</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="29"/>
      <source>Manually select templates</source>
      <translation type="unfinished">Manually select templates</translation>
    </message>
  </context>
  <context>
    <name>DestinationEditPage</name>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="27"/>
      <source>Edit Destination</source>
      <translation type="unfinished">Edit Destination</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="32"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Enviar Todo</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="38"/>
      <source>Bitcoin Cash Address</source>
      <translation type="unfinished">Bitcoin Cash Address</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="75"/>
      <source>Copy Address</source>
      <translation>Copiar la dirección</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="131"/>
      <source>Warning</source>
      <translation>Advertencia</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="139"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Esta es una dirección BTC, que es una moneda incompatible. Tus fondos podrían perderse y Flowee no tendrá forma de recuperarlos. ¿Estás seguro de que esta es la dirección correcta?</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="148"/>
      <source>I am certain</source>
      <translation type="unfinished">I am certain</translation>
    </message>
  </context>
  <context>
    <name>PayToOthers</name>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="28"/>
      <source>Build Transaction</source>
      <translation>Construir transacción</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="64"/>
      <source>Building Error</source>
      <comment>error during build</comment>
      <translation>Error al construir</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="72"/>
      <source>Add Payment Detail</source>
      <comment>page title</comment>
      <translation>Añadir detalles del pago</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="80"/>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="485"/>
      <source>Add Destination</source>
      <translation>Añadir destino</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="81"/>
      <source>an address to send money to</source>
      <translation>una dirección para enviar dinero a</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="99"/>
      <source>Confirm Sending</source>
      <comment>confirm we want to send the transaction</comment>
      <translation>Confirmar envío</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="119"/>
      <source>TXID</source>
      <translation>TXID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="127"/>
      <source>Copy transaction-ID</source>
      <translation>Copiar ID de la transacción</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="130"/>
      <source>Fee</source>
      <translation>Comisión</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="137"/>
      <source>Transaction size</source>
      <translation>Tamaño de la transacción</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="140"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="143"/>
      <source>Fee per byte</source>
      <translation>Comisión por byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="150"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="182"/>
      <source>Destination</source>
      <translation>Destino</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="191"/>
      <source>unset</source>
      <comment>indication of desination not being set</comment>
      <translation type="unfinished">unset</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="192"/>
      <source>invalid</source>
      <comment>address is not correct</comment>
      <translation>inválido</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="199"/>
      <source>Copy Address</source>
      <translation>Copiar dirección</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Edit</source>
      <translation>Arrastre para editar</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Delete</source>
      <translation>Arrastre para eliminar</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Unlock Wallet</source>
      <translation>Desbloquear Monedero</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Prepare Payment...</source>
      <translation>Preparar pago...</translation>
    </message>
  </context>
</TS>
