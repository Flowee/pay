<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>SendPage</name>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="28"/>
      <source>Sweep coins</source>
      <translation>Barrido de monedas</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="44"/>
      <source>Sweeping from address:</source>
      <translation>Borrando desde dirección:</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="59"/>
      <source>Found %1 coins on address.</source>
      <comment>this is a simple number</comment>
      <translation>
        <numerusform>Se encontró una moneda en la dirección.</numerusform>
        <numerusform>Se encontraron monedas %1 en la dirección.</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="63"/>
      <source>Ignoring %1 tokens.</source>
      <comment>Number of CashTokens</comment>
      <translation>
        <numerusform>Ignorando un token.</numerusform>
        <numerusform>Ignorando tokens %1.</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="118"/>
      <source>Failed to understand QR</source>
      <translation>Error al entender QR</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="124"/>
      <source>Indexer results invalid. Please try again.</source>
      <translation>Resultados del indexador no válidos. Por favor, inténtelo de nuevo.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="149"/>
      <source>Transfer to:</source>
      <translation>Transferir a:</translation>
    </message>
  </context>
  <context>
    <name>SendSweepModuleInfo</name>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="33"/>
      <source>Claim Cash Stamp</source>
      <translation type="unfinished">Claim Cash Stamp</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="34"/>
      <source>A QR code with a CashStamp can be taken to transfer the money to your wallet.</source>
      <translation type="unfinished">A QR code with a CashStamp can be taken to transfer the money to your wallet.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="36"/>
      <source>Claim a Cash Stamp</source>
      <translation>Reclamar un Cash Stamp</translation>
    </message>
  </context>
  <context>
    <name>StartScan</name>
    <message>
      <location filename="../modules/send-sweep/StartScan.qml" line="28"/>
      <source>Scan QR (WIF) to find funds</source>
      <comment>Please note that WIF and QR are names</comment>
      <translation type="unfinished">Scan QR (WIF) to find funds</translation>
    </message>
  </context>
</TS>
