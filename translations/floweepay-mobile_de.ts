<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>About</name>
    <message>
      <location filename="../guis/mobile/About.qml" line="23"/>
      <source>About</source>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="43"/>
      <source>Help translate this app</source>
      <translation>Hilf mit, diese App zu übersetzen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="48"/>
      <source>License</source>
      <translation>Lizenz</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="53"/>
      <location filename="../guis/mobile/About.qml" line="61"/>
      <source>Credits</source>
      <translation>Mitwirkende</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="54"/>
      <source>© 2020-2025 Tom Zander and contributors</source>
      <translation>©️ 2020-2025 Tom Zander und Mitwirkende</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="99"/>
      <source>Project Home</source>
      <translation>Projekt-Startseite</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="100"/>
      <source>With git repository and issues tracker</source>
      <translation>Mit git Repository und Issue Tracker</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="105"/>
      <source>Telegram</source>
      <translation>Telegram</translation>
    </message>
  </context>
  <context>
    <name>AccountHistory</name>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="29"/>
      <source>Home</source>
      <translation>Startseite</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="162"/>
      <source>Pay</source>
      <translation>Bezahlen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="168"/>
      <source>Receive</source>
      <translation>Empfange</translation>
    </message>
  </context>
  <context>
    <name>AccountPageListItem</name>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="41"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="64"/>
      <source>Archived wallets do not check for activities. Balance may be out of date</source>
      <translation>Archivierte Geldbörsen überprüfen nicht auf Aktivitäten. Das Guthaben kann veraltet sein</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="76"/>
      <source>Backup information</source>
      <translation>Backup-Informationen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="87"/>
      <source>Backup Details</source>
      <translation>Backup-Details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="121"/>
      <source>Wallet seed-phrase</source>
      <translation>Geldbörsen-Seed-Phrase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="148"/>
      <source>Password</source>
      <translation>Passwort</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="159"/>
      <source>Seed format</source>
      <translation>Seed-Format</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="168"/>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="269"/>
      <source>Starting Height</source>
      <comment>height refers to block-height</comment>
      <translation>Starthöhe</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="173"/>
      <source>Derivation Path</source>
      <translation>Ableitungspfad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="180"/>
      <source>xpub</source>
      <translation>xpub</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="210"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</source>
      <translation>Bitte speichern Sie die Seed-Phrase in der richtigen Reihenfolge mit dem Ableitungspfad. Mit diesem Seed können Sie Ihre Geldbörse wiederherstellen, falls Sie Ihr Handy verlieren.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="217"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Wichtig&lt;/b&gt;: Teilen Sie Ihre Seed-Phrase nie mit anderen!</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="229"/>
      <source>Wallet keys</source>
      <translation>Geldbörsen-Schlüssel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="232"/>
      <source>Show Index</source>
      <comment>toggle to show numbers</comment>
      <translation>Index anzeigen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="248"/>
      <source>Change Addresses</source>
      <translation>Wechselgeldadressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="251"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Wechselt zwischen Adressen auf welchen Sie von anderen Leuten bezahlt werden können, und Adressen die die Geldbörse verwendet, um Wechselgeld an sich selbst zu senden.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="254"/>
      <source>Used Addresses</source>
      <translation>Benutzte Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="257"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Wechselt zwischen ungenutzten und genutzten Bitcoin-Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="309"/>
      <source>Addresses and keys</source>
      <translation>Adressen und Schlüssel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="317"/>
      <source>Sync Status</source>
      <translation>Synchronisierungsstatus</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="357"/>
      <source>Hide balance in overviews</source>
      <translation>Guthaben in Übersichten ausblenden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="364"/>
      <source>Hide in private mode</source>
      <translation>Im privaten Modus ausblenden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Unarchive Wallet</source>
      <translation>Geldbörse entarchivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Archive Wallet</source>
      <translation>Geldbörse archivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="423"/>
      <source>Really Delete?</source>
      <translation>Wirklich löschen?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="424"/>
      <source>Removing wallet &quot;%1&quot; can not be undone.</source>
      <comment>argument is the wallet name</comment>
      <translation>Die Löschung der Geldbörse &quot;%1&quot; kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="403"/>
      <source>Remove Wallet</source>
      <translation>Geldbörse entfernen</translation>
    </message>
  </context>
  <context>
    <name>AccountSelectorPopup</name>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="51"/>
      <source>Your Wallets</source>
      <translation>Ihre Geldbörsen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="130"/>
      <source>last active</source>
      <translation>zuletzt aktiv</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="139"/>
      <source>Needs PIN to open</source>
      <translation>Benötigt PIN zum Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="175"/>
      <source>Balance Total</source>
      <translation>Gesamtguthaben</translation>
    </message>
  </context>
  <context>
    <name>AccountSyncState</name>
    <message>
      <location filename="../guis/mobile/AccountSyncState.qml" line="95"/>
      <source>Status: Offline</source>
      <translation>Status: Offline</translation>
    </message>
  </context>
  <context>
    <name>AccountsList</name>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallet</source>
      <translation>Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallets</source>
      <translation>Geldbörsen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="28"/>
      <source>Add Wallet</source>
      <translation>Geldbörse hinzufügen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Exit Private Mode</source>
      <translation>Privaten Modus beenden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Enter Private Mode</source>
      <translation>Privaten Modus aktivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="60"/>
      <source>Reveals wallets marked private</source>
      <translation>Zeigt als privat markierte Geldbörsen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="61"/>
      <source>Hides wallets marked private</source>
      <translation>Versteckt als privat markierte Geldbörsen</translation>
    </message>
  </context>
  <context>
    <name>BackgroundSyncConfig</name>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="24"/>
      <source>Background Synchronization</source>
      <translation>Hintergrund-Synchronisierung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="33"/>
      <source>Without background synchronization your wallets will not see new transactions until you open Flowee Pay</source>
      <translation>Ohne Hintergrund-Synchronisierung werden Ihre Brieftaschen erst dann neue Transaktionen sehen, wenn Sie Flowee Pay öffnen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="39"/>
      <source>Allow Background Synchronization</source>
      <translation>Hintergrund-Synchronisierung erlauben</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="106"/>
      <source>Every %1 hours</source>
      <translation>Alle %1 Stunden</translation>
    </message>
  </context>
  <context>
    <name>CurrencySelector</name>
    <message>
      <location filename="../guis/mobile/CurrencySelector.qml" line="24"/>
      <source>Select Currency</source>
      <translation>Währung auswählen</translation>
    </message>
  </context>
  <context>
    <name>ExploreModules</name>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="24"/>
      <source>Explore</source>
      <translation>Erkunden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="120"/>
      <source>Open</source>
      <translation>Öffnen</translation>
    </message>
  </context>
  <context>
    <name>FilterPopup</name>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="45"/>
      <source>Transactions Filter</source>
      <translation>Transaktions-Filter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="192"/>
      <source>Only with a comment</source>
      <comment>This is a statement about a transaction</comment>
      <translation>Nur mit einem Kommentar</translation>
    </message>
  </context>
  <context>
    <name>ImportWalletPage</name>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="26"/>
      <source>Import Wallet</source>
      <translation>Geldbörse importieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="159"/>
      <source>Select import method</source>
      <translation>Importmethode auswählen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="172"/>
      <source>Scan QR</source>
      <translation>Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="212"/>
      <source>OR</source>
      <translation>ODER</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="224"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Geheimnis als Text</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="263"/>
      <source>Unknown word(s) found</source>
      <translation>Unbekannte(s) Wort(e) gefunden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="274"/>
      <source>Next</source>
      <translation>Weiter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="298"/>
      <source>Address to import</source>
      <translation>Zu importierende Adresse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="312"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="446"/>
      <source>New Wallet Name</source>
      <translation>Neuer Geldbörsen Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="330"/>
      <source>Force Single Address</source>
      <translation>Erzwinge einzelne Adresse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="331"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Wenn aktiviert, werden keine zusätzlichen Adressen automatisch in dieser Geldbörse generiert.
Wechselgeld wird zum importierten Schlüssel zurückgeführt.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="336"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="505"/>
      <source>Oldest Transaction</source>
      <translation>Älteste Transaktion</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="342"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Überprüfe Alter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="384"/>
      <source>Nothing found for wallet</source>
      <translation>Nichts gefunden für Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="390"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="549"/>
      <source>Start</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="464"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Entdecke Details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="515"/>
      <source>Derivation Path</source>
      <translation>Ableitungspfad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="528"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Nichts für Seed gefunden. Hat es ein Passwort?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="536"/>
      <source>Password</source>
      <translation>Passwort</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="542"/>
      <source>imported wallet password</source>
      <translation>importiertes Geldbörsen-Passwort</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigButton</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Enable Instant Pay</source>
      <translation>Sofortzahlung aktivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Configure Instant Pay</source>
      <translation>Sofortzahlung konfigurieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="54"/>
      <source>Limits for %1</source>
      <comment>argument is a name</comment>
      <translation>Grenzen für %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="55"/>
      <source>Disabled for %1</source>
      <comment>argument is a name</comment>
      <translation>Deaktiviert für %1</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigPage</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="23"/>
      <source>Instant Pay</source>
      <translation>Sofortzahlung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="29"/>
      <source>Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</source>
      <translation>Das Scannen des QR-Codes mit aktivierter Sofortzahlung macht die Überweisung ohne Bestätigung. Solange er das festgelegte Limit nicht überschreitet.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="54"/>
      <source>Protected wallets can not be used for InstaPay because they require a PIN before usage</source>
      <translation>Geschützte Geldbörsen können nicht für Sofortzahlung verwendet werden, da sie vor der Nutzung eine PIN benötigen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="65"/>
      <source>Enable Instant Pay for this wallet</source>
      <translation>Sofortzahlung für diese Geldbörse aktivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="72"/>
      <source>Maximum Amount</source>
      <translation>Höchstbetrag</translation>
    </message>
  </context>
  <context>
    <name>LockApplication</name>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="25"/>
      <source>Security</source>
      <translation>Sicherheit</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="35"/>
      <source>PIN on startup</source>
      <translation>PIN beim Start</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter current PIN</source>
      <translation>Aktuelle PIN eintragen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter new PIN</source>
      <translation>Geben Sie eine neue PIN ein</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="80"/>
      <source>Repeat PIN</source>
      <translation>PIN wiederholen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Remove PIN</source>
      <translation>PIN entfernen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Set PIN</source>
      <translation>PIN setzen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="124"/>
      <source>PIN has been removed</source>
      <translation>PIN wurde entfernt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="127"/>
      <source>PIN has been set</source>
      <translation>PIN wurde gesetzt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="196"/>
      <source>Ok</source>
      <translation>Ok</translation>
    </message>
  </context>
  <context>
    <name>MainView</name>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="43"/>
      <source>Explore</source>
      <translation>Erkunden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="65"/>
      <source>Find More</source>
      <translation>Finde mehr</translation>
    </message>
  </context>
  <context>
    <name>MainViewBase</name>
    <message>
      <location filename="../guis/mobile/MainViewBase.qml" line="74"/>
      <source>No Internet Available</source>
      <translation>Keine Internetverbindung</translation>
    </message>
  </context>
  <context>
    <name>MenuOverlay</name>
    <message>
      <location filename="../guis/mobile/MenuOverlay.qml" line="318"/>
      <source>Add Wallet</source>
      <translation>Geldbörse hinzufügen</translation>
    </message>
  </context>
  <context>
    <name>NewAccount</name>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="26"/>
      <source>New Bitcoin Cash Wallet</source>
      <translation>Neue Bitcoin Cash Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="39"/>
      <source>New HD Wallet</source>
      <translation>Neue HD-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="45"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Seed-Phrase basiert</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="46"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Einfach zu sichern</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="47"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Am kompatibelsten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="52"/>
      <source>Import Existing Wallet</source>
      <translation>Import einer vorhandenen Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="58"/>
      <source>Imports seed-phrase</source>
      <translation>Importiert Seed-Phrase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="59"/>
      <source>Imports private key</source>
      <translation>Importiert privaten Schlüssel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="64"/>
      <source>New Basic Wallet</source>
      <translation>Neue Basis-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="70"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Private Schlüssel basiert</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="71"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Schwierig zu sichern</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="72"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideal für kurze Nutzung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="87"/>
      <source>New Wallet</source>
      <translation>Neue Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="95"/>
      <source>This creates a new empty wallet with simple multi-address capability</source>
      <translation>Dies erzeugt eine neue leere Geldbörse mit einfacher Mehradressen-Fähigkeit</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="101"/>
      <location filename="../guis/mobile/NewAccount.qml" line="155"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="111"/>
      <source>Force Single Address</source>
      <translation>Erzwinge einzelne Adresse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="112"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Wenn aktiviert, wird diese Geldbörse auf eine Adresse beschränkt.
Dies stellt sicher, dass nur ein privater Schlüssel gesichert werden muss</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="120"/>
      <location filename="../guis/mobile/NewAccount.qml" line="180"/>
      <source>Create</source>
      <translation>Erstelle</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="143"/>
      <source>New HD-Wallet</source>
      <translation>Neue HD-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="151"/>
      <source>This creates a new wallet that can be backed up with a seed-phrase</source>
      <translation>Dies erzeugt eine neue Geldbörse, die mit einer Seed-Phrase gesichert werden kann</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="163"/>
      <source>Derivation</source>
      <translation>Ableitung</translation>
    </message>
  </context>
  <context>
    <name>PayWithQR</name>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="26"/>
      <source>Approve Payment</source>
      <translation>Zahlung genehmigen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="31"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Alles senden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="41"/>
      <source>Show Address</source>
      <comment>to show a bitcoincash address</comment>
      <translation>Adresse anzeigen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="44"/>
      <source>Edit Amount</source>
      <comment>Edit amount of money to send</comment>
      <translation>Betrag bearbeiten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="98"/>
      <source>Invalid QR code</source>
      <translation>Ungültiger QR Code</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="112"/>
      <source>I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</source>
      <translation>Ich verstehe den gescannten Code nicht. Ich entschuldige mich, ich kann keine Zahlung starten.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="116"/>
      <source>details</source>
      <translation>details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="127"/>
      <source>Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</source>
      <translation>Gescannter Text: &lt;pre&gt;%1&lt;/pre&gt;</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="176"/>
      <source>Payment description</source>
      <translation>Zahlungsbeschreibung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="215"/>
      <source>Destination Address</source>
      <translation>Zieladresse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="409"/>
      <source>Unlock Wallet</source>
      <translation>Geldbörse entsperren</translation>
    </message>
  </context>
  <context>
    <name>PriceDetails</name>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="46"/>
      <source>1 BCH is: %1</source>
      <comment>Price of a whole bitcoin cash</comment>
      <translation>1 BCH ist: %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="94"/>
      <source>7d</source>
      <comment>7 days</comment>
      <translation>7T</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="96"/>
      <source>1m</source>
      <comment>1 month</comment>
      <translation>1M</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="98"/>
      <source>3m</source>
      <comment>3 months</comment>
      <translation>3M</translation>
    </message>
  </context>
  <context>
    <name>PriceInputWidget</name>
    <message>
      <location filename="../guis/mobile/PriceInputWidget.qml" line="200"/>
      <source>All Currencies</source>
      <translation>Alle Währungen</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTab</name>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="27"/>
      <source>Receive</source>
      <translation>Empfange</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="96"/>
      <source>Share this QR to receive</source>
      <translation>Diesen QR teilen um zu empfangen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="117"/>
      <source>Encrypted Wallet</source>
      <translation>Verschlüsselte Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="119"/>
      <source>Import Running...</source>
      <translation>Import läuft...</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="252"/>
      <source>Description</source>
      <translation>Beschreibung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="167"/>
      <source>Address</source>
      <comment>Bitcoin Cash address</comment>
      <translation>Adresse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="177"/>
      <source>Clear</source>
      <translation>Leeren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="318"/>
      <location filename="../guis/mobile/ReceiveTab.qml" line="333"/>
      <source>Payment Seen</source>
      <translation>Zahlung gesichtet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="329"/>
      <source>High risk transaction</source>
      <translation>Hochriskante Transaktion</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="331"/>
      <source>Partially Paid</source>
      <translation>Teilweise bezahlt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="335"/>
      <source>Payment Accepted</source>
      <translation>Zahlung akzeptiert</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="337"/>
      <source>Payment Settled</source>
      <translation>Zahlung abgeschlossen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="367"/>
      <source>Continue</source>
      <translation>Fortfahren</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultAccountPage</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="22"/>
      <source>Select Wallet</source>
      <translation>Geldbörse auswählen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="30"/>
      <source>Pick which wallet will be selected on starting Flowee Pay</source>
      <translation>Wählen Sie aus, welche Geldbörse beim Start von Flowee Pay ausgewählt wird</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="69"/>
      <source>No InstaPay limit set</source>
      <translation>Kein Sofortzahlungslimit gesetzt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="70"/>
      <source>InstaPay till %1</source>
      <translation>Sofortzahlung bis %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="72"/>
      <source>InstaPay is turned off</source>
      <translation>Sofortzahlung ist deaktiviert</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultConfigButton</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="22"/>
      <source>Default Wallet</source>
      <translation>Standard-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="32"/>
      <source>%1 is used on startup</source>
      <translation>%1 wird beim Starten verwendet</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionsTab</name>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="28"/>
      <source>Send</source>
      <translation>Senden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="43"/>
      <source>Start Payment</source>
      <translation>Starte Zahlung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="46"/>
      <source>Scan QR</source>
      <translation>Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="52"/>
      <source>Paste</source>
      <translation>Einfügen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="71"/>
      <source>Options</source>
      <translation>Optionen</translation>
    </message>
  </context>
  <context>
    <name>Settings</name>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="24"/>
      <source>Display Settings</source>
      <translation>Anzeigeeinstellungen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="39"/>
      <source>Font sizing</source>
      <translation>Schriftgröße</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="75"/>
      <source>Main View</source>
      <translation>Hauptansicht</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="80"/>
      <source>Show Bitcoin Cash value</source>
      <translation>Zeige Bitcoin Cash Wert</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="88"/>
      <source>Background Synchronization</source>
      <translation>Hintergrund-Synchronisierung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="89"/>
      <source>Keep your wallets synchronized by enabling this</source>
      <translation>Halten Sie Ihre Brieftaschen synchronisiert, indem Sie dies aktivieren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="96"/>
      <source>Unit</source>
      <translation>Einheit</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="156"/>
      <source>Change Currency</source>
      <translation>Währung ändern</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="163"/>
      <source>Dark Theme</source>
      <translation>Dunkles Design</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="165"/>
      <source>Follow System</source>
      <translation>Folge Systemeinstellung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="171"/>
      <source>Dark</source>
      <translation>Dunkel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="180"/>
      <source>Light</source>
      <translation>Hell</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="191"/>
      <source>Notifications</source>
      <translation>Benachrichtigungen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="195"/>
      <source>On new block found</source>
      <translation>Bei neu gefundenem Block</translation>
    </message>
  </context>
  <context>
    <name>SlideToApprove</name>
    <message>
      <location filename="../guis/mobile/SlideToApprove.qml" line="31"/>
      <source>SLIDE TO SEND</source>
      <translation>ZUM SENDEN SCHIEBEN</translation>
    </message>
  </context>
  <context>
    <name>StartupScreen</name>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="86"/>
      <source>Continue</source>
      <translation>Fortfahren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="76"/>
      <source>Moving the world towards a Bitcoin&#xa0;Cash economy</source>
      <translation>Bewege die Welt in Richtung einer Bitcoin&#xa0;Cash Wirtschaft</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="151"/>
      <source>Getting Started</source>
      <translation>Erste Schritte</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="157"/>
      <source>All Videos</source>
      <translation>Alle Videos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="171"/>
      <source>Claim a Cash Stamp</source>
      <translation>Cash Stamp beanspruchen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="188"/>
      <location filename="../guis/mobile/StartupScreen.qml" line="221"/>
      <source>OR</source>
      <translation>OR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="200"/>
      <source>Scan to send to your wallet</source>
      <translation>Scannen um an Ihre Geldbörse zu senden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="235"/>
      <source>Add a different wallet</source>
      <translation>Eine andere Geldbörse hinzufügen</translation>
    </message>
  </context>
  <context>
    <name>TextButton</name>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Enabled</source>
      <translation>Ein</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Disabled</source>
      <translation>Aus</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="29"/>
      <source>Transaction Details</source>
      <translation>Transaktionsdetails</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="32"/>
      <source>Open in Explorer</source>
      <translation>In Explorer öffnen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="46"/>
      <source>Transaction Hash</source>
      <translation>Transaktions-Hash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="93"/>
      <source>First Seen</source>
      <translation>Erstsichtung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="104"/>
      <source>Rejected</source>
      <translation>Abgelehnt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="106"/>
      <source>Waiting for block</source>
      <translation>Warte auf Block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="107"/>
      <source>Mined at</source>
      <translation>Abgebaut am</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionDetails.qml" line="121"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 Block her</numerusform>
        <numerusform>%1 Blöcke her</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="128"/>
      <source>Transaction comment</source>
      <translation>Transaktionskommentar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="150"/>
      <source>Size: %1 bytes</source>
      <translation>Größe: %1 Bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="153"/>
      <source>Coinbase</source>
      <translation>Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="161"/>
      <source>Fees paid</source>
      <translation>Bezahlte Gebühren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="164"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 Bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="177"/>
      <source>Fused from my addresses</source>
      <translation>Fusioniert von meinen Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="179"/>
      <source>Sent from my addresses</source>
      <translation>Gesendet von meinen Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="181"/>
      <source>Sent from addresses</source>
      <translation>Gesendet von den Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="230"/>
      <source>Fused into my addresses</source>
      <translation>Fusioniert in meine Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="232"/>
      <source>Received at addresses</source>
      <translation>Empfangen auf den Adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="233"/>
      <source>Received at my addresses</source>
      <translation>Empfangen auf meinen Adressen</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="40"/>
      <source>Transaction is rejected</source>
      <translation>Transaktion wurde abgelehnt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="42"/>
      <source>Processing</source>
      <translation>Verarbeite</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="62"/>
      <source>Mined</source>
      <translation>Gemined</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="75"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 Block her</numerusform>
        <numerusform>%1 Blöcke her</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="85"/>
      <source>Waiting for block</source>
      <translation>Warte auf Block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="95"/>
      <source>Miner Reward</source>
      <translation>Miner Belohnung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="97"/>
      <source>Fees</source>
      <translation>Gebühren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="103"/>
      <source>Received</source>
      <translation>Erhalten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="99"/>
      <source>Payment to self</source>
      <translation>Zahlung an sich selbst</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="104"/>
      <source>Sent</source>
      <translation>Gesendet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="135"/>
      <source>Holds a token</source>
      <translation>Hält ein Token</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="141"/>
      <source>Sent to</source>
      <translation>Gesendet an</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="165"/>
      <source>Transaction Details</source>
      <translation>Transaktionsdetails</translation>
    </message>
  </context>
  <context>
    <name>TransactionListItem</name>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="91"/>
      <source>Miner Reward</source>
      <translation>Miner Belohnung</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="93"/>
      <source>Fused</source>
      <translation>Fusioniert</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="95"/>
      <source>Received</source>
      <translation>Erhalten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="97"/>
      <source>Moved</source>
      <translation>Verschoben</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="99"/>
      <source>Sent</source>
      <translation>Gesendet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="149"/>
      <source>Rejected</source>
      <translation>Abgelehnt</translation>
    </message>
  </context>
  <context>
    <name>UnlockApplication</name>
    <message>
      <location filename="../guis/mobile/UnlockApplication.qml" line="84"/>
      <source>Quick Receive</source>
      <translation>Schnell empfangen</translation>
    </message>
  </context>
  <context>
    <name>UnlockWidget</name>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="172"/>
      <source>Enter your wallet passcode</source>
      <translation>Geben Sie Ihren Geldbörsen-Zugangscode ein</translation>
    </message>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="308"/>
      <source>Open</source>
      <comment>open wallet with PIN</comment>
      <translation>Öffnen</translation>
    </message>
  </context>
</TS>
