<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>BigTransferModuleInfo</name>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="33"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="38"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="44"/>
      <source>Wallet to Wallet</source>
      <translation>Geldbörse zu Geldbörse</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="34"/>
      <source>Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses.</source>
      <translation>Verschieben Sie viele Münzen zwischen den Geldbörsen. Optimieren Sie die Anonymität, indem Sie nur eine Transaktion pro Ursprungsadresse machen, während Sie sie auf verschiedene Zieladressen aufteilen können.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="39"/>
      <source>Move funds to another wallet</source>
      <translation>Guthaben an andere Geldbörse übertragen</translation>
    </message>
  </context>
  <context>
    <name>Main</name>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="28"/>
      <source>Wallet to Wallet</source>
      <translation>Geldbörse zu Geldbörse</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="38"/>
      <source>Select two wallets to transfer funds simply, using anonimity preserving transactions.</source>
      <translation>Wähle zwei Geldbörsen, um Guthaben zu übertragen, mittels Verwendung von Transaktionen, die die Anonymität wahren.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="42"/>
      <source>Spending Wallet</source>
      <translation>Zahlende Geldbörse</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="54"/>
      <source>Addresses</source>
      <translation>Adressen</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="60"/>
      <source>Coins</source>
      <translation>Münzen</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="68"/>
      <source>Destination Wallet</source>
      <translation>Empfangende Geldbörse</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="80"/>
      <source>Prepare...</source>
      <translation>Wird vorbereitet...</translation>
    </message>
  </context>
  <context>
    <name>ShowPrepared</name>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="29"/>
      <source>Verify %1 Transactions</source>
      <translation>
        <numerusform>%1 Transaktion verifiziert</numerusform>
        <numerusform>%1 Transaktionen verifiziert</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="103"/>
      <source>Coins</source>
      <translation>Münzen</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="190"/>
      <source>Send Now</source>
      <translation>Jetzt senden</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="205"/>
      <source>Create and send all %1 transactions</source>
      <translation>
        <numerusform>%1 Transaktion erstellen und senden</numerusform>
        <numerusform>Alle %1 Transaktionen erstellen und senden</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="282"/>
      <source>Target Coins</source>
      <comment>indicates a number</comment>
      <translation>Ziel Münzen</translation>
    </message>
  </context>
</TS>
