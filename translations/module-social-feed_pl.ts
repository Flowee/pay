<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>Listing</name>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="25"/>
      <source>Videos</source>
      <translation>Filmy</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/Listing.qml" line="80"/>
      <source>Run time:</source>
      <translation>Długość:</translation>
    </message>
  </context>
  <context>
    <name>SocialFeedModuleInfo</name>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="27"/>
      <source>Help and Learning</source>
      <translation>Pomoc i nauka</translation>
    </message>
    <message>
      <location filename="../modules/social-feed/SocialFeedModuleInfo.cpp" line="28"/>
      <source>Want to see the experts show how to use Bitcoin Cash with Flowee Pay? Find all you want via this library of videos.</source>
      <translation>Chcesz zobaczyć jak korzystać z Bitcoin Cash z pomocą Flowee Pay? Znajdź to, czego potrzebujesz, w naszych filmikach.</translation>
    </message>
  </context>
</TS>
