<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>BigTransferModuleInfo</name>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="33"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="38"/>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="44"/>
      <source>Wallet to Wallet</source>
      <translation>Portfel do portfela</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="34"/>
      <source>Move many coins between wallets, optimize for anonimity by offering one transaction per address while allowing it to split over various addresses.</source>
      <translation>Przenieś wiele monet między portfelami, optymalizując pod kątem anonimowości poprzez oferowanie jednej transakcji na adres, pozwalając na podział na różne adresy.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/BigTransferModuleInfo.cpp" line="39"/>
      <source>Move funds to another wallet</source>
      <translation>Przenieś środki do innego portfela</translation>
    </message>
  </context>
  <context>
    <name>Main</name>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="28"/>
      <source>Wallet to Wallet</source>
      <translation>Portfel do portfela</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="38"/>
      <source>Select two wallets to transfer funds simply, using anonimity preserving transactions.</source>
      <translation>Wybierz dwa portfele, aby przenieść środki używając transakcji zapewniających anonimowość.</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="42"/>
      <source>Spending Wallet</source>
      <translation>Portfel źródłowy</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="54"/>
      <source>Addresses</source>
      <translation>Adresy</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="60"/>
      <source>Coins</source>
      <translation>Monety</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="68"/>
      <source>Destination Wallet</source>
      <translation>Portfel docelowy</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/Main.qml" line="80"/>
      <source>Prepare...</source>
      <translation>Przygotuj...</translation>
    </message>
  </context>
  <context>
    <name>ShowPrepared</name>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="29"/>
      <source>Verify %1 Transactions</source>
      <translation>
        <numerusform>Zweryfikuj %1 transakcję</numerusform>
        <numerusform>Zweryfikuj %1 transakcje</numerusform>
        <numerusform>Zweryfikuj %1 transakcji</numerusform>
        <numerusform>Zweryfikuj %1 transakcji</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="103"/>
      <source>Coins</source>
      <translation>Monety</translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="190"/>
      <source>Send Now</source>
      <translation>Wyślij teraz</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="205"/>
      <source>Create and send all %1 transactions</source>
      <translation>
        <numerusform>Utwórz i wyślij %1 transakcję</numerusform>
        <numerusform>Utwórz i wyślij %1 transakcje</numerusform>
        <numerusform>Utwórz i wyślij %1 transakcji</numerusform>
        <numerusform>Utwórz i wyślij %1 transakcji</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/big-transfer/ShowPrepared.qml" line="282"/>
      <source>Target Coins</source>
      <comment>indicates a number</comment>
      <translation>Docelowe Monety</translation>
    </message>
  </context>
</TS>
