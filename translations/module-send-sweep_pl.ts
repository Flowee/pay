<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>SendPage</name>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="28"/>
      <source>Sweep coins</source>
      <translation>Zgarnij monety</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="44"/>
      <source>Sweeping from address:</source>
      <translation>Zgarnianie z adresu:</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="59"/>
      <source>Found %1 coins on address.</source>
      <comment>this is a simple number</comment>
      <translation>
        <numerusform>Znaleziono %1 monetę.</numerusform>
        <numerusform>Znaleziono %1 monety.</numerusform>
        <numerusform>Znaleziono %1 monet.</numerusform>
        <numerusform>Znaleziono %1 monety.</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="63"/>
      <source>Ignoring %1 tokens.</source>
      <comment>Number of CashTokens</comment>
      <translation>
        <numerusform>Ignorowanie %1 tokena.</numerusform>
        <numerusform>Ignorowanie %1 tokenów.</numerusform>
        <numerusform>Ignorowanie %1 tokenów.</numerusform>
        <numerusform>Ignorowanie %1 tokena.</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="118"/>
      <source>Failed to understand QR</source>
      <translation>Nie udało się zrozumieć QR</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="124"/>
      <source>Indexer results invalid. Please try again.</source>
      <translation>Wyniki indeksacji są nieprawidłowe. Spróbuj ponownie.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="149"/>
      <source>Transfer to:</source>
      <translation>Przekaż do:</translation>
    </message>
  </context>
  <context>
    <name>SendSweepModuleInfo</name>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="33"/>
      <source>Claim Cash Stamp</source>
      <translation>Odbierz Cash Stamp</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="34"/>
      <source>A QR code with a CashStamp can be taken to transfer the money to your wallet.</source>
      <translation>Wykorzystaj kod QR z CashStamp, aby otrzymać środki.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="36"/>
      <source>Claim a Cash Stamp</source>
      <translation>Odbierz Cash Stamp</translation>
    </message>
  </context>
  <context>
    <name>StartScan</name>
    <message>
      <location filename="../modules/send-sweep/StartScan.qml" line="28"/>
      <source>Scan QR (WIF) to find funds</source>
      <comment>Please note that WIF and QR are names</comment>
      <translation>Zeskanuj QR (WIF), aby znaleźć środki</translation>
    </message>
  </context>
</TS>
