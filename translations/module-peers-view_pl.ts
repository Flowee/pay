<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>NetView</name>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="27"/>
      <source>Peers</source>
      <translation>Peery</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="30"/>
      <source>Statistics</source>
      <translation>Statystyki</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="39"/>
      <source>No Internet Available</source>
      <translation>Brak dostępu do internetu</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="90"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adres</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="97"/>
      <source>Start-height: %1</source>
      <translation>Wysokość początkowa: %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="100"/>
      <source>ban-score: %1</source>
      <translation>punktacja banu: %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="111"/>
      <source>Opening Connection</source>
      <translation>Otwieranie połączenia</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="113"/>
      <source>Validating peer</source>
      <translation>Weryfikacja peera</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="115"/>
      <source>Validated</source>
      <translation>Zweryfikowano</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="117"/>
      <source>Good Peer</source>
      <comment>A useful peer</comment>
      <translation>Przydatny peer</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="123"/>
      <source>Peer for wallet: %1</source>
      <translation>Peer dla portfela: %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="151"/>
      <source>Disconnect Peer</source>
      <translation>Rozłącz peer</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="155"/>
      <source>Ban Peer</source>
      <translation>Zbanuj peera</translation>
    </message>
  </context>
  <context>
    <name>PeersViewModuleInfo</name>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="24"/>
      <source>Peers View</source>
      <translation>Widok peerów</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="25"/>
      <source>This module provides a view of network servers we connect to often called &apos;peers&apos;.</source>
      <translation>Ten moduł zapewnia widok serwerów sieciowych, z którymi się łączymy, nazywanych popularnie &apos;peerami&apos;.</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="30"/>
      <source>Network Details</source>
      <translation>Szczegóły sieci</translation>
    </message>
  </context>
  <context>
    <name>StatsPage</name>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="25"/>
      <source>IP-Address Statistics</source>
      <translation>Statystyki adresu IP</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="34"/>
      <source>Counts</source>
      <translation>Liczniki</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="37"/>
      <source>Total found</source>
      <translation>Łącznie znaleziono</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="40"/>
      <source>Tried</source>
      <translation>Wypróbowano</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="44"/>
      <source>Misbehaving IPs</source>
      <translation>Nieprawidłowe adresy IP</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="47"/>
      <source>Bad</source>
      <translation>Kiepskie</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="50"/>
      <source>Banned</source>
      <translation>Zbanowane</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="55"/>
      <source>Network</source>
      <translation>Sieć</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="68"/>
      <source>Pardon the Banned</source>
      <translation>Ułaskaw Zbanowanych</translation>
    </message>
  </context>
</TS>
