<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>About</name>
    <message>
      <location filename="../guis/mobile/About.qml" line="23"/>
      <source>About</source>
      <translation>O programie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="43"/>
      <source>Help translate this app</source>
      <translation>Pomóż w tłumaczeniu tej aplikacji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="48"/>
      <source>License</source>
      <translation>Licencja</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="53"/>
      <location filename="../guis/mobile/About.qml" line="61"/>
      <source>Credits</source>
      <translation>Autorzy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="54"/>
      <source>© 2020-2025 Tom Zander and contributors</source>
      <translation>©️ 2020-2025 Tom Zander i współtwórcy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="99"/>
      <source>Project Home</source>
      <translation>Strona projektu</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="100"/>
      <source>With git repository and issues tracker</source>
      <translation>Z repozytorium git i trackerem problemów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="105"/>
      <source>Telegram</source>
      <translation>Telegram</translation>
    </message>
  </context>
  <context>
    <name>AccountHistory</name>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="29"/>
      <source>Home</source>
      <translation>Strona główna</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="162"/>
      <source>Pay</source>
      <translation>Zapłać</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="168"/>
      <source>Receive</source>
      <translation>Otrzymaj</translation>
    </message>
  </context>
  <context>
    <name>AccountPageListItem</name>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="41"/>
      <source>Name</source>
      <translation>Nazwa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="64"/>
      <source>Archived wallets do not check for activities. Balance may be out of date</source>
      <translation>Zarchiwizowane portfele nie sprawdzają aktywności. Saldo może być nieaktualne</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="76"/>
      <source>Backup information</source>
      <translation>Dane kopii zapasowej</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="87"/>
      <source>Backup Details</source>
      <translation>Szczegóły kopii zapasowej</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="121"/>
      <source>Wallet seed-phrase</source>
      <translation>Fraza seedowa portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="148"/>
      <source>Password</source>
      <translation>Hasło</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="159"/>
      <source>Seed format</source>
      <translation>Format seeda</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="168"/>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="269"/>
      <source>Starting Height</source>
      <comment>height refers to block-height</comment>
      <translation>Wysokość początkowa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="173"/>
      <source>Derivation Path</source>
      <translation>Ścieżka derywacji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="180"/>
      <source>xpub</source>
      <translation>xpub</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="210"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</source>
      <translation>Prosimy o zapisanie seeda oraz ścieżki derywacji na papierze, z zachowaniem kolejności słów. Pozwoli to na odzyskanie portfela w przypadku awarii telefonu.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="217"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Ważne&lt;/b&gt;: Nigdy nie udostępniaj seeda innym!</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="229"/>
      <source>Wallet keys</source>
      <translation>Klucze portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="232"/>
      <source>Show Index</source>
      <comment>toggle to show numbers</comment>
      <translation>Pokaż indeks</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="248"/>
      <source>Change Addresses</source>
      <translation>Adresy zawierające resztę</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="251"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Przełącza pomiędzy adresami, na które możesz otrzymać środki od innych a adresami, na które portfel wysyła własną resztę.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="254"/>
      <source>Used Addresses</source>
      <translation>Wykorzystane adresy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="257"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Przełącza pomiędzy wykorzystanymi i niewykorzystanymi adresami</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="309"/>
      <source>Addresses and keys</source>
      <translation>Adresy i klucze</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="317"/>
      <source>Sync Status</source>
      <translation>Status synchronizacji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="357"/>
      <source>Hide balance in overviews</source>
      <translation>Ukryj saldo w podsumowaniach</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="364"/>
      <source>Hide in private mode</source>
      <translation>Ukryj w trybie prywatnym</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Unarchive Wallet</source>
      <translation>Przywróć portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Archive Wallet</source>
      <translation>Zarchiwizuj portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="423"/>
      <source>Really Delete?</source>
      <translation>Czy na pewno usunąć?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="424"/>
      <source>Removing wallet &quot;%1&quot; can not be undone.</source>
      <comment>argument is the wallet name</comment>
      <translation>Usunięcie portfela &quot;%1&quot; nie może zostać cofnięte.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="403"/>
      <source>Remove Wallet</source>
      <translation>Usuń portfel</translation>
    </message>
  </context>
  <context>
    <name>AccountSelectorPopup</name>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="51"/>
      <source>Your Wallets</source>
      <translation>Twoje portfele</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="130"/>
      <source>last active</source>
      <translation>Ostatnio aktywny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="139"/>
      <source>Needs PIN to open</source>
      <translation>Do otwarcia wymagany jest PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="175"/>
      <source>Balance Total</source>
      <translation>Saldo Całkowite</translation>
    </message>
  </context>
  <context>
    <name>AccountSyncState</name>
    <message>
      <location filename="../guis/mobile/AccountSyncState.qml" line="95"/>
      <source>Status: Offline</source>
      <translation>Status: Offline</translation>
    </message>
  </context>
  <context>
    <name>AccountsList</name>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallet</source>
      <translation>Portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallets</source>
      <translation>Portfele</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="28"/>
      <source>Add Wallet</source>
      <translation>Dodaj Portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Exit Private Mode</source>
      <translation>Wyjdź z trybu prywatnego</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Enter Private Mode</source>
      <translation>Włącz tryb prywatny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="60"/>
      <source>Reveals wallets marked private</source>
      <translation>Ujawnia portfele oznaczone jako prywatne</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="61"/>
      <source>Hides wallets marked private</source>
      <translation>Ukrywa portfele oznaczone jako prywatne</translation>
    </message>
  </context>
  <context>
    <name>BackgroundSyncConfig</name>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="24"/>
      <source>Background Synchronization</source>
      <translation>Synchronizacja w tle</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="33"/>
      <source>Without background synchronization your wallets will not see new transactions until you open Flowee Pay</source>
      <translation>Bez synchronizacji w tle portfele nie będą widzieć nowych transakcji dopóki nie otworzysz Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="39"/>
      <source>Allow Background Synchronization</source>
      <translation>Zezwól na synchronizację w tle</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="106"/>
      <source>Every %1 hours</source>
      <translation>Co %1 godz.</translation>
    </message>
  </context>
  <context>
    <name>CurrencySelector</name>
    <message>
      <location filename="../guis/mobile/CurrencySelector.qml" line="24"/>
      <source>Select Currency</source>
      <translation>Wybierz walutę</translation>
    </message>
  </context>
  <context>
    <name>ExploreModules</name>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="24"/>
      <source>Explore</source>
      <translation>Eksploruj</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="120"/>
      <source>Open</source>
      <translation>Otwórz</translation>
    </message>
  </context>
  <context>
    <name>FilterPopup</name>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="45"/>
      <source>Transactions Filter</source>
      <translation>Filtr transakcji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="192"/>
      <source>Only with a comment</source>
      <comment>This is a statement about a transaction</comment>
      <translation>Tylko z komentarzem</translation>
    </message>
  </context>
  <context>
    <name>ImportWalletPage</name>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="26"/>
      <source>Import Wallet</source>
      <translation>Importuj portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="159"/>
      <source>Select import method</source>
      <translation>Wybierz metodę importu</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="172"/>
      <source>Scan QR</source>
      <translation>Zeskanuj kod QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="212"/>
      <source>OR</source>
      <translation>LUB</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="224"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Sekret jako tekst</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="263"/>
      <source>Unknown word(s) found</source>
      <translation>Znaleziono nieznane słowa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="274"/>
      <source>Next</source>
      <translation>Dalej</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="298"/>
      <source>Address to import</source>
      <translation>Adres do zaimportowania</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="312"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="446"/>
      <source>New Wallet Name</source>
      <translation>Nazwa nowego portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="330"/>
      <source>Force Single Address</source>
      <translation>Wymuś pojedynczy adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="331"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Włączenie sprawi, że dodatkowe adresy nie zostaną automatycznie wygenerowane dla tego portfela. Reszta wydana z transakcji trafi na zaimportowany klucz.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="336"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="505"/>
      <source>Oldest Transaction</source>
      <translation>Najstarsza transakcja</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="342"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Sprawdź wiek portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="384"/>
      <source>Nothing found for wallet</source>
      <translation>Nie znaleziono nic dla portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="390"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="549"/>
      <source>Start</source>
      <translation>Rozpocznij</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="464"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Poznaj szczegóły</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="515"/>
      <source>Derivation Path</source>
      <translation>Ścieżka derywacji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="528"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Nic nie znaleziono dla seeda. Czy posiada hasło?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="536"/>
      <source>Password</source>
      <translation>Hasło</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="542"/>
      <source>imported wallet password</source>
      <translation>hasło do importowanego portfela</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigButton</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Enable Instant Pay</source>
      <translation>Włącz Szybką Płatność</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Configure Instant Pay</source>
      <translation>Skonfiguruj Szybką Płatność</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="54"/>
      <source>Limits for %1</source>
      <comment>argument is a name</comment>
      <translation>Limity dla %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="55"/>
      <source>Disabled for %1</source>
      <comment>argument is a name</comment>
      <translation>Wyłączone dla %1</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigPage</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="23"/>
      <source>Instant Pay</source>
      <translation>Szybka Płatność</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="29"/>
      <source>Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</source>
      <translation>Zeskanowanie kodu QR z włączoną natychmiastową płatnością sprawi, że w ramach ustalonego limitu transfer zostanie zrealizowany bez potwierdzenia.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="54"/>
      <source>Protected wallets can not be used for InstaPay because they require a PIN before usage</source>
      <translation>Zabezpieczone portfele nie mogą być używane do Szybkich Płatności, ponieważ wymagają PIN-u przed użyciem</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="65"/>
      <source>Enable Instant Pay for this wallet</source>
      <translation>Włącz Szybką Płatność w tym portfelu</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="72"/>
      <source>Maximum Amount</source>
      <translation>Maksymalna kwota</translation>
    </message>
  </context>
  <context>
    <name>LockApplication</name>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="25"/>
      <source>Security</source>
      <translation>Bezpieczeństwo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="35"/>
      <source>PIN on startup</source>
      <translation>PIN podczas uruchamiania</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter current PIN</source>
      <translation>Wprowadź obecny PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter new PIN</source>
      <translation>Wprowadź nowy PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="80"/>
      <source>Repeat PIN</source>
      <translation>Powtórz PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Remove PIN</source>
      <translation>Usuń PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Set PIN</source>
      <translation>Ustaw PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="124"/>
      <source>PIN has been removed</source>
      <translation>PIN został usunięty</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="127"/>
      <source>PIN has been set</source>
      <translation>PIN został ustawiony</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="196"/>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
  </context>
  <context>
    <name>MainView</name>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="43"/>
      <source>Explore</source>
      <translation>Eksploruj</translation>
    </message>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="65"/>
      <source>Find More</source>
      <translation>Znajdź więcej</translation>
    </message>
  </context>
  <context>
    <name>MainViewBase</name>
    <message>
      <location filename="../guis/mobile/MainViewBase.qml" line="74"/>
      <source>No Internet Available</source>
      <translation>Brak dostępu do internetu</translation>
    </message>
  </context>
  <context>
    <name>MenuOverlay</name>
    <message>
      <location filename="../guis/mobile/MenuOverlay.qml" line="318"/>
      <source>Add Wallet</source>
      <translation>Dodaj portfel</translation>
    </message>
  </context>
  <context>
    <name>NewAccount</name>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="26"/>
      <source>New Bitcoin Cash Wallet</source>
      <translation>Nowy portfel Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="39"/>
      <source>New HD Wallet</source>
      <translation>Nowy portfel HD</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="45"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Portfel bazujący na ciągu losowych słów (seedzie)</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="46"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Łatwa kopia zapasowa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="47"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Najbardziej kompatybilny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="52"/>
      <source>Import Existing Wallet</source>
      <translation>Importuj istniejący portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="58"/>
      <source>Imports seed-phrase</source>
      <translation>Importuje seeda</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="59"/>
      <source>Imports private key</source>
      <translation>Importuje klucz prywatny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="64"/>
      <source>New Basic Wallet</source>
      <translation>Nowy portfel podstawowy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="70"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Portfel bazujący na kluczach prywatnych</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="71"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Trudna kopia zapasowa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="72"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Świetny do krótkotrwałego użytku</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="87"/>
      <source>New Wallet</source>
      <translation>Nowy portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="95"/>
      <source>This creates a new empty wallet with simple multi-address capability</source>
      <translation>Tworzy nowy pusty portfel z prostą funkcją wieloadresową</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="101"/>
      <location filename="../guis/mobile/NewAccount.qml" line="155"/>
      <source>Name</source>
      <translation>Nazwa</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="111"/>
      <source>Force Single Address</source>
      <translation>Wymuś jeden adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="112"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Gdy włączone, ten portfel będzie ograniczony do jednego adresu.
Pozwala to na tworzenie kopii zapasowej tylko jednego klucza prywatnego</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="120"/>
      <location filename="../guis/mobile/NewAccount.qml" line="180"/>
      <source>Create</source>
      <translation>Utwórz</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="143"/>
      <source>New HD-Wallet</source>
      <translation>Nowy portfel HD</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="151"/>
      <source>This creates a new wallet that can be backed up with a seed-phrase</source>
      <translation>Tworzy nowy portfel, który może być zachowany przy pomocy frazy seeda</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="163"/>
      <source>Derivation</source>
      <translation>Derywacja</translation>
    </message>
  </context>
  <context>
    <name>PayWithQR</name>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="26"/>
      <source>Approve Payment</source>
      <translation>Zatwierdź płatność</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="31"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Wyślij wszystko</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="41"/>
      <source>Show Address</source>
      <comment>to show a bitcoincash address</comment>
      <translation>Pokaż adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="44"/>
      <source>Edit Amount</source>
      <comment>Edit amount of money to send</comment>
      <translation>Edytuj kwotę</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="98"/>
      <source>Invalid QR code</source>
      <translation>Nieprawidłowy kod QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="112"/>
      <source>I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</source>
      <translation>Nie rozumiem zeskanowanego kodu. Przepraszam, mogę rozpocząć płatności.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="116"/>
      <source>details</source>
      <translation>szczegóły</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="127"/>
      <source>Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</source>
      <translation>Zeskanowany tekst: &lt;pre&gt;%1&lt;/pre&gt;</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="176"/>
      <source>Payment description</source>
      <translation>Opis płatności</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="215"/>
      <source>Destination Address</source>
      <translation>Adres docelowy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="409"/>
      <source>Unlock Wallet</source>
      <translation>Odblokuj portfel</translation>
    </message>
  </context>
  <context>
    <name>PriceDetails</name>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="46"/>
      <source>1 BCH is: %1</source>
      <comment>Price of a whole bitcoin cash</comment>
      <translation>1 BCH to: %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="94"/>
      <source>7d</source>
      <comment>7 days</comment>
      <translation>7d</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="96"/>
      <source>1m</source>
      <comment>1 month</comment>
      <translation>1m</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="98"/>
      <source>3m</source>
      <comment>3 months</comment>
      <translation>3m</translation>
    </message>
  </context>
  <context>
    <name>PriceInputWidget</name>
    <message>
      <location filename="../guis/mobile/PriceInputWidget.qml" line="200"/>
      <source>All Currencies</source>
      <translation>Wszystkie waluty</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTab</name>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="27"/>
      <source>Receive</source>
      <translation>Otrzymaj</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="96"/>
      <source>Share this QR to receive</source>
      <translation>Udostępnij ten QR kod, aby otrzymać</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="117"/>
      <source>Encrypted Wallet</source>
      <translation>Zaszyfrowany portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="119"/>
      <source>Import Running...</source>
      <translation>Trwa Importowanie...</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="252"/>
      <source>Description</source>
      <translation>Opis</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="167"/>
      <source>Address</source>
      <comment>Bitcoin Cash address</comment>
      <translation>Adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="177"/>
      <source>Clear</source>
      <translation>Wyczyść</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="318"/>
      <location filename="../guis/mobile/ReceiveTab.qml" line="333"/>
      <source>Payment Seen</source>
      <translation>Płatność zaobserwowana</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="329"/>
      <source>High risk transaction</source>
      <translation>Transakcja o wysokim ryzyku</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="331"/>
      <source>Partially Paid</source>
      <translation>Częściowo opłacona</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="335"/>
      <source>Payment Accepted</source>
      <translation>Płatność zaakceptowana</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="337"/>
      <source>Payment Settled</source>
      <translation>Płatność rozliczona</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="367"/>
      <source>Continue</source>
      <translation>Kontynuuj</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultAccountPage</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="22"/>
      <source>Select Wallet</source>
      <translation>Wybierz portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="30"/>
      <source>Pick which wallet will be selected on starting Flowee Pay</source>
      <translation>Wybierz portfel, który zostanie pokazany przy uruchomieniu FloweePay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="69"/>
      <source>No InstaPay limit set</source>
      <translation>Nie ustawiono limitu Szybkich Płatności</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="70"/>
      <source>InstaPay till %1</source>
      <translation>Szybkie płatności do %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="72"/>
      <source>InstaPay is turned off</source>
      <translation>Szybkie Płatności wyłączone</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultConfigButton</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="22"/>
      <source>Default Wallet</source>
      <translation>Domyślny Portfel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="32"/>
      <source>%1 is used on startup</source>
      <translation>%1 jest używany przy starcie</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionsTab</name>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="28"/>
      <source>Send</source>
      <translation>Wyślij</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="43"/>
      <source>Start Payment</source>
      <translation>Rozpocznij płatność</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="46"/>
      <source>Scan QR</source>
      <translation>Zeskanuj kod QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="52"/>
      <source>Paste</source>
      <translation>Wklej</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="71"/>
      <source>Options</source>
      <translation>Opcje</translation>
    </message>
  </context>
  <context>
    <name>Settings</name>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="24"/>
      <source>Display Settings</source>
      <translation>Ustawienia wyświetlania</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="39"/>
      <source>Font sizing</source>
      <translation>Rozmiar czcionki</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="75"/>
      <source>Main View</source>
      <translation>Ekran główny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="80"/>
      <source>Show Bitcoin Cash value</source>
      <translation>Pokazuj wartość Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="88"/>
      <source>Background Synchronization</source>
      <translation>Synchronizacja w tle</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="89"/>
      <source>Keep your wallets synchronized by enabling this</source>
      <translation>Włącz, by zawsze mieć zsynchronizowane portfele</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="96"/>
      <source>Unit</source>
      <translation>Jednostka</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="156"/>
      <source>Change Currency</source>
      <translation>Zmień walutę</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="163"/>
      <source>Dark Theme</source>
      <translation>Tryb ciemny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="165"/>
      <source>Follow System</source>
      <translation>Użyj systemowego</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="171"/>
      <source>Dark</source>
      <translation>Ciemny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="180"/>
      <source>Light</source>
      <translation>Jasny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="191"/>
      <source>Notifications</source>
      <translation>Powiadomienia</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="195"/>
      <source>On new block found</source>
      <translation>o odkryciu nowego bloku</translation>
    </message>
  </context>
  <context>
    <name>SlideToApprove</name>
    <message>
      <location filename="../guis/mobile/SlideToApprove.qml" line="31"/>
      <source>SLIDE TO SEND</source>
      <translation>PRZECIĄGNIJ ŻEBY WYSŁAĆ</translation>
    </message>
  </context>
  <context>
    <name>StartupScreen</name>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="86"/>
      <source>Continue</source>
      <translation>Kontynuuj</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="76"/>
      <source>Moving the world towards a Bitcoin&#xa0;Cash economy</source>
      <translation>Przesuwamy świat w kierunku ekonomii Bitcoin&#xa0;Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="151"/>
      <source>Getting Started</source>
      <translation>Wprowadzenie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="157"/>
      <source>All Videos</source>
      <translation>Filmiki</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="171"/>
      <source>Claim a Cash Stamp</source>
      <translation>Odbierz Cash Stamp</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="188"/>
      <location filename="../guis/mobile/StartupScreen.qml" line="221"/>
      <source>OR</source>
      <translation>LUB</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="200"/>
      <source>Scan to send to your wallet</source>
      <translation>Zeskanuj, aby wysłać do portfela</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="235"/>
      <source>Add a different wallet</source>
      <translation>Dodaj inny portfel</translation>
    </message>
  </context>
  <context>
    <name>TextButton</name>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Enabled</source>
      <translation>Aktywny</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Disabled</source>
      <translation>Wyłączony</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="29"/>
      <source>Transaction Details</source>
      <translation>Szczegóły transakcji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="32"/>
      <source>Open in Explorer</source>
      <translation>Otwórz w Eksploratorze</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="46"/>
      <source>Transaction Hash</source>
      <translation>Hasz Transakcji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="93"/>
      <source>First Seen</source>
      <translation>Zaobserwowano</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="104"/>
      <source>Rejected</source>
      <translation>Odrzucona</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="106"/>
      <source>Waiting for block</source>
      <translation>Oczekiwanie na blok</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="107"/>
      <source>Mined at</source>
      <translation>Wykopano</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionDetails.qml" line="121"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 blok temu</numerusform>
        <numerusform>%1 bloki temu</numerusform>
        <numerusform>%1 bloków temu</numerusform>
        <numerusform>%1 bloku temu</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="128"/>
      <source>Transaction comment</source>
      <translation>Komentarz do transakcji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="150"/>
      <source>Size: %1 bytes</source>
      <translation>Rozmiar: %1 bajtów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="153"/>
      <source>Coinbase</source>
      <translation>Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="161"/>
      <source>Fees paid</source>
      <translation>Koszt transakcji</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="164"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bajtów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="177"/>
      <source>Fused from my addresses</source>
      <translation>Fuzja z moich adresów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="179"/>
      <source>Sent from my addresses</source>
      <translation>Wysłano z moich adresów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="181"/>
      <source>Sent from addresses</source>
      <translation>Wysłano z adresów</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="230"/>
      <source>Fused into my addresses</source>
      <translation>Fuzja na mój adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="232"/>
      <source>Received at addresses</source>
      <translation>Wpłynęło na adresy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="233"/>
      <source>Received at my addresses</source>
      <translation>Wpłynęło na moje adresy</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="40"/>
      <source>Transaction is rejected</source>
      <translation>Transakcja została odrzucona</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="42"/>
      <source>Processing</source>
      <translation>Przetwarzanie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="62"/>
      <source>Mined</source>
      <translation>Wykopano</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="75"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 blok temu</numerusform>
        <numerusform>%1 bloki temu</numerusform>
        <numerusform>%1 bloków temu</numerusform>
        <numerusform>%1 bloku temu</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="85"/>
      <source>Waiting for block</source>
      <translation>Oczekiwanie na blok</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="95"/>
      <source>Miner Reward</source>
      <translation>Nagroda dla górnika</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="97"/>
      <source>Fees</source>
      <translation>Opłaty</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="103"/>
      <source>Received</source>
      <translation>Otrzymano</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="99"/>
      <source>Payment to self</source>
      <translation>Płatność dla siebie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="104"/>
      <source>Sent</source>
      <translation>Wysłano</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="135"/>
      <source>Holds a token</source>
      <translation>Przechowuje token</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="141"/>
      <source>Sent to</source>
      <translation>Wysłano do</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="165"/>
      <source>Transaction Details</source>
      <translation>Szczegóły transakcji</translation>
    </message>
  </context>
  <context>
    <name>TransactionListItem</name>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="91"/>
      <source>Miner Reward</source>
      <translation>Nagroda dla górnika</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="93"/>
      <source>Fused</source>
      <translation>Fused</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="95"/>
      <source>Received</source>
      <translation>Otrzymano</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="97"/>
      <source>Moved</source>
      <translation>Przeniesiono</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="99"/>
      <source>Sent</source>
      <translation>Wysłano</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="149"/>
      <source>Rejected</source>
      <translation>Odrzucono</translation>
    </message>
  </context>
  <context>
    <name>UnlockApplication</name>
    <message>
      <location filename="../guis/mobile/UnlockApplication.qml" line="84"/>
      <source>Quick Receive</source>
      <translation>Szybka wpłata</translation>
    </message>
  </context>
  <context>
    <name>UnlockWidget</name>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="172"/>
      <source>Enter your wallet passcode</source>
      <translation>Wprowadź hasło</translation>
    </message>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="308"/>
      <source>Open</source>
      <comment>open wallet with PIN</comment>
      <translation>Otwórz</translation>
    </message>
  </context>
</TS>
