<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt-BR" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation>Desconectado</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation>Carteira: atualizada</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation type="unfinished">
        <numerusform>Atrasado: %1 semanas, %2 dias</numerusform>
        <numerusform>Behind: %1 weeks, %2 days</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation type="unfinished">
        <numerusform>Atrasado: %1 dias</numerusform>
        <numerusform>Behind: %1 days</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation>Atualizado</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation>Atualizando</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation type="unfinished">
        <numerusform>Ainda %1 horas atrasado</numerusform>
        <numerusform>Still %1 hours behind</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation>Carteira com endereço único.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation>Carteira baseada numa senha HD</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation>Carteira com múltiplos endereços.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation>próprio</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="65"/>
      <source>Sending Payment</source>
      <translation>Enviando pagamento</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="82"/>
      <source>Payment Sent</source>
      <translation>Pagamento enviado</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="91"/>
      <source>Failed</source>
      <translation type="unfinished">Failed</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="92"/>
      <source>Transaction rejected by network</source>
      <translation>Transação rejeitada pela rede</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="145"/>
      <source>Payment has been sent to:</source>
      <translation type="unfinished">Payment has been sent to:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="178"/>
      <source>Copied address to clipboard</source>
      <translation type="unfinished">Copied address to clipboard</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="183"/>
      <source>Opening Website</source>
      <translation>Abrindo página</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="194"/>
      <source>Add a personal note</source>
      <translation>Adicionar nota pessoal</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="204"/>
      <source>Close</source>
      <translation>Fechar</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation>Moeda fundida para maior anonimato</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation type="unfinished">Value now</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation type="unfinished">Value then</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="470"/>
      <source>Initial Wallet</source>
      <translation>Carteira inicial</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="655"/>
      <location filename="../src/FloweePay.cpp" line="713"/>
      <source>Today</source>
      <translation>Hoje</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="657"/>
      <location filename="../src/FloweePay.cpp" line="715"/>
      <source>Yesterday</source>
      <translation>Ontem</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="702"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation>Agora</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="703"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation>
        <numerusform>%1 minuto atrás</numerusform>
        <numerusform>%1 minutos atrás</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="707"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation>Meia hora atrás</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="709"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation>
        <numerusform>%1 hora atrás</numerusform>
        <numerusform>%1 horas atrás</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation>Copiar</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Explore</source>
      <translation>Explorar</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Settings</source>
      <translation>Configurações</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>Security</source>
      <translation>Segurança</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>About</source>
      <translation>Sobre</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Wallets</source>
      <translation>Carteiras</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="47"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Bloco Bitcoin Cash minerado. Último bloco: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="49"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>tBCH (testnet4) bloco minerado: %1</translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="101"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Bloco Bitcoin Cash minerado. Último bloco: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>tBCH (testnet4) bloco minerado: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>Mute</source>
      <translation>Silenciar</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="147"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation type="unfinished">
        <numerusform>New Transactions</numerusform>
        <numerusform>New Transactions</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="165"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation>%1Novas transações de %2 carteiras encontradas (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="170"/>
      <source>A payment of %1 has been sent</source>
      <translation>Um pagamento de %1 foi enviado</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="174"/>
      <source>%1 new transactions found (%2)</source>
      <translation>
        <numerusform>%1 novas transações encontradas (%2)</numerusform>
        <numerusform>%1 novas transações encontradas (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation>PIN inválido</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation type="unfinished">Wallet is locked</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation>Saldo insuficiente para taxas</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation>Saldo insuficiente para realizar pagamento!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation>Valor muito alto. A quantia selecionada requer moedas demais.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation type="unfinished">Request received over insecure channel. Anyone could have altered it!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation type="unfinished">Download of payment request failed.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation type="unfinished">Payment request unreadable</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation type="unfinished">Payment request expired</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation type="unfinished">Instant Pay limit is %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation type="unfinished">Selected wallet: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="110"/>
      <source>Copied to clipboard</source>
      <translation>Copiado para área de transferência</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation>Alterar %1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation type="unfinished">Main #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation type="unfinished">Unconfirmed</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation>
        <numerusform>%1 hora</numerusform>
        <numerusform>%1 horas</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation>
        <numerusform>%1 dia</numerusform>
        <numerusform>%1 dias</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation type="unfinished">
        <numerusform>%1 semanas</numerusform>
        <numerusform>%1 weeks</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation type="unfinished">
        <numerusform>%1 months</numerusform>
        <numerusform>%1 months</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation>Alterar %1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="230"/>
      <source>Today</source>
      <translation>Hoje</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="232"/>
      <source>Yesterday</source>
      <translation>Ontem</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="234"/>
      <source>Earlier this week</source>
      <translation type="unfinished">Earlier this week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="236"/>
      <source>This week</source>
      <translation type="unfinished">This week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="238"/>
      <source>Earlier this month</source>
      <translation type="unfinished">Earlier this month</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation type="unfinished">Explanation</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation type="unfinished">Coins a / b
 a) active coin-count.
 b) historical coin-count.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="126"/>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="159"/>
      <source>Copy Address</source>
      <translation type="unfinished">Copy Address</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="163"/>
      <source>QR of Address</source>
      <translation type="unfinished">QR of Address</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="170"/>
      <source>Copy Private Key</source>
      <translation type="unfinished">Copy Private Key</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="174"/>
      <source>QR of Private Key</source>
      <translation type="unfinished">QR of Private Key</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="203"/>
      <source>Coins: %1 / %2</source>
      <translation type="unfinished">Coins: %1 / %2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="225"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation type="unfinished">Signed with Schnorr signatures in the past</translation>
    </message>
  </context>
</TS>
