<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation>Offline</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation>Portemonnee: gesynchroniseerd</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation>
        <numerusform>%1 week, %2 dagen te gaan</numerusform>
        <numerusform>%1 weken, %2 dagen te gaan</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation>
        <numerusform>%1 dag oude data</numerusform>
        <numerusform>%1 dagen oude info</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation>Is volledig bijgewerkt</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation>Aan het bijwerken</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation>
        <numerusform>Nog één uur</numerusform>
        <numerusform>Nog %1 uur</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation>Deze portemonnee heeft één enkel adres.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation>Dit is een portemonnee gebaseerd op een HD-herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation>Dit is een eenvoudige portemonnee met diverse adressen.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation>zelf</translation>
    </message>
  </context>
  <context>
    <name>BigCloseButton</name>
    <message>
      <location filename="../guis/Flowee/BigCloseButton.qml" line="31"/>
      <source>Close</source>
      <translation>Sluiten</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="96"/>
      <source>Sending Payment</source>
      <translation>Betaling wordt verzonden</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="98"/>
      <source>Payment Sent</source>
      <translation>Betaling Verzonden</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="100"/>
      <source>Failed</source>
      <translation>Mislukt</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="133"/>
      <source>Transaction rejected by network</source>
      <translation>Transactie afgewezen door het netwerk</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="179"/>
      <source>The payment has been sent to:</source>
      <comment>Followed by the address</comment>
      <translation>Betaling is verzonden naar:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="202"/>
      <source>Add a personal note</source>
      <translation>Voeg een persoonlijke notitie toe</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation>Munt is gefuseerd voor verhoogde anonimiteit</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation>Waarde nu</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation>Waarde toen</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="433"/>
      <source>Initial Wallet</source>
      <translation>Eerste portemonnee</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="615"/>
      <location filename="../src/FloweePay.cpp" line="673"/>
      <source>Today</source>
      <translation>Vandaag</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="617"/>
      <location filename="../src/FloweePay.cpp" line="675"/>
      <source>Yesterday</source>
      <translation>Gisteren</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="662"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation>Nu</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="663"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation>
        <numerusform>%1 minuut geleden</numerusform>
        <numerusform>%1 minuten geleden</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="667"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation>Half uur geleden</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="669"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation>
        <numerusform>%1 uur geleden</numerusform>
        <numerusform>%1 uren geleden</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation>Kopieer</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Explore</source>
      <translation>Ontdek</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Settings</source>
      <translation>Instellingen</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Security</source>
      <translation>Beveiliging</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>About</source>
      <translation>Over Ons</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>Wallets</source>
      <translation>Portemonnees</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager.cpp" line="79"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation>%1 nieuwe transacties in %2 portemonnees gevonden (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager.cpp" line="81"/>
      <source>A payment of %1 has been sent</source>
      <translation>Een betaling van %1 is verzonden</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager.cpp" line="82"/>
      <source>%1 new transactions found (%2)</source>
      <translation>
        <numerusform>%1 nieuwe transactie gevonden (%2)</numerusform>
        <numerusform>%1 nieuwe transacties gevonden (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="86"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Bitcoin Cash-blok gemijnd. Hoogte: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="88"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>tBCH (testnet4) blok gemijnd: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="107"/>
      <source>Mute</source>
      <translation>Negeren</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="142"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation>
        <numerusform>Nieuwe transactie</numerusform>
        <numerusform>Nieuwe transacties</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation>Ongeldige PIN</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation>Portemonnee is vergrendeld</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation>Onvoldoende saldo voor transactiekosten</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation>Niet genoeg saldo in portemonnee om te betalen!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation>Transactie te groot. Geselecteerd bedrag vereist te veel munten.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation>Verzoek ontvangen via onveilig kanaal. Hij kan door eenieder gewijzigd zijn!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation>Downloaden van betalingsverzoek mislukt.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation>Betalingsverzoek onleesbaar</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation>Betalingsverzoek verlopen</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation>Directbetalen limiet is %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation>Geselecteerde portemonnee: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="109"/>
      <source>Copied to clipboard</source>
      <translation>Naar klembord gekopieerd</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Copy</source>
      <translation>Kopieer</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="85"/>
      <source>Paste</source>
      <translation>Plak</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation>Plak</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation>Wisselmunt #%1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation>Standaard#%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation>Onbevestigd</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation>
        <numerusform>één uur</numerusform>
        <numerusform>%1 uren</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation>
        <numerusform>%1 dag</numerusform>
        <numerusform>%1 dagen</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation>
        <numerusform>%1 week</numerusform>
        <numerusform>%1 weken</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation>
        <numerusform>%1 maand</numerusform>
        <numerusform>%1 maanden</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation>Wisselmunt #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="250"/>
      <source>Today</source>
      <translation>Vandaag</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="252"/>
      <source>Yesterday</source>
      <translation>Gisteren</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="254"/>
      <source>Earlier this week</source>
      <translation>Eerder deze week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="256"/>
      <source>This week</source>
      <translation>Deze week</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="258"/>
      <source>Earlier this month</source>
      <translation>Eerder deze maand</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsModel</name>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="83"/>
      <source>Change #%1</source>
      <translation>Wisselmunt #%1</translation>
    </message>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="85"/>
      <source>Main #%1</source>
      <translation>Standaard#%1</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation>Uitleg</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation>Munten a / b
 a) actieve munten aantal.
 b) historische munten.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="162"/>
      <source>Copy Address</source>
      <translation>Kopieer adres</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="166"/>
      <source>QR of Address</source>
      <translation>QR van het adres</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="173"/>
      <source>Copy Private Key</source>
      <translation>Kopieer privésleutel</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="177"/>
      <source>QR of Private Key</source>
      <translation>QR van de privésleutel</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="206"/>
      <source>Coins: %1 / %2</source>
      <translation>Munten: %1 / %2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="228"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation>In het verleden ondertekend met Schnorr</translation>
    </message>
  </context>
</TS>
