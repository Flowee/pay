<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ha" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation>Baya kan na'ura</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation>Wallet: Na zamani</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation>
        <numerusform>Bayan: %1 makonni, %2 kwanaki</numerusform>
        <numerusform>Bayan: %1 makonni, %2 kwanaki</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation>
        <numerusform>Bayan: %1 kwanaki</numerusform>
        <numerusform>Bayan: %1 kwanaki</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation>Na zamani</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation>Sabontawa</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation>
        <numerusform>Har yanzu %1 a baya</numerusform>
        <numerusform>Har yanzu %1 a baya</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation>Wannan asusun jakar adireshi ɗaya ce.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation>Wannan asusun ya dogara ne akan jimlar iri na HD</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation>Wannan asusun ɗin adireshi ne mai sauƙi da yawa.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation>Kai</translation>
    </message>
  </context>
  <context>
    <name>BigCloseButton</name>
    <message>
      <location filename="../guis/Flowee/BigCloseButton.qml" line="31"/>
      <source>Close</source>
      <translation>Kulle</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="96"/>
      <source>Sending Payment</source>
      <translation>Aika Biyan Kuɗi</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="98"/>
      <source>Payment Sent</source>
      <translation>An aika Biya</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="100"/>
      <source>Failed</source>
      <translation>Ba a yi nasara ba</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="133"/>
      <source>Transaction rejected by network</source>
      <translation>hanyar sadarwa ta ƙi ciniki</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="179"/>
      <source>The payment has been sent to:</source>
      <comment>Followed by the address</comment>
      <translation>An aika biyan kuɗi zuwa:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="202"/>
      <source>Add a personal note</source>
      <translation>Ƙara bayanin kula na sirri</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation>An haɗa tsabar kuɗi don ƙara ɓoye suna</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation>Daraja yanzu</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation>Daraja zuwa ga</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="433"/>
      <source>Initial Wallet</source>
      <translation>Asusu na farko</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="615"/>
      <location filename="../src/FloweePay.cpp" line="673"/>
      <source>Today</source>
      <translation>Yau</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="617"/>
      <location filename="../src/FloweePay.cpp" line="675"/>
      <source>Yesterday</source>
      <translation>Jiya</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="662"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation>Yanzu</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="663"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation>
        <numerusform>%1 da yawuce</numerusform>
        <numerusform>%1 minti da yawuce</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="667"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation>½ awa da ya wuce</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="669"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation>
        <numerusform>%1 Awa da ya wuce</numerusform>
        <numerusform>%1 Awa da ya wuce</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation>Kwafi</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Explore</source>
      <translation>Bincika</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Settings</source>
      <translation>Saituna</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Security</source>
      <translation>Tsaro</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>About</source>
      <translation>Game da</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>Wallets</source>
      <translation>Asusu</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager.cpp" line="79"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation>%1 sabbin ma'amaloli a cikin %2 asusun da aka samu (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager.cpp" line="81"/>
      <source>A payment of %1 has been sent</source>
      <translation>An aika biyan kuɗi na %1</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager.cpp" line="82"/>
      <source>%1 new transactions found (%2)</source>
      <translation>
        <numerusform>%1 sababbin ma'amaloli akasamu (%2)</numerusform>
        <numerusform>%1 sababbin ma'amaloli aka samu (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="86"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Bitcoin Cash tubali hako ma'adanai ne. Tsawo: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="88"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>tBCH (testnet4) toshe ma'adinai: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="107"/>
      <source>Mute</source>
      <translation>Shiru</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="142"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation type="unfinished">
        <numerusform>Sabbin Ma'amaloli</numerusform>
        <numerusform>New Transactions</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation>Makulli Mara inganci</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation type="unfinished">Wallet is locked</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation>Babu isassun kuɗin da aka zaɓa don kuɗi</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation>Babu isassun kuɗi a cikin asusu don biyan kuɗi!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation>Ma'amala yayi girma sosai. Adadin da aka zaɓa yana buƙatar tsabar kuɗi da yawa.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation>An karɓi buƙatar akan tashar da ba ta da tsaro. Kowa zai iya canza shi!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation>Sauke buƙatar biyan kuɗi ya gagara.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation>Ba za a iya karanta buƙatar biyan kuɗi ba</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation>Neman biyan kuɗi ya ƙare</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation>Iyakar Biyan Nan take shine %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation>Asusun da aka zaɓa: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="109"/>
      <source>Copied to clipboard</source>
      <translation>An kwafo zuwa allo</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Copy</source>
      <translation>Kwafi</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="85"/>
      <source>Paste</source>
      <translation>Wallafa</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation>Wallafa</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation>Chanji #%1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation>Mafarin #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation>Ba'a tabbatar ba</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation>
        <numerusform>%1 awowi</numerusform>
        <numerusform>%1 Awowi</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation>
        <numerusform>%1 Ranaku</numerusform>
        <numerusform>%1 Ranaku</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation>
        <numerusform>%1 makonni</numerusform>
        <numerusform>%1 Makonni</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation>
        <numerusform>%1 Watanni</numerusform>
        <numerusform>%1 Watanni</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation>Chanji #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="250"/>
      <source>Today</source>
      <translation>Yau</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="252"/>
      <source>Yesterday</source>
      <translation>Jiya</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="254"/>
      <source>Earlier this week</source>
      <translation>A farkon wannan makon</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="256"/>
      <source>This week</source>
      <translation>Wannan Makon</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="258"/>
      <source>Earlier this month</source>
      <translation>A farkon wannan watan</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsModel</name>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="83"/>
      <source>Change #%1</source>
      <translation>Chanji #%1</translation>
    </message>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="85"/>
      <source>Main #%1</source>
      <translation>Mafarin #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation>Bayani</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation>Tsabar kudi a / b
a) ƙidaya tsabar kudi
b) tsabar kudi na tarihi.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="162"/>
      <source>Copy Address</source>
      <translation>Kwafi Adireshi</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="166"/>
      <source>QR of Address</source>
      <translation type="unfinished">QR of Address</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="173"/>
      <source>Copy Private Key</source>
      <translation>Kwafi Keɓaɓɓen Maɓalli</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="177"/>
      <source>QR of Private Key</source>
      <translation type="unfinished">QR of Private Key</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="206"/>
      <source>Coins: %1 / %2</source>
      <translation>Tsabar kudi: %1/%2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="228"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation>Sa hannu tare da sa hannun Schnorr a baya</translation>
    </message>
  </context>
</TS>
