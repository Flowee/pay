<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl" sourcelanguage="en">
  <context>
    <name>AccountInfo</name>
    <message>
      <location filename="../src/AccountInfo.cpp" line="141"/>
      <source>Offline</source>
      <translation>Rozłączony</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="142"/>
      <source>Wallet: Up to date</source>
      <translation>Portfel: Aktualny</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="153"/>
      <source>Behind: %1 weeks, %2 days</source>
      <comment>counter on weeks</comment>
      <translation>
        <numerusform>W tyle: %1 tyg., %2 d.</numerusform>
        <numerusform>W tyle: %1 tyg., %2 d.</numerusform>
        <numerusform>W tyle: %1 tyg., %2 d.</numerusform>
        <numerusform>W tyle: %1 tyg., %2 d.</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="157"/>
      <source>Behind: %1 days</source>
      <translation>
        <numerusform>W tyle: %1 dzień</numerusform>
        <numerusform>W tyle: %1 dni</numerusform>
        <numerusform>W tyle: %1 dni</numerusform>
        <numerusform>W tyle: %1 dnia</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="159"/>
      <source>Up to date</source>
      <translation>Na bieżąco</translation>
    </message>
    <message>
      <location filename="../src/AccountInfo.cpp" line="161"/>
      <source>Updating</source>
      <translation>Aktualizowanie</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/AccountInfo.cpp" line="162"/>
      <source>Still %1 hours behind</source>
      <translation>
        <numerusform>Nadal %1 godzinę w tyle</numerusform>
        <numerusform>Nadal %1 godzin w tyle</numerusform>
        <numerusform>Nadal %1 godziny w tyle</numerusform>
        <numerusform>Nadal %1 godzin w tyle</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>AccountTypeLabel</name>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="31"/>
      <source>This wallet is a single-address wallet.</source>
      <translation>Ten portfel jest portfelem jednoadresowym.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="33"/>
      <source>This wallet is based on a HD seed-phrase</source>
      <translation>Ten portfel bazuje na ciągu losowych słów (seedzie)</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/AccountTypeLabel.qml" line="35"/>
      <source>This wallet is a simple multiple-address wallet.</source>
      <translation>Ten portfel jest prostym portfelem wieloadresowym.</translation>
    </message>
  </context>
  <context>
    <name>AddressInfoWidget</name>
    <message>
      <location filename="../guis/Flowee/AddressInfoWidget.qml" line="57"/>
      <source>self</source>
      <comment>payment to self</comment>
      <translation>własny</translation>
    </message>
  </context>
  <context>
    <name>BigCloseButton</name>
    <message>
      <location filename="../guis/Flowee/BigCloseButton.qml" line="31"/>
      <source>Close</source>
      <translation>Zamknij</translation>
    </message>
  </context>
  <context>
    <name>BroadcastFeedback</name>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="96"/>
      <source>Sending Payment</source>
      <translation>Wysyłanie Płatności</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="98"/>
      <source>Payment Sent</source>
      <translation>Płatność Wysłana</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="100"/>
      <source>Failed</source>
      <translation>Niepowodzenie</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="133"/>
      <source>Transaction rejected by network</source>
      <translation>Transakcja odrzucona przez sieć</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="179"/>
      <source>The payment has been sent to:</source>
      <comment>Followed by the address</comment>
      <translation>Płatność została wysłana do:</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/BroadcastFeedback.qml" line="202"/>
      <source>Add a personal note</source>
      <translation>Dodaj osobistą notatkę</translation>
    </message>
  </context>
  <context>
    <name>CFIcon</name>
    <message>
      <location filename="../guis/Flowee/CFIcon.qml" line="11"/>
      <source>Coin has been fused for increased anonymity</source>
      <translation>Moneta poddana fuzji dla podniesienia anonimowości</translation>
    </message>
  </context>
  <context>
    <name>FiatTxInfo</name>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="54"/>
      <source>Value now</source>
      <translation>Wartość teraz</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/FiatTxInfo.qml" line="71"/>
      <source>Value then</source>
      <translation>Wartość wtedy</translation>
    </message>
  </context>
  <context>
    <name>FloweePay</name>
    <message>
      <location filename="../src/FloweePay.cpp" line="433"/>
      <source>Initial Wallet</source>
      <translation>Portfel Początkowy</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="615"/>
      <location filename="../src/FloweePay.cpp" line="673"/>
      <source>Today</source>
      <translation>Dzisiaj</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="617"/>
      <location filename="../src/FloweePay.cpp" line="675"/>
      <source>Yesterday</source>
      <translation>Wczoraj</translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="662"/>
      <source>Now</source>
      <comment>timestamp</comment>
      <translation>Teraz</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="663"/>
      <source>%1 minutes ago</source>
      <comment>relative time stamp</comment>
      <translation>
        <numerusform>%1 minuta temu</numerusform>
        <numerusform>%1 minuty temu</numerusform>
        <numerusform>%1 minut temu</numerusform>
        <numerusform>%1 minuty temu</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/FloweePay.cpp" line="667"/>
      <source>½ hour ago</source>
      <comment>timestamp</comment>
      <translation>½ godziny temu</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/FloweePay.cpp" line="669"/>
      <source>%1 hours ago</source>
      <comment>timestamp</comment>
      <translation>
        <numerusform>%1 godzina temu</numerusform>
        <numerusform>%1 godziny temu</numerusform>
        <numerusform>%1 godzin temu</numerusform>
        <numerusform>%1 godziny temu</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>LabelWithClipboard</name>
    <message>
      <location filename="../guis/Flowee/LabelWithClipboard.qml" line="27"/>
      <source>Copy</source>
      <translation>Skopiuj</translation>
    </message>
  </context>
  <context>
    <name>MenuModel</name>
    <message>
      <location filename="../src/MenuModel.cpp" line="31"/>
      <source>Explore</source>
      <translation>Eksploruj</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="27"/>
      <source>Settings</source>
      <translation>Ustawienia</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="28"/>
      <source>Security</source>
      <translation>Bezpieczeństwo</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="29"/>
      <source>About</source>
      <translation>O programie</translation>
    </message>
    <message>
      <location filename="../src/MenuModel.cpp" line="30"/>
      <source>Wallets</source>
      <translation>Portfele</translation>
    </message>
  </context>
  <context>
    <name>NotificationManager</name>
    <message>
      <location filename="../src/NotificationManager.cpp" line="79"/>
      <source>%1 new transactions across %2 wallets found (%3)</source>
      <translation>%1 nowe transakcje w %2 portfelach (%3)</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager.cpp" line="81"/>
      <source>A payment of %1 has been sent</source>
      <translation>Wysłano płatność w wysokości %1</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager.cpp" line="82"/>
      <source>%1 new transactions found (%2)</source>
      <translation>
        <numerusform>Znaleziono %1 nową transakcję (%2)</numerusform>
        <numerusform>Znaleziono %1 nowe transakcje (%2)</numerusform>
        <numerusform>Znaleziono %1 nowych transakcji (%2)</numerusform>
        <numerusform>Znaleziono %1 nowej transakcji (%2)</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>NotificationManagerPrivate</name>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="86"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="103"/>
      <source>Bitcoin Cash block mined. Height: %1</source>
      <translation>Blok Bitcoin Cash został wykopany. Wysokość: %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_android.cpp" line="88"/>
      <location filename="../src/NotificationManager_dbus.cpp" line="105"/>
      <source>tBCH (testnet4) block mined: %1</source>
      <translation>Wykopano blok tBCH (testnet4): %1</translation>
    </message>
    <message>
      <location filename="../src/NotificationManager_dbus.cpp" line="107"/>
      <source>Mute</source>
      <translation>Wycisz</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/NotificationManager_dbus.cpp" line="142"/>
      <source>New Transactions</source>
      <comment>dialog-title</comment>
      <translation>
        <numerusform>Nowe transakcje</numerusform>
        <numerusform>Nowe transakcje</numerusform>
        <numerusform>Nowe transakcje</numerusform>
        <numerusform>Nowe transakcje</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>Payment</name>
    <message>
      <location filename="../src/Payment.cpp" line="158"/>
      <source>Invalid PIN</source>
      <translation>Nieprawidłowy PIN</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="195"/>
      <source>Wallet is locked</source>
      <translation>Portfel zablokowany</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="265"/>
      <source>Not enough funds selected for fees</source>
      <translation>Nie wybrano wystarczającej ilości środków, by pokryć koszt transakcji</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="274"/>
      <source>Not enough funds in wallet to make payment!</source>
      <translation>Niewystarczająca ilość środków w portfelu, aby dokonać płatności!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="343"/>
      <source>Transaction too large. Amount selected needs too many coins.</source>
      <translation>Transakcja jest zbyt duża. Wybrana kwota wymaga zbyt wielu monet.</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="518"/>
      <source>Request received over insecure channel. Anyone could have altered it!</source>
      <translation>Żądanie otrzymane przez niezabezpieczony kanał. Ktoś mógł je zmodyfikować!</translation>
    </message>
    <message>
      <location filename="../src/Payment.cpp" line="521"/>
      <source>Download of payment request failed.</source>
      <translation>Pobieranie żądania płatności nie powiodło się.</translation>
    </message>
  </context>
  <context>
    <name>PaymentProtocolBip70</name>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="230"/>
      <source>Payment request unreadable</source>
      <translation>Żądanie płatności nieczytelne</translation>
    </message>
    <message>
      <location filename="../src/PaymentProtocol.cpp" line="297"/>
      <location filename="../src/PaymentProtocol.cpp" line="328"/>
      <source>Payment request expired</source>
      <translation>Żądanie płatności wygasło</translation>
    </message>
  </context>
  <context>
    <name>QRScanner</name>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="171"/>
      <source>Instant Pay limit is %1</source>
      <translation>Limit szybkiej płatności wynosi %1</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/QRScanner.qml" line="174"/>
      <source>Selected wallet: &apos;%1&apos;</source>
      <translation>Wybrany portfel: &apos;%1&apos;</translation>
    </message>
  </context>
  <context>
    <name>QRWidget</name>
    <message>
      <location filename="../guis/Flowee/QRWidget.qml" line="109"/>
      <source>Copied to clipboard</source>
      <translation>Skopiowano do schowka</translation>
    </message>
  </context>
  <context>
    <name>TextField</name>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="81"/>
      <source>Copy</source>
      <translation>Skopiuj</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/TextField.qml" line="85"/>
      <source>Paste</source>
      <translation>Wklej</translation>
    </message>
  </context>
  <context>
    <name>TextPasteDecorator</name>
    <message>
      <location filename="../guis/Flowee/TextPasteDecorator.qml" line="90"/>
      <source>Paste</source>
      <translation>Wklej</translation>
    </message>
  </context>
  <context>
    <name>Wallet</name>
    <message>
      <location filename="../src/Wallet_support.cpp" line="147"/>
      <location filename="../src/Wallet_support.cpp" line="255"/>
      <source>Change #%1</source>
      <translation>Reszta #%1</translation>
    </message>
    <message>
      <location filename="../src/Wallet_support.cpp" line="150"/>
      <location filename="../src/Wallet_support.cpp" line="258"/>
      <source>Main #%1</source>
      <translation>Główny #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletCoinsModel</name>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="95"/>
      <source>Unconfirmed</source>
      <translation>Niepotwierdzona</translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="100"/>
      <source>%1 hours</source>
      <comment>age, like: hours old</comment>
      <translation>
        <numerusform>%1 godzinę</numerusform>
        <numerusform>%1 godziny</numerusform>
        <numerusform>%1 godzin</numerusform>
        <numerusform>%1 godziny</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="104"/>
      <source>%1 days</source>
      <comment>age, like: days old</comment>
      <translation>
        <numerusform>%1 dzień</numerusform>
        <numerusform>%1 dni</numerusform>
        <numerusform>%1 dni</numerusform>
        <numerusform>%1 dnia</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="107"/>
      <source>%1 weeks</source>
      <comment>age, like: weeks old</comment>
      <translation>
        <numerusform>%1 tydzień</numerusform>
        <numerusform>%1 tygodnie</numerusform>
        <numerusform>%1 tygodni</numerusform>
        <numerusform>%1 tygodnia</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../src/WalletCoinsModel.cpp" line="110"/>
      <source>%1 months</source>
      <comment>age, like: months old</comment>
      <translation>
        <numerusform>%1 miesiąc</numerusform>
        <numerusform>%1 miesiące</numerusform>
        <numerusform>%1 miesięcy</numerusform>
        <numerusform>%1 miesiąca</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../src/WalletCoinsModel.cpp" line="148"/>
      <source>Change #%1</source>
      <translation>Reszta #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletHistoryModel</name>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="250"/>
      <source>Today</source>
      <translation>Dzisiaj</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="252"/>
      <source>Yesterday</source>
      <translation>Wczoraj</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="254"/>
      <source>Earlier this week</source>
      <translation>Wcześniej w tym tygodniu</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="256"/>
      <source>This week</source>
      <translation>W tym tygodniu</translation>
    </message>
    <message>
      <location filename="../src/WalletHistoryModel.cpp" line="258"/>
      <source>Earlier this month</source>
      <translation>Wcześniej w tym miesiącu</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsModel</name>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="83"/>
      <source>Change #%1</source>
      <translation>Reszta #%1</translation>
    </message>
    <message>
      <location filename="../src/WalletSecretsModel.cpp" line="85"/>
      <source>Main #%1</source>
      <translation>Główny #%1</translation>
    </message>
  </context>
  <context>
    <name>WalletSecretsView</name>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="74"/>
      <source>Explanation</source>
      <translation>Wyjaśnienie</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="80"/>
      <source>Coins a / b
 a) active coin-count.
 b) historical coin-count.</source>
      <translation>Monety a / b
 a) liczba monet obecnie.
 b) liczba monet w przeszłości.</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="162"/>
      <source>Copy Address</source>
      <translation>Skopiuj adres</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="166"/>
      <source>QR of Address</source>
      <translation>Kod QR adresu</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="173"/>
      <source>Copy Private Key</source>
      <translation>Skopiuj klucz prywatny</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="177"/>
      <source>QR of Private Key</source>
      <translation>Kod QR klucza prywatnego</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="206"/>
      <source>Coins: %1 / %2</source>
      <translation>Monety: %1 / %2</translation>
    </message>
    <message>
      <location filename="../guis/Flowee/WalletSecretsView.qml" line="228"/>
      <source>Signed with Schnorr signatures in the past</source>
      <translation>Podpisano w przeszłości podpisem Schnorr</translation>
    </message>
  </context>
</TS>
