<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>AccountConfigMenu</name>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="26"/>
      <source>Details</source>
      <translation>Detalles</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Unarchive</source>
      <translation>Desarchivar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Archive Wallet</source>
      <translation>Archivar billetera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>Make Primary</source>
      <translation>Convertir en Principal</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>★ Primary</source>
      <translation>★ Principal</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="51"/>
      <source>Protect With Pin...</source>
      <translation>Proteger con Pin...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="58"/>
      <source>Open</source>
      <comment>Open encrypted wallet</comment>
      <translation>Abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="63"/>
      <source>Close</source>
      <comment>Close encrypted wallet</comment>
      <translation>Cerrar</translation>
    </message>
  </context>
  <context>
    <name>AccountDetails</name>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="76"/>
      <source>Wallet Details</source>
      <translation>Detalles del monedero</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="100"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="140"/>
      <source>Sync Status</source>
      <translation>Estado de la sincronización</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="189"/>
      <source>Encryption</source>
      <translation>Cifrado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="200"/>
      <location filename="../guis/desktop/AccountDetails.qml" line="367"/>
      <source>Password</source>
      <translation>Contraseña</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="229"/>
      <source>Open</source>
      <translation>Abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="239"/>
      <source>Invalid PIN</source>
      <translation>PIN inválido</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="255"/>
      <source>Include balance in total</source>
      <translation>Incluir en el balance total</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="267"/>
      <source>Hide in private mode</source>
      <translation>Ocultar en modo privado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="277"/>
      <source>Address List</source>
      <translation>Lista de direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="283"/>
      <source>Change Addresses</source>
      <translation>Cambiar direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="286"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Alterna entre direcciones donde otros pueden pagarte y direcciones que el monedero usa para enviarte el cambio de vuelta.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="290"/>
      <source>Used Addresses</source>
      <translation>Direcciones usadas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="293"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Alterna entre direcciones no usadas y usadas de Bitcoin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="313"/>
      <source>Backup details</source>
      <translation>Detalles de la copia de seguridad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="330"/>
      <source>Seed-phrase</source>
      <translation>Frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="348"/>
      <source>Copy</source>
      <translation>Copiar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="376"/>
      <source>Seed format</source>
      <translation>Formato de semilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="387"/>
      <source>Derivation</source>
      <translation>Derivación</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="400"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case of computer failure.</source>
      <translation>Por favor, guarde la frase semilla en papel, en el orden correcto, con la ruta de derivación. Esta semilla le permitirá recuperar su cartera en caso de que falle su hardware.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="410"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Importante&lt;/b&gt;: ¡Nunca comparta su frase semilla con otros!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="418"/>
      <source>This wallet is protected by password (pin-to-pay). To see the backup details you need to provide the password.</source>
      <translation>Este monedero está protegido por contraseña (pin to pay). Para ver los detalles de la copia de seguridad necesita proporcionar la contraseña.</translation>
    </message>
  </context>
  <context>
    <name>AccountListItem</name>
    <message>
      <location filename="../guis/desktop/AccountListItem.qml" line="144"/>
      <source>Last Transaction</source>
      <translation>Última transacción</translation>
    </message>
  </context>
  <context>
    <name>ActivityConfigBar</name>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="66"/>
      <source>Filter</source>
      <translation type="unfinished">Filter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="156"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="197"/>
      <source>Received</source>
      <translation>Recibido</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="175"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="204"/>
      <source>Sent</source>
      <translation>Enviado</translation>
    </message>
  </context>
  <context>
    <name>AddressDbStats</name>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="30"/>
      <source>IP Addresses</source>
      <translation>Direcciones de IP</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="49"/>
      <source>Total found</source>
      <translation>Total encontrado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="57"/>
      <source>Tried</source>
      <translation>Intentado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="64"/>
      <source>Punished count</source>
      <translation>Número de castigados</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="71"/>
      <source>Banned count</source>
      <translation>Número de baneados</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="79"/>
      <source>IP-v4 count</source>
      <translation>Recuento IP-v4</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="87"/>
      <source>IP-v6 count</source>
      <translation>Recuento IP-v6</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="97"/>
      <source>Pardon the Banned</source>
      <translation>Perdonar los Baneados</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="123"/>
      <source>Close</source>
      <translation>Cerrar</translation>
    </message>
  </context>
  <context>
    <name>NetView</name>
    <message numerus="yes">
      <location filename="../guis/desktop/NetView.qml" line="31"/>
      <source>Peers (%1)</source>
      <translation>
        <numerusform>Pares (%1)</numerusform>
        <numerusform>Pares (%1)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="83"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Dirección</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="90"/>
      <source>Start-height: %1</source>
      <translation>Altura de inicio: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="93"/>
      <source>ban-score: %1</source>
      <translation>calificación de no confiabilidad: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="116"/>
      <source>Peer for wallet: %1</source>
      <translation>Par para el monedero: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="139"/>
      <source>Disconnect Peer</source>
      <translation>Desconectar par</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="143"/>
      <source>Ban Peer</source>
      <translation>Banear par</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="162"/>
      <source>Close</source>
      <translation>Cerrar</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateBasicAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="39"/>
      <source>Create a new empty wallet with simple multi-address capability </source>
      <translation>Crear una nueva cartera vacía con una simple capacidad multi-dirección </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="48"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="59"/>
      <source>Force Single Address</source>
      <translation>Forzar dirección única</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="60"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Cuando está habilitado, este monedero se limitará a una dirección.
Esto asegura que solo una clave privada tendrá que ser respaldada</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="67"/>
      <source>Go</source>
      <translation>Ir</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateHDAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="33"/>
      <source>Create a new wallet with smart creation of addresses from a single seed-phrase</source>
      <translation>Crea una nueva cartera con la creación inteligente de direcciones a partir de una sola frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="41"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="55"/>
      <source>Go</source>
      <translation>Ir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="71"/>
      <source>Advanced Options</source>
      <translation>Opciones Avanzadas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="76"/>
      <source>Derivation</source>
      <translation>Derivación</translation>
    </message>
  </context>
  <context>
    <name>NewAccountImportAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="90"/>
      <source>Select import method</source>
      <translation>Seleccionar método de importación</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="103"/>
      <source>Camera</source>
      <translation>Cámara</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="142"/>
      <source>OR</source>
      <translation>O</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="154"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Secreto como texto</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="194"/>
      <source>Unknown word(s) found</source>
      <translation>Palabra(s) desconocidas encontradas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="217"/>
      <source>Address to import</source>
      <translation>Dirección a importar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="232"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="353"/>
      <source>New Wallet Name</source>
      <translation>Nuevo nombre de billetera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="252"/>
      <source>Force Single Address</source>
      <translation>Forzar dirección única</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="253"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Cuando está habilitado, no se generarán automáticamente direcciones adicionales en este monedero.
El cambio volverá a la clave importada.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="258"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="414"/>
      <source>Oldest Transaction</source>
      <translation>Transacción más antigua</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="266"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Comprobar edad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="310"/>
      <source>Nothing found for wallet</source>
      <translation>No se encontró nada para esta cartera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="317"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="461"/>
      <source>Start</source>
      <translation>Comenzar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="373"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Detalles de Descubre</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="425"/>
      <source>Derivation</source>
      <translation>Derivación</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="439"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation type="unfinished">Nothing found for seed. Does it have a password?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="447"/>
      <source>Password</source>
      <translation>Contraseña</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="454"/>
      <source>imported wallet password</source>
      <translation>Contraseña del monedero importada</translation>
    </message>
  </context>
  <context>
    <name>NewAccountPane</name>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="64"/>
      <source>New HD wallet</source>
      <translation>Nuevo monedero-HD</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="70"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Basado en frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="71"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Fácil de respaldar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="72"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>El más compatible</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="77"/>
      <source>Import Existing Wallet</source>
      <translation>Importar un monedero existente</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="83"/>
      <source>Imports seed-phrase</source>
      <translation>Importa la frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="84"/>
      <source>Imports private key</source>
      <translation>Importa la clave privada</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="90"/>
      <source>New Basic Wallet</source>
      <translation>Nuevo monedero básico</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="95"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Basado en Claves privadas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="96"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Difícil de respaldar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="97"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideal para un uso breve</translation>
    </message>
  </context>
  <context>
    <name>PaymentTweakingPanel</name>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="80"/>
      <source>Add Detail</source>
      <translation>Agregar detalles</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="94"/>
      <source>Coin Selector</source>
      <translation>Selector de moneda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="104"/>
      <source>To override the default selection of coins that are used to pay a transaction, you can add the &apos;Coin Selector&apos; where the wallets coins will be made visible.</source>
      <translation>Para reemplazar la selección predeterminada de monedas que se utilizan para pagar una transacción, puedes añadir el &apos;Selector de Monedas&apos; donde se harán visibles las monedas.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="107"/>
      <source>Comment</source>
      <translation>Comentario</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="117"/>
      <source>This allows adding a public comment, that will be included in the transaction and seen by everyone.</source>
      <translation>Esto permite añadir un comentario público que será incluido en la transacción y visto por todos.</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTransactionPane</name>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="51"/>
      <source>Share your QR code or copy address to receive</source>
      <translation>Comparte tu código QR o copia tu dirección para recibir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="68"/>
      <source>Encrypted Wallet</source>
      <translation>Monedero encriptado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="70"/>
      <source>Import Running...</source>
      <translation>Ejecutando Importación...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="146"/>
      <source>Checking</source>
      <translation>Comprobando</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="167"/>
      <source>High risk transaction</source>
      <translation>Transacción de alto riesgo</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="169"/>
      <source>Payment Seen</source>
      <translation>Pago Enviado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="171"/>
      <source>Payment Accepted</source>
      <translation>Pago Aceptado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="173"/>
      <source>Payment Settled</source>
      <translation>Pago realizado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="191"/>
      <source>Instant payment failed. Wait for confirmation. (double spent proof received)</source>
      <translation>Pago instantáneo fallido. Espere la confirmación. (prueba de doble gasto recibida)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="209"/>
      <source>Description</source>
      <translation>Descripción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="221"/>
      <source>Amount</source>
      <translation>Monto</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Clear</source>
      <translation>Borrar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Done</source>
      <translation>Hecho</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionPane</name>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="125"/>
      <source>Confirm delete</source>
      <translation>Confirmar eliminación</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="126"/>
      <source>Do you really want to delete this detail?</source>
      <translation>¿Realmente quieres borrar esta especificación?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="135"/>
      <source>Add Destination</source>
      <translation>Añadir destino</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="141"/>
      <source>Prepare</source>
      <translation>Preparar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="157"/>
      <source>Enter your PIN</source>
      <translation>Introduce tu código PIN</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="166"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="454"/>
      <source>Warning</source>
      <translation>Advertencia</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="173"/>
      <source>Payment request warnings:</source>
      <translation>Advertencias de la solicitud de pago:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="191"/>
      <source>Transaction Details</source>
      <translation>Detalles de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="205"/>
      <source>Not prepared yet</source>
      <translation>Aún no preparado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="211"/>
      <source>Copy transaction-ID</source>
      <translation>Copiar ID de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="214"/>
      <source>Fee</source>
      <translation>Comisión</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="224"/>
      <source>Transaction size</source>
      <translation>Tamaño de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="232"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="237"/>
      <source>Fee per byte</source>
      <translation>Comisión por byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="248"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="262"/>
      <source>Send</source>
      <translation>Enviar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="320"/>
      <source>Destination</source>
      <translation>Destino</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="327"/>
      <source>Max available</source>
      <comment>The maximum balance available</comment>
      <translation>Máximo disponible</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="336"/>
      <source>%1 to %2</source>
      <comment>summary text to pay X-euro to address M</comment>
      <translation>%1 a %2</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="354"/>
      <source>Enter Bitcoin Cash Address</source>
      <translation>Introduzca la dirección de Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="379"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="666"/>
      <source>Copy Address</source>
      <translation>Copiar dirección</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="389"/>
      <source>Amount</source>
      <translation>Monto</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="413"/>
      <source>Max</source>
      <translation>Máx</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="459"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Esta es una dirección BTC, que es una moneda incompatible. Tus fondos podrían perderse y Flowee no tendrá forma de recuperarlos. ¿Estás seguro de que esta es la dirección correcta?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="471"/>
      <source>Continue</source>
      <translation>Continuar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="475"/>
      <source>Cancel</source>
      <translation>Cancelar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="496"/>
      <source>Coin Selector</source>
      <translation>Selector de monedas</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/SendTransactionPane.qml" line="497"/>
      <source>Selected %1 %2 in %3 coins</source>
      <comment>selected 2 BCH in 5 coins</comment>
      <translation>
        <numerusform>Se seleccionaron %1 %2 en %3 monedas</numerusform>
        <numerusform>Se seleccionaron %1 %2 en %3 monedas</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="508"/>
      <source>Total</source>
      <comment>Number of coins</comment>
      <translation>Total</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="515"/>
      <source>Needed</source>
      <translation>Necesario</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="525"/>
      <source>Selected</source>
      <translation>Seleccionado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="532"/>
      <source>Value</source>
      <translation>Valor</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="570"/>
      <source>Locked coins will never be used for payments. Right-click for menu.</source>
      <translation>Las monedas bloqueadas nunca se utilizarán para los pagos. Clic derecho para desplegar el menú.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="609"/>
      <source>Age</source>
      <translation>Edad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Unselect All</source>
      <translation>Deseleccionar Todo</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Select All</source>
      <translation>Seleccionar Todo</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Unlock coin</source>
      <translation>Desbloquear moneda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Lock coin</source>
      <translation>Bloquear moneda</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="694"/>
      <source>Public-comment</source>
      <translation>Comentario-público</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="702"/>
      <source>Add a comment you want to include in the transaction, visible for everyone.</source>
      <translation>Agrega un comentario que quieras incluir en la transacción, visible para todos.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="703"/>
      <source>Custom message, to be included in the transaction.</source>
      <translation>Mensaje personalizado, a ser incluido en la transacción.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="708"/>
      <source>Text</source>
      <translation>Texto</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="718"/>
      <source>Size</source>
      <translation>Tamaño</translation>
    </message>
  </context>
  <context>
    <name>SettingsPane</name>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="26"/>
      <source>Settings</source>
      <translation>Configuraciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="37"/>
      <source>Unit</source>
      <translation>Unidad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="107"/>
      <source>Show Bitcoin Cash value on Activity page</source>
      <translation>Mostrar valor de Bitcoin Cash en la página de Actividad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="120"/>
      <source>Show Block Notifications</source>
      <translation>Mostrar notificaciones bloqueadas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="121"/>
      <source>When a new block is mined, Flowee Pay shows a desktop notification</source>
      <translation>Cuando un nuevo bloque es minado, Flowee Pay muestra una notificación de escritorio</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="135"/>
      <source>Night Mode</source>
      <translation>Modo nocturno</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="146"/>
      <source>Private Mode</source>
      <translation>Modo Privado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="147"/>
      <source>Hides private wallets while enabled</source>
      <translation>Oculta monederos privados mientras está habilitado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="153"/>
      <source>Font sizing</source>
      <translation>Tamaño de fuente</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="194"/>
      <source>Version</source>
      <translation>Versión</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="202"/>
      <source>Library Version</source>
      <translation>Versión de la biblioteca</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="211"/>
      <source>Synchronization</source>
      <translation>Sincronización</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="216"/>
      <source>Network Status</source>
      <translation>Estado de la red</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="226"/>
      <source>Address Stats</source>
      <translation>Estadísticas de dirección</translation>
    </message>
  </context>
  <context>
    <name>Transaction</name>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="65"/>
      <source>Miner Reward</source>
      <translation>Recompensa del minero</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="67"/>
      <source>Fused</source>
      <translation>Fusionado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="69"/>
      <source>Received</source>
      <translation>Recibido</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="71"/>
      <source>Moved</source>
      <translation>Movido</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="72"/>
      <source>Sent</source>
      <translation>Enviado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="80"/>
      <source>rejected</source>
      <translation>rechazada</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="31"/>
      <source>Transaction Details</source>
      <translation>Detalles de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="101"/>
      <source>First Seen</source>
      <translation>Visto por primera vez</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="115"/>
      <source>Rejected</source>
      <translation>Rechazado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="117"/>
      <source>Mined at</source>
      <translation>Minado en</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionDetails.qml" line="134"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>Hace %1 bloque</numerusform>
        <numerusform>Hace %1 bloques</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="141"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="146"/>
      <source>Comment</source>
      <translation>Comentario</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="163"/>
      <source>Fees paid</source>
      <translation>Comisiones pagadas</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="169"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="184"/>
      <source>Size</source>
      <translation>Tamaño</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="188"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="195"/>
      <source>Is Coinbase</source>
      <translation>Es Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="205"/>
      <source>Copy transaction-ID</source>
      <translation>Copiar ID de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="219"/>
      <source>Fused from my addresses</source>
      <translation>Fusionado desde mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="221"/>
      <source>Sent from my addresses</source>
      <translation>Enviado desde mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="223"/>
      <source>Sent from addresses</source>
      <translation>Enviado desde direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="281"/>
      <source>Fused into my addresses</source>
      <translation>Fusionado en mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="283"/>
      <source>Received at addresses</source>
      <translation>Recibido en direcciones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="284"/>
      <source>Received at my addresses</source>
      <translation>Recibido en mis direcciones</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="44"/>
      <source>Transaction is rejected</source>
      <translation>Transacción rechazada</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="46"/>
      <source>Processing</source>
      <translation>Procesando</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="59"/>
      <source>Mined</source>
      <translation>Minado</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="70"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>Hace %1 bloque</numerusform>
        <numerusform>Hace %1 bloques</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="106"/>
      <source>Fees</source>
      <translation>Comisiones</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="121"/>
      <source>Copy transaction-ID</source>
      <translation>Copiar ID de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="146"/>
      <source>Holds a token</source>
      <translation>Contiene un token</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="168"/>
      <source>Opening Website</source>
      <translation>Abriendo Sitio Web</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryption</name>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="63"/>
      <source>Protect your wallet with a password</source>
      <translation>Protege tu monedero con una contraseña</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="88"/>
      <source>Pin to Pay</source>
      <translation>PIN para pagar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="93"/>
      <source>Protect your funds</source>
      <comment>pin to pay</comment>
      <translation>Proteja sus fondos</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="94"/>
      <source>Fully open, except for sending funds</source>
      <comment>pin to pay</comment>
      <translation>Totalmente abierto, excepto para el envío de fondos</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="95"/>
      <source>Keeps in sync</source>
      <comment>pin to pay</comment>
      <translation>Mantener sincronizado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="100"/>
      <source>Pin to Open</source>
      <translation>PIN para abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="105"/>
      <source>Protect your entire wallet</source>
      <comment>pin to open</comment>
      <translation>Protege todo tu monedero</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="106"/>
      <source>Balance and history protected</source>
      <comment>pin to open</comment>
      <translation>Balance e historial protegidos</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="107"/>
      <source>Requires Pin to view, sync or pay</source>
      <comment>pin to open</comment>
      <translation>Requiere Pin para ver, sincronizar o pagar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="117"/>
      <source>Make &quot;%1&quot; wallet require a pin to pay</source>
      <translation>Hacer &quot;%1&quot; monedero requiere un pin para pagar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="118"/>
      <source>Make &quot;%1&quot; wallet require a pin to open</source>
      <translation>Hacer &quot;%1&quot; monedero requiere un pin para abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="130"/>
      <location filename="../guis/desktop/WalletEncryption.qml" line="142"/>
      <source>Wallet already has pin to open applied</source>
      <translation>El monedero ya tiene un pin para abrir aplicado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="132"/>
      <source>Wallet already has pin to pay applied</source>
      <translation>El monedero ya tiene pin para pagar aplicado</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="133"/>
      <source>Your wallet will get partially encrypted and payments will become impossible without a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Su monedero se cifrará parcialmente y los pagos serán imposibles sin una contraseña. Si no tienes una copia de seguridad de esta cartera, haz una primero.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="143"/>
      <source>Your full wallet gets encrypted, opening it will need a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Tu monedero completo se cifra, para abrirla necesitará una contraseña. Si no tienes una copia de seguridad de esta cartera, haz una primero.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="169"/>
      <source>Password</source>
      <translation>Contraseña</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="192"/>
      <source>Wallet</source>
      <translation>Monedero</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="214"/>
      <source>Encrypt</source>
      <translation>Encriptar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="221"/>
      <source>Invalid password to open this wallet</source>
      <translation>Contraseña no válida para abrir este monedero</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="233"/>
      <source>Close</source>
      <translation>Cerrar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="243"/>
      <source>Repeat password</source>
      <translation>Repetir contraseña</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="244"/>
      <source>Please confirm the password by entering it again</source>
      <translation>Por favor confirme su contraseña introduciéndola nuevamente</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="249"/>
      <source>Typed passwords do not match</source>
      <translation>Las contraseñas escritas no coinciden</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryptionStatus</name>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="46"/>
      <source>Pin to Open</source>
      <translation>PIN para abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="48"/>
      <source>Pin to Pay</source>
      <translation>PIN para pagar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="50"/>
      <source>(Opened)</source>
      <comment>Wallet is decrypted</comment>
      <translation>(Abierto)</translation>
    </message>
  </context>
  <context>
    <name>locked</name>
    <message>
      <location filename="../guis/desktop/locked.qml" line="51"/>
      <source>Already running?</source>
      <translation type="unfinished">Already running?</translation>
    </message>
  </context>
  <context>
    <name>main</name>
    <message>
      <location filename="../guis/desktop/main.qml" line="210"/>
      <source>Activity</source>
      <translation>Actividad</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="250"/>
      <source>Archived wallets do not check for activities. Balance may be out of date.</source>
      <translation>Las carteras archivadas no verifican las actividades. El saldo puede estar desactualizado.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="254"/>
      <source>Unarchive</source>
      <translation>Desarchivar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="287"/>
      <source>This wallet needs a password to open.</source>
      <translation>Esta cartera necesita una contraseña para abrirse.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="295"/>
      <source>Password:</source>
      <translation>Contraseña:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="315"/>
      <source>Invalid password</source>
      <translation>Contraseña invalida</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="320"/>
      <source>Open</source>
      <translation>Abrir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="398"/>
      <source>Send</source>
      <translation>Enviar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="406"/>
      <source>Receive</source>
      <translation>Recibir</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="525"/>
      <source>Balance</source>
      <translation>Balance</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="594"/>
      <source>Main</source>
      <comment>balance (money), non specified</comment>
      <translation>Principal</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="604"/>
      <source>Unconfirmed</source>
      <comment>balance (money)</comment>
      <translation>Sin confirmar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="613"/>
      <source>Immature</source>
      <comment>balance (money)</comment>
      <translation>Sin madurar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="662"/>
      <source>1 BCH is: %1</source>
      <translation>1 BCH es: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="709"/>
      <source>Network status</source>
      <translation>Estado de la red</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="723"/>
      <source>Offline</source>
      <translation>Sin conexión</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="747"/>
      <source>Add Bitcoin Cash wallet</source>
      <translation>Añadir monedero de Bitcoin Cash</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/main.qml" line="778"/>
      <source>Archived wallets [%1]</source>
      <comment>Arg is wallet count</comment>
      <translation>
        <numerusform>Billeteras archivadas [%1]</numerusform>
        <numerusform>Billeteras archivadas [%1]</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="808"/>
      <source>Preparing...</source>
      <translation>Preparando...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="891"/>
      <source>QR-Scan</source>
      <translation>Escaneo de QR</translation>
    </message>
  </context>
</TS>
