<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>AccountConfigMenu</name>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="26"/>
      <source>Details</source>
      <translation>Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Unarchive</source>
      <translation>Uit archief halen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Archive Wallet</source>
      <translation>Portemonnee Archiveren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>Make Primary</source>
      <translation>Maak primair</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>★ Primary</source>
      <translation>★ Primair</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="51"/>
      <source>Protect With Pin...</source>
      <translation>Bescherm met Pin...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="58"/>
      <source>Open</source>
      <comment>Open encrypted wallet</comment>
      <translation>Open</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="63"/>
      <source>Close</source>
      <comment>Close encrypted wallet</comment>
      <translation>Sluiten</translation>
    </message>
  </context>
  <context>
    <name>AccountDetails</name>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="76"/>
      <source>Wallet Details</source>
      <translation>Portemonnee Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="100"/>
      <source>Name</source>
      <translation>Naam</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="140"/>
      <source>Sync Status</source>
      <translation>Synchronisatie status</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="189"/>
      <source>Encryption</source>
      <translation>Encryptie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="200"/>
      <location filename="../guis/desktop/AccountDetails.qml" line="367"/>
      <source>Password</source>
      <translation>Wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="229"/>
      <source>Open</source>
      <translation>Open</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="239"/>
      <source>Invalid PIN</source>
      <translation>Ongeldige PIN</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="255"/>
      <source>Include balance in total</source>
      <translation>Saldo opnemen in totaal</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="267"/>
      <source>Hide in private mode</source>
      <translation>Verbergen in privémodus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="277"/>
      <source>Address List</source>
      <translation>Adreslijst</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="283"/>
      <source>Change Addresses</source>
      <translation>Wisselgeldadres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="286"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Schakelt tussen adressen waar anderen je op kunnen betalen, en adressen welke de portemonnee voor wisselgeld gebruikt.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="290"/>
      <source>Used Addresses</source>
      <translation>Gebruikte adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="293"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Schakelt tussen ongebruikt en gebruikte Bitcoin adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="313"/>
      <source>Backup details</source>
      <translation>Back-up details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="330"/>
      <source>Seed-phrase</source>
      <translation>Herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="348"/>
      <source>Copy</source>
      <translation>Kopieer</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="376"/>
      <source>Seed format</source>
      <translation>Herstelzin formaat</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="387"/>
      <source>Derivation</source>
      <translation>Derivatie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="400"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case of computer failure.</source>
      <translation>Schrijf de herstelzin op papier, in de juiste volgorde, samen met het derivatie pad. Deze herstelzin stelt u in staat om uw portemonnee te herstellen in geval van een computerfout.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="410"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Belangrijk&lt;/b&gt;: Deel nooit uw herstelzin met anderen!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="418"/>
      <source>This wallet is protected by password (pin-to-pay). To see the backup details you need to provide the password.</source>
      <translation>Deze portemonnee is beveiligd met een wachtwoord (pin-to-pay). Om de back-upgegevens te zien moet u het wachtwoord invullen.</translation>
    </message>
  </context>
  <context>
    <name>AccountListItem</name>
    <message>
      <location filename="../guis/desktop/AccountListItem.qml" line="144"/>
      <source>Last Transaction</source>
      <translation>Laatste Transactie</translation>
    </message>
  </context>
  <context>
    <name>ActivityConfigBar</name>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="66"/>
      <source>Filter</source>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="156"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="197"/>
      <source>Received</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="175"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="204"/>
      <source>Sent</source>
      <translation>Verzonden</translation>
    </message>
  </context>
  <context>
    <name>AddressDbStats</name>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="30"/>
      <source>IP Addresses</source>
      <translation>IP-adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="49"/>
      <source>Total found</source>
      <translation>Totaal gevonden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="57"/>
      <source>Tried</source>
      <translation>Geprobeerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="64"/>
      <source>Punished count</source>
      <translation>Aantal bestraft</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="71"/>
      <source>Banned count</source>
      <translation>Aantal verbannen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="79"/>
      <source>IP-v4 count</source>
      <translation>IP-v4-teller</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="87"/>
      <source>IP-v6 count</source>
      <translation>IP-v6 teller</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="97"/>
      <source>Pardon the Banned</source>
      <translation>Pardon de verbannen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="123"/>
      <source>Close</source>
      <translation>Sluiten</translation>
    </message>
  </context>
  <context>
    <name>NetView</name>
    <message numerus="yes">
      <location filename="../guis/desktop/NetView.qml" line="31"/>
      <source>Peers (%1)</source>
      <translation>
        <numerusform>Peer (%1)</numerusform>
        <numerusform>Peers (%1)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="83"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="90"/>
      <source>Start-height: %1</source>
      <translation>Beginhoogte: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="93"/>
      <source>ban-score: %1</source>
      <translation>ban-score: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="116"/>
      <source>Peer for wallet: %1</source>
      <translation>Peer voor portemonnee: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="139"/>
      <source>Disconnect Peer</source>
      <translation>Verbreek verbinding met peer</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="143"/>
      <source>Ban Peer</source>
      <translation>Verban Peer</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="162"/>
      <source>Close</source>
      <translation>Sluiten</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateBasicAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="39"/>
      <source>Create a new empty wallet with simple multi-address capability </source>
      <translation>Maak een nieuwe lege portemonnee met eenvoudige multi-address mogelijkheden </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="48"/>
      <source>Name</source>
      <translation>Naam</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="59"/>
      <source>Force Single Address</source>
      <translation>Forceer één adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="60"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Wanneer ingeschakeld zal deze portemonnee worden beperkt tot één adres.
Dit zorgt ervoor dat er slechts één privésleutel zal moeten worden geback-upt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="67"/>
      <source>Go</source>
      <translation>Start</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateHDAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="33"/>
      <source>Create a new wallet with smart creation of addresses from a single seed-phrase</source>
      <translation>Maak een nieuwe portemonnee met slimme aanmaak van adressen van één enkele herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="41"/>
      <source>Name</source>
      <translation>Naam</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="55"/>
      <source>Go</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="71"/>
      <source>Advanced Options</source>
      <translation>Geavanceerde Opties</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="76"/>
      <source>Derivation</source>
      <translation>Derivatie</translation>
    </message>
  </context>
  <context>
    <name>NewAccountImportAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="90"/>
      <source>Select import method</source>
      <translation>Kies import-methode</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="103"/>
      <source>Camera</source>
      <translation>Kamera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="142"/>
      <source>OR</source>
      <translation>OF</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="154"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Geheim als tekst</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="194"/>
      <source>Unknown word(s) found</source>
      <translation>Onbekende woord(en) gevonden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="217"/>
      <source>Address to import</source>
      <translation>Te importeren adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="232"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="353"/>
      <source>New Wallet Name</source>
      <translation>Nieuwe naam Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="252"/>
      <source>Force Single Address</source>
      <translation>Forceer één adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="253"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Wanneer ingeschakeld, zullen er geen extra adressen automatisch worden gegenereerd in deze portemonnee.
Wisselgeld zal teruggestort worden op de geïmporteerde sleutel.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="258"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="414"/>
      <source>Oldest Transaction</source>
      <translation>Oudste transactie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="266"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Leeftijd opzoeken</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="310"/>
      <source>Nothing found for wallet</source>
      <translation>Niets gevonden voor portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="317"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="461"/>
      <source>Start</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="373"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Vind de details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="425"/>
      <source>Derivation</source>
      <translation>Derivatie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="439"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Niets gevonden voor herstelzin. Behoeft het een wachtwoord?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="447"/>
      <source>Password</source>
      <translation>Wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="454"/>
      <source>imported wallet password</source>
      <translation>Wachtwoord geïmporteerde portemonnee</translation>
    </message>
  </context>
  <context>
    <name>NewAccountPane</name>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="64"/>
      <source>New HD wallet</source>
      <translation>Nieuwe HD portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="70"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Herstelzin gebaseerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="71"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Back-upt makkelijk</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="72"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Meest compatibel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="77"/>
      <source>Import Existing Wallet</source>
      <translation>Importeer bestaande portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="83"/>
      <source>Imports seed-phrase</source>
      <translation>Importeert herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="84"/>
      <source>Imports private key</source>
      <translation>Importeert Privésleutel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="90"/>
      <source>New Basic Wallet</source>
      <translation>Nieuwe Basic Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="95"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Privé sleutels gebaseerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="96"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Back-up moeilijk</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="97"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideaal voor kort gebruik</translation>
    </message>
  </context>
  <context>
    <name>PaymentTweakingPanel</name>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="80"/>
      <source>Add Detail</source>
      <translation>Details toevoegen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="94"/>
      <source>Coin Selector</source>
      <translation>Muntselectie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="104"/>
      <source>To override the default selection of coins that are used to pay a transaction, you can add the &apos;Coin Selector&apos; where the wallets coins will be made visible.</source>
      <translation>De standaard selectie van munten die een transactie betalen kunt u veranderen, u kunt een eigen &apos;Muntselectie&apos; selecteren waar de munten van deze portemonnee beschikbaar zijn.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="107"/>
      <source>Comment</source>
      <translation>Opmerking</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="117"/>
      <source>This allows adding a public comment, that will be included in the transaction and seen by everyone.</source>
      <translation>Hiermee kan een publieke opmerking worden toegevoegd in de transactie, die door iedereen kan worden gelezen.</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTransactionPane</name>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="51"/>
      <source>Share your QR code or copy address to receive</source>
      <translation>Deel uw QR code of kopieer het adres waarop u Bitcoin Cash kan ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="68"/>
      <source>Encrypted Wallet</source>
      <translation>Versleutelde portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="70"/>
      <source>Import Running...</source>
      <translation>Bezig met importeren...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="146"/>
      <source>Checking</source>
      <translation>Controleren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="167"/>
      <source>High risk transaction</source>
      <translation>Transactie met hoog risico</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="169"/>
      <source>Payment Seen</source>
      <translation>Betaling gezien</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="171"/>
      <source>Payment Accepted</source>
      <translation>Betaling geaccepteerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="173"/>
      <source>Payment Settled</source>
      <translation>Betaling Afgewikkeld</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="191"/>
      <source>Instant payment failed. Wait for confirmation. (double spent proof received)</source>
      <translation>Directe betaling is mislukt. Wacht op bevestiging. (dubbel uitgegeven bewijs ontvangen)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="209"/>
      <source>Description</source>
      <translation>Omschrijving</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="221"/>
      <source>Amount</source>
      <translation>Bedrag</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Clear</source>
      <translation>Wissen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Done</source>
      <translation>Klaar</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionPane</name>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="125"/>
      <source>Confirm delete</source>
      <translation>Verwijderen bevestigen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="126"/>
      <source>Do you really want to delete this detail?</source>
      <translation>Wilt u dit detail echt wissen?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="135"/>
      <source>Add Destination</source>
      <translation>Voeg bestemming toe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="141"/>
      <source>Prepare</source>
      <translation>Bereid voor</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="157"/>
      <source>Enter your PIN</source>
      <translation>Voer uw PIN in</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="166"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="454"/>
      <source>Warning</source>
      <translation>Waarschuwing</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="173"/>
      <source>Payment request warnings:</source>
      <translation>Betalingsverzoek meldingen:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="191"/>
      <source>Transaction Details</source>
      <translation>Transactiedetails</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="205"/>
      <source>Not prepared yet</source>
      <translation>Nog niet voorbereid</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="211"/>
      <source>Copy transaction-ID</source>
      <translation>Kopieer transactie-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="214"/>
      <source>Fee</source>
      <translation>Transactiekosten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="224"/>
      <source>Transaction size</source>
      <translation>Transactie grootte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="232"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="237"/>
      <source>Fee per byte</source>
      <translation>Transactiekosten per byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="248"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="262"/>
      <source>Send</source>
      <translation>Verstuur</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="320"/>
      <source>Destination</source>
      <translation>Bestemming</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="327"/>
      <source>Max available</source>
      <comment>The maximum balance available</comment>
      <translation>Max. beschikbaar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="336"/>
      <source>%1 to %2</source>
      <comment>summary text to pay X-euro to address M</comment>
      <translation>%1 aan %2</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="354"/>
      <source>Enter Bitcoin Cash Address</source>
      <translation>Voer Bitcoin Cash adres in</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="379"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="666"/>
      <source>Copy Address</source>
      <translation>Kopieer adres</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="389"/>
      <source>Amount</source>
      <translation>Bedrag</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="413"/>
      <source>Max</source>
      <translation>Max</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="459"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Dit is een verzoek om te betalen aan een BTC-adres, wat een incompatibele munt is. Uw tegoeden konden verloren gaan en Flowee zal geen manier hebben om ze te herstellen. Weet u zeker dat u aan dit BTC-adres wilt betalen?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="471"/>
      <source>Continue</source>
      <translation>Doorgaan</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="475"/>
      <source>Cancel</source>
      <translation>Afbreken</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="496"/>
      <source>Coin Selector</source>
      <translation>Muntselectie</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/SendTransactionPane.qml" line="497"/>
      <source>Selected %1 %2 in %3 coins</source>
      <comment>selected 2 BCH in 5 coins</comment>
      <translation>
        <numerusform>%1 %2 geselecteerd in %3 munt</numerusform>
        <numerusform>%1 %2 geselecteerd in %3 munten</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="508"/>
      <source>Total</source>
      <comment>Number of coins</comment>
      <translation>Totaal</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="515"/>
      <source>Needed</source>
      <translation>Benodigd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="525"/>
      <source>Selected</source>
      <translation>Geselecteerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="532"/>
      <source>Value</source>
      <translation>Waarde</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="570"/>
      <source>Locked coins will never be used for payments. Right-click for menu.</source>
      <translation>Vergrendelde munten worden nooit gebruikt voor betalingen. Rechtsklik voor menu.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="609"/>
      <source>Age</source>
      <translation>Leeftijd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Unselect All</source>
      <translation>Alles deselecteren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Select All</source>
      <translation>Alles selecteren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Unlock coin</source>
      <translation>Ontgrendel munt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Lock coin</source>
      <translation>Vergrendel munt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="694"/>
      <source>Public-comment</source>
      <translation>Publiek commentaar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="702"/>
      <source>Add a comment you want to include in the transaction, visible for everyone.</source>
      <translation>Voeg een opmerking toe die u wilt opnemen in de transactie, zichtbaar voor iedereen.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="703"/>
      <source>Custom message, to be included in the transaction.</source>
      <translation>Speciaal bericht dat wordt opgenomen in de transactie.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="708"/>
      <source>Text</source>
      <translation>Tekst</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="718"/>
      <source>Size</source>
      <translation>Grootte</translation>
    </message>
  </context>
  <context>
    <name>SettingsPane</name>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="26"/>
      <source>Settings</source>
      <translation>Instellingen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="37"/>
      <source>Unit</source>
      <translation>Eenheid</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="107"/>
      <source>Show Bitcoin Cash value on Activity page</source>
      <translation>Toon Bitcoin Cash waarde op de activiteitenpagina</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="120"/>
      <source>Show Block Notifications</source>
      <translation>Toon blok notificaties</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="121"/>
      <source>When a new block is mined, Flowee Pay shows a desktop notification</source>
      <translation>Wanneer een nieuw blok is gedolven, toont Flowee Pay een desktop notificatie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="135"/>
      <source>Night Mode</source>
      <translation>Nachtmodus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="146"/>
      <source>Private Mode</source>
      <translation>Privé modus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="147"/>
      <source>Hides private wallets while enabled</source>
      <translation>Verbergt privé portemonnees</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="153"/>
      <source>Font sizing</source>
      <translation>Lettertypegrootte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="194"/>
      <source>Version</source>
      <translation>Versie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="202"/>
      <source>Library Version</source>
      <translation>Bibliotheek versie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="211"/>
      <source>Synchronization</source>
      <translation>Synchronisatie</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="216"/>
      <source>Network Status</source>
      <translation>Netwerk Status</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="226"/>
      <source>Address Stats</source>
      <translation>Adres statistieken</translation>
    </message>
  </context>
  <context>
    <name>Transaction</name>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="65"/>
      <source>Miner Reward</source>
      <translation>Miner Beloning</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="67"/>
      <source>Fused</source>
      <translation>Gefuseerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="69"/>
      <source>Received</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="71"/>
      <source>Moved</source>
      <translation>Verschoven</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="72"/>
      <source>Sent</source>
      <translation>Verzonden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="80"/>
      <source>rejected</source>
      <translation>afgewezen</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="31"/>
      <source>Transaction Details</source>
      <translation>Transactiedetails</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="101"/>
      <source>First Seen</source>
      <translation>Eerst gezien</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="115"/>
      <source>Rejected</source>
      <translation>Afgewezen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="117"/>
      <source>Mined at</source>
      <translation>Gedolven in</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionDetails.qml" line="134"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 blok terug</numerusform>
        <numerusform>%1 blokken terug</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="141"/>
      <source>Waiting for block</source>
      <translation>Wachten op blok</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="146"/>
      <source>Comment</source>
      <translation>Opmerking</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="163"/>
      <source>Fees paid</source>
      <translation>Betaalde kosten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="169"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="184"/>
      <source>Size</source>
      <translation>Grootte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="188"/>
      <source>%1 bytes</source>
      <translation>%1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="195"/>
      <source>Is Coinbase</source>
      <translation>Is Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="205"/>
      <source>Copy transaction-ID</source>
      <translation>Kopieer transactie-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="219"/>
      <source>Fused from my addresses</source>
      <translation>Mijn gefuseerde adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="221"/>
      <source>Sent from my addresses</source>
      <translation>Verzonden vanaf mijn adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="223"/>
      <source>Sent from addresses</source>
      <translation>Verzonden vanaf adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="281"/>
      <source>Fused into my addresses</source>
      <translation>Gefuseerd naar mijn adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="283"/>
      <source>Received at addresses</source>
      <translation>Ontvangen op adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="284"/>
      <source>Received at my addresses</source>
      <translation>Ontvangen op mijn adressen</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="44"/>
      <source>Transaction is rejected</source>
      <translation>Transactie geweigerd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="46"/>
      <source>Processing</source>
      <translation>In behandeling</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="59"/>
      <source>Mined</source>
      <translation>Gedolven</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="70"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 blok terug</numerusform>
        <numerusform>%1 blokken terug</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="106"/>
      <source>Fees</source>
      <translation>Kosten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="121"/>
      <source>Copy transaction-ID</source>
      <translation>Kopieer transactie-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="146"/>
      <source>Holds a token</source>
      <translation>Heeft een token</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="168"/>
      <source>Opening Website</source>
      <translation>Open Website</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryption</name>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="63"/>
      <source>Protect your wallet with a password</source>
      <translation>Beveilig uw portomonee met een wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="88"/>
      <source>Pin to Pay</source>
      <translation>PIN bij betalen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="93"/>
      <source>Protect your funds</source>
      <comment>pin to pay</comment>
      <translation>Bescherm uw geld</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="94"/>
      <source>Fully open, except for sending funds</source>
      <comment>pin to pay</comment>
      <translation>Volledig open, behalve voor het verzenden van geld</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="95"/>
      <source>Keeps in sync</source>
      <comment>pin to pay</comment>
      <translation>Synchroniseert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="100"/>
      <source>Pin to Open</source>
      <translation>PIN bij openen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="105"/>
      <source>Protect your entire wallet</source>
      <comment>pin to open</comment>
      <translation>Bescherm uw gehele portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="106"/>
      <source>Balance and history protected</source>
      <comment>pin to open</comment>
      <translation>Saldo en geschiedenis beveiligd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="107"/>
      <source>Requires Pin to view, sync or pay</source>
      <comment>pin to open</comment>
      <translation>Vereist Pin om te bekijken, synchroniseren of betalen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="117"/>
      <source>Make &quot;%1&quot; wallet require a pin to pay</source>
      <translation>Verplicht een Pin voor betaling op portemonnee &quot;%1&quot;</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="118"/>
      <source>Make &quot;%1&quot; wallet require a pin to open</source>
      <translation>Verplicht een Pin om portemonnee &quot;%1&quot; te openen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="130"/>
      <location filename="../guis/desktop/WalletEncryption.qml" line="142"/>
      <source>Wallet already has pin to open applied</source>
      <translation>Portomonee heeft al Pin to Open</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="132"/>
      <source>Wallet already has pin to pay applied</source>
      <translation>Portomonee heeft al Pin to Pay</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="133"/>
      <source>Your wallet will get partially encrypted and payments will become impossible without a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Uw portemonnee zal gedeeltelijk versleuteld worden en betalen zal onmogelijk worden zonder een wachtwoord. Als u geen backup heeft van deze portemonnee, maak er dan eerst een.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="143"/>
      <source>Your full wallet gets encrypted, opening it will need a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Uw volledige portemonnee wordt versleuteld, bij openen heeft u een wachtwoord nodig. Als u geen backup heeft van deze portemonnee, maak er eerst een.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="169"/>
      <source>Password</source>
      <translation>Wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="192"/>
      <source>Wallet</source>
      <translation>Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="214"/>
      <source>Encrypt</source>
      <translation>Versleutelen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="221"/>
      <source>Invalid password to open this wallet</source>
      <translation>Ongeldig wachtwoord om deze portemonnee te openen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="233"/>
      <source>Close</source>
      <translation>Sluiten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="243"/>
      <source>Repeat password</source>
      <translation>Herhaal wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="244"/>
      <source>Please confirm the password by entering it again</source>
      <translation>Bevestig het wachtwoord door het opnieuw in te voeren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="249"/>
      <source>Typed passwords do not match</source>
      <translation>Wachtwoorden komen niet overeen</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryptionStatus</name>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="46"/>
      <source>Pin to Open</source>
      <translation>PIN bij openen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="48"/>
      <source>Pin to Pay</source>
      <translation>PIN bij betalen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="50"/>
      <source>(Opened)</source>
      <comment>Wallet is decrypted</comment>
      <translation>(Geopend)</translation>
    </message>
  </context>
  <context>
    <name>locked</name>
    <message>
      <location filename="../guis/desktop/locked.qml" line="51"/>
      <source>Already running?</source>
      <translation>Al actief?</translation>
    </message>
  </context>
  <context>
    <name>main</name>
    <message>
      <location filename="../guis/desktop/main.qml" line="210"/>
      <source>Activity</source>
      <translation>Activiteit</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="250"/>
      <source>Archived wallets do not check for activities. Balance may be out of date.</source>
      <translation>Gearchiveerde portemonnees controleren niet op activiteiten. Saldo is mogelijk verouderd.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="254"/>
      <source>Unarchive</source>
      <translation>Uit archief halen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="287"/>
      <source>This wallet needs a password to open.</source>
      <translation>Deze portemonnee heeft een wachtwoord nodig om te openen.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="295"/>
      <source>Password:</source>
      <translation>Wachtwoord:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="315"/>
      <source>Invalid password</source>
      <translation>Ongeldig wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="320"/>
      <source>Open</source>
      <translation>Open</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="398"/>
      <source>Send</source>
      <translation>Versturen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="406"/>
      <source>Receive</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="525"/>
      <source>Balance</source>
      <translation>Saldo</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="594"/>
      <source>Main</source>
      <comment>balance (money), non specified</comment>
      <translation>Algemeen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="604"/>
      <source>Unconfirmed</source>
      <comment>balance (money)</comment>
      <translation>Onbevestigd</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="613"/>
      <source>Immature</source>
      <comment>balance (money)</comment>
      <translation>Ongerijpt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="662"/>
      <source>1 BCH is: %1</source>
      <translation>1 BCH is: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="709"/>
      <source>Network status</source>
      <translation>Netwerkstatus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="723"/>
      <source>Offline</source>
      <translation>Offline</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="747"/>
      <source>Add Bitcoin Cash wallet</source>
      <translation>Bitcoin Cash portemonnee toevoegen</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/main.qml" line="778"/>
      <source>Archived wallets [%1]</source>
      <comment>Arg is wallet count</comment>
      <translation>
        <numerusform>Gearchiveerde portemonnee</numerusform>
        <numerusform>Gearchiveerde portemonnees [%1]</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="808"/>
      <source>Preparing...</source>
      <translation>Voorbereiden...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="891"/>
      <source>QR-Scan</source>
      <translation>QR-Scannen</translation>
    </message>
  </context>
</TS>
