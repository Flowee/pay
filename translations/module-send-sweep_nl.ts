<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>SendPage</name>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="28"/>
      <source>Sweep coins</source>
      <translation>Munten opvegen</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="44"/>
      <source>Sweeping from address:</source>
      <translation>Opvegen van adres:</translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="59"/>
      <source>Found %1 coins on address.</source>
      <comment>this is a simple number</comment>
      <translation>
        <numerusform>%1 munt gevonden op adres.</numerusform>
        <numerusform>%1 munten gevonden op adres.</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../modules/send-sweep/SendPage.qml" line="63"/>
      <source>Ignoring %1 tokens.</source>
      <comment>Number of CashTokens</comment>
      <translation>
        <numerusform>Negeer %1 token.</numerusform>
        <numerusform>Negeer %1 tokens.</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="118"/>
      <source>Failed to understand QR</source>
      <translation>Kon QR niet begrijpen</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="124"/>
      <source>Indexer results invalid. Please try again.</source>
      <translation>Indexeren resultaten ongeldig. Probeer het opnieuw.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendPage.qml" line="149"/>
      <source>Transfer to:</source>
      <translation>Overmaken naar:</translation>
    </message>
  </context>
  <context>
    <name>SendSweepModuleInfo</name>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="33"/>
      <source>Claim Cash Stamp</source>
      <translation>Claim een Cash Stamp</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="34"/>
      <source>A QR code with a CashStamp can be taken to transfer the money to your wallet.</source>
      <translation>Een QR-code met een Cash Stamp kan worden gebruikt om het geld naar uw portemonnee te verplaatsen.</translation>
    </message>
    <message>
      <location filename="../modules/send-sweep/SendSweepModuleInfo.cpp" line="36"/>
      <source>Claim a Cash Stamp</source>
      <translation>Claim een Cash Stamp</translation>
    </message>
  </context>
  <context>
    <name>StartScan</name>
    <message>
      <location filename="../modules/send-sweep/StartScan.qml" line="28"/>
      <source>Scan QR (WIF) to find funds</source>
      <comment>Please note that WIF and QR are names</comment>
      <translation>Scan QR (WIF) om geld te vinden</translation>
    </message>
  </context>
</TS>
