<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>BuildTransactionModuleInfo</name>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="23"/>
      <source>Create Transactions</source>
      <translation>Transacties creëren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="24"/>
      <source>This module allows building more powerful transactions in one simple user interface.</source>
      <translation>Deze module maakt het mogelijk om krachtigere transacties te bouwen in één eenvoudige gebruikersinterface.</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="28"/>
      <source>Build Transaction</source>
      <translation>Bouw transactie</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="29"/>
      <source>Manually select templates</source>
      <translation>Selecteer templates handmatig</translation>
    </message>
  </context>
  <context>
    <name>DestinationEditPage</name>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="27"/>
      <source>Edit Destination</source>
      <translation>Bewerk Bestemming</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="32"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Alles verzenden</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="38"/>
      <source>Bitcoin Cash Address</source>
      <translation>Bitcoin Cash-adres</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="75"/>
      <source>Copy Address</source>
      <translation>Kopieer adres</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="131"/>
      <source>Warning</source>
      <translation>Waarschuwing</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="139"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Dit is een verzoek om te betalen aan een BTC-adres, wat een incompatibele munt is. Uw tegoeden konden verloren gaan en Flowee zal geen manier hebben om ze te herstellen. Weet u zeker dat u aan dit BTC-adres wilt betalen?</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="148"/>
      <source>I am certain</source>
      <translation>Ik weet het zeker</translation>
    </message>
  </context>
  <context>
    <name>PayToOthers</name>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="28"/>
      <source>Build Transaction</source>
      <translation>Bouw transactie</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="64"/>
      <source>Building Error</source>
      <comment>error during build</comment>
      <translation>Fout bij bouwen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="72"/>
      <source>Add Payment Detail</source>
      <comment>page title</comment>
      <translation>Voeg betalingsgegevens toe</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="80"/>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="485"/>
      <source>Add Destination</source>
      <translation>Voeg bestemming toe</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="81"/>
      <source>an address to send money to</source>
      <translation>een adres om geld naar te sturen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="99"/>
      <source>Confirm Sending</source>
      <comment>confirm we want to send the transaction</comment>
      <translation>Verzenden bevestigen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="119"/>
      <source>TXID</source>
      <translation>TXID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="127"/>
      <source>Copy transaction-ID</source>
      <translation>Transactie-ID kopiëren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="130"/>
      <source>Fee</source>
      <translation>Transactiekosten</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="137"/>
      <source>Transaction size</source>
      <translation>Transactie grootte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="140"/>
      <source>%1 bytes</source>
      <translation>%1 byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="143"/>
      <source>Fee per byte</source>
      <translation>Transactiekosten per byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="150"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 sat/byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="182"/>
      <source>Destination</source>
      <translation>Bestemming</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="191"/>
      <source>unset</source>
      <comment>indication of desination not being set</comment>
      <translation>niet ingesteld</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="192"/>
      <source>invalid</source>
      <comment>address is not correct</comment>
      <translation>ongeldig</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="199"/>
      <source>Copy Address</source>
      <translation>Kopieer adres</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Edit</source>
      <translation>Sleep om te bewerken</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Delete</source>
      <translation>Sleep om te verwijderen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Unlock Wallet</source>
      <translation>Portemonnee ontgrendelen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Prepare Payment...</source>
      <translation>Betaling voorbereiden...</translation>
    </message>
  </context>
</TS>
