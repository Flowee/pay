<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
  <context>
    <name>About</name>
    <message>
      <location filename="../guis/mobile/About.qml" line="23"/>
      <source>About</source>
      <translation>Over Ons</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="43"/>
      <source>Help translate this app</source>
      <translation>Help deze app te vertalen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="48"/>
      <source>License</source>
      <translation>Licentie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="53"/>
      <location filename="../guis/mobile/About.qml" line="61"/>
      <source>Credits</source>
      <translation>Met dank aan</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="54"/>
      <source>© 2020-2025 Tom Zander and contributors</source>
      <translation>© 2020-2025 Tom Zander en bijdragers</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="99"/>
      <source>Project Home</source>
      <translation>Startpagina project</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="100"/>
      <source>With git repository and issues tracker</source>
      <translation>Met git data en takenlijst</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="105"/>
      <source>Telegram</source>
      <translation>Telegram</translation>
    </message>
  </context>
  <context>
    <name>AccountHistory</name>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="29"/>
      <source>Home</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="162"/>
      <source>Pay</source>
      <translation>Betaal</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="168"/>
      <source>Receive</source>
      <translation>Ontvang</translation>
    </message>
  </context>
  <context>
    <name>AccountPageListItem</name>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="41"/>
      <source>Name</source>
      <translation>Naam</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="64"/>
      <source>Archived wallets do not check for activities. Balance may be out of date</source>
      <translation>Gearchiveerde portemonnees controleren niet op activiteiten. Saldo is mogelijk verouderd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="76"/>
      <source>Backup information</source>
      <translation>Back-up Informatie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="87"/>
      <source>Backup Details</source>
      <translation>Back-up gegevens</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="121"/>
      <source>Wallet seed-phrase</source>
      <translation>Herstelzin opslaan</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="148"/>
      <source>Password</source>
      <translation>Wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="159"/>
      <source>Seed format</source>
      <translation>Herstelzin formaat</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="168"/>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="269"/>
      <source>Starting Height</source>
      <comment>height refers to block-height</comment>
      <translation>Beginhoogte</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="173"/>
      <source>Derivation Path</source>
      <translation>Derivatie pad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="180"/>
      <source>xpub</source>
      <translation>xpub</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="210"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</source>
      <translation>Schrijf de herstelzin op papier, in de juiste volgorde, samen met het derivatie pad. Deze herstelzin stelt u in staat om uw portemonnee te herstellen in geval van een computerfout.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="217"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Belangrijk&lt;/b&gt;: Deel nooit uw herstelzin met anderen!</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="229"/>
      <source>Wallet keys</source>
      <translation>Sleutels van portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="232"/>
      <source>Show Index</source>
      <comment>toggle to show numbers</comment>
      <translation>Toon Index</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="248"/>
      <source>Change Addresses</source>
      <translation>Wisselgeldadres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="251"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Schakelt tussen adressen waar anderen je op kunnen betalen, en adressen welke de portemonnee voor wisselgeld gebruikt.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="254"/>
      <source>Used Addresses</source>
      <translation>Gebruikte adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="257"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Schakelt tussen ongebruikt en gebruikte Bitcoin adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="309"/>
      <source>Addresses and keys</source>
      <translation>Adressen en sleutels</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="317"/>
      <source>Sync Status</source>
      <translation>Synchronisatie status</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="357"/>
      <source>Hide balance in overviews</source>
      <translation>Balans in overzichten verbergen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="364"/>
      <source>Hide in private mode</source>
      <translation>Verbergen in privémodus</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Unarchive Wallet</source>
      <translation>Portemonnee De-archiveren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Archive Wallet</source>
      <translation>Portemonnee Archiveren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="423"/>
      <source>Really Delete?</source>
      <translation>Echt verwijderen?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="424"/>
      <source>Removing wallet &quot;%1&quot; can not be undone.</source>
      <comment>argument is the wallet name</comment>
      <translation>Verwijderen van portemonnee &quot;%1&quot; kan niet ongedaan worden gemaakt.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="403"/>
      <source>Remove Wallet</source>
      <translation>Portemonnee verwijderen</translation>
    </message>
  </context>
  <context>
    <name>AccountSelectorPopup</name>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="51"/>
      <source>Your Wallets</source>
      <translation>Uw portemonnees</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="130"/>
      <source>last active</source>
      <translation>Laatst actief</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="139"/>
      <source>Needs PIN to open</source>
      <translation>Benodigd pincode bij openen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="175"/>
      <source>Balance Total</source>
      <translation>Totale saldo</translation>
    </message>
  </context>
  <context>
    <name>AccountSyncState</name>
    <message>
      <location filename="../guis/mobile/AccountSyncState.qml" line="95"/>
      <source>Status: Offline</source>
      <translation>Status: offline</translation>
    </message>
  </context>
  <context>
    <name>AccountsList</name>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallet</source>
      <translation>Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallets</source>
      <translation>Portemonnees</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="28"/>
      <source>Add Wallet</source>
      <translation>Portemonnee toevoegen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Exit Private Mode</source>
      <translation>Verlaat privémodus</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Enter Private Mode</source>
      <translation>Start privémodus</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="60"/>
      <source>Reveals wallets marked private</source>
      <translation>Onthult portefeuilles gemarkeerd als privé</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="61"/>
      <source>Hides wallets marked private</source>
      <translation>Verbergt portemonnees gemarkeerd als privé</translation>
    </message>
  </context>
  <context>
    <name>BackgroundSyncConfig</name>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="24"/>
      <source>Background Synchronization</source>
      <translation>Achtergrond synchronisatie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="33"/>
      <source>Without background synchronization your wallets will not see new transactions until you open Flowee Pay</source>
      <translation>Zonder achtergrond synchronisatie zien uw portemonnees geen nieuwe transacties totdat u Flowee Pay opent</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="39"/>
      <source>Allow Background Synchronization</source>
      <translation>Achtergrond synchronisatie toestaan</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="106"/>
      <source>Every %1 hours</source>
      <translation>Iedere %1 uur</translation>
    </message>
  </context>
  <context>
    <name>CurrencySelector</name>
    <message>
      <location filename="../guis/mobile/CurrencySelector.qml" line="24"/>
      <source>Select Currency</source>
      <translation>Selecteer valuta</translation>
    </message>
  </context>
  <context>
    <name>ExploreModules</name>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="24"/>
      <source>Explore</source>
      <translation>Ontdek</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="120"/>
      <source>Open</source>
      <translation>Open</translation>
    </message>
  </context>
  <context>
    <name>FilterPopup</name>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="45"/>
      <source>Transactions Filter</source>
      <translation>Transacties filter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="192"/>
      <source>Only with a comment</source>
      <comment>This is a statement about a transaction</comment>
      <translation>Alleen met omschrijving</translation>
    </message>
  </context>
  <context>
    <name>ImportWalletPage</name>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="26"/>
      <source>Import Wallet</source>
      <translation>Portemonnee importeren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="159"/>
      <source>Select import method</source>
      <translation>Kies import-methode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="172"/>
      <source>Scan QR</source>
      <translation>Scan QR-code</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="212"/>
      <source>OR</source>
      <translation>OF</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="224"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Geheim als tekst</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="263"/>
      <source>Unknown word(s) found</source>
      <translation>Onbekende woord(en) gevonden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="274"/>
      <source>Next</source>
      <translation>Volgende</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="298"/>
      <source>Address to import</source>
      <translation>Te importeren adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="312"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="446"/>
      <source>New Wallet Name</source>
      <translation>Nieuwe naam Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="330"/>
      <source>Force Single Address</source>
      <translation>Forceer één adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="331"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Als ingeschakeld zullen er geen extra adressen automatisch toegevoegd worden in deze portemonnee.
Wisselgeld gaat weer naar de geïmporteerde sleutel.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="336"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="505"/>
      <source>Oldest Transaction</source>
      <translation>Oudste transactie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="342"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Leeftijd opzoeken</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="384"/>
      <source>Nothing found for wallet</source>
      <translation>Niets gevonden voor portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="390"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="549"/>
      <source>Start</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="464"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Vind de details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="515"/>
      <source>Derivation Path</source>
      <translation>Derivatie pad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="528"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Niets gevonden voor herstelzin. Behoeft het een wachtwoord?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="536"/>
      <source>Password</source>
      <translation>Wachtwoord</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="542"/>
      <source>imported wallet password</source>
      <translation>Wachtwoord geïmporteerde portemonnee</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigButton</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Enable Instant Pay</source>
      <translation>Direct Betalen inschakelen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Configure Instant Pay</source>
      <translation>Configureer Direct Betalen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="54"/>
      <source>Limits for %1</source>
      <comment>argument is a name</comment>
      <translation>Limieten voor %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="55"/>
      <source>Disabled for %1</source>
      <comment>argument is a name</comment>
      <translation>Uitgeschakeld voor %1</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigPage</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="23"/>
      <source>Instant Pay</source>
      <translation>Direct Betalen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="29"/>
      <source>Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</source>
      <translation>Bij lezen van QR-code met Direct Betalen aan wordt de betaling direct verstuurd, zonder bevestiging. Tenminste als deze onder de ingestelde limiet blijft.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="54"/>
      <source>Protected wallets can not be used for InstaPay because they require a PIN before usage</source>
      <translation>Beveiligde portemonnees kunnen niet worden gebruikt voor Direct Betalen ze een PIN vereisen voor gebruik</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="65"/>
      <source>Enable Instant Pay for this wallet</source>
      <translation>Actief maken voor portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="72"/>
      <source>Maximum Amount</source>
      <translation>Maximum Bedrag</translation>
    </message>
  </context>
  <context>
    <name>LockApplication</name>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="25"/>
      <source>Security</source>
      <translation>Beveiliging</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="35"/>
      <source>PIN on startup</source>
      <translation>Pincode voor starten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter current PIN</source>
      <translation>Voer huidige pincode in</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter new PIN</source>
      <translation>Voer nieuwe pincode in</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="80"/>
      <source>Repeat PIN</source>
      <translation>Herhaal pincode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Remove PIN</source>
      <translation>Verwijder pincode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Set PIN</source>
      <translation>Pincode instellen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="124"/>
      <source>PIN has been removed</source>
      <translation>Pincode verwijderd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="127"/>
      <source>PIN has been set</source>
      <translation>Pincode gezet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="196"/>
      <source>Ok</source>
      <translation>Ok</translation>
    </message>
  </context>
  <context>
    <name>MainView</name>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="43"/>
      <source>Explore</source>
      <translation>Ontdek</translation>
    </message>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="65"/>
      <source>Find More</source>
      <translation>Vind meer</translation>
    </message>
  </context>
  <context>
    <name>MainViewBase</name>
    <message>
      <location filename="../guis/mobile/MainViewBase.qml" line="74"/>
      <source>No Internet Available</source>
      <translation>Geen Internet gevonden</translation>
    </message>
  </context>
  <context>
    <name>MenuOverlay</name>
    <message>
      <location filename="../guis/mobile/MenuOverlay.qml" line="318"/>
      <source>Add Wallet</source>
      <translation>Portemonnee toevoegen</translation>
    </message>
  </context>
  <context>
    <name>NewAccount</name>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="26"/>
      <source>New Bitcoin Cash Wallet</source>
      <translation>Nieuwe Bitcoin Cash Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="39"/>
      <source>New HD Wallet</source>
      <translation>Nieuwe HD portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="45"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Herstelzin gebaseerd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="46"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Back-upt makkelijk</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="47"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Meest compatibel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="52"/>
      <source>Import Existing Wallet</source>
      <translation>Importeer bestaande portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="58"/>
      <source>Imports seed-phrase</source>
      <translation>Importeert herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="59"/>
      <source>Imports private key</source>
      <translation>Importeert Privésleutel</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="64"/>
      <source>New Basic Wallet</source>
      <translation>Nieuwe Basic Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="70"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Privé sleutels gebaseerd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="71"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Back-up moeilijk</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="72"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideaal voor kort gebruik</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="87"/>
      <source>New Wallet</source>
      <translation>Nieuwe Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="95"/>
      <source>This creates a new empty wallet with simple multi-address capability</source>
      <translation>Dit maakt een nieuwe lege portemonnee met eenvoudige multi-adres mogelijkheden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="101"/>
      <location filename="../guis/mobile/NewAccount.qml" line="155"/>
      <source>Name</source>
      <translation>Naam</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="111"/>
      <source>Force Single Address</source>
      <translation>Forceer één adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="112"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Wanneer ingeschakeld zal deze portemonnee worden beperkt tot één adres.
Dit zorgt ervoor dat er slechts één privésleutel zal moeten worden geback-upt</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="120"/>
      <location filename="../guis/mobile/NewAccount.qml" line="180"/>
      <source>Create</source>
      <translation>Creëer</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="143"/>
      <source>New HD-Wallet</source>
      <translation>Nieuwe Portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="151"/>
      <source>This creates a new wallet that can be backed up with a seed-phrase</source>
      <translation>Dit start een nieuwe portemonnee die gebackupt kan worden met een herstelzin</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="163"/>
      <source>Derivation</source>
      <translation>Derivatie</translation>
    </message>
  </context>
  <context>
    <name>PayWithQR</name>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="26"/>
      <source>Approve Payment</source>
      <translation>Betaling goedkeuren</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="31"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Alles verzenden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="41"/>
      <source>Show Address</source>
      <comment>to show a bitcoincash address</comment>
      <translation>Toon adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="44"/>
      <source>Edit Amount</source>
      <comment>Edit amount of money to send</comment>
      <translation>Bedrag aanpassen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="98"/>
      <source>Invalid QR code</source>
      <translation>Ongeldige QR-code</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="112"/>
      <source>I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</source>
      <translation>Ik begrijp de gelezen code niet. Sorry, ik kan de betaling niet starten.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="116"/>
      <source>details</source>
      <translation>details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="127"/>
      <source>Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</source>
      <translation>Gelezen tekst: &lt;pre&gt;%1&lt;/pre&gt;</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="176"/>
      <source>Payment description</source>
      <translation>Omschrijving betaling</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="215"/>
      <source>Destination Address</source>
      <translation>Bestemmingsadres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="409"/>
      <source>Unlock Wallet</source>
      <translation>Portemonnee ontgrendelen</translation>
    </message>
  </context>
  <context>
    <name>PriceDetails</name>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="46"/>
      <source>1 BCH is: %1</source>
      <comment>Price of a whole bitcoin cash</comment>
      <translation>1 BCH is: %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="94"/>
      <source>7d</source>
      <comment>7 days</comment>
      <translation>7d</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="96"/>
      <source>1m</source>
      <comment>1 month</comment>
      <translation>1m</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="98"/>
      <source>3m</source>
      <comment>3 months</comment>
      <translation>3m</translation>
    </message>
  </context>
  <context>
    <name>PriceInputWidget</name>
    <message>
      <location filename="../guis/mobile/PriceInputWidget.qml" line="200"/>
      <source>All Currencies</source>
      <translation>Alle valuta's</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTab</name>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="27"/>
      <source>Receive</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="96"/>
      <source>Share this QR to receive</source>
      <translation>Betaler moet deze QR lezen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="117"/>
      <source>Encrypted Wallet</source>
      <translation>Versleutelde portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="119"/>
      <source>Import Running...</source>
      <translation>Bezig met importeren...</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="252"/>
      <source>Description</source>
      <translation>Omschrijving</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="167"/>
      <source>Address</source>
      <comment>Bitcoin Cash address</comment>
      <translation>Adres</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="177"/>
      <source>Clear</source>
      <translation>Wissen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="318"/>
      <location filename="../guis/mobile/ReceiveTab.qml" line="333"/>
      <source>Payment Seen</source>
      <translation>Betaling gezien</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="329"/>
      <source>High risk transaction</source>
      <translation>Transactie met hoog risico</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="331"/>
      <source>Partially Paid</source>
      <translation>Deels betaald</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="335"/>
      <source>Payment Accepted</source>
      <translation>Betaling geaccepteerd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="337"/>
      <source>Payment Settled</source>
      <translation>Betaling Afgewikkeld</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="367"/>
      <source>Continue</source>
      <translation>Doorgaan</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultAccountPage</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="22"/>
      <source>Select Wallet</source>
      <translation>Selecteer portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="30"/>
      <source>Pick which wallet will be selected on starting Flowee Pay</source>
      <translation>Kies welke portefeuille zal worden geselecteerd bij het starten van Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="69"/>
      <source>No InstaPay limit set</source>
      <translation>Geen Direct betalen limiet ingesteld</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="70"/>
      <source>InstaPay till %1</source>
      <translation>Direct Betalen tot %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="72"/>
      <source>InstaPay is turned off</source>
      <translation>Direct Betalen staat uit</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultConfigButton</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="22"/>
      <source>Default Wallet</source>
      <translation>Standaard portemonnee</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="32"/>
      <source>%1 is used on startup</source>
      <translation>%1 wordt gebruikt bij start</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionsTab</name>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="28"/>
      <source>Send</source>
      <translation>Versturen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="43"/>
      <source>Start Payment</source>
      <translation>Betaling starten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="46"/>
      <source>Scan QR</source>
      <translation>Scan QR-code</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="52"/>
      <source>Paste</source>
      <translation>Plak</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="71"/>
      <source>Options</source>
      <translation>Opties</translation>
    </message>
  </context>
  <context>
    <name>Settings</name>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="24"/>
      <source>Display Settings</source>
      <translation>Scherminstellingen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="39"/>
      <source>Font sizing</source>
      <translation>Lettertypegrootte</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="75"/>
      <source>Main View</source>
      <translation>Opties voor Overzicht</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="80"/>
      <source>Show Bitcoin Cash value</source>
      <translation>Toon Bitcoin Cash waarde</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="88"/>
      <source>Background Synchronization</source>
      <translation>Achtergrond synchronisatie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="89"/>
      <source>Keep your wallets synchronized by enabling this</source>
      <translation>Houd uw portemonnees gesynchroniseerd door dit in te schakelen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="96"/>
      <source>Unit</source>
      <translation>Eenheid</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="156"/>
      <source>Change Currency</source>
      <translation>Verander valuta</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="163"/>
      <source>Dark Theme</source>
      <translation>Donker Thema</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="165"/>
      <source>Follow System</source>
      <translation>Volg systeemsinstelling</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="171"/>
      <source>Dark</source>
      <translation>Donker</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="180"/>
      <source>Light</source>
      <translation>Licht</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="191"/>
      <source>Notifications</source>
      <translation>Notificaties</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="195"/>
      <source>On new block found</source>
      <translation>Bij nieuw gevonden blok</translation>
    </message>
  </context>
  <context>
    <name>SlideToApprove</name>
    <message>
      <location filename="../guis/mobile/SlideToApprove.qml" line="31"/>
      <source>SLIDE TO SEND</source>
      <translation>SWIPE VOOR VERZENDEN</translation>
    </message>
  </context>
  <context>
    <name>StartupScreen</name>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="86"/>
      <source>Continue</source>
      <translation>Doorgaan</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="76"/>
      <source>Moving the world towards a Bitcoin&#xa0;Cash economy</source>
      <translation>We lopen het pad naar een Bitcoin&#xa0;Cash economie</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="151"/>
      <source>Getting Started</source>
      <translation>Om te beginnen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="157"/>
      <source>All Videos</source>
      <translation>Alle video's</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="171"/>
      <source>Claim a Cash Stamp</source>
      <translation>Claim een Cash Stamp</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="188"/>
      <location filename="../guis/mobile/StartupScreen.qml" line="221"/>
      <source>OR</source>
      <translation>OF</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="200"/>
      <source>Scan to send to your wallet</source>
      <translation>Scan om naar uw portemonnee te verzenden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="235"/>
      <source>Add a different wallet</source>
      <translation>Een andere portemonnee toevoegen</translation>
    </message>
  </context>
  <context>
    <name>TextButton</name>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Enabled</source>
      <translation>Aan</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Disabled</source>
      <translation>Uitgeschakeld</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="29"/>
      <source>Transaction Details</source>
      <translation>Transactiedetails</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="32"/>
      <source>Open in Explorer</source>
      <translation>Open in Verkenner</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="46"/>
      <source>Transaction Hash</source>
      <translation>Transactie hash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="93"/>
      <source>First Seen</source>
      <translation>Eerst gezien</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="104"/>
      <source>Rejected</source>
      <translation>Afgewezen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="106"/>
      <source>Waiting for block</source>
      <translation>Wachten op blok</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="107"/>
      <source>Mined at</source>
      <translation>Gedolven in</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionDetails.qml" line="121"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>Meest recente blok</numerusform>
        <numerusform>%1 blokken terug</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="128"/>
      <source>Transaction comment</source>
      <translation>Transactie omschrijving</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="150"/>
      <source>Size: %1 bytes</source>
      <translation>Grootte: %1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="153"/>
      <source>Coinbase</source>
      <translation>Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="161"/>
      <source>Fees paid</source>
      <translation>Betaalde kosten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="164"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="177"/>
      <source>Fused from my addresses</source>
      <translation>Mijn gefuseerde adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="179"/>
      <source>Sent from my addresses</source>
      <translation>Verzonden vanaf mijn adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="181"/>
      <source>Sent from addresses</source>
      <translation>Verzonden vanaf adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="230"/>
      <source>Fused into my addresses</source>
      <translation>Gefuseerd naar mijn adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="232"/>
      <source>Received at addresses</source>
      <translation>Ontvangen op adressen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="233"/>
      <source>Received at my addresses</source>
      <translation>Ontvangen op mijn adressen</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="40"/>
      <source>Transaction is rejected</source>
      <translation>Transactie geweigerd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="42"/>
      <source>Processing</source>
      <translation>In behandeling</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="62"/>
      <source>Mined</source>
      <translation>Gedolven</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="75"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 blok terug</numerusform>
        <numerusform>%1 blokken terug</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="85"/>
      <source>Waiting for block</source>
      <translation>Wachten op blok</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="95"/>
      <source>Miner Reward</source>
      <translation>Miner Beloning</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="97"/>
      <source>Fees</source>
      <translation>Kosten</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="103"/>
      <source>Received</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="99"/>
      <source>Payment to self</source>
      <translation>Betaling aan uzelf</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="104"/>
      <source>Sent</source>
      <translation>Verzonden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="135"/>
      <source>Holds a token</source>
      <translation>Heeft een token</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="141"/>
      <source>Sent to</source>
      <translation>Verstuurd naar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="165"/>
      <source>Transaction Details</source>
      <translation>Transactiedetails</translation>
    </message>
  </context>
  <context>
    <name>TransactionListItem</name>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="91"/>
      <source>Miner Reward</source>
      <translation>Miner Beloning</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="93"/>
      <source>Fused</source>
      <translation>Gefuseerd</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="95"/>
      <source>Received</source>
      <translation>Ontvangen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="97"/>
      <source>Moved</source>
      <translation>Verschoven</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="99"/>
      <source>Sent</source>
      <translation>Verzonden</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="149"/>
      <source>Rejected</source>
      <translation>Afgewezen</translation>
    </message>
  </context>
  <context>
    <name>UnlockApplication</name>
    <message>
      <location filename="../guis/mobile/UnlockApplication.qml" line="84"/>
      <source>Quick Receive</source>
      <translation>Snel Ontvangen</translation>
    </message>
  </context>
  <context>
    <name>UnlockWidget</name>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="172"/>
      <source>Enter your wallet passcode</source>
      <translation>Voer pincode voor portemonnee in</translation>
    </message>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="308"/>
      <source>Open</source>
      <comment>open wallet with PIN</comment>
      <translation>Open</translation>
    </message>
  </context>
</TS>
