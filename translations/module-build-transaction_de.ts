<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>BuildTransactionModuleInfo</name>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="23"/>
      <source>Create Transactions</source>
      <translation>Transaktionen erstellen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="24"/>
      <source>This module allows building more powerful transactions in one simple user interface.</source>
      <translation>Dieses Modul ermöglicht das Erstellen von leistungsfähigeren Transaktionen in einer einfachen Benutzeroberfläche.</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="28"/>
      <source>Build Transaction</source>
      <translation>Transaktion erstellen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/BuildTransactionModuleInfo.cpp" line="29"/>
      <source>Manually select templates</source>
      <translation>Templates manuell auswählen</translation>
    </message>
  </context>
  <context>
    <name>DestinationEditPage</name>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="27"/>
      <source>Edit Destination</source>
      <translation>Empfänger definieren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="32"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Alles senden</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="38"/>
      <source>Bitcoin Cash Address</source>
      <translation>Bitcoin Cash Adresse</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="75"/>
      <source>Copy Address</source>
      <translation>Adresse kopieren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="131"/>
      <source>Warning</source>
      <translation>Warnung</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="139"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Dies ist eine BTC-Adresse, welche eine inkompatible Währung ist. Ihr Guthaben könnte verloren gehen und Flowee wird keine Möglichkeit haben, es wiederherzustellen. Sind Sie sicher, dass dies die richtige Adresse ist?</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/DestinationEditPage.qml" line="148"/>
      <source>I am certain</source>
      <translation>Ich bin mir sicher</translation>
    </message>
  </context>
  <context>
    <name>PayToOthers</name>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="28"/>
      <source>Build Transaction</source>
      <translation>Baue Transaktion</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="64"/>
      <source>Building Error</source>
      <comment>error during build</comment>
      <translation>Fehler beim Erstellen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="72"/>
      <source>Add Payment Detail</source>
      <comment>page title</comment>
      <translation>Zahlungsdetails hinzufügen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="80"/>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="485"/>
      <source>Add Destination</source>
      <translation>Empfänger hinzufügen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="81"/>
      <source>an address to send money to</source>
      <translation>eine Adresse zum Überweisen von Geld</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="99"/>
      <source>Confirm Sending</source>
      <comment>confirm we want to send the transaction</comment>
      <translation>Senden bestätigen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="119"/>
      <source>TXID</source>
      <translation>TXID</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="127"/>
      <source>Copy transaction-ID</source>
      <translation>Transaktions-ID kopieren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="130"/>
      <source>Fee</source>
      <translation>Gebühr</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="137"/>
      <source>Transaction size</source>
      <translation>Transaktionsgröße</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="140"/>
      <source>%1 bytes</source>
      <translation>%1 Bytes</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="143"/>
      <source>Fee per byte</source>
      <translation>Gebühr pro Byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="150"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 Sat/Byte</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="182"/>
      <source>Destination</source>
      <translation>Empfänger</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="191"/>
      <source>unset</source>
      <comment>indication of desination not being set</comment>
      <translation>nicht eingestellt</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="192"/>
      <source>invalid</source>
      <comment>address is not correct</comment>
      <translation>ungültig</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="199"/>
      <source>Copy Address</source>
      <translation>Adresse kopieren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Edit</source>
      <translation>Ziehen zum Bearbeiten</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="462"/>
      <source>Drag to Delete</source>
      <translation>Ziehen zum Löschen</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Unlock Wallet</source>
      <translation>Geldbörse entsperren</translation>
    </message>
    <message>
      <location filename="../modules/build-transaction/PayToOthers.qml" line="496"/>
      <source>Prepare Payment...</source>
      <translation>Zahlung vorbereiten...</translation>
    </message>
  </context>
</TS>
