<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ha" sourcelanguage="en">
  <context>
    <name>NetView</name>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="27"/>
      <source>Peers</source>
      <translation>Takwarori</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="30"/>
      <source>Statistics</source>
      <translation type="unfinished">Statistics</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="39"/>
      <source>No Internet Available</source>
      <translation type="unfinished">No Internet Available</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="90"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adireshi</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="97"/>
      <source>Start-height: %1</source>
      <translation>Fara-tsawo: %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="100"/>
      <source>ban-score: %1</source>
      <translation>Tsame-ci: %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="111"/>
      <source>Opening Connection</source>
      <translation type="unfinished">Opening Connection</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="113"/>
      <source>Validating peer</source>
      <translation type="unfinished">Validating peer</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="115"/>
      <source>Validated</source>
      <translation type="unfinished">Validated</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="117"/>
      <source>Good Peer</source>
      <comment>A useful peer</comment>
      <translation type="unfinished">Good Peer</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="123"/>
      <source>Peer for wallet: %1</source>
      <translation>Haɗin asusu %1</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="151"/>
      <source>Disconnect Peer</source>
      <translation type="unfinished">Disconnect Peer</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/NetView.qml" line="155"/>
      <source>Ban Peer</source>
      <translation type="unfinished">Ban Peer</translation>
    </message>
  </context>
  <context>
    <name>PeersViewModuleInfo</name>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="24"/>
      <source>Peers View</source>
      <translation>Takwarorin duba</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="25"/>
      <source>This module provides a view of network servers we connect to often called &apos;peers&apos;.</source>
      <translation>Wannan tsarin yana ba da ra'ayi na sabar cibiyar sadarwa da muke haɗawa da yawa ana kiranta &apos;takwarori&apos;.</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/PeersViewModuleInfo.cpp" line="30"/>
      <source>Network Details</source>
      <translation>Matsayin hanyar sadarwa</translation>
    </message>
  </context>
  <context>
    <name>StatsPage</name>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="25"/>
      <source>IP-Address Statistics</source>
      <translation type="unfinished">IP-Address Statistics</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="34"/>
      <source>Counts</source>
      <translation type="unfinished">Counts</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="37"/>
      <source>Total found</source>
      <translation type="unfinished">Total found</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="40"/>
      <source>Tried</source>
      <translation type="unfinished">Tried</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="44"/>
      <source>Misbehaving IPs</source>
      <translation type="unfinished">Misbehaving IPs</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="47"/>
      <source>Bad</source>
      <translation type="unfinished">Bad</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="50"/>
      <source>Banned</source>
      <translation type="unfinished">Banned</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="55"/>
      <source>Network</source>
      <translation type="unfinished">Network</translation>
    </message>
    <message>
      <location filename="../modules/peers-view/StatsPage.qml" line="68"/>
      <source>Pardon the Banned</source>
      <translation type="unfinished">Pardon the Banned</translation>
    </message>
  </context>
</TS>
