<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="en">
  <context>
    <name>About</name>
    <message>
      <location filename="../guis/mobile/About.qml" line="23"/>
      <source>About</source>
      <translation type="unfinished">About</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="43"/>
      <source>Help translate this app</source>
      <translation type="unfinished">Help translate this app</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="48"/>
      <source>License</source>
      <translation type="unfinished">License</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="53"/>
      <location filename="../guis/mobile/About.qml" line="61"/>
      <source>Credits</source>
      <translation type="unfinished">Credits</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="54"/>
      <source>© 2020-2025 Tom Zander and contributors</source>
      <translation type="unfinished">© 2020-2025 Tom Zander and contributors</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="99"/>
      <source>Project Home</source>
      <translation type="unfinished">Project Home</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="100"/>
      <source>With git repository and issues tracker</source>
      <translation type="unfinished">With git repository and issues tracker</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="105"/>
      <source>Telegram</source>
      <translation type="unfinished">Telegram</translation>
    </message>
  </context>
  <context>
    <name>AccountHistory</name>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="29"/>
      <source>Home</source>
      <translation type="unfinished">Home</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="162"/>
      <source>Pay</source>
      <translation type="unfinished">Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="168"/>
      <source>Receive</source>
      <translation type="unfinished">Receive</translation>
    </message>
  </context>
  <context>
    <name>AccountPageListItem</name>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="41"/>
      <source>Name</source>
      <translation type="unfinished">Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="64"/>
      <source>Archived wallets do not check for activities. Balance may be out of date</source>
      <translation type="unfinished">Archived wallets do not check for activities. Balance may be out of date</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="76"/>
      <source>Backup information</source>
      <translation type="unfinished">Backup information</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="87"/>
      <source>Backup Details</source>
      <translation type="unfinished">Backup Details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="121"/>
      <source>Wallet seed-phrase</source>
      <translation type="unfinished">Wallet seed-phrase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="148"/>
      <source>Password</source>
      <translation type="unfinished">Password</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="159"/>
      <source>Seed format</source>
      <translation type="unfinished">Seed format</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="168"/>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="269"/>
      <source>Starting Height</source>
      <comment>height refers to block-height</comment>
      <translation type="unfinished">Starting Height</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="173"/>
      <source>Derivation Path</source>
      <translation type="unfinished">Derivation Path</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="180"/>
      <source>xpub</source>
      <translation type="unfinished">xpub</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="210"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</source>
      <translation type="unfinished">Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="217"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation type="unfinished">&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="229"/>
      <source>Wallet keys</source>
      <translation type="unfinished">Wallet keys</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="232"/>
      <source>Show Index</source>
      <comment>toggle to show numbers</comment>
      <translation type="unfinished">Show Index</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="248"/>
      <source>Change Addresses</source>
      <translation type="unfinished">Change Addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="251"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation type="unfinished">Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="254"/>
      <source>Used Addresses</source>
      <translation type="unfinished">Used Addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="257"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation type="unfinished">Switches between unused and used Bitcoin addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="309"/>
      <source>Addresses and keys</source>
      <translation type="unfinished">Addresses and keys</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="317"/>
      <source>Sync Status</source>
      <translation type="unfinished">Sync Status</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="357"/>
      <source>Hide balance in overviews</source>
      <translation type="unfinished">Hide balance in overviews</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="364"/>
      <source>Hide in private mode</source>
      <translation type="unfinished">Hide in private mode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Unarchive Wallet</source>
      <translation type="unfinished">Unarchive Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Archive Wallet</source>
      <translation type="unfinished">Archive Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="423"/>
      <source>Really Delete?</source>
      <translation type="unfinished">Really Delete?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="424"/>
      <source>Removing wallet &quot;%1&quot; can not be undone.</source>
      <comment>argument is the wallet name</comment>
      <translation type="unfinished">Removing wallet &quot;%1&quot; can not be undone.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="403"/>
      <source>Remove Wallet</source>
      <translation type="unfinished">Remove Wallet</translation>
    </message>
  </context>
  <context>
    <name>AccountSelectorPopup</name>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="51"/>
      <source>Your Wallets</source>
      <translation type="unfinished">Your Wallets</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="130"/>
      <source>last active</source>
      <translation type="unfinished">last active</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="139"/>
      <source>Needs PIN to open</source>
      <translation type="unfinished">Needs PIN to open</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="175"/>
      <source>Balance Total</source>
      <translation type="unfinished">Balance Total</translation>
    </message>
  </context>
  <context>
    <name>AccountSyncState</name>
    <message>
      <location filename="../guis/mobile/AccountSyncState.qml" line="95"/>
      <source>Status: Offline</source>
      <translation type="unfinished">Status: Offline</translation>
    </message>
  </context>
  <context>
    <name>AccountsList</name>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallet</source>
      <translation type="unfinished">Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallets</source>
      <translation type="unfinished">Wallets</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="28"/>
      <source>Add Wallet</source>
      <translation type="unfinished">Add Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Exit Private Mode</source>
      <translation type="unfinished">Exit Private Mode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Enter Private Mode</source>
      <translation type="unfinished">Enter Private Mode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="60"/>
      <source>Reveals wallets marked private</source>
      <translation type="unfinished">Reveals wallets marked private</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="61"/>
      <source>Hides wallets marked private</source>
      <translation type="unfinished">Hides wallets marked private</translation>
    </message>
  </context>
  <context>
    <name>BackgroundSyncConfig</name>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="24"/>
      <source>Background Synchronization</source>
      <translation type="unfinished">Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="33"/>
      <source>Without background synchronization your wallets will not see new transactions until you open Flowee Pay</source>
      <translation type="unfinished">Without background synchronization your wallets will not see new transactions until you open Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="39"/>
      <source>Allow Background Synchronization</source>
      <translation type="unfinished">Allow Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="106"/>
      <source>Every %1 hours</source>
      <translation type="unfinished">Every %1 hours</translation>
    </message>
  </context>
  <context>
    <name>CurrencySelector</name>
    <message>
      <location filename="../guis/mobile/CurrencySelector.qml" line="24"/>
      <source>Select Currency</source>
      <translation type="unfinished">Select Currency</translation>
    </message>
  </context>
  <context>
    <name>ExploreModules</name>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="24"/>
      <source>Explore</source>
      <translation type="unfinished">Explore</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="120"/>
      <source>Open</source>
      <translation type="unfinished">Open</translation>
    </message>
  </context>
  <context>
    <name>FilterPopup</name>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="45"/>
      <source>Transactions Filter</source>
      <translation type="unfinished">Transactions Filter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="192"/>
      <source>Only with a comment</source>
      <comment>This is a statement about a transaction</comment>
      <translation type="unfinished">Only with a comment</translation>
    </message>
  </context>
  <context>
    <name>ImportWalletPage</name>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="26"/>
      <source>Import Wallet</source>
      <translation type="unfinished">Import Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="159"/>
      <source>Select import method</source>
      <translation type="unfinished">Select import method</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="172"/>
      <source>Scan QR</source>
      <translation type="unfinished">Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="212"/>
      <source>OR</source>
      <translation type="unfinished">OR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="224"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation type="unfinished">Secret as text</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="263"/>
      <source>Unknown word(s) found</source>
      <translation type="unfinished">Unknown word(s) found</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="274"/>
      <source>Next</source>
      <translation type="unfinished">Next</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="298"/>
      <source>Address to import</source>
      <translation type="unfinished">Address to import</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="312"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="446"/>
      <source>New Wallet Name</source>
      <translation type="unfinished">New Wallet Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="330"/>
      <source>Force Single Address</source>
      <translation type="unfinished">Force Single Address</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="331"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation type="unfinished">When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="336"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="505"/>
      <source>Oldest Transaction</source>
      <translation type="unfinished">Oldest Transaction</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="342"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation type="unfinished">Check Age</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="384"/>
      <source>Nothing found for wallet</source>
      <translation type="unfinished">Nothing found for wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="390"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="549"/>
      <source>Start</source>
      <translation type="unfinished">Start</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="464"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation type="unfinished">Discover Details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="515"/>
      <source>Derivation Path</source>
      <translation type="unfinished">Derivation Path</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="528"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation type="unfinished">Nothing found for seed. Does it have a password?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="536"/>
      <source>Password</source>
      <translation type="unfinished">Password</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="542"/>
      <source>imported wallet password</source>
      <translation type="unfinished">imported wallet password</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigButton</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Enable Instant Pay</source>
      <translation type="unfinished">Enable Instant Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Configure Instant Pay</source>
      <translation type="unfinished">Configure Instant Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="54"/>
      <source>Limits for %1</source>
      <comment>argument is a name</comment>
      <translation type="unfinished">Limits for %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="55"/>
      <source>Disabled for %1</source>
      <comment>argument is a name</comment>
      <translation type="unfinished">Disabled for %1</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigPage</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="23"/>
      <source>Instant Pay</source>
      <translation type="unfinished">Instant Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="29"/>
      <source>Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</source>
      <translation type="unfinished">Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="54"/>
      <source>Protected wallets can not be used for InstaPay because they require a PIN before usage</source>
      <translation type="unfinished">Protected wallets can not be used for InstaPay because they require a PIN before usage</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="65"/>
      <source>Enable Instant Pay for this wallet</source>
      <translation type="unfinished">Enable Instant Pay for this wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="72"/>
      <source>Maximum Amount</source>
      <translation type="unfinished">Maximum Amount</translation>
    </message>
  </context>
  <context>
    <name>LockApplication</name>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="25"/>
      <source>Security</source>
      <translation type="unfinished">Security</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="35"/>
      <source>PIN on startup</source>
      <translation type="unfinished">PIN on startup</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter current PIN</source>
      <translation type="unfinished">Enter current PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter new PIN</source>
      <translation type="unfinished">Enter new PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="80"/>
      <source>Repeat PIN</source>
      <translation type="unfinished">Repeat PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Remove PIN</source>
      <translation type="unfinished">Remove PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Set PIN</source>
      <translation type="unfinished">Set PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="124"/>
      <source>PIN has been removed</source>
      <translation type="unfinished">PIN has been removed</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="127"/>
      <source>PIN has been set</source>
      <translation type="unfinished">PIN has been set</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="196"/>
      <source>Ok</source>
      <translation type="unfinished">Ok</translation>
    </message>
  </context>
  <context>
    <name>MainView</name>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="43"/>
      <source>Explore</source>
      <translation type="unfinished">Explore</translation>
    </message>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="65"/>
      <source>Find More</source>
      <translation type="unfinished">Find More</translation>
    </message>
  </context>
  <context>
    <name>MainViewBase</name>
    <message>
      <location filename="../guis/mobile/MainViewBase.qml" line="74"/>
      <source>No Internet Available</source>
      <translation type="unfinished">No Internet Available</translation>
    </message>
  </context>
  <context>
    <name>MenuOverlay</name>
    <message>
      <location filename="../guis/mobile/MenuOverlay.qml" line="318"/>
      <source>Add Wallet</source>
      <translation type="unfinished">Add Wallet</translation>
    </message>
  </context>
  <context>
    <name>NewAccount</name>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="26"/>
      <source>New Bitcoin Cash Wallet</source>
      <translation type="unfinished">New Bitcoin Cash Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="39"/>
      <source>New HD Wallet</source>
      <translation type="unfinished">New HD Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="45"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation type="unfinished">Seed-phrase based</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="46"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation type="unfinished">Easy to backup</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="47"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation type="unfinished">Most compatible</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="52"/>
      <source>Import Existing Wallet</source>
      <translation type="unfinished">Import Existing Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="58"/>
      <source>Imports seed-phrase</source>
      <translation type="unfinished">Imports seed-phrase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="59"/>
      <source>Imports private key</source>
      <translation type="unfinished">Imports private key</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="64"/>
      <source>New Basic Wallet</source>
      <translation type="unfinished">New Basic Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="70"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation type="unfinished">Private keys based</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="71"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation type="unfinished">Difficult to backup</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="72"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation type="unfinished">Great for brief usage</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="87"/>
      <source>New Wallet</source>
      <translation type="unfinished">New Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="95"/>
      <source>This creates a new empty wallet with simple multi-address capability</source>
      <translation type="unfinished">This creates a new empty wallet with simple multi-address capability</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="101"/>
      <location filename="../guis/mobile/NewAccount.qml" line="155"/>
      <source>Name</source>
      <translation type="unfinished">Name</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="111"/>
      <source>Force Single Address</source>
      <translation type="unfinished">Force Single Address</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="112"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation type="unfinished">When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="120"/>
      <location filename="../guis/mobile/NewAccount.qml" line="180"/>
      <source>Create</source>
      <translation type="unfinished">Create</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="143"/>
      <source>New HD-Wallet</source>
      <translation type="unfinished">New HD-Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="151"/>
      <source>This creates a new wallet that can be backed up with a seed-phrase</source>
      <translation type="unfinished">This creates a new wallet that can be backed up with a seed-phrase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="163"/>
      <source>Derivation</source>
      <translation type="unfinished">Derivation</translation>
    </message>
  </context>
  <context>
    <name>PayWithQR</name>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="26"/>
      <source>Approve Payment</source>
      <translation type="unfinished">Approve Payment</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="31"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation type="unfinished">Send All</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="41"/>
      <source>Show Address</source>
      <comment>to show a bitcoincash address</comment>
      <translation type="unfinished">Show Address</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="44"/>
      <source>Edit Amount</source>
      <comment>Edit amount of money to send</comment>
      <translation type="unfinished">Edit Amount</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="98"/>
      <source>Invalid QR code</source>
      <translation type="unfinished">Invalid QR code</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="112"/>
      <source>I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</source>
      <translation type="unfinished">I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="116"/>
      <source>details</source>
      <translation type="unfinished">details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="127"/>
      <source>Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</source>
      <translation type="unfinished">Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="176"/>
      <source>Payment description</source>
      <translation type="unfinished">Payment description</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="215"/>
      <source>Destination Address</source>
      <translation type="unfinished">Destination Address</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="409"/>
      <source>Unlock Wallet</source>
      <translation type="unfinished">Unlock Wallet</translation>
    </message>
  </context>
  <context>
    <name>PriceDetails</name>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="46"/>
      <source>1 BCH is: %1</source>
      <comment>Price of a whole bitcoin cash</comment>
      <translation type="unfinished">1 BCH is: %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="94"/>
      <source>7d</source>
      <comment>7 days</comment>
      <translation type="unfinished">7d</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="96"/>
      <source>1m</source>
      <comment>1 month</comment>
      <translation type="unfinished">1m</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="98"/>
      <source>3m</source>
      <comment>3 months</comment>
      <translation type="unfinished">3m</translation>
    </message>
  </context>
  <context>
    <name>PriceInputWidget</name>
    <message>
      <location filename="../guis/mobile/PriceInputWidget.qml" line="200"/>
      <source>All Currencies</source>
      <translation type="unfinished">All Currencies</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTab</name>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="27"/>
      <source>Receive</source>
      <translation type="unfinished">Receive</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="96"/>
      <source>Share this QR to receive</source>
      <translation type="unfinished">Share this QR to receive</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="117"/>
      <source>Encrypted Wallet</source>
      <translation type="unfinished">Encrypted Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="119"/>
      <source>Import Running...</source>
      <translation type="unfinished">Import Running...</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="252"/>
      <source>Description</source>
      <translation type="unfinished">Description</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="167"/>
      <source>Address</source>
      <comment>Bitcoin Cash address</comment>
      <translation type="unfinished">Address</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="177"/>
      <source>Clear</source>
      <translation type="unfinished">Clear</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="318"/>
      <location filename="../guis/mobile/ReceiveTab.qml" line="333"/>
      <source>Payment Seen</source>
      <translation type="unfinished">Payment Seen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="329"/>
      <source>High risk transaction</source>
      <translation type="unfinished">High risk transaction</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="331"/>
      <source>Partially Paid</source>
      <translation type="unfinished">Partially Paid</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="335"/>
      <source>Payment Accepted</source>
      <translation type="unfinished">Payment Accepted</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="337"/>
      <source>Payment Settled</source>
      <translation type="unfinished">Payment Settled</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="367"/>
      <source>Continue</source>
      <translation type="unfinished">Continue</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultAccountPage</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="22"/>
      <source>Select Wallet</source>
      <translation type="unfinished">Select Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="30"/>
      <source>Pick which wallet will be selected on starting Flowee Pay</source>
      <translation type="unfinished">Pick which wallet will be selected on starting Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="69"/>
      <source>No InstaPay limit set</source>
      <translation type="unfinished">No InstaPay limit set</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="70"/>
      <source>InstaPay till %1</source>
      <translation type="unfinished">InstaPay till %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="72"/>
      <source>InstaPay is turned off</source>
      <translation type="unfinished">InstaPay is turned off</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultConfigButton</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="22"/>
      <source>Default Wallet</source>
      <translation type="unfinished">Default Wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="32"/>
      <source>%1 is used on startup</source>
      <translation type="unfinished">%1 is used on startup</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionsTab</name>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="28"/>
      <source>Send</source>
      <translation type="unfinished">Send</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="43"/>
      <source>Start Payment</source>
      <translation type="unfinished">Start Payment</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="46"/>
      <source>Scan QR</source>
      <translation type="unfinished">Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="52"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="71"/>
      <source>Options</source>
      <translation type="unfinished">Options</translation>
    </message>
  </context>
  <context>
    <name>Settings</name>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="24"/>
      <source>Display Settings</source>
      <translation type="unfinished">Display Settings</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="39"/>
      <source>Font sizing</source>
      <translation type="unfinished">Font sizing</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="75"/>
      <source>Main View</source>
      <translation type="unfinished">Main View</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="80"/>
      <source>Show Bitcoin Cash value</source>
      <translation type="unfinished">Show Bitcoin Cash value</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="88"/>
      <source>Background Synchronization</source>
      <translation type="unfinished">Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="89"/>
      <source>Keep your wallets synchronized by enabling this</source>
      <translation type="unfinished">Keep your wallets synchronized by enabling this</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="96"/>
      <source>Unit</source>
      <translation type="unfinished">Unit</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="156"/>
      <source>Change Currency</source>
      <translation type="unfinished">Change Currency</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="163"/>
      <source>Dark Theme</source>
      <translation type="unfinished">Dark Theme</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="165"/>
      <source>Follow System</source>
      <translation type="unfinished">Follow System</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="171"/>
      <source>Dark</source>
      <translation type="unfinished">Dark</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="180"/>
      <source>Light</source>
      <translation type="unfinished">Light</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="191"/>
      <source>Notifications</source>
      <translation type="unfinished">Notifications</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="195"/>
      <source>On new block found</source>
      <translation type="unfinished">On new block found</translation>
    </message>
  </context>
  <context>
    <name>SlideToApprove</name>
    <message>
      <location filename="../guis/mobile/SlideToApprove.qml" line="31"/>
      <source>SLIDE TO SEND</source>
      <translation type="unfinished">SLIDE TO SEND</translation>
    </message>
  </context>
  <context>
    <name>StartupScreen</name>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="86"/>
      <source>Continue</source>
      <translation type="unfinished">Continue</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="76"/>
      <source>Moving the world towards a Bitcoin&#xa0;Cash economy</source>
      <translation type="unfinished">Moving the world towards a Bitcoin&#xa0;Cash economy</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="151"/>
      <source>Getting Started</source>
      <translation type="unfinished">Getting Started</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="157"/>
      <source>All Videos</source>
      <translation type="unfinished">All Videos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="171"/>
      <source>Claim a Cash Stamp</source>
      <translation type="unfinished">Claim a Cash Stamp</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="188"/>
      <location filename="../guis/mobile/StartupScreen.qml" line="221"/>
      <source>OR</source>
      <translation type="unfinished">OR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="200"/>
      <source>Scan to send to your wallet</source>
      <translation type="unfinished">Scan to send to your wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="235"/>
      <source>Add a different wallet</source>
      <translation type="unfinished">Add a different wallet</translation>
    </message>
  </context>
  <context>
    <name>TextButton</name>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Enabled</source>
      <translation type="unfinished">Enabled</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Disabled</source>
      <translation type="unfinished">Disabled</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="29"/>
      <source>Transaction Details</source>
      <translation type="unfinished">Transaction Details</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="32"/>
      <source>Open in Explorer</source>
      <translation type="unfinished">Open in Explorer</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="46"/>
      <source>Transaction Hash</source>
      <translation type="unfinished">Transaction Hash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="93"/>
      <source>First Seen</source>
      <translation type="unfinished">First Seen</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="104"/>
      <source>Rejected</source>
      <translation type="unfinished">Rejected</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="106"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="107"/>
      <source>Mined at</source>
      <translation type="unfinished">Mined at</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionDetails.qml" line="121"/>
      <source>%1 blocks ago</source>
      <translation type="unfinished">
        <numerusform>%1 blocks ago</numerusform>
        <numerusform>%1 blocks ago</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="128"/>
      <source>Transaction comment</source>
      <translation type="unfinished">Transaction comment</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="150"/>
      <source>Size: %1 bytes</source>
      <translation type="unfinished">Size: %1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="153"/>
      <source>Coinbase</source>
      <translation type="unfinished">Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="161"/>
      <source>Fees paid</source>
      <translation type="unfinished">Fees paid</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="164"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation type="unfinished">%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="177"/>
      <source>Fused from my addresses</source>
      <translation type="unfinished">Fused from my addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="179"/>
      <source>Sent from my addresses</source>
      <translation type="unfinished">Sent from my addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="181"/>
      <source>Sent from addresses</source>
      <translation type="unfinished">Sent from addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="230"/>
      <source>Fused into my addresses</source>
      <translation type="unfinished">Fused into my addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="232"/>
      <source>Received at addresses</source>
      <translation type="unfinished">Received at addresses</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="233"/>
      <source>Received at my addresses</source>
      <translation type="unfinished">Received at my addresses</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="40"/>
      <source>Transaction is rejected</source>
      <translation type="unfinished">Transaction is rejected</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="42"/>
      <source>Processing</source>
      <translation type="unfinished">Processing</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="62"/>
      <source>Mined</source>
      <translation type="unfinished">Mined</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="75"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation type="unfinished">
        <numerusform>%1 blocks ago</numerusform>
        <numerusform>%1 blocks ago</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="85"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="95"/>
      <source>Miner Reward</source>
      <translation type="unfinished">Miner Reward</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="97"/>
      <source>Fees</source>
      <translation type="unfinished">Fees</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="103"/>
      <source>Received</source>
      <translation type="unfinished">Received</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="99"/>
      <source>Payment to self</source>
      <translation type="unfinished">Payment to self</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="104"/>
      <source>Sent</source>
      <translation type="unfinished">Sent</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="135"/>
      <source>Holds a token</source>
      <translation type="unfinished">Holds a token</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="141"/>
      <source>Sent to</source>
      <translation type="unfinished">Sent to</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="165"/>
      <source>Transaction Details</source>
      <translation type="unfinished">Transaction Details</translation>
    </message>
  </context>
  <context>
    <name>TransactionListItem</name>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="91"/>
      <source>Miner Reward</source>
      <translation type="unfinished">Miner Reward</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="93"/>
      <source>Fused</source>
      <translation type="unfinished">Fused</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="95"/>
      <source>Received</source>
      <translation type="unfinished">Received</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="97"/>
      <source>Moved</source>
      <translation type="unfinished">Moved</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="99"/>
      <source>Sent</source>
      <translation type="unfinished">Sent</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="149"/>
      <source>Rejected</source>
      <translation type="unfinished">Rejected</translation>
    </message>
  </context>
  <context>
    <name>UnlockApplication</name>
    <message>
      <location filename="../guis/mobile/UnlockApplication.qml" line="84"/>
      <source>Quick Receive</source>
      <translation type="unfinished">Quick Receive</translation>
    </message>
  </context>
  <context>
    <name>UnlockWidget</name>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="172"/>
      <source>Enter your wallet passcode</source>
      <translation type="unfinished">Enter your wallet passcode</translation>
    </message>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="308"/>
      <source>Open</source>
      <comment>open wallet with PIN</comment>
      <translation type="unfinished">Open</translation>
    </message>
  </context>
</TS>
