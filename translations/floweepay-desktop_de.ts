<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>AccountConfigMenu</name>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="26"/>
      <source>Details</source>
      <translation>Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Unarchive</source>
      <translation>Entarchivieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="33"/>
      <source>Archive Wallet</source>
      <translation>Geldbörse archivieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>Make Primary</source>
      <translation>Primär setzen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="47"/>
      <source>★ Primary</source>
      <translation>★ Primär</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="51"/>
      <source>Protect With Pin...</source>
      <translation>Mit Pin schützen...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="58"/>
      <source>Open</source>
      <comment>Open encrypted wallet</comment>
      <translation>Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountConfigMenu.qml" line="63"/>
      <source>Close</source>
      <comment>Close encrypted wallet</comment>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>AccountDetails</name>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="76"/>
      <source>Wallet Details</source>
      <translation>Geldbörsen Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="100"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="140"/>
      <source>Sync Status</source>
      <translation>Sync Status</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="189"/>
      <source>Encryption</source>
      <translation>Verschlüsselung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="200"/>
      <location filename="../guis/desktop/AccountDetails.qml" line="367"/>
      <source>Password</source>
      <translation>Passwort</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="229"/>
      <source>Open</source>
      <translation>Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="239"/>
      <source>Invalid PIN</source>
      <translation>Ungültige PIN</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="255"/>
      <source>Include balance in total</source>
      <translation>Inkludiere Guthaben in Gesamtsumme</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="267"/>
      <source>Hide in private mode</source>
      <translation>Im privaten Modus ausblenden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="277"/>
      <source>Address List</source>
      <translation>Adressliste</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="283"/>
      <source>Change Addresses</source>
      <translation>Wechselgeldadressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="286"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Wechselt zwischen Adressen, auf welchen Sie von anderen Leuten bezahlt werden können, und Adressen, die die Geldbörse verwendet, um Wechselgeld an sich selbst zu senden.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="290"/>
      <source>Used Addresses</source>
      <translation>Benutzte Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="293"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Wechselt zwischen ungenutzten und genutzten Bitcoin-Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="313"/>
      <source>Backup details</source>
      <translation>Backup-Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="330"/>
      <source>Seed-phrase</source>
      <translation>Seed-Phrase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="348"/>
      <source>Copy</source>
      <translation>Kopieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="376"/>
      <source>Seed format</source>
      <translation>Seed-Format</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="387"/>
      <source>Derivation</source>
      <translation>Ableitung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="400"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case of computer failure.</source>
      <translation>Bitte speichern Sie die Seed-Phrase in der richtigen Reihenfolge mit dem Ableitungspfad. Mit diesem Seed können Sie Ihre Geldbörse im Falle eines Computerfehlers wiederherstellen.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="410"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Wichtig&lt;/b&gt;: Teilen Sie Ihre Seed-Phrase nie mit anderen!</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AccountDetails.qml" line="418"/>
      <source>This wallet is protected by password (pin-to-pay). To see the backup details you need to provide the password.</source>
      <translation>Diese Geldbörse ist passwortgeschützt (pin-to-pay). Um die Backup-Details zu sehen, müssen Sie das Passwort angeben.</translation>
    </message>
  </context>
  <context>
    <name>AccountListItem</name>
    <message>
      <location filename="../guis/desktop/AccountListItem.qml" line="144"/>
      <source>Last Transaction</source>
      <translation>Letzte Transaktion</translation>
    </message>
  </context>
  <context>
    <name>ActivityConfigBar</name>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="66"/>
      <source>Filter</source>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="156"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="197"/>
      <source>Received</source>
      <translation>Erhalten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="175"/>
      <location filename="../guis/desktop/ActivityConfigBar.qml" line="204"/>
      <source>Sent</source>
      <translation>Gesendet</translation>
    </message>
  </context>
  <context>
    <name>AddressDbStats</name>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="30"/>
      <source>IP Addresses</source>
      <translation>IP-Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="49"/>
      <source>Total found</source>
      <translation>Gesamt gefunden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="57"/>
      <source>Tried</source>
      <translation>Versuchte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="64"/>
      <source>Punished count</source>
      <translation>Abgestrafte Anzahl</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="71"/>
      <source>Banned count</source>
      <translation>Gebannte Anzahl</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="79"/>
      <source>IP-v4 count</source>
      <translation>IPv4-Anzahl</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="87"/>
      <source>IP-v6 count</source>
      <translation>IPv6-Anzahl</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="97"/>
      <source>Pardon the Banned</source>
      <translation>Gesperrte Knoten entsperren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/AddressDbStats.qml" line="123"/>
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>NetView</name>
    <message numerus="yes">
      <location filename="../guis/desktop/NetView.qml" line="31"/>
      <source>Peers (%1)</source>
      <translation>
        <numerusform>Peers (%1)</numerusform>
        <numerusform>Peers (%1)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="83"/>
      <source>Address</source>
      <comment>network address (IP)</comment>
      <translation>Adresse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="90"/>
      <source>Start-height: %1</source>
      <translation>Starthöhe: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="93"/>
      <source>ban-score: %1</source>
      <translation>ban-score: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="116"/>
      <source>Peer for wallet: %1</source>
      <translation>Peer für Geldbörse: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="139"/>
      <source>Disconnect Peer</source>
      <translation>Trenne Knoten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="143"/>
      <source>Ban Peer</source>
      <translation>Sperre Knoten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NetView.qml" line="162"/>
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateBasicAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="39"/>
      <source>Create a new empty wallet with simple multi-address capability </source>
      <translation>Dies erzeugt eine neue leere Geldbörse mit einfacher Mehradressen-Fähigkeit </translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="48"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="59"/>
      <source>Force Single Address</source>
      <translation>Erzwinge einzelne Adresse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="60"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Wenn aktiviert, wird diese Geldbörse auf eine Adresse beschränkt.
Dies stellt sicher, dass nur ein privater Schlüssel gesichert werden muss</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateBasicAccount.qml" line="67"/>
      <source>Go</source>
      <translation>Los</translation>
    </message>
  </context>
  <context>
    <name>NewAccountCreateHDAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="33"/>
      <source>Create a new wallet with smart creation of addresses from a single seed-phrase</source>
      <translation>Erstellt eine neue Geldbörse mit intelligenter Erstellung von Adressen aus einer einzigen Seed-Phrase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="41"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="55"/>
      <source>Go</source>
      <translation>Los</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="71"/>
      <source>Advanced Options</source>
      <translation>Erweiterte Optionen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountCreateHDAccount.qml" line="76"/>
      <source>Derivation</source>
      <translation>Ableitung</translation>
    </message>
  </context>
  <context>
    <name>NewAccountImportAccount</name>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="90"/>
      <source>Select import method</source>
      <translation>Importmethode auswählen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="103"/>
      <source>Camera</source>
      <translation>Kamera</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="142"/>
      <source>OR</source>
      <translation>ODER</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="154"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Geheimnis als Text</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="194"/>
      <source>Unknown word(s) found</source>
      <translation>Unbekannte(s) Wort(e) gefunden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="217"/>
      <source>Address to import</source>
      <translation>Zu importierende Adresse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="232"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="353"/>
      <source>New Wallet Name</source>
      <translation>Name der neuen Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="252"/>
      <source>Force Single Address</source>
      <translation>Erzwinge einzelne Adresse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="253"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Wenn aktiviert, werden keine zusätzlichen Adressen automatisch in dieser Geldbörse generiert.
Wechselgeld wird zum importierten Schlüssel zurückgeführt.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="258"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="414"/>
      <source>Oldest Transaction</source>
      <translation>Älteste Transaktion</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="266"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Überprüfe Alter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="310"/>
      <source>Nothing found for wallet</source>
      <translation>Nichts gefunden für Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="317"/>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="461"/>
      <source>Start</source>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="373"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Finde Details</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="425"/>
      <source>Derivation</source>
      <translation>Ableitung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="439"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation>Nichts für Seed gefunden. Hat es ein Passwort?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="447"/>
      <source>Password</source>
      <translation>Passwort</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountImportAccount.qml" line="454"/>
      <source>imported wallet password</source>
      <translation>Passwort der zu importierenden Geldbörse</translation>
    </message>
  </context>
  <context>
    <name>NewAccountPane</name>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="64"/>
      <source>New HD wallet</source>
      <translation>Neue HD-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="70"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Seed-Phrase basiert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="71"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Einfach zu sichern</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="72"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>Am kompatibelsten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="77"/>
      <source>Import Existing Wallet</source>
      <translation>Vorhandene Geldbörse importieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="83"/>
      <source>Imports seed-phrase</source>
      <translation>Importiert Seed-Phrase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="84"/>
      <source>Imports private key</source>
      <translation>Importiert privaten Schlüssel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="90"/>
      <source>New Basic Wallet</source>
      <translation>Neue Basis-Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="95"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Basiert auf privaten Schlüsseln</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="96"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Schwierig zu sichern</translation>
    </message>
    <message>
      <location filename="../guis/desktop/NewAccountPane.qml" line="97"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideal für kurze Nutzung</translation>
    </message>
  </context>
  <context>
    <name>PaymentTweakingPanel</name>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="80"/>
      <source>Add Detail</source>
      <translation>Detail hinzufügen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="94"/>
      <source>Coin Selector</source>
      <translation>Coin Auswahl</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="104"/>
      <source>To override the default selection of coins that are used to pay a transaction, you can add the &apos;Coin Selector&apos; where the wallets coins will be made visible.</source>
      <translation>Um die Standardauswahl von Coins zu überschreiben, die zur Zahlung einer Transaktion verwendet werden, können Sie den &apos;Coin Selektor&apos; hinzufügen, in dem die Coins der Geldbörse sichtbar gemacht werden.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="107"/>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/PaymentTweakingPanel.qml" line="117"/>
      <source>This allows adding a public comment, that will be included in the transaction and seen by everyone.</source>
      <translation>Dies ermöglicht das Hinzufügen eines öffentlichen Kommentars, der in die Transaktion aufgenommen wird und von allen gesehen wird.</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTransactionPane</name>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="51"/>
      <source>Share your QR code or copy address to receive</source>
      <translation>Teilen Sie Ihren QR-Code oder kopieren Sie Ihre Empfangsadresse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="68"/>
      <source>Encrypted Wallet</source>
      <translation>Verschlüsselte Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="70"/>
      <source>Import Running...</source>
      <translation>Import läuft...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="146"/>
      <source>Checking</source>
      <translation>Überprüfe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="167"/>
      <source>High risk transaction</source>
      <translation>Hochriskante Transaktion</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="169"/>
      <source>Payment Seen</source>
      <translation>Zahlung gesichtet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="171"/>
      <source>Payment Accepted</source>
      <translation>Zahlung akzeptiert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="173"/>
      <source>Payment Settled</source>
      <translation>Zahlung abgeschlossen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="191"/>
      <source>Instant payment failed. Wait for confirmation. (double spent proof received)</source>
      <translation>Sofortige Zahlung fehlgeschlagen. Warten Sie auf Bestätigung. (Double-Spend Proof erhalten)</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="209"/>
      <source>Description</source>
      <translation>Beschreibung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="221"/>
      <source>Amount</source>
      <translation>Betrag</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Clear</source>
      <translation>Leeren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/ReceiveTransactionPane.qml" line="245"/>
      <source>Done</source>
      <translation>Erledigt</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionPane</name>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="125"/>
      <source>Confirm delete</source>
      <translation>Löschen bestätigen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="126"/>
      <source>Do you really want to delete this detail?</source>
      <translation>Möchten Sie dieses Detail wirklich löschen?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="135"/>
      <source>Add Destination</source>
      <translation>Empfänger hinzufügen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="141"/>
      <source>Prepare</source>
      <translation>Vorbereiten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="157"/>
      <source>Enter your PIN</source>
      <translation>Geben Sie Ihre PIN ein</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="166"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="454"/>
      <source>Warning</source>
      <translation>Warnung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="173"/>
      <source>Payment request warnings:</source>
      <translation>Warnungen für Zahlungsanforderung:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="191"/>
      <source>Transaction Details</source>
      <translation>Transaktionsdetails</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="205"/>
      <source>Not prepared yet</source>
      <translation>Noch nicht vorbereitet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="211"/>
      <source>Copy transaction-ID</source>
      <translation>Transaktions-ID kopieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="214"/>
      <source>Fee</source>
      <translation>Gebühr</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="224"/>
      <source>Transaction size</source>
      <translation>Transaktionsgröße</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="232"/>
      <source>%1 bytes</source>
      <translation>%1 Bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="237"/>
      <source>Fee per byte</source>
      <translation>Gebühr pro Byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="248"/>
      <source>%1 sat/byte</source>
      <comment>fee</comment>
      <translation>%1 Sat/Byte</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="262"/>
      <source>Send</source>
      <translation>Senden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="320"/>
      <source>Destination</source>
      <translation>Ziel</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="327"/>
      <source>Max available</source>
      <comment>The maximum balance available</comment>
      <translation>Max. verfügbar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="336"/>
      <source>%1 to %2</source>
      <comment>summary text to pay X-euro to address M</comment>
      <translation>%1 zu %2</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="354"/>
      <source>Enter Bitcoin Cash Address</source>
      <translation>Geben Sie eine Bitcoin Cash Adresse ein</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="379"/>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="666"/>
      <source>Copy Address</source>
      <translation>Adresse kopieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="389"/>
      <source>Amount</source>
      <translation>Betrag</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="413"/>
      <source>Max</source>
      <translation>Max</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="459"/>
      <source>This is a BTC address, which is an incompatible coin. Your funds could get lost and Flowee will have no way to recover them. Are you sure this is the right address?</source>
      <translation>Dies ist eine BTC-Adresse, welche eine inkompatible Währung ist. Ihr Guthaben könnte verloren gehen und Flowee wird keine Möglichkeit haben, es wiederherzustellen. Sind Sie sicher, dass dies die richtige Adresse ist?</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="471"/>
      <source>Continue</source>
      <translation>Fortfahren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="475"/>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="496"/>
      <source>Coin Selector</source>
      <translation>Coin Selektor</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/SendTransactionPane.qml" line="497"/>
      <source>Selected %1 %2 in %3 coins</source>
      <comment>selected 2 BCH in 5 coins</comment>
      <translation>
        <numerusform>%1 %2 in %3 Coins ausgewählt</numerusform>
        <numerusform>%1 %2 in %3 Coins ausgewählt</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="508"/>
      <source>Total</source>
      <comment>Number of coins</comment>
      <translation>Gesamt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="515"/>
      <source>Needed</source>
      <translation>Benötigt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="525"/>
      <source>Selected</source>
      <translation>Ausgewählt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="532"/>
      <source>Value</source>
      <translation>Wert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="570"/>
      <source>Locked coins will never be used for payments. Right-click for menu.</source>
      <translation>Gesperrte Coins werden nie für Zahlungen verwendet. Rechtsklick für Menü.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="609"/>
      <source>Age</source>
      <translation>Alter</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Unselect All</source>
      <translation>Alles abwählen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="649"/>
      <source>Select All</source>
      <translation>Alles auswählen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Unlock coin</source>
      <translation>Coin entsperren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="659"/>
      <source>Lock coin</source>
      <translation>Coin sperren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="694"/>
      <source>Public-comment</source>
      <translation>Öffentlicher Kommentar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="702"/>
      <source>Add a comment you want to include in the transaction, visible for everyone.</source>
      <translation>Fügen Sie einen Kommentar hinzu, den Sie in die Transaktion einfügen möchten, sichtbar für alle.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="703"/>
      <source>Custom message, to be included in the transaction.</source>
      <translation>Benutzerdefinierte Nachricht, die in die Transaktion aufgenommen werden soll.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="708"/>
      <source>Text</source>
      <translation>Text</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SendTransactionPane.qml" line="718"/>
      <source>Size</source>
      <translation>Größe</translation>
    </message>
  </context>
  <context>
    <name>SettingsPane</name>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="26"/>
      <source>Settings</source>
      <translation>Einstellungen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="37"/>
      <source>Unit</source>
      <translation>Einheit</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="107"/>
      <source>Show Bitcoin Cash value on Activity page</source>
      <translation>Bitcoin Cash Wert auf der Aktivitätsseite anzeigen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="120"/>
      <source>Show Block Notifications</source>
      <translation>Zeige Blockbenachrichtigungen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="121"/>
      <source>When a new block is mined, Flowee Pay shows a desktop notification</source>
      <translation>Wenn ein neuer Block erzeugt wird, zeigt Flowee Pay eine Desktop-Benachrichtigung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="135"/>
      <source>Night Mode</source>
      <translation>Nachtmodus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="146"/>
      <source>Private Mode</source>
      <translation>Privater Modus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="147"/>
      <source>Hides private wallets while enabled</source>
      <translation>Versteckt private Geldbörsen, wenn aktiviert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="153"/>
      <source>Font sizing</source>
      <translation>Schriftgröße</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="194"/>
      <source>Version</source>
      <translation>Version</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="202"/>
      <source>Library Version</source>
      <translation>Version der Bibliothek</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="211"/>
      <source>Synchronization</source>
      <translation>Synchronisation</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="216"/>
      <source>Network Status</source>
      <translation>Netzwerkstatus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/SettingsPane.qml" line="226"/>
      <source>Address Stats</source>
      <translation>Adressstatistik</translation>
    </message>
  </context>
  <context>
    <name>Transaction</name>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="65"/>
      <source>Miner Reward</source>
      <translation>Miner Belohnung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="67"/>
      <source>Fused</source>
      <translation>Fusioniert</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="69"/>
      <source>Received</source>
      <translation>Erhalten</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="71"/>
      <source>Moved</source>
      <translation>Verschoben</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="72"/>
      <source>Sent</source>
      <translation>Gesendet</translation>
    </message>
    <message>
      <location filename="../guis/desktop/Transaction.qml" line="80"/>
      <source>rejected</source>
      <translation>abgelehnt</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="31"/>
      <source>Transaction Details</source>
      <translation>Transaktionsdetails</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="101"/>
      <source>First Seen</source>
      <translation>Erstsichtung</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="115"/>
      <source>Rejected</source>
      <translation>Abgelehnt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="117"/>
      <source>Mined at</source>
      <translation>Abgebaut am</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionDetails.qml" line="134"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>%1 Block her</numerusform>
        <numerusform>%1 Blöcke her</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="141"/>
      <source>Waiting for block</source>
      <translation>Warte auf Block</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="146"/>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="163"/>
      <source>Fees paid</source>
      <translation>Bezahlte Gebühren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="169"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 Bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="184"/>
      <source>Size</source>
      <translation>Größe</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="188"/>
      <source>%1 bytes</source>
      <translation>%1 Bytes</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="195"/>
      <source>Is Coinbase</source>
      <translation>Ist Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="205"/>
      <source>Copy transaction-ID</source>
      <translation>Kopiere Transaktions-ID</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="219"/>
      <source>Fused from my addresses</source>
      <translation>Fusioniert von meinen Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="221"/>
      <source>Sent from my addresses</source>
      <translation>Gesendet von meinen Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="223"/>
      <source>Sent from addresses</source>
      <translation>Gesendet von den Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="281"/>
      <source>Fused into my addresses</source>
      <translation>Fusioniert in meine Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="283"/>
      <source>Received at addresses</source>
      <translation>Empfangen auf den Adressen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionDetails.qml" line="284"/>
      <source>Received at my addresses</source>
      <translation>Empfangen auf meinen Adressen</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="44"/>
      <source>Transaction is rejected</source>
      <translation>Transaktion wurde abgelehnt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="46"/>
      <source>Processing</source>
      <translation>Verarbeite</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="59"/>
      <source>Mined</source>
      <translation>Gemined</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="70"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>%1 Block her</numerusform>
        <numerusform>%1 Blöcke her</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="106"/>
      <source>Fees</source>
      <translation>Gebühren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="121"/>
      <source>Copy transaction-ID</source>
      <translation>Transaktions-ID kopieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="146"/>
      <source>Holds a token</source>
      <translation>Hält ein Token</translation>
    </message>
    <message>
      <location filename="../guis/desktop/TransactionInfoSmall.qml" line="168"/>
      <source>Opening Website</source>
      <translation>Öffne Website</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryption</name>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="63"/>
      <source>Protect your wallet with a password</source>
      <translation>Schützen Sie Ihre Geldbörse mit einem Passwort</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="88"/>
      <source>Pin to Pay</source>
      <translation>Pin um zu bezahlen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="93"/>
      <source>Protect your funds</source>
      <comment>pin to pay</comment>
      <translation>Schützen Sie Ihr Guthaben</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="94"/>
      <source>Fully open, except for sending funds</source>
      <comment>pin to pay</comment>
      <translation>Vollständig geöffnet, außer für das Senden von Guthaben</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="95"/>
      <source>Keeps in sync</source>
      <comment>pin to pay</comment>
      <translation>Bleibt synchron</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="100"/>
      <source>Pin to Open</source>
      <translation>Pin zum Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="105"/>
      <source>Protect your entire wallet</source>
      <comment>pin to open</comment>
      <translation>Schützen Sie Ihre gesamte Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="106"/>
      <source>Balance and history protected</source>
      <comment>pin to open</comment>
      <translation>Guthaben und Verlauf geschützt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="107"/>
      <source>Requires Pin to view, sync or pay</source>
      <comment>pin to open</comment>
      <translation>Benötigt Pin zum Ansehen, Synchronisieren oder Bezahlen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="117"/>
      <source>Make &quot;%1&quot; wallet require a pin to pay</source>
      <translation>Erfordere einen Pin zum Bezahlen für &quot;%1&quot; Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="118"/>
      <source>Make &quot;%1&quot; wallet require a pin to open</source>
      <translation>Erfordere einen Pin zum Öffnen für &quot;%1&quot; Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="130"/>
      <location filename="../guis/desktop/WalletEncryption.qml" line="142"/>
      <source>Wallet already has pin to open applied</source>
      <translation>Geldbörse hat bereits eine PIN zum Öffnen an</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="132"/>
      <source>Wallet already has pin to pay applied</source>
      <translation>Geldbörse hat bereits eine PIN zum Bezahlen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="133"/>
      <source>Your wallet will get partially encrypted and payments will become impossible without a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Ihre Geldbörse wird teilweise verschlüsselt und Zahlungen werden ohne Passwort nicht möglich. Wenn Sie über kein Backup dieser Geldbörse verfügen, erstellen Sie zuerst eines.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="143"/>
      <source>Your full wallet gets encrypted, opening it will need a password. If you don&apos;t have a backup of this wallet, make one first.</source>
      <translation>Ihre vollständige Geldbörse wird verschlüsselt, um sie zu öffnen, wird ein Passwort benötigen. Wenn Sie über kein Backup dieser Geldbörse verfügen, erstellen Sie zuerst eines.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="169"/>
      <source>Password</source>
      <translation>Passwort</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="192"/>
      <source>Wallet</source>
      <translation>Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="214"/>
      <source>Encrypt</source>
      <translation>Verschlüsseln</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="221"/>
      <source>Invalid password to open this wallet</source>
      <translation>Ungültiges Passwort zum Öffnen dieser Geldbörse</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="233"/>
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="243"/>
      <source>Repeat password</source>
      <translation>Passwort wiederholen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="244"/>
      <source>Please confirm the password by entering it again</source>
      <translation>Bitte bestätigen Sie Ihr Passwort, indem Sie es erneut eingeben</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryption.qml" line="249"/>
      <source>Typed passwords do not match</source>
      <translation>Die eingegebenen Passwörter stimmen nicht überein</translation>
    </message>
  </context>
  <context>
    <name>WalletEncryptionStatus</name>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="46"/>
      <source>Pin to Open</source>
      <translation>Pin zum Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="48"/>
      <source>Pin to Pay</source>
      <translation>Pin um zu bezahlen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/WalletEncryptionStatus.qml" line="50"/>
      <source>(Opened)</source>
      <comment>Wallet is decrypted</comment>
      <translation>(Geöffnet)</translation>
    </message>
  </context>
  <context>
    <name>locked</name>
    <message>
      <location filename="../guis/desktop/locked.qml" line="51"/>
      <source>Already running?</source>
      <translation>Wird bereits ausgeführt?</translation>
    </message>
  </context>
  <context>
    <name>main</name>
    <message>
      <location filename="../guis/desktop/main.qml" line="210"/>
      <source>Activity</source>
      <translation>Aktivität</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="250"/>
      <source>Archived wallets do not check for activities. Balance may be out of date.</source>
      <translation>Archivierte Geldbörsen überprüfen nicht auf Aktivitäten. Das Guthaben kann veraltet sein.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="254"/>
      <source>Unarchive</source>
      <translation>Entarchivieren</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="287"/>
      <source>This wallet needs a password to open.</source>
      <translation>Diese Geldbörse benötigt ein Passwort zum Öffnen.</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="295"/>
      <source>Password:</source>
      <translation>Passwort:</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="315"/>
      <source>Invalid password</source>
      <translation>Ungültiges Passwort</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="320"/>
      <source>Open</source>
      <translation>Öffnen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="398"/>
      <source>Send</source>
      <translation>Senden</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="406"/>
      <source>Receive</source>
      <translation>Empfangen</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="525"/>
      <source>Balance</source>
      <translation>Guthaben</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="594"/>
      <source>Main</source>
      <comment>balance (money), non specified</comment>
      <translation>Haupt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="604"/>
      <source>Unconfirmed</source>
      <comment>balance (money)</comment>
      <translation>Unbestätigt</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="613"/>
      <source>Immature</source>
      <comment>balance (money)</comment>
      <translation>Immature</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="662"/>
      <source>1 BCH is: %1</source>
      <translation>1 BCH ist: %1</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="709"/>
      <source>Network status</source>
      <translation>Netzwerkstatus</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="723"/>
      <source>Offline</source>
      <translation>Offline</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="747"/>
      <source>Add Bitcoin Cash wallet</source>
      <translation>Bitcoin Cash-Geldbörse hinzufügen</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/desktop/main.qml" line="778"/>
      <source>Archived wallets [%1]</source>
      <comment>Arg is wallet count</comment>
      <translation>
        <numerusform>Archivierte Geldbörsen [%1]</numerusform>
        <numerusform>Archivierte Geldbörsen [%1]</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="808"/>
      <source>Preparing...</source>
      <translation>Wird vorbereitet...</translation>
    </message>
    <message>
      <location filename="../guis/desktop/main.qml" line="891"/>
      <source>QR-Scan</source>
      <translation>QR-Scan</translation>
    </message>
  </context>
</TS>
