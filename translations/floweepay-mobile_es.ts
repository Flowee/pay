<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES" sourcelanguage="en">
  <context>
    <name>About</name>
    <message>
      <location filename="../guis/mobile/About.qml" line="23"/>
      <source>About</source>
      <translation>Acerca de</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="43"/>
      <source>Help translate this app</source>
      <translation>Ayuda en la traducción de esta app</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="48"/>
      <source>License</source>
      <translation>Licencia</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="53"/>
      <location filename="../guis/mobile/About.qml" line="61"/>
      <source>Credits</source>
      <translation>Créditos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="54"/>
      <source>© 2020-2025 Tom Zander and contributors</source>
      <translation>©️ 2020-2025 Tom Zander y colaboradores</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="99"/>
      <source>Project Home</source>
      <translation>Página del proyecto</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="100"/>
      <source>With git repository and issues tracker</source>
      <translation>Con repositorio git y seguimiento de incidencias</translation>
    </message>
    <message>
      <location filename="../guis/mobile/About.qml" line="105"/>
      <source>Telegram</source>
      <translation>Telegram</translation>
    </message>
  </context>
  <context>
    <name>AccountHistory</name>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="29"/>
      <source>Home</source>
      <translation>Inicio</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="162"/>
      <source>Pay</source>
      <translation>Pagar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountHistory.qml" line="168"/>
      <source>Receive</source>
      <translation>Recibir</translation>
    </message>
  </context>
  <context>
    <name>AccountPageListItem</name>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="41"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="64"/>
      <source>Archived wallets do not check for activities. Balance may be out of date</source>
      <translation>Las carteras archivadas no verifican las actividades. El saldo puede estar desactualizado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="76"/>
      <source>Backup information</source>
      <translation>Respaldar información</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="87"/>
      <source>Backup Details</source>
      <translation>Detalles de la copia de seguridad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="121"/>
      <source>Wallet seed-phrase</source>
      <translation>Frase-semilla del monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="148"/>
      <source>Password</source>
      <translation>Contraseña</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="159"/>
      <source>Seed format</source>
      <translation>Formato de semilla</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="168"/>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="269"/>
      <source>Starting Height</source>
      <comment>height refers to block-height</comment>
      <translation>Altura inicial</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="173"/>
      <source>Derivation Path</source>
      <translation>Ruta de Derivación</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="180"/>
      <source>xpub</source>
      <translation>xpub</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="210"/>
      <source>Please save the seed-phrase on paper, in the right order, with the derivation path. This seed will allow you to recover your wallet in case you lose your mobile.</source>
      <translation>Por favor, guarde la frase semilla en papel, en el orden correcto, con la ruta de derivación. Esta semilla le permitirá recuperar su cartera en caso de que pierda su móvil.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="217"/>
      <source>&lt;b&gt;Important&lt;/b&gt;: Never share your seed-phrase with others!</source>
      <translation>&lt;b&gt;Importante&lt;/b&gt;: ¡Nunca comparta su frase semilla con otros!</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="229"/>
      <source>Wallet keys</source>
      <translation>Llaves del monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="232"/>
      <source>Show Index</source>
      <comment>toggle to show numbers</comment>
      <translation>Mostrar índice</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="248"/>
      <source>Change Addresses</source>
      <translation>Cambiar direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="251"/>
      <source>Switches between addresses others can pay you on, and addresses the wallet uses to send change back to yourself.</source>
      <translation>Intercambia entre direcciones que otros pueden pagarle y direcciones que el monedero usa para enviarte el cambio de vuelta.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="254"/>
      <source>Used Addresses</source>
      <translation>Direcciones usadas</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="257"/>
      <source>Switches between unused and used Bitcoin addresses</source>
      <translation>Alterna entre direcciones no usadas y usadas de Bitcoin</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="309"/>
      <source>Addresses and keys</source>
      <translation>Direcciones y claves</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="317"/>
      <source>Sync Status</source>
      <translation>Estado de la sincronización</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="357"/>
      <source>Hide balance in overviews</source>
      <translation>Ocultar balance en vistas generales</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="364"/>
      <source>Hide in private mode</source>
      <translation>Ocultar en modo privado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Unarchive Wallet</source>
      <translation>Desarchivar monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="380"/>
      <source>Archive Wallet</source>
      <translation>Archivar monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="423"/>
      <source>Really Delete?</source>
      <translation type="unfinished">Really Delete?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="424"/>
      <source>Removing wallet &quot;%1&quot; can not be undone.</source>
      <comment>argument is the wallet name</comment>
      <translation type="unfinished">Removing wallet &quot;%1&quot; can not be undone.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountPageListItem.qml" line="403"/>
      <source>Remove Wallet</source>
      <translation>Eliminar Monedero</translation>
    </message>
  </context>
  <context>
    <name>AccountSelectorPopup</name>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="51"/>
      <source>Your Wallets</source>
      <translation>Tus monederos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="130"/>
      <source>last active</source>
      <translation>Última vez activo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="139"/>
      <source>Needs PIN to open</source>
      <translation>Necesita PIN para abrir</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountSelectorPopup.qml" line="175"/>
      <source>Balance Total</source>
      <translation>Balance total</translation>
    </message>
  </context>
  <context>
    <name>AccountSyncState</name>
    <message>
      <location filename="../guis/mobile/AccountSyncState.qml" line="95"/>
      <source>Status: Offline</source>
      <translation>Estado: Desconectado</translation>
    </message>
  </context>
  <context>
    <name>AccountsList</name>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallet</source>
      <translation>Monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="25"/>
      <source>Wallets</source>
      <translation>Monederos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="28"/>
      <source>Add Wallet</source>
      <translation>Añadir Monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Exit Private Mode</source>
      <translation>Salir del modo privado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="59"/>
      <source>Enter Private Mode</source>
      <translation>Entrar en modo privado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="60"/>
      <source>Reveals wallets marked private</source>
      <translation>Revela monederos marcados como privados</translation>
    </message>
    <message>
      <location filename="../guis/mobile/AccountsList.qml" line="61"/>
      <source>Hides wallets marked private</source>
      <translation>Oculta monederos marcados como privados</translation>
    </message>
  </context>
  <context>
    <name>BackgroundSyncConfig</name>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="24"/>
      <source>Background Synchronization</source>
      <translation type="unfinished">Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="33"/>
      <source>Without background synchronization your wallets will not see new transactions until you open Flowee Pay</source>
      <translation type="unfinished">Without background synchronization your wallets will not see new transactions until you open Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="39"/>
      <source>Allow Background Synchronization</source>
      <translation type="unfinished">Allow Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/BackgroundSyncConfig.qml" line="106"/>
      <source>Every %1 hours</source>
      <translation type="unfinished">Every %1 hours</translation>
    </message>
  </context>
  <context>
    <name>CurrencySelector</name>
    <message>
      <location filename="../guis/mobile/CurrencySelector.qml" line="24"/>
      <source>Select Currency</source>
      <translation>Seleccionar moneda</translation>
    </message>
  </context>
  <context>
    <name>ExploreModules</name>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="24"/>
      <source>Explore</source>
      <translation>Explorar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ExploreModules.qml" line="120"/>
      <source>Open</source>
      <translation>Abrir</translation>
    </message>
  </context>
  <context>
    <name>FilterPopup</name>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="45"/>
      <source>Transactions Filter</source>
      <translation type="unfinished">Transactions Filter</translation>
    </message>
    <message>
      <location filename="../guis/mobile/FilterPopup.qml" line="192"/>
      <source>Only with a comment</source>
      <comment>This is a statement about a transaction</comment>
      <translation type="unfinished">Only with a comment</translation>
    </message>
  </context>
  <context>
    <name>ImportWalletPage</name>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="26"/>
      <source>Import Wallet</source>
      <translation>Importar monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="159"/>
      <source>Select import method</source>
      <translation>Seleccionar método de importación</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="172"/>
      <source>Scan QR</source>
      <translation type="unfinished">Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="212"/>
      <source>OR</source>
      <translation>O</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="224"/>
      <source>Secret as text</source>
      <comment>The seed-phrase or private key</comment>
      <translation>Secreto como texto</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="263"/>
      <source>Unknown word(s) found</source>
      <translation>Palabra(s) desconocidas encontradas</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="274"/>
      <source>Next</source>
      <translation>Siguiente</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="298"/>
      <source>Address to import</source>
      <translation>Dirección a importar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="312"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="446"/>
      <source>New Wallet Name</source>
      <translation>Nuevo nombre de billetera</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="330"/>
      <source>Force Single Address</source>
      <translation>Forzar dirección única</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="331"/>
      <source>When enabled, no extra addresses will be auto-generated in this wallet.
Change will come back to the imported key.</source>
      <translation>Cuando está habilitado, no se generarán automáticamente direcciones adicionales en este monedero.
El cambio volverá a la clave importada.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="336"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="505"/>
      <source>Oldest Transaction</source>
      <translation>Transacción más antigua</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="342"/>
      <source>Check Age</source>
      <comment>online check for wallet age</comment>
      <translation>Comprobar edad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="384"/>
      <source>Nothing found for wallet</source>
      <translation>No se encontró nada para esta cartera</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="390"/>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="549"/>
      <source>Start</source>
      <translation>Comenzar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="464"/>
      <source>Discover Details</source>
      <comment>online check for wallet details</comment>
      <translation>Detalles de Descubre</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="515"/>
      <source>Derivation Path</source>
      <translation>Ruta de Derivación</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="528"/>
      <source>Nothing found for seed. Does it have a password?</source>
      <translation type="unfinished">Nothing found for seed. Does it have a password?</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="536"/>
      <source>Password</source>
      <translation>Contraseña</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ImportWalletPage.qml" line="542"/>
      <source>imported wallet password</source>
      <translation>Contraseña del monedero importada</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigButton</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Enable Instant Pay</source>
      <translation>Activar Pago Instantáneo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="24"/>
      <source>Configure Instant Pay</source>
      <translation>Configurar Pago Instantáneo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="54"/>
      <source>Limits for %1</source>
      <comment>argument is a name</comment>
      <translation type="unfinished">Limits for %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigButton.qml" line="55"/>
      <source>Disabled for %1</source>
      <comment>argument is a name</comment>
      <translation type="unfinished">Disabled for %1</translation>
    </message>
  </context>
  <context>
    <name>InstaPayConfigPage</name>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="23"/>
      <source>Instant Pay</source>
      <translation>Pago instantáneo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="29"/>
      <source>Scanning QR code with Instant Pay enabled will make the transfer go out without confirmation. As long as it does not exceed the set limit.</source>
      <translation>Escanear el código QR con el Pago Instantáneo habilitado hará que la transferencia salga sin confirmación. Siempre y cuando no exceda el límite establecido.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="54"/>
      <source>Protected wallets can not be used for InstaPay because they require a PIN before usage</source>
      <translation>Los monederos protegidos no se pueden utilizar para InstaPagos porque requieren un PIN antes de usarse</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="65"/>
      <source>Enable Instant Pay for this wallet</source>
      <translation>Habilitar Pago Instantáneo para este monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/InstaPayConfigPage.qml" line="72"/>
      <source>Maximum Amount</source>
      <translation>Monto máximo</translation>
    </message>
  </context>
  <context>
    <name>LockApplication</name>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="25"/>
      <source>Security</source>
      <translation>Seguridad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="35"/>
      <source>PIN on startup</source>
      <translation>PIN al iniciar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter current PIN</source>
      <translation>Introduzca el PIN actual</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="45"/>
      <source>Enter new PIN</source>
      <translation>Introduce un nuevo PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="80"/>
      <source>Repeat PIN</source>
      <translation>Repetir PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Remove PIN</source>
      <translation>Eliminar PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="117"/>
      <source>Set PIN</source>
      <translation>Establecer PIN</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="124"/>
      <source>PIN has been removed</source>
      <translation>El PIN ha sido eliminado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="127"/>
      <source>PIN has been set</source>
      <translation>El PIN ha sido establecido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/LockApplication.qml" line="196"/>
      <source>Ok</source>
      <translation>Aceptar</translation>
    </message>
  </context>
  <context>
    <name>MainView</name>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="43"/>
      <source>Explore</source>
      <translation>Explorar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/MainView.qml" line="65"/>
      <source>Find More</source>
      <translation type="unfinished">Find More</translation>
    </message>
  </context>
  <context>
    <name>MainViewBase</name>
    <message>
      <location filename="../guis/mobile/MainViewBase.qml" line="74"/>
      <source>No Internet Available</source>
      <translation type="unfinished">No Internet Available</translation>
    </message>
  </context>
  <context>
    <name>MenuOverlay</name>
    <message>
      <location filename="../guis/mobile/MenuOverlay.qml" line="318"/>
      <source>Add Wallet</source>
      <translation>Añadir Monedero</translation>
    </message>
  </context>
  <context>
    <name>NewAccount</name>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="26"/>
      <source>New Bitcoin Cash Wallet</source>
      <translation>Nuevo monedero Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="39"/>
      <source>New HD Wallet</source>
      <translation>Nuevo monedero-HD</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="45"/>
      <source>Seed-phrase based</source>
      <comment>Context: wallet type</comment>
      <translation>Basado en frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="46"/>
      <source>Easy to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Fácil de respaldar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="47"/>
      <source>Most compatible</source>
      <comment>The most compatible wallet type</comment>
      <translation>El más compatible</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="52"/>
      <source>Import Existing Wallet</source>
      <translation>Importar un monedero existente</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="58"/>
      <source>Imports seed-phrase</source>
      <translation>Importa la frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="59"/>
      <source>Imports private key</source>
      <translation>Importa la clave privada</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="64"/>
      <source>New Basic Wallet</source>
      <translation>Nuevo monedero básico</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="70"/>
      <source>Private keys based</source>
      <comment>Property of a wallet</comment>
      <translation>Basado en Claves privadas</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="71"/>
      <source>Difficult to backup</source>
      <comment>Context: wallet type</comment>
      <translation>Difícil de respaldar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="72"/>
      <source>Great for brief usage</source>
      <comment>Context: wallet type</comment>
      <translation>Ideal para un uso breve</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="87"/>
      <source>New Wallet</source>
      <translation>Nuevo Monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="95"/>
      <source>This creates a new empty wallet with simple multi-address capability</source>
      <translation>Esto crea un nuevo monedero vacío con capacidad de multi-dirección simple</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="101"/>
      <location filename="../guis/mobile/NewAccount.qml" line="155"/>
      <source>Name</source>
      <translation>Nombre</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="111"/>
      <source>Force Single Address</source>
      <translation>Forzar dirección única</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="112"/>
      <source>When enabled, this wallet will be limited to one address.
This ensures only one private key will need to be backed up</source>
      <translation>Cuando está habilitado, este monedero se limitará a una dirección.
Esto asegura que solo una clave privada tendrá que ser respaldada</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="120"/>
      <location filename="../guis/mobile/NewAccount.qml" line="180"/>
      <source>Create</source>
      <translation>Crear</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="143"/>
      <source>New HD-Wallet</source>
      <translation>Nuevo monedero-HD</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="151"/>
      <source>This creates a new wallet that can be backed up with a seed-phrase</source>
      <translation>Esto crea un nuevo monedero que puede ser respaldado con una frase semilla</translation>
    </message>
    <message>
      <location filename="../guis/mobile/NewAccount.qml" line="163"/>
      <source>Derivation</source>
      <translation>Derivación</translation>
    </message>
  </context>
  <context>
    <name>PayWithQR</name>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="26"/>
      <source>Approve Payment</source>
      <translation>Aprobar pago</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="31"/>
      <source>Send All</source>
      <comment>all money in wallet</comment>
      <translation>Enviar Todo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="41"/>
      <source>Show Address</source>
      <comment>to show a bitcoincash address</comment>
      <translation>Mostrar dirección</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="44"/>
      <source>Edit Amount</source>
      <comment>Edit amount of money to send</comment>
      <translation>Editar cantidad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="98"/>
      <source>Invalid QR code</source>
      <translation>Código QR inválido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="112"/>
      <source>I don&apos;t understand the scanned code. I&apos;m sorry, I can&apos;t start a payment.</source>
      <translation>No entiendo el código escaneado. Lo siento, no puedo iniciar un pago.</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="116"/>
      <source>details</source>
      <translation>detalles</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="127"/>
      <source>Scanned text: &lt;pre&gt;%1&lt;/pre&gt;</source>
      <translation>Texto escaneado: &lt;pre&gt;%1&lt;/pre&gt;</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="176"/>
      <source>Payment description</source>
      <translation>Descripción del pago</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="215"/>
      <source>Destination Address</source>
      <translation>Dirección de destino</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PayWithQR.qml" line="409"/>
      <source>Unlock Wallet</source>
      <translation>Desbloquear Monedero</translation>
    </message>
  </context>
  <context>
    <name>PriceDetails</name>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="46"/>
      <source>1 BCH is: %1</source>
      <comment>Price of a whole bitcoin cash</comment>
      <translation>1 BCH es: %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="94"/>
      <source>7d</source>
      <comment>7 days</comment>
      <translation>7d</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="96"/>
      <source>1m</source>
      <comment>1 month</comment>
      <translation>1m</translation>
    </message>
    <message>
      <location filename="../guis/mobile/PriceDetails.qml" line="98"/>
      <source>3m</source>
      <comment>3 months</comment>
      <translation>3m</translation>
    </message>
  </context>
  <context>
    <name>PriceInputWidget</name>
    <message>
      <location filename="../guis/mobile/PriceInputWidget.qml" line="200"/>
      <source>All Currencies</source>
      <translation>Todas las divisas</translation>
    </message>
  </context>
  <context>
    <name>ReceiveTab</name>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="27"/>
      <source>Receive</source>
      <translation>Recibir</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="96"/>
      <source>Share this QR to receive</source>
      <translation>Comparte este QR para recibir</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="117"/>
      <source>Encrypted Wallet</source>
      <translation>Monedero encriptado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="119"/>
      <source>Import Running...</source>
      <translation>Ejecutando Importación...</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="252"/>
      <source>Description</source>
      <translation>Descripción</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="167"/>
      <source>Address</source>
      <comment>Bitcoin Cash address</comment>
      <translation>Dirección</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="177"/>
      <source>Clear</source>
      <translation>Borrar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="318"/>
      <location filename="../guis/mobile/ReceiveTab.qml" line="333"/>
      <source>Payment Seen</source>
      <translation>Pago Enviado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="329"/>
      <source>High risk transaction</source>
      <translation>Transacción de alto riesgo</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="331"/>
      <source>Partially Paid</source>
      <translation>Parcialmente pagado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="335"/>
      <source>Payment Accepted</source>
      <translation>Pago Aceptado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="337"/>
      <source>Payment Settled</source>
      <translation>Pago Enviado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/ReceiveTab.qml" line="367"/>
      <source>Continue</source>
      <translation>Continuar</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultAccountPage</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="22"/>
      <source>Select Wallet</source>
      <translation>Seleccione el monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="30"/>
      <source>Pick which wallet will be selected on starting Flowee Pay</source>
      <translation>Seleccione que monedero será seleccionado al iniciarse Flowee Pay</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="69"/>
      <source>No InstaPay limit set</source>
      <translation>No hay límite para InstaPay establecido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="70"/>
      <source>InstaPay till %1</source>
      <translation>InstaPay hasta %1</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultAccountPage.qml" line="72"/>
      <source>InstaPay is turned off</source>
      <translation>InstaPay está desactivado</translation>
    </message>
  </context>
  <context>
    <name>SelectDefaultConfigButton</name>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="22"/>
      <source>Default Wallet</source>
      <translation>Monedero predeterminado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SelectDefaultConfigButton.qml" line="32"/>
      <source>%1 is used on startup</source>
      <translation>%1 se utiliza al iniciar</translation>
    </message>
  </context>
  <context>
    <name>SendTransactionsTab</name>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="28"/>
      <source>Send</source>
      <translation>Enviar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="43"/>
      <source>Start Payment</source>
      <translation>Iniciar pago</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="46"/>
      <source>Scan QR</source>
      <translation type="unfinished">Scan QR</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="52"/>
      <source>Paste</source>
      <translation>Pegar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/SendTransactionsTab.qml" line="71"/>
      <source>Options</source>
      <translation type="unfinished">Options</translation>
    </message>
  </context>
  <context>
    <name>Settings</name>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="24"/>
      <source>Display Settings</source>
      <translation>Mostrar ajustes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="39"/>
      <source>Font sizing</source>
      <translation>Tamaño de fuente</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="75"/>
      <source>Main View</source>
      <translation>Vista principal</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="80"/>
      <source>Show Bitcoin Cash value</source>
      <translation>Mostrar valor de Bitcoin Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="88"/>
      <source>Background Synchronization</source>
      <translation type="unfinished">Background Synchronization</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="89"/>
      <source>Keep your wallets synchronized by enabling this</source>
      <translation type="unfinished">Keep your wallets synchronized by enabling this</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="96"/>
      <source>Unit</source>
      <translation>Unidad</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="156"/>
      <source>Change Currency</source>
      <translation type="unfinished">Change Currency</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="163"/>
      <source>Dark Theme</source>
      <translation>Tema Oscuro</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="165"/>
      <source>Follow System</source>
      <translation type="unfinished">Follow System</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="171"/>
      <source>Dark</source>
      <translation type="unfinished">Dark</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="180"/>
      <source>Light</source>
      <translation type="unfinished">Light</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="191"/>
      <source>Notifications</source>
      <translation type="unfinished">Notifications</translation>
    </message>
    <message>
      <location filename="../guis/mobile/Settings.qml" line="195"/>
      <source>On new block found</source>
      <translation type="unfinished">On new block found</translation>
    </message>
  </context>
  <context>
    <name>SlideToApprove</name>
    <message>
      <location filename="../guis/mobile/SlideToApprove.qml" line="31"/>
      <source>SLIDE TO SEND</source>
      <translation>DESLIZAR PARA ENVIAR</translation>
    </message>
  </context>
  <context>
    <name>StartupScreen</name>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="86"/>
      <source>Continue</source>
      <translation>Continuar</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="76"/>
      <source>Moving the world towards a Bitcoin&#xa0;Cash economy</source>
      <translation>Moviendo el mundo hacia una economía de Bitcoin&#xa0;Cash</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="151"/>
      <source>Getting Started</source>
      <translation type="unfinished">Getting Started</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="157"/>
      <source>All Videos</source>
      <translation type="unfinished">All Videos</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="171"/>
      <source>Claim a Cash Stamp</source>
      <translation>Reclamar un Cash Stamp</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="188"/>
      <location filename="../guis/mobile/StartupScreen.qml" line="221"/>
      <source>OR</source>
      <translation>O</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="200"/>
      <source>Scan to send to your wallet</source>
      <translation type="unfinished">Scan to send to your wallet</translation>
    </message>
    <message>
      <location filename="../guis/mobile/StartupScreen.qml" line="235"/>
      <source>Add a different wallet</source>
      <translation>Añadir un monedero diferente</translation>
    </message>
  </context>
  <context>
    <name>TextButton</name>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Enabled</source>
      <translation type="unfinished">Enabled</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TextButton.qml" line="79"/>
      <source>Disabled</source>
      <translation type="unfinished">Disabled</translation>
    </message>
  </context>
  <context>
    <name>TransactionDetails</name>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="29"/>
      <source>Transaction Details</source>
      <translation>Detalles de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="32"/>
      <source>Open in Explorer</source>
      <translation>Abrir en el explorador</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="46"/>
      <source>Transaction Hash</source>
      <translation>Hash de transacción</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="93"/>
      <source>First Seen</source>
      <translation>Visto por primera vez</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="104"/>
      <source>Rejected</source>
      <translation>Rechazada</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="106"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="107"/>
      <source>Mined at</source>
      <translation>Minado en</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionDetails.qml" line="121"/>
      <source>%1 blocks ago</source>
      <translation>
        <numerusform>Hace %1 bloque</numerusform>
        <numerusform>Hace %1 bloques</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="128"/>
      <source>Transaction comment</source>
      <translation>Comentario de la transacción</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="150"/>
      <source>Size: %1 bytes</source>
      <translation>Tamaño: %1 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="153"/>
      <source>Coinbase</source>
      <translation>Coinbase</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="161"/>
      <source>Fees paid</source>
      <translation>Comisiones pagadas</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="164"/>
      <source>%1 Satoshi / 1000 bytes</source>
      <translation>%1 Satoshi / 1000 bytes</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="177"/>
      <source>Fused from my addresses</source>
      <translation>Fusionado desde mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="179"/>
      <source>Sent from my addresses</source>
      <translation>Enviado desde mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="181"/>
      <source>Sent from addresses</source>
      <translation>Enviado desde direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="230"/>
      <source>Fused into my addresses</source>
      <translation>Fusionado en mis direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="232"/>
      <source>Received at addresses</source>
      <translation>Recibido en direcciones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionDetails.qml" line="233"/>
      <source>Received at my addresses</source>
      <translation>Recibido en mis direcciones</translation>
    </message>
  </context>
  <context>
    <name>TransactionInfoSmall</name>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="40"/>
      <source>Transaction is rejected</source>
      <translation>Transacción rechazada</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="42"/>
      <source>Processing</source>
      <translation>Procesando</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="62"/>
      <source>Mined</source>
      <translation>Minado</translation>
    </message>
    <message numerus="yes">
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="75"/>
      <source>%1 blocks ago</source>
      <comment>Confirmations</comment>
      <translation>
        <numerusform>Hace %1 bloque</numerusform>
        <numerusform>Hace %1 bloques</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="85"/>
      <source>Waiting for block</source>
      <translation type="unfinished">Waiting for block</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="95"/>
      <source>Miner Reward</source>
      <translation>Recompensa del minero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="97"/>
      <source>Fees</source>
      <translation>Comisiones</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="103"/>
      <source>Received</source>
      <translation>Recibido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="99"/>
      <source>Payment to self</source>
      <translation>Auto pago</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="104"/>
      <source>Sent</source>
      <translation>Enviado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="135"/>
      <source>Holds a token</source>
      <translation>Contiene un token</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="141"/>
      <source>Sent to</source>
      <translation>Enviado a</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionInfoSmall.qml" line="165"/>
      <source>Transaction Details</source>
      <translation>Detalles de la transacción</translation>
    </message>
  </context>
  <context>
    <name>TransactionListItem</name>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="91"/>
      <source>Miner Reward</source>
      <translation>Recompensa del minero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="93"/>
      <source>Fused</source>
      <translation>Fusionado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="95"/>
      <source>Received</source>
      <translation>Recibido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="97"/>
      <source>Moved</source>
      <translation>Movido</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="99"/>
      <source>Sent</source>
      <translation>Enviado</translation>
    </message>
    <message>
      <location filename="../guis/mobile/TransactionListItem.qml" line="149"/>
      <source>Rejected</source>
      <translation>Rechazado</translation>
    </message>
  </context>
  <context>
    <name>UnlockApplication</name>
    <message>
      <location filename="../guis/mobile/UnlockApplication.qml" line="84"/>
      <source>Quick Receive</source>
      <translation type="unfinished">Quick Receive</translation>
    </message>
  </context>
  <context>
    <name>UnlockWidget</name>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="172"/>
      <source>Enter your wallet passcode</source>
      <translation>Introduzca su código de acceso al monedero</translation>
    </message>
    <message>
      <location filename="../guis/mobile/UnlockWidget.qml" line="308"/>
      <source>Open</source>
      <comment>open wallet with PIN</comment>
      <translation>Abrir</translation>
    </message>
  </context>
</TS>
